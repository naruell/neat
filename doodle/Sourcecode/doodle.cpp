﻿// GAM150
// doodle.cpp
// Team Neat
// Primary : All of the teammates.
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Engine/Engine.h"

#include "Game/Splash.h"
#include "Game/prototype_effectSystem.h"
#include "Game/prototype_carddraw.h"
#include "Game/prototype_battlefield.h"
#include "Game/RosterManager.h"
#include "Game/prototype_MapandRoom.h"
#include "Game/prototype_textBox.h"
#include "Game/alpha_HeroSelect.h"
#include "Game/alpha_Title.h"
#include "Game/alpha_Credit.h"
#include "Game/alpha_CardManaging.h"
#include "Game/final_EventRoom.h"
#include "Game/final_tutorial.h"
#include "Game/Ending.h"

int main(void)
{
	Engine& engine = Engine::Instance();

	Engine::GetLogger().LogDebug("Testing Logging Debug");
	Engine::GetLogger().LogError("Testing Logging Error");
	Engine::GetLogger().LogEvent("Testing Logging Event");
	Engine::GetLogger().LogVerbose("Testing Logging Verbose");
	
	try 
	{
		engine.Init("Not Bomb");
		Splash                 splash;
		Alpha_Title            alpha_title;
		Alpha_Credit           alpha_credit;
		Alpha_HeroSelect       alpha_HeroSelect;
		Rostermanager          rostermanager;
		Prototype_battlefield  prototype_battlefield;
		Prototype_effectSystem prototype_effectSystem;
		Prototype_carddraw     prototype_carddraw;
		Prototype_MapandRoom   prototype_mapandroom;
		Prototype_textBox      prototype_textBox;
		Alpha_CardManaging alpha_CardManaging;
		Final_EventRoom final_EventRoom;
		Final_Tutorial final_Tutorial;
		Ending ending;

		engine.GetGameStateManager().AddGameState(splash);
		engine.GetGameStateManager().AddGameState(alpha_title);
		engine.GetGameStateManager().AddGameState(alpha_credit);
		engine.GetGameStateManager().AddGameState(alpha_HeroSelect);
		engine.GetGameStateManager().AddGameState(rostermanager);
		engine.GetGameStateManager().AddGameState(prototype_battlefield);
		engine.GetGameStateManager().AddGameState(prototype_effectSystem);
		engine.GetGameStateManager().AddGameState(prototype_carddraw);
		engine.GetGameStateManager().AddGameState(prototype_mapandroom);
		engine.GetGameStateManager().AddGameState(prototype_textBox);
		engine.GetGameStateManager().AddGameState(alpha_CardManaging);
		engine.GetGameStateManager().AddGameState(final_EventRoom);
		engine.GetGameStateManager().AddGameState(final_Tutorial);
		engine.GetGameStateManager().AddGameState(ending);
		engine.GetSFML().Load();

		while (engine.HasGameEnded() == false) 
		{
			engine.Update();
			if(Engine::GetInput().IsKeyReleased(Input::KeyboardButton::Escape) == true)
			{
				Engine::GetGameStateManager().Shutdown();
			}
		}
		engine.Shutdown();
		return 0;
	}
	catch (std::exception & e) 
	{
		Engine::GetLogger().LogError(e.what());
		return -1;
	}
}
