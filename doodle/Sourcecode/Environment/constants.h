﻿#pragma once
// GAM150
// constants.h
// Team Neat
// Primary : All of the teammates
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.
/*
 * 에셋/파일 경로 저장 변수는 PATH_FILENAME 으로 네이밍
 */

#include <string>
#include <vector>

const std::string PATH_FONT_COMICSANSMS = "Assets/Font/ComicSansMS.fnt";

const std::string PATH_FONT_CHILLER = "Assets/Font/Chiller.fnt";

const std::string PATH_FONT_AGENCYFB = "Assets/Font/AgencyFB.fnt";

const std::string PATH_FONT_COOPERBLACK = "Assets/Font/CooperBlack.fnt";

const std::string PATH_FONT_BELLMT = "Assets/Font/BellMT.fnt";

const std::vector<std::string> FONT_PATHS = 
{
	PATH_FONT_COMICSANSMS,
	PATH_FONT_CHILLER,
	PATH_FONT_AGENCYFB,
	PATH_FONT_COOPERBLACK,
	PATH_FONT_BELLMT
};

const std::string PATH_CLOCK_IMAGE = "Assets/Clock/GameClock.spt";

const std::string PATH_IMAGE = "Assets/Graphics/System/logo(org).spt";

const std::string PATH_SAVEFILE = "Savedata/savefile.txt";

const std::string PATH_SPLASHLOGO = "Assets/Graphics/System/logo(org).png";

const std::string PATH_GRAVESTONE_IMAGE = "Assets/GraveStone/GraveStone.spt";

// icon(png)

const std::string PATH_ADDAMAGE_IMAGE = "Assets/Characters/CharacterStats/Sword.png";

const std::string PATH_APDAMAGE_IMAGE = "Assets/Characters/CharacterStats/Rod.png";

const std::string PATH_HEAL_IMAGE = "Assets/Characters/CharacterStats/heal.png";

const std::string PATH_HEALTH_IMAGE = "Assets/Characters/CharacterStats/Heart.png";

const std::string PATH_MOVE_IMAGE = "Assets/Characters/CharacterStats/Shoes.png";

const std::string PATH_ADARMOR_IMAGE = "Assets/Characters/CharacterStats/ADarmor.png";

const std::string PATH_APARMOR_IMAGE = "Assets/Characters/CharacterStats/AParmor.png";

// hero
const std::string PATH_WARRIOR_IMAGE = "Assets/Characters/CharacterTypes/WARRIOR.spt";

const std::string PATH_MAGE_IMAGE = "Assets/Characters/CharacterTypes/MAGE.spt";

const std::string PATH_PRIEST_IMAGE = "Assets/Characters/CharacterTypes/PRIEST.spt";

const std::string PATH_HIGHNOON_IMAGE = "Assets/Characters/CharacterTypes/HIGHNOON.spt";

const std::string PATH_SOLAIRE_IMAGE = "Assets/Characters/CharacterTypes/SOLAIRE.spt";

const std::string PATH_VAMPIRE_IMAGE = "Assets/Characters/CharacterTypes/VAMPIRE.spt";

// enemy
const std::string PATH_SLIME_IMAGE = "Assets/Enemies/SLIME.spt";

const std::string PATH_SLIME_HORI_IMAGE = "Assets/Enemies/SLIME_HORI.spt";

const std::string PATH_POISONEDSLIME_IMAGE = "Assets/Enemies/POISONEDSLIME.spt";

const std::string PATH_SLIME2_HORI_IMAGE = "Assets/Enemies/SLIME2_HORI.spt";

const std::string PATH_KWAANG_IMAGE = "Assets/Enemies/KWAANG.spt";

const std::string PATH_RED_KWAANG_IMAGE = "Assets/Enemies/BBAL_KWAANG.spt";

const std::string PATH_PURPLE_KWAANG_IMAGE = "Assets/Enemies/BO_KWAANG.spt";

const std::string PATH_BOMA_IMAGE = "Assets/Enemies/BOMA.spt";

const std::string PATH_BOMBERGORILLA_IMAGE = "Assets/Enemies/BOMBERGORILLA.spt";

const std::string PATH_LICH_IMAGE = "Assets/Enemies/LICH.spt";

//

const std::string PATH_EVENT_BACKGROUND_RAWIMAGE = "Assets/Graphics/Event/eventBackground.png";

const std::string PATH_BATTLEFIELD_BACKGROUND_RAWIMAGE = "Assets/Graphics/Battlefield/background.png";

const std::string PATH_MENU_BACKGROUND_RAWIMAGE = "Assets/Graphics/MainMenu/background.png";

const std::string PATH_HEROSELECTION_BACKGROUND_RAWIMAGE = "Assets/Graphics/HeroSelection/background.png";

const std::string PATH_ROSTER_BACKGROUND_RAWIMAGE = "Assets/Graphics/Roster/background.png";

const std::string PATH_CARDMANAGER_BACKGROUND_RAWIMAGE = "Assets/Graphics/CardManager/background.png";

const std::string PATH_BATTLEFIELD_BLACK_IMAGE = "Assets/Graphics/Battlefield/black.spt";

const std::string PATH_CARDDIVDING_IMAGE = "Assets/CardDesign/ornamentalDivider.spt";

// Credit

const std::string PATH_CREDIT_TXT = "Savedata/credit.txt";

// Music

// Sound


//Map Path
const std::string PATH_FIRST_MAP = "Assets/Room&&Map/FirstMap.neat";
const std::string PATH_FIRST_MAP_BACKGROUND = "Assets/Room&&Map/FirstMapBackground.png";
const std::string PATH_SECOND_MAP = "Assets/Room&&Map/SecondMap.neat";
const std::string PATH_SECOND_MAP_BACKGROUND = "Assets/Room&&Map/SecondMapBackground.png";


// Room Path
const std::string PATH_DEFAULT_ROOM = "Assets/Room&&Map/DefaultRoomICON.png";
const std::string PATH_UNCLEARED_BATTLE_ROOM = "Assets/Room&&Map/BattleRoomICON.png";
const std::string PATH_CLEARED_BATTLE_ROOM = "Assets/Room&&Map/BattleRoomICON_HasCleared.png";
const std::string PATH_UNCLEARED_EVENT_ROOM = "Assets/Room&&Map/EventRoomICON.png";
const std::string PATH_CLEARED_EVENT_ROOM = "Assets/Room&&Map/EventRoomICON_HasCleared.png";


namespace space_roster 
{
	const std::string PATH_MOUSE_NORMAL = "Assets/Mouse/Roaster/mouse_normal.spt";

	const std::string PATH_MOUSE_PRESSING = "Assets/Mouse/Roaster/mouse_pressing.spt";

	const std::string PATH_MOUSE_HOLDING = "Assets/Mouse/Roaster/mouse_holding.spt";
}

namespace space_tutorial
{
	const std::vector<std::string> tutorial_image_path
	{
		"Assets/Tutorial/tutorial_0.png",
		"Assets/Tutorial/tutorial_1.png",
		"Assets/Tutorial/tutorial_2.png",
		"Assets/Tutorial/tutorial_3.png",
		"Assets/Tutorial/tutorial_4.png",
		"Assets/Tutorial/tutorial_5.png",
		"Assets/Tutorial/tutorial_6.png",
		"Assets/Tutorial/tutorial_7.png",
		"Assets/Tutorial/tutorial_8.png",
		"Assets/Tutorial/tutorial_9.png"
	};
	
	const std::string PATH_MOUSE_NEXT = "Assets/Mouse/Tutorial/next.png";
	
	const std::string PATH_MOUSE_PREV = "Assets/Mouse/Tutorial/prev.png";
}

enum class OBJECTSTATE
{
	HOVERED,
	PRESSED,
	RELEASED,
	DEFAULT
};

enum class TimeOfDay
{
	NIGHT,
	DAY,
	NIGHTANDDAY,
	NORMAL
};

enum class CHARACTERTYPE : int
{
	UI = -1,
	NEUTRAL = 0,
	WARRIOR,
	MAGE,
	PRIEST,
	VAMPIRE,
	CRUSADER,
	SOLAIRE,
	HIGHNOON,
	NIGHT_ELF,
	THEIF,
	INFECTOR,
	COUNT
};

enum class ENEMYTYPE
{
	SLIME,
	LICH,
	KWAANG,
	BOMA,
	BOMBERGORILLA,
};

enum class E_BGM
{
	BATTLEBGM,
	NONBATTLEBGM,
};