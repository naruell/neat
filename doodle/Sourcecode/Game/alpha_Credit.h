﻿// GAM150
// alpha_credit.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include <memory>

#include "../Engine/GameState.h"

struct TextBox;

class Alpha_Credit : public GameState
{

public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() override { return "Alpha_Credit"; }

private:
	std::unique_ptr<TextBox> creditBox;
};