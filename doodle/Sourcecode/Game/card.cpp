﻿// GAM150
// card.cpp
// Team Neat
// Primary : Duhwan Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "card.h"

#include <doodle/drawing.hpp>
#include <doodle/environment.hpp>
#include <doodle/input.hpp>

#include "../Engine/Engine.h"
#include "../Engine/helper.h"
#include "GameClock.h"

Card::Card()
	: cardID({ 0, 0 }), original_name("Noname"), original_type(Card_Type::NEUTRAL), original_subtype({ Card_Subtype::MAGICAL }), original_notBomb(TimeOfDay::NORMAL), original_spendingTime(100)
{
	Reset();
}

Card::Card(std::pair<int, int> cardID, std::string name, Card_Type type, std::vector<Card_Subtype> subtype, TimeOfDay notBomb, int spendingTime, std::vector<Card_Effect> effectList)
	: cardID(cardID), original_name(name), original_type(type), original_subtype(subtype), original_notBomb(notBomb), original_spendingTime(spendingTime)
{
	Reset();

	for (Card_Effect cardEffect : effectList)
	{
		targetAndEffectMap.insert(std::make_pair(cardEffect, Battlefield::BattleObject()));
	}

	int repeatAmount = 0;
	std::string temp_explanation = "";
	std::string lastExplanation = "";
	for (std::pair<const Card_Effect, Battlefield::BattleObject>& targetAndEffect : GetTargetAndEffectMap())
	{
		if (lastExplanation == targetAndEffect.first.explanation)
		{
			repeatAmount++;
		}
		else
		{
			if (repeatAmount != 0)
			{
				temp_explanation += "Repeat it " + std::to_string(repeatAmount + 1) + " times.\n";
				repeatAmount = 0;
			}
			temp_explanation += targetAndEffect.first.explanation;
			temp_explanation += "\n";
		}
		lastExplanation = targetAndEffect.first.explanation;
	}
	if (repeatAmount != 0)
	{
		temp_explanation += "Repeat it " + std::to_string(repeatAmount + 1) + "times.";
	}

	TextInfo cardExplanationTextInfo
	{
		geometry.textSize,
		TextAlignX::CENTER,
		TextAlignY::TOP,
		doodle::Color(255, 255),
		doodle::Color(255, 255)
	};
	explanationTextBox = std::make_unique<TextBox>(3, temp_explanation, Vector2D{ geometry.cur_posX, geometry.cur_posY - geometry.cur_height / 4}, Vector2D{geometry.cur_width, geometry.cur_height / 2}, cardExplanationTextInfo, AutoNextLine::BY_WORD);
	
	TextInfo cardNameTextInfo
	{
		geometry.textSize * 0.9,
		TextAlignX::CENTER,
		TextAlignY::CENTER,
		doodle::Color(255, 255),
		doodle::Color(255, 255)
	};
	nameTextBox = std::make_unique<TextBox>(4, current_name, Vector2D{ geometry.cur_posX, geometry.cur_posY + geometry.cur_height * 3 / 8 }, Vector2D{ geometry.cur_width, geometry.cur_height / 4 }, cardNameTextInfo, AutoNextLine::BY_WORD);

	TextInfo cardSpendingTimeTextInfo
	{
		geometry.textSize * 1.5,
		TextAlignX::CENTER,
		TextAlignY::CENTER,
		doodle::Color(255, 255),
		doodle::Color(255, 255)
	};
	spendingTimeBox = std::make_unique<TextBox>(5, std::to_string(original_spendingTime), Vector2D{ geometry.cur_posX, geometry.cur_posY + geometry.textSize }, Vector2D{ geometry.textSize * 1.5, geometry.textSize * 1.5 }, cardSpendingTimeTextInfo, AutoNextLine::BY_WORD);
	
	//effectListOnActivated.emplace_back(new TestEffect(Vector2D{ 0.0, 0.0 })); ////////////////////// 테스트용 이펙트

	dividerSpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardBase_Divider.png");
	nameSpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardBase_Name.png");

	notSpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardBase_Not.png");
	bombSpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardBase_Bomb.png");
	notBombSpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardBase_Normal.png");
	UISpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardBase_UI.png");

	ADiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/Sword.png");
	APiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/Rod.png");
	MoveiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/Shoes.png");
	HealiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/heal.png");
	TimeAddiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/TimeAdd.png");
	TimeFlowiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/TimeFlow.png");
	DrawiconSpt = std::make_unique<doodle::Image>("Assets/CardDesign/CardDraw.png");


	Engine::GetLogger().LogDebug("Card is generated : " + current_name + "\t" + temp_explanation);
}

void Card::Draw()
{
	doodle::push_settings();

#ifdef _DEBUG
	// 판정 Range
	doodle::set_outline_color(doodle::HexColor(0xFFFFFFFF)); 
	doodle::no_fill();
	doodle::draw_rectangle(geometry.posX, geometry.posY, geometry.width, geometry.height);
#endif

	doodle::apply_translate(geometry.cur_posX, geometry.cur_posY);
	doodle::apply_rotate(geometry.cur_angle);
	doodle::apply_translate(-geometry.cur_posX, -geometry.cur_posY);

	doodle::Color outlineColor = doodle::HexColor(0xFFFFFFFF);

	doodle::Color cardOutsideFillColor = GetHeroColor(static_cast<CHARACTERTYPE>(cardID.first));
	doodle::Color cardInsideFillColor = doodle::HexColor(0x00000050);

	doodle::set_rectangle_mode(doodle::RectMode::Center);
	doodle::set_image_mode(doodle::RectMode::Center);

	double alpha = Map(discardAnimationDuration, 0, 1.5, 255.0, 0);
	if (discardAnimationDuration > 1.5) { alpha = 0.0; }

	double scale = geometry.cur_width / (geometry.width * geometry.hoveredTextScale);

	// Card Divider Image
	doodle::push_settings();
	doodle::apply_translate(geometry.cur_posX, geometry.cur_posY);
	doodle::apply_scale(scale);
	doodle::draw_image(*dividerSpt, 0, 0);
	doodle::pop_settings();

	// Card Outside
	doodle::set_fill_color(cardOutsideFillColor);
	doodle::no_outline();
	doodle::draw_rectangle(geometry.cur_posX, geometry.cur_posY, geometry.cur_width, geometry.cur_height);

	// Card Inside
	doodle::set_fill_color(cardInsideFillColor);
	doodle::no_outline();
	doodle::draw_rectangle(geometry.cur_posX, geometry.cur_posY, geometry.cur_width - geometry.outline_width, geometry.cur_height - geometry.outline_width);

	// Name Image
	doodle::push_settings();
	doodle::apply_translate(geometry.cur_posX, geometry.cur_posY);
	doodle::apply_scale(scale);
	doodle::set_tint_color(cardOutsideFillColor);
	doodle::draw_image(*nameSpt, 0, 0);
	doodle::set_tint_color(doodle::HexColor(0x00000034));
	doodle::draw_image(*nameSpt, 0, 0);
	doodle::pop_settings();
	
	// Name TextBox
	/*
	doodle::set_fill_color(cardOutsideFillColor);
	doodle::draw_rectangle(nameTextBox->GetPosition().x, nameTextBox->GetPosition().y, nameTextBox->GetSize().x, nameTextBox->GetSize().y);

	doodle::set_fill_color(doodle::HexColor(0x00000034));
	doodle::draw_rectangle(nameTextBox->GetPosition().x, nameTextBox->GetPosition().y, nameTextBox->GetSize().x, nameTextBox->GetSize().y);
	*/
	// Type Image
	doodle::push_settings();
	doodle::apply_translate(geometry.cur_posX, geometry.cur_posY);
	doodle::apply_scale(scale);
	if(std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::UI) != current_subtype.end())
	{
		doodle::draw_image(*UISpt, 0, 0);
	}
	else
	{
		switch (current_notBomb)
		{
		case TimeOfDay::DAY:
			doodle::draw_image(*notSpt, 0, 0);
			break;

		case TimeOfDay::NIGHT:
			doodle::draw_image(*bombSpt, 0, 0);
			break;

		case TimeOfDay::NORMAL:
			doodle::draw_image(*notBombSpt, 0, 0);
			break;
		}
	}
	doodle::pop_settings();

	// explanationTextBox
	doodle::set_fill_color(cardOutsideFillColor);
	doodle::draw_rectangle(explanationTextBox->GetPosition().x, explanationTextBox->GetPosition().y, explanationTextBox->GetSize().x, explanationTextBox->GetSize().y);

	doodle::set_fill_color(doodle::HexColor(0x00000064));
	doodle::draw_rectangle(explanationTextBox->GetPosition().x, explanationTextBox->GetPosition().y, explanationTextBox->GetSize().x, explanationTextBox->GetSize().y);

	nameTextBox->Draw();
	
	doodle::set_fill_color(cardOutsideFillColor);
	doodle::draw_ellipse(geometry.cur_posX - geometry.cur_width / 2 + geometry.cur_textSize * 1.5 / 4, geometry.cur_posY + geometry.cur_height / 2 - geometry.cur_textSize * 1.5 / 4, geometry.cur_textSize * 1.5, geometry.cur_textSize * 1.5);

	doodle::set_outline_width(geometry.outline_width / 6);
	doodle::set_outline_color(doodle::HexColor(0x00000064));
	doodle::set_fill_color(doodle::HexColor(0x00000034));
	doodle::draw_ellipse(geometry.cur_posX - geometry.cur_width / 2 + geometry.cur_textSize * 1.5 / 4, geometry.cur_posY + geometry.cur_height / 2 - geometry.cur_textSize * 1.5 / 4, geometry.cur_textSize * 1.5, geometry.cur_textSize * 1.5);

	doodle::no_outline();
	explanationTextBox->Draw();
	spendingTimeBox->Draw();
	
	doodle::no_fill();
	doodle::draw_ellipse(geometry.cur_posX, geometry.cur_posY + geometry.cur_textSize, geometry.cur_textSize, geometry.cur_textSize);


	doodle::push_settings();
	doodle::apply_translate(geometry.cur_posX, geometry.cur_posY + geometry.cur_width / 4);
	doodle::apply_scale(scale * 1.5);

	// 아이콘 넣는 코드
	int subtypeAmount = static_cast<int>(current_subtype.size());
	for (int i = 0; i < subtypeAmount; ++i)
	{
		double iconSize = 30;
		double iconPosX = 0;

		switch (subtypeAmount % 2)
		{
		case 0:
			if((i + 1) <= subtypeAmount / 2)
			{
				iconPosX = (subtypeAmount / 2 + 1 - (i + 1)) * iconSize * -1.0 + iconSize / 2.0;
			}
			else
			{
				iconPosX = ((i + 1) - subtypeAmount / 2) * iconSize - iconSize / 2.0;
			}
			break;

		case 1:
			if((i + 1) == subtypeAmount / 2 + 1)
			{
				iconPosX = 0;
			}
			else if ((i + 1) <= subtypeAmount / 2)
			{
				iconPosX =(subtypeAmount / 2 + 1 - (i + 1)) * iconSize * - 1.0;
			}
			else
			{
				iconPosX = ((i + 1) - subtypeAmount / 2 - 1) * iconSize;
			}
			break;
		}
		
		switch (current_subtype[i])
		{
		case Card_Subtype::PHYSICAL:
			doodle::draw_image(*ADiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::MAGICAL:
			doodle::draw_image(*APiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::MOVE:
			doodle::draw_image(*MoveiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::HEAL:
			doodle::draw_image(*HealiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::TIMEFLOW:
			doodle::draw_image(*TimeFlowiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::TIMEADD:
			doodle::draw_image(*TimeAddiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::DRAW:
			doodle::draw_image(*DrawiconSpt, iconPosX, 0);
			break;

		case Card_Subtype::ELSE:
			break;
		}
	}
	doodle::pop_settings();
	
	doodle::pop_settings();
}

void Card::Update()
{
	pCard_state = card_state;
	card_state = Card_State::NORMAL;

	switch (pCard_state)
	{
	case Card_State::NORMAL:

		if (geometry.cur_posX != geometry.posX)
		{
			geometry.cur_posX += static_cast<double>((geometry.posX - geometry.cur_posX) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_posY != geometry.posY)
		{
			geometry.cur_posY += static_cast<double>((geometry.posY - geometry.cur_posY) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_angle != geometry.angle)
		{
			geometry.cur_angle += static_cast<double>((geometry.angle - geometry.cur_angle) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_textSize != geometry.textSize)
		{
			geometry.cur_textSize += static_cast<double>((geometry.textSize - geometry.cur_textSize) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_width != geometry.width)
		{
			geometry.cur_width += static_cast<double>((geometry.width - geometry.cur_width) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_height != geometry.height)
		{
			geometry.cur_height += static_cast<double>((geometry.height - geometry.cur_height) * doodle::DeltaTime) * 10.0;
		}
		break;

	case Card_State::DRAWING:

		drawAnimationDuration += static_cast<double>(doodle::DeltaTime);
		card_state = Card_State::DRAWING;

		if (drawAnimationDuration >= 1.0)
		{
			card_state = Card_State::NORMAL;
			drawAnimationDuration = 0.0;
			return;
		}

		if (geometry.cur_posX != geometry.draw_checkPoint_posX)
		{
			geometry.cur_posX += static_cast<double>((geometry.draw_checkPoint_posX - geometry.cur_posX) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_posY != geometry.draw_checkPoint_posY)
		{
			geometry.cur_posY += static_cast<double>((geometry.draw_checkPoint_posY - geometry.cur_posY) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_angle != geometry.hoveredAngle)
		{
			geometry.cur_angle += static_cast<double>((geometry.hoveredAngle - geometry.cur_angle) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_textSize != geometry.textSize * geometry.hoveredTextScale)
		{
			geometry.cur_textSize += static_cast<double>((geometry.textSize * geometry.hoveredTextScale - geometry.cur_textSize) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_width != geometry.width * geometry.hoveredTextScale)
		{
			geometry.cur_width += static_cast<double>((geometry.width * geometry.hoveredTextScale - geometry.cur_width) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_height != geometry.height * geometry.hoveredTextScale)
		{
			geometry.cur_height += static_cast<double>((geometry.height * geometry.hoveredTextScale - geometry.cur_height) * doodle::DeltaTime) * 20.0;
		}
		break;

	case Card_State::DISCARDING:

		discardAnimationDuration += static_cast<double>(doodle::DeltaTime);
		card_state = Card_State::DISCARDING;

		if (discardAnimationDuration >= 2.0)
		{
			card_state = Card_State::ACTIVATED;
			return;
		}

		if (geometry.cur_posX != geometry.posX)
		{
			geometry.cur_posX += static_cast<double>((geometry.posX - geometry.cur_posX) * doodle::DeltaTime) * 8.0;
		}

		if (geometry.cur_posY != geometry.discard_checkPoint_posY)
		{
			geometry.cur_posY += static_cast<double>((geometry.discard_checkPoint_posY - geometry.cur_posY) * doodle::DeltaTime) * 3.0;
		}

		geometry.cur_angle = geometry.hoveredAngle;
		geometry.cur_textSize = geometry.textSize * geometry.hoveredTextScale;
		geometry.cur_width = geometry.width * geometry.hoveredTextScale;
		geometry.cur_height = geometry.height * geometry.hoveredTextScale;
		break;

	case Card_State::HOVERED:

		if (geometry.cur_posX != geometry.posX + geometry.hoveredXoffset)
		{
			geometry.cur_posX += static_cast<double>((geometry.posX + geometry.hoveredXoffset - geometry.cur_posX) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_posY != geometry.posY + geometry.hoveredYoffset)
		{
			geometry.cur_posY += static_cast<double>((geometry.posY + geometry.hoveredYoffset - geometry.cur_posY) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_angle != geometry.hoveredAngle)
		{
			geometry.cur_angle = geometry.hoveredAngle;
		}

		if (geometry.cur_textSize != geometry.textSize * geometry.hoveredTextScale)
		{
			geometry.cur_textSize += static_cast<double>((geometry.textSize * geometry.hoveredTextScale - geometry.cur_textSize) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_width != geometry.width * geometry.hoveredTextScale)
		{
			geometry.cur_width += static_cast<double>((geometry.width * geometry.hoveredTextScale - geometry.cur_width) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_height != geometry.height * geometry.hoveredTextScale)
		{
			geometry.cur_height += static_cast<double>((geometry.height * geometry.hoveredTextScale - geometry.cur_height) * doodle::DeltaTime) * 20.0;
		}
		break;

	case Card_State::CLICKED:
		card_state = Card_State::CLICKED;

		geometry.cur_angle = geometry.hoveredAngle;
		geometry.cur_textSize = geometry.textSize * geometry.hoveredTextScale;
		geometry.cur_width = geometry.width * geometry.hoveredTextScale;
		geometry.cur_height = geometry.height * geometry.hoveredTextScale;
		geometry.cur_posX += static_cast<double>(doodle::get_mouse_x() - doodle::get_previous_mouse_x());
		geometry.cur_posY += static_cast<double>(doodle::get_mouse_y() - doodle::get_previous_mouse_y());
		break;

	case Card_State::TARGETING:
		card_state = Card_State::TARGETING;

		if (geometry.cur_posX != geometry.targeting_posX)
		{
			geometry.cur_posX += static_cast<double>((geometry.targeting_posX - geometry.cur_posX) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_posY != geometry.targeting_posY)
		{
			geometry.cur_posY += static_cast<double>((geometry.targeting_posY - geometry.cur_posY) * doodle::DeltaTime) * 10.0;
		}

		if (geometry.cur_angle != geometry.hoveredAngle)
		{
			geometry.cur_angle = geometry.hoveredAngle;
		}

		if (geometry.cur_textSize != geometry.textSize * geometry.hoveredTextScale)
		{
			geometry.cur_textSize += static_cast<double>((geometry.textSize * geometry.hoveredTextScale - geometry.cur_textSize) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_width != geometry.width * geometry.hoveredTextScale)
		{
			geometry.cur_width += static_cast<double>((geometry.width * geometry.hoveredTextScale - geometry.cur_width) * doodle::DeltaTime) * 20.0;
		}

		if (geometry.cur_height != geometry.height * geometry.hoveredTextScale)
		{
			geometry.cur_height += static_cast<double>((geometry.height * geometry.hoveredTextScale - geometry.cur_height) * doodle::DeltaTime) * 20.0;
		}
		break;

	case Card_State::ACTIVATING:
		card_state = Card_State::ACTIVATING;

		geometry.cur_angle = geometry.angle;
		geometry.cur_textSize = geometry.textSize;
		geometry.cur_width = geometry.width;
		geometry.cur_height = geometry.height;
		geometry.cur_posX = geometry.disappearPosX;
		geometry.cur_posY = geometry.disappearPosY;

		for (int i = static_cast<int>(effectListOnActivated.size()) - 1; i >= 0; --i)
		{
			if (effectListOnActivated[i]->isEnded())
			{
				effectListOnActivated.erase(effectListOnActivated.begin() + i);
			}
		}

		if (static_cast<int>(effectListOnActivated.size()) == 0)
		{
			card_state = Card_State::ACTIVATED;
		}
		break;
	}

	// 카드 상태와 상관 없는 텍스트박스 사이즈
	if (explanationTextBox->GetPosition().x != geometry.cur_posX ||
		explanationTextBox->GetPosition().y != geometry.cur_posY - geometry.cur_height / 4)
	{
		explanationTextBox->setPosition({ geometry.cur_posX, geometry.cur_posY - geometry.cur_height / 4 });
	}

	if (explanationTextBox->GetSize().x != geometry.cur_width - geometry.outline_width ||
		explanationTextBox->GetSize().y != geometry.cur_height / 2 - geometry.outline_width)
	{
		explanationTextBox->SetSize({ geometry.cur_width - geometry.outline_width, geometry.cur_height / 2 - geometry.outline_width });
	}

	if (explanationTextBox->GetTextInfo().doodleTextSize != geometry.cur_textSize)
	{
		explanationTextBox->SetTextSize(geometry.cur_textSize);
	}

	
	if (nameTextBox->GetPosition().x != geometry.cur_posX ||
		nameTextBox->GetPosition().y != geometry.cur_posY + geometry.cur_height * 3 / 8)
	{
		nameTextBox->setPosition({ geometry.cur_posX, geometry.cur_posY + geometry.cur_height * 3 / 8 });
	}

	if (nameTextBox->GetSize().x != geometry.cur_width - geometry.outline_width ||
		nameTextBox->GetSize().y != geometry.cur_height / 4 - geometry.outline_width)
	{
		nameTextBox->SetSize({ geometry.cur_width - geometry.outline_width, geometry.cur_height / 4 - geometry.outline_width });
	}

	if (nameTextBox->GetTextInfo().doodleTextSize != geometry.cur_textSize * 0.9)
	{
		nameTextBox->SetTextSize(geometry.cur_textSize * 0.9);
	}
	
	if (spendingTimeBox->GetPosition().x != geometry.cur_posX - geometry.cur_width / 2 + geometry.cur_textSize * 1.5 / 4 ||
		spendingTimeBox->GetPosition().y != geometry.cur_posY + geometry.cur_height / 2 - geometry.cur_textSize * 1.5 / 4)
	{
		spendingTimeBox->setPosition({ geometry.cur_posX - geometry.cur_width / 2 + geometry.cur_textSize * 1.5 / 4, geometry.cur_posY + geometry.cur_height / 2 - geometry.cur_textSize * 1.5 / 4 });
	}

	if (spendingTimeBox->GetSize().x != geometry.cur_textSize ||
		spendingTimeBox->GetSize().y != geometry.cur_textSize)
	{
		spendingTimeBox->SetSize({ geometry.cur_textSize * 1.5, geometry.cur_textSize * 1.5 });
	}

	if (spendingTimeBox->GetTextInfo().doodleTextSize != geometry.cur_textSize * 1.5)
	{
		spendingTimeBox->SetTextSize(geometry.cur_textSize * 1.5);
	}
	
	geometry.outline_width = geometry.cur_width * 0.08;
}

void Card::OnHovered()
{
	card_state = Card_State::HOVERED;

	if (pCard_state == Card_State::HOVERED && ToBeExecutedOnHovered)
	{
		ToBeExecutedOnHovered();
	}
}

void Card::OnClicked()
{
	card_state = Card_State::CLICKED;

	if (pCard_state == Card_State::CLICKED && ToBeExecutedOnClicked)
	{
		ToBeExecutedOnClicked();
	}
}

void Card::OnUsed()
{
	for (std::pair<Card_Effect, Battlefield::BattleObject> targetAndEffect : targetAndEffectMap)
	{
		//////////////////////// 이전 효과가 끝날 때 까지 기다리는 등의 코드 작성 가능
		if (targetAndEffect.second.GetAllyType() != targetAndEffect.first.targetAllyType)
		{
			Engine::GetLogger().LogError("Error! Current Target Type and Effect's Target Type is different!!");
		}
	}
}

void Card::AllocateOnHovered(std::function<void()> newFunction)
{
	ToBeExecutedOnHovered = newFunction;
}

void Card::AllocateOnClicked(std::function<void()> newFunction)
{
	ToBeExecutedOnClicked = newFunction;
}

void Card::ForceToDiscard()
{
	card_state = Card_State::DISCARDING;

	if (doesUsingTime == true)
	{
		GameClock::ChangeCurrentTime(current_spendingTime);
	}
	geometry.cur_posX = geometry.posX;
	geometry.cur_posY = geometry.posY;
	geometry.discard_checkPoint_posY = geometry.cur_posY + 300;
}

std::multimap<Card_Effect, Battlefield::BattleObject>& Card::GetTargetAndEffectMap()
{
	return targetAndEffectMap;
}

void Card::SetOffset(Vector2D offsetTo)
{
	drawOffset = offsetTo;
}

const Vector2D Card::GetOffset()
{
	return drawOffset;
}

void Card::SetCardCastingHeroIndex(Vector2DInt index)
{
	cardCastingHeroIndex = index;
}

Vector2DInt Card::GetCardCastingHeroIndex() const
{
	return cardCastingHeroIndex;
}

void Card::SetExplanation(Battlefield::BattleObject* casterPtr)
{
	if(casterPtr == nullptr)
	{
		SetDefaultExplanation();
	}
	else
	{
		if (casterPtr->GetCharacterData() != nullptr)
		{
			if (std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::PHYSICAL) != current_subtype.end() || std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::MAGICAL) != current_subtype.end() ||
				std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::HEAL) != current_subtype.end())
			{
				for (auto& targetAndEffect : targetAndEffectMap)
				{
					int targetOffset = 0;
					switch (current_subtype[0])
					{
					case Card_Subtype::PHYSICAL:
						targetOffset = casterPtr->GetCharacterData()->currentOffset.adOffset;
						break;

					case Card_Subtype::MAGICAL:
						targetOffset = casterPtr->GetCharacterData()->currentOffset.apOffset;
						break;

					case Card_Subtype::HEAL:
						targetOffset = casterPtr->GetCharacterData()->currentOffset.healOffset;
						break;
					}

					std::string offsetString = "(";
					if (targetOffset >= 0) { offsetString += "+"; }
					offsetString += std::to_string(targetOffset);
					offsetString += ")";

					std::string cardEffectString = targetAndEffect.first.explanation;
					int cardEffectStringSize = static_cast<int>(cardEffectString.size());
					for (int j = 0; j < cardEffectStringSize; ++j)
					{
						if (cardEffectString[j] == '%' && cardEffectString[j + 1] >= '0' && cardEffectString[j + 1] <= '9')
						{
							int valueIndex = cardEffectString[j + 1] - '0';
							int value = targetAndEffect.first.effectValue[valueIndex];

							std::string frontStr = cardEffectString.substr(0, j);
							std::string backStr = cardEffectString.substr(j + 2);

							std::string valueStr = std::to_string(value) + offsetString;

							cardEffectString = frontStr + valueStr + backStr;
							cardEffectStringSize = static_cast<int>(cardEffectString.size());
						}
					}
					explanationTextBox->SetText(cardEffectString);
				}

				int repeatAmount = 0;
				std::string temp_explanation = "\n";
				std::string lastExplanation = "";
				for (auto& targetAndEffect : targetAndEffectMap)
				{
					if (lastExplanation == targetAndEffect.first.explanation)
					{
						repeatAmount++;
					}
					else
					{
						if (repeatAmount != 0)
						{
							temp_explanation += "Repeat it " + std::to_string(repeatAmount + 1) + " times.\n";
							repeatAmount = 0;
						}
					}
					lastExplanation = targetAndEffect.first.explanation;
				}
				if (repeatAmount != 0)
				{
					temp_explanation += "Repeat it " + std::to_string(repeatAmount + 1) + "times.";
				}

				if (temp_explanation != "\n")
				{
					std::string textBoxString = explanationTextBox->GetText();
					explanationTextBox->SetText(textBoxString + temp_explanation);
				}
			}
		}
		else
		{
			SetDefaultExplanation();
		}
	}
}

void Card::SetDefaultExplanation() const noexcept
{
	if (std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::PHYSICAL) != current_subtype.end() || std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::MAGICAL) != current_subtype.end() ||
		std::find(current_subtype.begin(), current_subtype.end(), Card_Subtype::HEAL) != current_subtype.end())
	{
		for (auto& targetAndEffect : targetAndEffectMap)
		{
			std::string cardEffectString = targetAndEffect.first.explanation;
			int cardEffectStringSize = static_cast<int>(cardEffectString.size());
			for (int j = 0; j < cardEffectStringSize; ++j)
			{
				if (cardEffectString[j] == '%' && cardEffectString[j + 1] >= '0' && cardEffectString[j + 1] <= '9')
				{
					int valueIndex = cardEffectString[j + 1] - '0';
					int value = targetAndEffect.first.effectValue[valueIndex];

					std::string frontStr = cardEffectString.substr(0, j);
					std::string backStr = cardEffectString.substr(j + 2);

					std::string valueStr = std::to_string(value);

					cardEffectString = frontStr + valueStr + backStr;
					cardEffectStringSize = static_cast<int>(cardEffectString.size());
				}
			}
			explanationTextBox->SetText(cardEffectString);
		}
		
		int repeatAmount = 0;
		std::string temp_explanation = "\n";
		std::string lastExplanation = "";
		for (auto& targetAndEffect : targetAndEffectMap)
		{
			if (lastExplanation == targetAndEffect.first.explanation)
			{
				repeatAmount++;
			}
			else
			{
				if (repeatAmount != 0)
				{
					temp_explanation += "Repeat it " + std::to_string(repeatAmount + 1) + " times.\n";
					repeatAmount = 0;
				}
			}
			lastExplanation = targetAndEffect.first.explanation;
		}
		if (repeatAmount != 0)
		{
			temp_explanation += "Repeat it " + std::to_string(repeatAmount + 1) + "times.";
		}

		if(temp_explanation != "\n")
		{
			std::string textBoxString = explanationTextBox->GetText();
			explanationTextBox->SetText(textBoxString + temp_explanation);
		}
	}
}

std::pair<int, int> Card::GetCardID() const noexcept
{
	return cardID;
}

void Card::Reset()
{
	cardLevel = 0;
	current_name = original_name;
	current_type = original_type;
	current_subtype = original_subtype;
	current_notBomb = original_notBomb;
	current_spendingTime = original_spendingTime;
}