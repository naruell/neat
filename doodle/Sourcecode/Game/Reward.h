﻿#pragma once
// GAM150
// reward.h
// Team Neat
// Primary : Sunghwan Cho, Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>

class Card;

class Reward {
public:
	static constexpr double magnification = 2.0;
	static constexpr int rewardOptionsCount = 4; // 몇 장 첨에 나오는지
	
	Reward() = default;
	~Reward() = default;
	void Load(int rewardTimes);
	void Update();
	void Draw();
	void Unload();

	bool IsRewardEnd();
private:

	std::vector<std::pair<int, int>> cardID;
	std::vector<Card*> cardList;
	int rewardPage; // 지금 페이지, 로드 때 0됨
	int rewardLastPage; // Load 파라미터 숫자랑 같아짐.
	bool isRewardEnd;
};

extern Reward reward;