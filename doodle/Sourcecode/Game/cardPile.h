﻿#pragma once
// GAM150
// cardPile.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>
#include <doodle/image.hpp>

#include "../Engine/helper.h"
#include "../Engine/Geometry/Vector2D.h"

class CardPile
{
public:
	CardPile(Vector2D position);

	void Draw(double radian);

	void Shuffle();
	void Sort();

	std::vector<std::pair<int, int>> Copy() const noexcept;
	void Clear();
	void Push_back(std::pair<int, int> id);
	void Push_back(std::vector<std::pair<int, int>> idList);

	void Log();

	std::pair<int, int> MoveCard(int index = 0);

	int GetSize();
	std::pair<int, int> GetCardID(int index = 0);

	static inline const std::vector<std::pair<int, int>> init_deck =
	{
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),0}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),0},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),1}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),1},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),2}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),2},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),3}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),3},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),4}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),5},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),6}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),6},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),7}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),8},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),8}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),9},
		std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),10}, std::pair<int, int>{ static_cast<int>(CHARACTERTYPE::NEUTRAL),10}
	};

protected:
	std::vector<std::pair<int, int>> cardList;

	Vector2D position;

	std::unique_ptr<doodle::Image>coverImage; // 뒷면 이미지
};

extern CardPile deck;
extern CardPile graveyard;