﻿#pragma once
// GAM150
// GameClock.h
// Team Neat
// Primary : Junhyuk Cha
// Secondary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/Sprite.h"
#include "../Environment\constants.h"

class GameClock
{
public:
	static void Load();
	static void Unload();
	static void CalculateNextLimitedTime();
	static void Update();
	static void Draw();
	static void DrawLimitedTime();
	static void ChangeCurrentTime(int time);
	static void ChangeLimitedTime(int time);
	static int GetCurrentTime();
	static int GetLimitedTime();
	static void SetCurrentTime(int time);
	static void SetLimitedTime(int time);
	static TimeOfDay GetcurrentTimeOfDay();
	static int GetRemainingTime();

private:
	inline static std::unique_ptr<Sprite> sprite = nullptr;
	inline static doodle::Image* curTimeNeedle = nullptr;
	inline static doodle::Image* remainingTimeNeedle = nullptr;
	inline static int currentTime = 0;
	inline static const int givenTime = 5;
	inline static int limitedTime = 0;
	inline static TimeOfDay currentTimeOfDay = TimeOfDay::NIGHTANDDAY;
	inline static double middleArrowLength = 92;
	inline static double shortArrowLength = 10;
	inline static double currentMiddleArrowDegree = 0;
	inline static double limitedMiddleArrowDegree = 0;
	inline static double currentMiddleArrowDegreeTo = 0;

	inline static double previousMiddleArrowDegree = 0;
	inline static const int clockVibrationAmount = 3;
	inline static int remainingClockVibration = 0;
	inline static double animationTimer = 0;
	inline static double animationTimerTo = 0;
};
