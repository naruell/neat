﻿// GAM150
// Vampire.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Vampire.h"
#include "../../Environment/constants.h"

void Vampire::Load()
{
	sprite.Load(PATH_VAMPIRE_IMAGE);
}
