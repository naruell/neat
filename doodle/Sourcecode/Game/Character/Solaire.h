﻿#pragma once
// GAM150
// Solaire.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

class Solaire : public Character
{
public:
	using Character::Character;

	~Solaire() {};
	void Load() override;
private:

};