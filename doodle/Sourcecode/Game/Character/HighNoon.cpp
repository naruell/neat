﻿// GAM150
// HighNoon.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "HighNoon.h"
#include "../../Environment/constants.h"

void HighNoon::Load()
{
	sprite.Load(PATH_HIGHNOON_IMAGE);
}