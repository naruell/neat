﻿#pragma once
// GAM150
// Mage.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

class Mage : public Character
{
public:
	using Character::Character;

	~Mage() {};
	void Load() override;
private:

};