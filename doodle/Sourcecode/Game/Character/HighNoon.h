﻿#pragma once
// GAM150
// HighNoon.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

class HighNoon : public Character
{
public:
	using Character::Character;

	~HighNoon() {};
	void Load() override;
private:

};