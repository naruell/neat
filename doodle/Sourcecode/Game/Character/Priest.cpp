﻿// GAM150
// Priest.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Priest.h"
#include "../../Environment/constants.h"

void Priest::Load()
{
	sprite.Load(PATH_PRIEST_IMAGE);
}