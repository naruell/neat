﻿#pragma once
// GAM150
// EventRoom.h
// Team Neat
// Primary : Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>
#include <memory>

#include "../Engine/text.h"

// to make this readable, this enum will not use upper letters
enum class EVENTROOMTYPE
{
	None = -1,
	HeroOffset, // decrease & increase hero's offset								// 영웅 오프셋 증, 감
	CardDeleting, // card managing : you can delete any card you want				// 덱에서 카드 제거
	CardFinding, // get a class card												// 공짜 카드 발견 <- 이거 중립카드임.
	HealHerosWithPenalty, // heal all heros but max HP will be decrease				// 모두의 최대 체력 -1 하고 풀피
	IncreaseOneHerosMaxHP, // add 2 to max HP to one hero							// 한 캐릭터 최대 체력 +2
	OffsetIncreaseWithPenalty, // increase offset with 3 damage						// 피해를 3 받고 오프셋 업하는거
	AddRandomClassCards, // get 3 random class cards per every party memebers		// 현재 있는 파티원의 랜덤카드 하나 덱에 넣기
	Boss_StatIncrease, // offset + 2 for every heros								// 모든 영웅들에게 선택한 오프셋 +2
	COUNT
};

class Character;

class Event
{
public:
	Event() = default;

	void Load(int eventType);
	void Update();
	void Draw();
	void Unload();

	void UpdateText();

	void setIsUpdated(bool update);

	int GetRoomType();
	void SetRoomType(int value);

	int GetEventPhase();
	void SetEventPhase(int value);

	bool GetIsEventEnd();
	void SetIsEventEnd(bool EndOrNot);

	Character* GetTargetCharacter();
	void SetTargetCharacter(Character* targetChar);

	int* GetTargetOffset();
	void SetTargetOffset(int& targetOff);

	std::vector<int> GetCharacterTypesVector();
	
private:
	EVENTROOMTYPE eventRoomType = EVENTROOMTYPE::None;
	bool isEventEnd;
	int eventPhase; // 페이즈따라서 뭐 뜰지 정함
	std::vector<int> characterTypes{};
	std::unique_ptr<TextBox> eventText;

	// std::unique_ptr<Character> targetCharacter; // 개구리가 원본을 로우포인터로 만들엇서~~~~ 가 아니라 원래 로우 포인터여야 하는 건가..?
	Character* targetCharacter; // 오프셋 변경 이벤트면, 변경할 대상 저장할 곳
	int* targetOffset; // 오프셋 변경 이벤트면, 변경할 대상의 스탯

	bool isUpdateEnd = false;
};

extern Event event;