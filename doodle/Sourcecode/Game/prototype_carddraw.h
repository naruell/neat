﻿#pragma once
// GAM150
// prototype_carddraw.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/Input.h"
#include "../Engine/helper.h"

class Prototype_carddraw : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() { return "Prototype_carddraw"; }

private:
	Input::InputKey removeCardKey = Input::KeyboardButton::Backspace;
	Input::InputKey clearHandKey = Input::KeyboardButton::Escape;

	Input::InputKey nextLevelKey = Input::KeyboardButton::Enter;
};