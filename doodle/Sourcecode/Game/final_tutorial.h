﻿#pragma once
// GAM150
// final_tutorial.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <memory>
#include <doodle/image.hpp>

#include "../Engine/GameState.h"

class Final_Tutorial : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() override { return "Final_Tutorial"; }
private:
	
	int index = 0;
	std::vector<std::unique_ptr<doodle::Image>> tutorial_images;

	std::unique_ptr<doodle::Image> prev_mouse_image = nullptr;
	std::unique_ptr<doodle::Image> next_mouse_image = nullptr;
};