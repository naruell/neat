﻿// GAM150
// prototype_effectSystem.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "prototype_effectSystem.h"
#include "../Engine\Engine.h"
#include "../Environment\constants.h"
#include "../Engine/Particle/effectInterface.h"

void Prototype_effectSystem::Load()
{

}

void Prototype_effectSystem::Update(double)
{
	if (removeEffectKey.IsKeyPressed() == true)
	{
		if (Engine::GetEffectSystem().effectList.size() != 0)
		{
			Engine::GetEffectSystem().effectList[Engine::GetEffectSystem().effectList.size() - 1]->setEndCondition(true);
		}
	}
	else if (testEffectGenerateKey.IsKeyPressed() == true)
	{
		Engine::GetEffectSystem().GenerateTestEffect({ 0, 0 });
	}
	else if (image1EffectGenerateKey.IsKeyPressed() == true)
	{
		Engine::GetEffectSystem().GenerateImage1Effect({ -512, 123 }, PATH_SPLASHLOGO, 2);
	}

	else if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
	{
		Engine::GetEffectSystem().GenerateClick1Effect();
	}
	else if (nextLevelKey.IsKeyReleased() == true)
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_TEXTBOX);
	}
}

void Prototype_effectSystem::Draw()
{
	Engine::GetWindow().Clear({ 0, 0, 0, 255 });
	Engine::GetEffectSystem().Run();
	infoInterface.Run();
}

void Prototype_effectSystem::Unload()
{

}
