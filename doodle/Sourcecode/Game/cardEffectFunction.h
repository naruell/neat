﻿#pragma once
// GAM150
// cardEffectFunction.h
// Team Neat
// Primary : Duhwan Kim
// Secondary : Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "card.h"
#include "hand.h"
#include "GameClock.h"
#include "battlefield.h"
#include "alpha_HeroSelect.h"
#include "CardManager.h"
#include "cardPile.h"
#include "EventRoom.h"
#include "HeroSelect.h"

inline void exampleCardEffectFunction_AD(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 4;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect exampleCardEffect_AD{ "Deals %0 AD damage", Card_Target::ENEMY, BattleAllyType::ENEMY, exampleCardEffectFunction_AD, {4} };

inline void exampleCardEffectFunction_AP(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 4;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.apOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtApDamage(damageAmount, offset);
}
inline Card_Effect exampleCardEffect_AP{ "Deals %0 AP damage", Card_Target::ENEMY, BattleAllyType::ENEMY, exampleCardEffectFunction_AP, {4} };

inline void exampleCardEffectFunction_Heal(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int healAmount = 3;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.healOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetHealed(healAmount, offset);
}
inline Card_Effect exampleCardEffect_Heal{ "Gains %0 life", Card_Target::CHARACTER, BattleAllyType::CHARACTER, exampleCardEffectFunction_Heal, {3} };

inline void exampleCardEffectFunction_LifeDrain(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();

	int drainAmount = 2;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	
	int previousTargetHP = battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData()->current_HP;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(drainAmount, offset);
	int currentTargetHP = battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData()->current_HP;
	int stealAmount = previousTargetHP - currentTargetHP;

	battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetHealed(stealAmount, 0);
}
inline Card_Effect exampleCardEffect_LifeDrain{ "Deals %0 AD damage.\nCaster gains life that much.", Card_Target::ENEMY, BattleAllyType::ENEMY, exampleCardEffectFunction_LifeDrain, {2} };



////**** Warrior ****////
inline void Warrior_CardEffectFunction_ShieldSlam(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 4;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset * 2;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect Warrior_CardEffect_ShieldSlam{ "Deal %0 AD damage to target enemy.\n This spell gets double bonus from AD Offset", Card_Target::ENEMY, BattleAllyType::ENEMY, Warrior_CardEffectFunction_ShieldSlam, {4} };

inline void Warrior_CardEffectFunction_Whirlwind(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 2;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect Warrior_CardEffect_WhirlWind{ "Deal %0 AD damage to ALL enemies.", Card_Target::ALL_ENEMY, BattleAllyType::ENEMY, Warrior_CardEffectFunction_Whirlwind, {2} };

inline void Warrior_CardEffectFunction_Execute(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	int damageAmount = 6;
	
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
	if (battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData() == nullptr ||
		battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData()->current_HP <= 0)
	{
		player_hand.Draw_from_deck(1);
	}
}
inline Card_Effect Warrior_CardEffect_Execute{ "Deal %0 AD damage to target enemy.\nIf it dead, draw a card", Card_Target::ENEMY, BattleAllyType::ENEMY, Warrior_CardEffectFunction_Execute, {6} };



////**** Mage ****////
inline void Mage_CardEffectFunction_Blink(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();

	battlefield.SwapObject(cardCastingHeroIndex, targetIndex);
}
inline Card_Effect Mage_CardEffect_Blink{ "Teleport to the target empty battlefield.", Card_Target::EMPTY, BattleAllyType::EMPTY, Mage_CardEffectFunction_Blink, {}, { {0, 1}, {0, 2}, {0, -1}, {0, -2}, {-1, 0}, {-1, 1}, {-1, 2}, {-1, -1}, {-1, -2}, {1, 0}, {1, 1}, {1, 2}, {1, -1}, {1, -2}} };

inline void Mage_CardEffectFunction_ArcaneIntellect(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject, const Card_Effect&)
{
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.apOffset;
	
	player_hand.Draw_from_deck(offset + 1);

	int damageAmount = 0;
	battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].DealtApDamage(damageAmount, offset);

	// 행동력 추가
	battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->UpdateActableCount(1);
}
inline Card_Effect Mage_CardEffect_ArcaneIntellect{ "Draw cards as much as caster's AP offset + %0. Caster get %1 AP damage.\nCaster can cast one more spell this turn.", Card_Target::NONE, BattleAllyType::CHARACTER, Mage_CardEffectFunction_ArcaneIntellect, {1, 0} };

inline void Mage_CardEffectFunction_Shock(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject, const Card_Effect&)
{
	Vector2DInt targetIndex = { cardCastingHeroIndex.x + 1, cardCastingHeroIndex.y };

	int damageAmount = 8;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.apOffset;

	if(battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetAllyType() == BattleAllyType::ENEMY &&
	   battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData() != nullptr)
	{
		battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtTrueDamage(damageAmount, offset);
	}
	else if (battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetAllyType() == BattleAllyType::CHARACTER &&
		     battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetCharacterData() != nullptr)
	{
		battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtTrueDamage(damageAmount, offset);
	}
}
inline Card_Effect Mage_CardEffect_Shock{ "Deals %0 AP damage to the anyone in front of the caster.\nIgnore all armors.", Card_Target::NONE, BattleAllyType::ENEMY, Mage_CardEffectFunction_Shock, {8} };



////**** Priest ****////
inline void Priest_CardEffectFunction_HealingHands(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 2;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.healOffset;

	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtTrueDamage(damageAmount, 0);
	battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetHealed(damageAmount, offset);
}
inline Card_Effect Priest_CardEffect_HealingHands{"Deals 2 Normal damage to the anyone.\nIgnore all armors.\nCaster restore %0 Health.", Card_Target::ALL, BattleAllyType::CHARACTER, Priest_CardEffectFunction_HealingHands, {2} };

// Repeats 3 times
inline void Priest_CardEffectFunction_HealingRain(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int healAmount = 1;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.healOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetHealed(healAmount, offset);
}
inline Card_Effect Preist_CardEffect_HealingRain{ "Restore %0 Health to random hero.", Card_Target::RANDOM_CHARACTER, BattleAllyType::CHARACTER, Priest_CardEffectFunction_HealingRain, {2} };

inline void Priest_CardEffectFunction_CircleofHealing(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int healAmount = 1;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.healOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetHealed(healAmount, offset);
}
inline Card_Effect Priest_CardEffect_CircleofHealing{"Restore %0 Health to all heros.", Card_Target::ALL_CHARACTER, BattleAllyType::CHARACTER, Priest_CardEffectFunction_CircleofHealing, {2} };



////**** HighNoon ****////
// Repeats 3 times
inline void HighNoon_CardEffectFunction_IndiscriminateShot(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 2;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect HighNoon_CardEffect_IndiscriminateShot{ "Deal %0 AD damage to random enemy.", Card_Target::RANDOM_ENEMY, BattleAllyType::ENEMY, HighNoon_CardEffectFunction_IndiscriminateShot, {3} };

inline void HighNoon_CardEffectFunction_FastReload(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	for (int i = 0; i < 3; ++i)
	{
		if(deck.GetSize() == 0 && graveyard.GetSize() == 0)
		{
			continue;
		}
		
		player_hand.Draw_from_deck(1);

		auto& card = player_hand.GetCard(player_hand.GetLastCardIndex());
		
		card->pCard_state = Card_State::TARGETING;
		card->card_state = Card_State::TARGETING;
		card->SetCardCastingHeroIndex(player_hand.GetLastCardCasterIndex());
		card->doesUsingTime = false;

		for (auto& targetAndEffect : card->GetTargetAndEffectMap())
		{
			int rowIndex;

			Vector2DInt emptyPos;
			Vector2DInt randomTargetPos;
			bool isMoreTargetAndEffectMapNeeded;
			int columnFieldSize = static_cast<int>(battlefield.battleFieldVector[0].size());
			switch (targetAndEffect.first.targetType)
			{
			case Card_Target::NONE:
				break;

			case Card_Target::EMPTY:
				if (targetAndEffect.first.moveableIndex.size() != 0)
				{
					std::vector<Vector2DInt> availableIndex;
					for (Vector2DInt vec2 : targetAndEffect.first.moveableIndex)
					{
						Vector2DInt result = card->GetCardCastingHeroIndex() + vec2;
						if (0 <= result.x && result.x < battlefield.battleFieldVector.size() / 2 &&
							0 <= result.y && result.y < battlefield.battleFieldVector[0].size())
						{
							availableIndex.push_back(result);
						}
					}

					if (availableIndex.size() == 0)
					{
						card->ForceToDiscard();
					}
					else
					{
						int selectedIndexIndex;
						if (availableIndex.size() == 1)
						{
							selectedIndexIndex = 0;
						}
						else
						{
							selectedIndexIndex = doodle::random(0, static_cast<int>(availableIndex.size()));
						}
						Vector2DInt selectedIndex = availableIndex[selectedIndexIndex];
						targetAndEffect.second = battlefield.battleFieldVector[selectedIndex.x][selectedIndex.y];
					}
				}
				else
				{
					emptyPos = battlefield.GetRandomEmptyTarget();
					if (emptyPos == Vector2DInt{ -1, -1 })
					{
						card->ForceToDiscard();
					}
					else
					{
						targetAndEffect.second = battlefield.battleFieldVector[emptyPos.x][emptyPos.y];
					}
				}
				break;

			case Card_Target::CHARACTER:
			case Card_Target::ENEMY:
			case Card_Target::ALL:
				randomTargetPos = battlefield.GetRandomTarget();
				if (randomTargetPos == Vector2DInt{ -1, -1 })
				{
					card->ForceToDiscard();
				}
				else
				{
					targetAndEffect.second = battlefield.battleFieldVector[randomTargetPos.x][randomTargetPos.y];
				}
				break;

			case Card_Target::LINE_CHARACTER:
			case Card_Target::LINE_ENEMY:
			case Card_Target::LINE_ALL:
				rowIndex = doodle::random(0, static_cast<int>(battlefield.battleFieldVector.size()));
				isMoreTargetAndEffectMapNeeded = false;
				for (int y = 0; y < columnFieldSize; ++y)
				{
					if (battlefield.battleFieldVector[rowIndex][y].GetAllyType() != BattleAllyType::EMPTY)
					{
						if (isMoreTargetAndEffectMapNeeded == false)
						{
							targetAndEffect.second = battlefield.battleFieldVector[rowIndex][y];
							isMoreTargetAndEffectMapNeeded = true;
						}
						else
						{
							auto copiedTargetAndEffect = targetAndEffect;
							copiedTargetAndEffect.second = battlefield.battleFieldVector[rowIndex][y];

							card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
						}
					}
				}
				if (isMoreTargetAndEffectMapNeeded == false)
				{
					card->ForceToDiscard();
				}
				break;

			}
		}
		Engine::GetLogger().LogEvent(card->current_name + "card drawed by \"Fast Reload\" Card.");
	}
}
inline Card_Effect HighNoon_CardEffect_FastReload{ "Draw three cards.\nUse those cards with random targets.\nIf a player can't use the card, it is discarded.", Card_Target::RANDOM_ALL, BattleAllyType::ENEMY, HighNoon_CardEffectFunction_FastReload, {} };

inline void HighNoon_CardEffectFunction_BullsEye(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 4;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	
	int diceRoll = doodle::random(0, 2);
	if (diceRoll == 1)
	{
		offset *= 2;
		damageAmount *= 2;
	}
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect HighNoon_CardEffect_BullsEye{ "Deal %0 AD damage to any target.\nHave 50% chance to deal double damage.", Card_Target::ALL, BattleAllyType::ENEMY, HighNoon_CardEffectFunction_BullsEye, {4} };



////**** Vampire ****////
inline void Vampire_CardEffectFunction_TotheMoon(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject, const Card_Effect&)
{
	if (GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHT || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		player_hand.Draw_from_deck(1);
	}
	
	int timeFlow = 4;
	GameClock::ChangeCurrentTime(timeFlow);
	GameClock::ChangeLimitedTime(timeFlow);

	battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->UpdateActableCount(1);
}
inline Card_Effect Vampire_CardEffect_TotheMoon{ "Let time flows 4 spaces.\nCaster can cast one more spell this turn.\nThe time when you cast this card was night, draw a card.", Card_Target::NONE, BattleAllyType::CHARACTER, Vampire_CardEffectFunction_TotheMoon, {} };

inline void Vampire_CardEffectFunction_BloodReckoning(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();

	int damageAmount = 4;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.apOffset;

	int previousTargetHP = battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData()->current_HP;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
	int currentTargetHP  = battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetEnemyData()->current_HP;
	int stealAmount = previousTargetHP - currentTargetHP;

	if(GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHT || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetHealed(stealAmount, 0);
	}
}
inline Card_Effect Vampire_CardEffect_BloodReckoning{ "Deal %0 AP damage to target enemy.\nIf it's night, Caster drain life that much.", Card_Target::ENEMY, BattleAllyType::ENEMY, Vampire_CardEffectFunction_BloodReckoning, {4} };

inline void Vampire_CardEffectFunction_ChaliceofBlood(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	int amountOfLife = 0;
	int heroAmount = 0;

	for(auto& battleObject : battlefield.GetAllyBattleField())
	{
		if(battleObject.GetCharacterData() != nullptr)
		{
			amountOfLife += battleObject.GetCharacterData()->current_HP;
			++heroAmount;
		}
	}

	int eachOfLife = static_cast<int>(amountOfLife / heroAmount);
	
	for (auto& battleObject : battlefield.GetAllyBattleField())
	{
		if (battleObject.GetCharacterData() != nullptr)
		{
			battleObject.GetCharacterData()->current_HP = 0;
			battleObject.GetHealed(eachOfLife, 0);
		}
	}
}
inline Card_Effect Vampire_CardEffect_ChaliceofBlood{ "Gather all hero's life to Chalice of Blood.\nAll hero drink it equally.", Card_Target::NONE, BattleAllyType::CHARACTER, Vampire_CardEffectFunction_ChaliceofBlood, {} };



////**** Solaire *****////
inline void Solaire_CardEffectFunction_SolarBlaze(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 0;

	if (GameClock::GetcurrentTimeOfDay() == TimeOfDay::DAY || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		damageAmount = 3;
	}
	
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;

	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect Solaire_CardEffect_SolarBlaze{ "Deal %0 AD damage to all enemies. If it's day, deal %1 AD damage instead.", Card_Target::ALL_ENEMY, BattleAllyType::ENEMY, Solaire_CardEffectFunction_SolarBlaze, {0, 3} };

inline void Solaire_CardEffectFunction_HourofGlory(Vector2DInt , Battlefield::BattleObject, const Card_Effect&)
{
	int remainingTime = GameClock::GetRemainingTime();
	GameClock::SetCurrentTime(0);
	GameClock::SetLimitedTime(remainingTime);
}
inline Card_Effect Solaire_CardEffect_HourofGlory{ "Change the time to the moment of the sun's dawning.", Card_Target::NONE, BattleAllyType::CHARACTER, Solaire_CardEffectFunction_HourofGlory, {} };

inline void Solaire_CardEffectFunction_SwelteringSun(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damage1Amount = 5;
	int damage2Amount = 2;
	
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;

	if (GameClock::GetcurrentTimeOfDay() == TimeOfDay::DAY || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		damage1Amount *= 2;
		damage2Amount *= 2;
		offset *= 2;
	}
	
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damage1Amount, offset);
	battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].DealtAdDamage(damage2Amount, offset);
}
inline Card_Effect Solaire_CardEffect_SwelteringSun{ "Sweltering sun deals %0 AD damage to target enemy and %1 AD damage to the caster.\nIf it's day, double damage.", Card_Target::ENEMY, BattleAllyType::ENEMY, Solaire_CardEffectFunction_SwelteringSun, {5, 2} };



////**** Neutral ****////
inline void Neutral_CardEffectFunction_Smash(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 5;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);
}
inline Card_Effect Neutral_CardEffect_Smash{ "Deal %0 AD damage to target enemy.", Card_Target::ENEMY, BattleAllyType::ENEMY, Neutral_CardEffectFunction_Smash, {5} };

inline void Neutral_CardEffectFunction_Fireball(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 5;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.apOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtApDamage(damageAmount, offset);
}
inline Card_Effect Neutral_CardEffect_Fireball{ "Deal %0 AP damage to target enemy.", Card_Target::ENEMY, BattleAllyType::ENEMY, Neutral_CardEffectFunction_Fireball, {5} };

inline void Neutral_CardEffectFunction_Slash(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 2;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.adOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, offset);

}
inline Card_Effect Neutral_CardEffect_Slash{ "Deal %0 AD damage to target enemy line.", Card_Target::LINE_ENEMY, BattleAllyType::ENEMY, Neutral_CardEffectFunction_Slash, {2} };

inline void Neutral_CardEffectFunction_Flamestrike(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int damageAmount = 2;
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.apOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtApDamage(damageAmount, offset);

}
inline Card_Effect Neutral_CardEffect_Flamestrike{ "Deal %0 AP damage to target enemy line.", Card_Target::LINE_ENEMY, BattleAllyType::ENEMY, Neutral_CardEffectFunction_Flamestrike, {2} };

inline void Neutral_CardEffectFunction_UpandDown(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	
	battlefield.SwapObject(cardCastingHeroIndex, targetIndex);
}
inline Card_Effect Neutral_CardEffect_UpandDown{ "Move up or down.", Card_Target::EMPTY, BattleAllyType::EMPTY, Neutral_CardEffectFunction_UpandDown, {}, { {0, -1}, {0, 1} } };

inline void Neutral_CardEffectFunction_LeftandRight(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();

	battlefield.SwapObject(cardCastingHeroIndex, targetIndex);
}
inline Card_Effect Neutral_CardEffect_LeftandRight{ "Move left or right.", Card_Target::EMPTY, BattleAllyType::EMPTY, Neutral_CardEffectFunction_LeftandRight, {}, { {-1, 0}, {1, 0} } };

inline void Neutral_CardEffectFunction_LikeaKing(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();

	battlefield.SwapObject(cardCastingHeroIndex, targetIndex);
}
inline Card_Effect Neutral_CardEffect_LikeaKing{ "Move one square in any direction.", Card_Target::EMPTY, BattleAllyType::EMPTY, Neutral_CardEffectFunction_LikeaKing, {}, { {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1} } };

inline void Netural_CardEffectFunction_Cooperation(Vector2DInt, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();

	if(battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetCharacterData() != nullptr)
	{		
		battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetCharacterData()->UpdateActableCount(1);
	}

	if(GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHT || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		player_hand.Draw_from_deck(1);
	}
}
inline Card_Effect Neutral_CardEffect_Cooperation{ "Target hero can cast one more card.\nIf it's night, Draw a card.", Card_Target::CHARACTER, BattleAllyType::CHARACTER, Netural_CardEffectFunction_Cooperation, {} };

inline void Netural_CardEffectFunction_TimeManaging(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	int additionalTime = 1;
	GameClock::ChangeLimitedTime(additionalTime);

	if(GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHT || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		player_hand.Draw_from_deck(2);
	}
}
inline Card_Effect Netural_CardEffect_TimeManaging{ "Time Management is super important.\nYou gain 1 more hours.\nIf it's night, Draw two cards.", Card_Target::NONE, BattleAllyType::CHARACTER, Netural_CardEffectFunction_TimeManaging, {} };

inline void Netural_CardEffectFunction_FirstAid(Vector2DInt cardCastingHeroIndex, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	int healAmount = 1;
	
	if(GameClock::GetcurrentTimeOfDay() == TimeOfDay::DAY || GameClock::GetcurrentTimeOfDay() == TimeOfDay::NIGHTANDDAY)
	{
		healAmount = 3;
	}
	
	int offset = battlefield.battleFieldVector[cardCastingHeroIndex.x][cardCastingHeroIndex.y].GetCharacterData()->currentOffset.healOffset;
	battlefield.battleFieldVector[targetIndex.x][targetIndex.y].GetHealed(healAmount, offset);
}
inline Card_Effect Netural_CardEffect_FirstAid{ "Restore %0 Health to target hero. If it's day, restore %1 instead.", Card_Target::CHARACTER, BattleAllyType::CHARACTER, Netural_CardEffectFunction_FirstAid, {1, 3} };

inline void Netural_CardEffectFunction_SunSpike(Vector2DInt, Battlefield::BattleObject target, const Card_Effect&)
{
	Vector2DInt targetIndex = target.GetPosition();
	
	int damageAmount = 8 - GameClock::GetCurrentTime();
	if (damageAmount <= 0) { damageAmount = 0; }

	if(target.GetAllyType() == BattleAllyType::CHARACTER)
	{
		battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtAdDamage(damageAmount, 0);
	}
	else if(target.GetAllyType() == BattleAllyType::ENEMY)
	{
		battlefield.battleFieldVector[targetIndex.x][targetIndex.y].DealtTrueDamage(damageAmount, 0);
	}
}
inline Card_Effect Netural_CardEffect_SunSpike{ "Deal X damage to any target.\nX is remaining time until the sunset.\nIgnore all armors.", Card_Target::ALL, BattleAllyType::CHARACTER, Netural_CardEffectFunction_SunSpike, {} };



////**** UI Cards ****////
#include "..\Engine\Engine.h"
inline void UI_CardEffectFunction_NewGame(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::ALPHA_HEROSELECT);
}
inline Card_Effect UI_CardEffect_NewGame{ "Let's start a new adventure!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_NewGame, {} };

inline void UI_CardEffectFunction_Credit(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::CREDIT);
}
inline Card_Effect UI_CardEffect_Credit{ "Let's see who made this game!!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Credit , {} };

inline void UI_CardEffectFunction_Quit(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().Shutdown();
}
inline Card_Effect UI_CardEffect_Quit{ "Good bye...\nWe'll miss you...", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Quit , {} };

inline void UI_CardEffectFunction_Title(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::TITLE);
}
inline Card_Effect UI_CardEffect_Title{ "Let's go back to the title, and start again!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Title , {} };

inline void UI_CardEffectFunction_CreateTestEffect(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Vector2DInt mousePos = Engine::GetInput().GetMousePos();
	Engine::GetEffectSystem().GenerateTestEffect({mousePos.x * 1.0, mousePos.y * 1.0});
}
inline Card_Effect UI_CardEffect_CreateTestEffect{ "Generate a red and blue particle effect!!\n\n(Just for fun)", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_CreateTestEffect , {} };

inline void UI_CardEffectFunction_DrawCardRemoveExplanation(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 10 });
}
inline Card_Effect UI_CardEffect_DrawCardRemoveExplanation{ "You can remove a card by right clicking!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_DrawCardRemoveExplanation , {} };

inline void UI_CardEffectFunction_DrawCardHeroInformationExplanation(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 11 });
}
inline Card_Effect UI_CardEffect_DrawCardHeroInformationExplanation{ "You can see hero's information by right clicking!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_DrawCardHeroInformationExplanation , {} };

inline void UI_CardEffectFunction_CardManagingFinish(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	cardManager.Unload();
}
inline Card_Effect UI_CardEffect_CardManagingFinish{ "It's perfect!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_CardManagingFinish , {} };

inline void UI_CardEffectFunction_ConfirmHeroSelect(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	if (heroSelect.getSelectingVector().size() >= 1) {
		Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_MAPANDROOM);
	}
	else {
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 6 });
	}
}
inline Card_Effect UI_CardEffect_ConfirmHeroSelect{ "I'm prepared!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_ConfirmHeroSelect , {} };

inline void UI_CardEffectFunction_ConfirmRoster(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_BATTLEFIELD);
}
inline Card_Effect UI_CardEffect_ConfirmRoster{ "We're gonna be rich!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_ConfirmRoster , {} };

inline void UI_CardEffectFunction_DeckManaging(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::ALPHA_CARDMANAGING);
}
inline Card_Effect UI_CardEffect_DeckManaging{ "Not bomb, just our deck.", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_DeckManaging , {} };

inline void UI_CardEffectFunction_ConfirmDeckManaging(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_ROSTER);
}
inline Card_Effect UI_CardEffect_ConfirmDeckManaging{ "This is Top Tier Deck!\nMaybe....?", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_ConfirmDeckManaging , {} };

inline void UI_CardEffectFunction_AgreeEvent(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	event.SetEventPhase(event.GetEventPhase() + 1);
	event.setIsUpdated(false);
}
inline Card_Effect UI_CardEffect_AgreeEvent{ "Let me touch the stone.\n(accept the event)", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_AgreeEvent , {} };

inline void UI_CardEffectFunction_DisagreeEvent(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	event.SetIsEventEnd(true);
}
inline Card_Effect UI_CardEffect_DisagreeEvent{ "Let me leave\n(refuse the event)", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_DisagreeEvent , {} };

namespace CardManaging {
	inline void UI_CardEffectFunction_PageUp(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		cardManager.SetNextPage(1);
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 20 });
	}
	inline Card_Effect UI_CardEffect_PageUp{ "Go to next page!\n(If there's no next page, it doesn't work)", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_PageUp , {} };

	inline void UI_CardEffectFunction_PageDown(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		cardManager.SetNextPage(-1);
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 21 });
	}
	inline Card_Effect UI_CardEffect_PageDown{ "Go to back page!\n(If there's no back page, it doesn't work)", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_PageDown , {} };
}

inline void UI_CardEffectFunction_Tutorial(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::FINAL_TUTORIAL);
}
inline Card_Effect UI_CardEffect_Tutorial{ "Is it your first time playing our game?", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Tutorial , {} };

inline void UI_CardEffectFunction_SkipTutorial(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
{

}
inline Card_Effect UI_CardEffect_SkipTutorial{ "Do you want to skip this?", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_SkipTutorial , {} };


namespace HeroPicking {
	inline void UI_CardEffectFunction_Warrior(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		auto heroVector{ battlefield.GetAllyBattleField() };
		int vecSize{ static_cast<int>(heroVector.size()) };
		for (int i = 0; i < vecSize; i++) {
			if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
			if (heroVector[i].GetCharacterData()->GetCharacterType() == CHARACTERTYPE::WARRIOR) {
				event.SetTargetCharacter(heroVector[i].GetCharacterData());
				Engine::GetLogger().LogEvent("Target character has been set : Warrior");
				event.SetEventPhase(event.GetEventPhase() + 1);
				return;
			}
		}
	}
	inline Card_Effect UI_CardEffect_Warrior{ "My selection is warrior!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Warrior , {} };

	inline void UI_CardEffectFunction_Mage(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		auto heroVector{ battlefield.GetAllyBattleField() };
		int vecSize{ static_cast<int>(heroVector.size()) };
		for (int i = 0; i < vecSize; i++) {
			if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
			if (heroVector[i].GetCharacterData()->GetCharacterType() == CHARACTERTYPE::MAGE) {
				event.SetTargetCharacter(heroVector[i].GetCharacterData());
				Engine::GetLogger().LogEvent("Target character has been set : Mage");
				event.SetEventPhase(event.GetEventPhase() + 1);
				return;
			}
		}
	}
	inline Card_Effect UI_CardEffect_Mage{ "My selection is mage!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Mage , {} };

	inline void UI_CardEffectFunction_Priest(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		auto heroVector{ battlefield.GetAllyBattleField() };
		int vecSize{ static_cast<int>(heroVector.size()) };
		for (int i = 0; i < vecSize; i++) {
			if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
			if (heroVector[i].GetCharacterData()->GetCharacterType() == CHARACTERTYPE::PRIEST) {
				event.SetTargetCharacter(heroVector[i].GetCharacterData());
				Engine::GetLogger().LogEvent("Target character has been set : Priest");
				event.SetEventPhase(event.GetEventPhase() + 1);
				return;
			}
		}
	}
	inline Card_Effect UI_CardEffect_Priest{ "My selection is priest!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Priest , {} };

	inline void UI_CardEffectFunction_Vampire(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		auto heroVector{ battlefield.GetAllyBattleField() };
		int vecSize{ static_cast<int>(heroVector.size()) };
		for (int i = 0; i < vecSize; i++) {
			if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
			if (heroVector[i].GetCharacterData()->GetCharacterType() == CHARACTERTYPE::VAMPIRE) {
				event.SetTargetCharacter(heroVector[i].GetCharacterData());
				Engine::GetLogger().LogEvent("Target character has been set : Vampire");
				event.SetEventPhase(event.GetEventPhase() + 1);
				return;
			}
		}
	}
	inline Card_Effect UI_CardEffect_Vampire{ "My selection is vampire!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Vampire , {} };

	inline void UI_CardEffectFunction_Solaire(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		auto heroVector{ battlefield.GetAllyBattleField() };
		int vecSize{ static_cast<int>(heroVector.size()) };
		for (int i = 0; i < vecSize; i++) {
			if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
			if (heroVector[i].GetCharacterData()->GetCharacterType() == CHARACTERTYPE::SOLAIRE) {
				event.SetTargetCharacter(heroVector[i].GetCharacterData());
				Engine::GetLogger().LogEvent("Target character has been set : Solaire");
				event.SetEventPhase(event.GetEventPhase() + 1);
				return;
			}
		}
	}
	inline Card_Effect UI_CardEffect_Solaire{ "My selection is solaire!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Solaire , {} };

	inline void UI_CardEffectFunction_Highnoon(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		auto heroVector{ battlefield.GetAllyBattleField() };
		int vecSize{ static_cast<int>(heroVector.size()) };
		for (int i = 0; i < vecSize; i++) {
			if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
			if (heroVector[i].GetCharacterData()->GetCharacterType() == CHARACTERTYPE::HIGHNOON) {
				event.SetTargetCharacter(heroVector[i].GetCharacterData());
				Engine::GetLogger().LogEvent("Target character has been set : Highnoon");
				event.SetEventPhase(event.GetEventPhase() + 1);
				return;
			}
		}
	}
	inline Card_Effect UI_CardEffect_Highnoon{ "My selection is highnoon!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Highnoon , {} };
}

namespace HeroOffset
{
	inline void UI_CardEffectFunction_AD(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		event.SetTargetOffset(event.GetTargetCharacter()->currentOffset.adOffset);
		event.SetEventPhase(event.GetEventPhase() + 1);
		event.setIsUpdated(false);
	}
	inline Card_Effect UI_CardEffect_AD{ "My selection is AD!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_AD , {} };

	inline void UI_CardEffectFunction_AP(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		event.SetTargetOffset(event.GetTargetCharacter()->currentOffset.apOffset);
		event.SetEventPhase(event.GetEventPhase() + 1);
		event.setIsUpdated(false);
	}
	inline Card_Effect UI_CardEffect_AP{ "My selection is AP!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_AP , {} };

	inline void UI_CardEffectFunction_Movement(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		event.SetTargetOffset(event.GetTargetCharacter()->currentOffset.movementOffset);
		event.SetEventPhase(event.GetEventPhase() + 1);
		event.setIsUpdated(false);
	}
	inline Card_Effect UI_CardEffect_Movement{ "My selection is Movement!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Movement , {} };

	inline void UI_CardEffectFunction_Heal(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)
	{
		event.SetTargetOffset(event.GetTargetCharacter()->currentOffset.healOffset);
		event.SetEventPhase(event.GetEventPhase() + 1);
		event.setIsUpdated(false);
	}
	inline Card_Effect UI_CardEffect_Heal{ "My selection is Heal!", Card_Target::NONE, BattleAllyType::EMPTY, UI_CardEffectFunction_Heal , {} };
}