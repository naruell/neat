﻿#pragma once
// Gam150
// Room.h
// Team NEAT
// Primary: Sunghwan Cho
// Secondary : Duhwan Kim
// 5/20/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <doodle/drawing.hpp>

#include "../Engine/Geometry/Vector2D.h"

enum class RoomType
{
	None,
	BattleRoom,
	EventRoom,
	PromotionRoom
};

enum class BattleRoomType
{
	NONE,
	FIRSTBATTLEROOM,
	SECONDBATTLEROOM,
	THIRDBATTLEROOM,
	BOSSBATTLEROOM,
	COUNT
};

struct Room
{
public:
	Room() = default;
	Room(Vector2D roomPos);
	virtual ~Room();
	virtual BattleRoomType GetBattleRoomType();
	
protected:
	Vector2D position;
	static constexpr double length = 60;
	bool isOnHovered = false;
	bool isOnClicked = false;
	bool isAbleToHover = false;
	bool isCleared = false;
	bool hasCleared = false;

	std::unique_ptr<doodle::Image> unclearedRoom = nullptr;
	std::unique_ptr<doodle::Image> clearedRoom = nullptr;
	
public:
	virtual RoomType GetType();

	virtual void Load();
	virtual void Draw();
	virtual void Draw(const double size);
	virtual void Update();
	virtual void OnHovered();
	virtual void OnClicked();
	
	virtual Vector2D GetPosition();
	virtual void SetIsOnClicked(bool set);
	virtual bool GetIsOnClicked();
	virtual void SetIsAbleToHover(bool set);
	virtual bool GetIsAbleToHover();
	virtual void SetIsOnHovered(bool set);
	virtual bool GetIsOnHovered();
	virtual void SetIsCleared(bool set);
	virtual bool GetIsCleared();
	virtual void SetHasCleared(bool set);
	virtual	bool GetHasCleared();
	
	virtual void ToBeExecuteOnClicked();
};

struct BattleRoom : Room
{
public:
	BattleRoom() = default;
	BattleRoom(Vector2D roomPos, int type);
	~BattleRoom();

	RoomType GetType() override { return  RoomType::BattleRoom; }

	void Load() override;
	void ToBeExecuteOnClicked() override;

	BattleRoomType GetBattleRoomType() override;
	
private:
	
	BattleRoomType type = static_cast<BattleRoomType>(0);
};

struct EventRoom : Room
{
public:
	EventRoom() = default;
	EventRoom(Vector2D roomPos);
	~EventRoom();

	RoomType GetType() override { return  RoomType::EventRoom; }

	void Load() override;
	void ToBeExecuteOnClicked() override;
};
