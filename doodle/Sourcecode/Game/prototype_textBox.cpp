﻿// GAM150
// prototype_textBox.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "prototype_textBox.h"

#include <doodle/environment.hpp>

#include "../Engine/Engine.h"

Prototype_textBox::Prototype_textBox() : fontIndex(1)
{
}

void Prototype_textBox::Load()
{
	TextInfo textInfo
	{
		22,
		TextAlignX::LEFT,
		TextAlignY::TOP,
		doodle::Color(255, 255),
		doodle::Color(255, 0)
	};

	textBox = new TextBox(fontIndex, "", { 0, 0 }, { 300, 300 }, textInfo, AutoNextLine::BY_WORD);
}

void Prototype_textBox::Update(double)
{
	Input::KeyboardButton key = Engine::GetInput().GetPressedKey();

	infoInterface.positionX = -doodle::Width / 2.0;
	infoInterface.positionY = doodle::Height / 2.0 - infoInterface.fontSize * 2;

	if (Engine::GetInput().IsMouseDown(Input::MouseButton::Left) == true)
	{
		textBox->setPosition(textBox->GetPosition() + Engine::GetInput().GetMouseDelta());
	}
	else if (Engine::GetInput().IsMouseDown(Input::MouseButton::Right) == true)
	{
		Vector2DInt delta = Engine::GetInput().GetMouseDelta();
		delta.y *= -1;
		textBox->SetSize(textBox->GetSize() + delta);
	}

	else if (nextLevelKey.IsKeyReleased() == true)
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_CARDDRAW);
	}

	std::string str = textBox->GetText();
	TextInfo textInfo = textBox->GetTextInfo();
	switch (key)
	{
	case Input::KeyboardButton::Enter:
		textBox->SetText(textBox->GetText() + '\n');
		break;

	case Input::KeyboardButton::Escape:
		textBox->SetText("");
		break;

	case Input::KeyboardButton::Space:
		textBox->SetText(textBox->GetText() + ' ');
		break;

	case Input::KeyboardButton::Left:
		switch (textBox->GetTextInfo().alignX)
		{
		case TextAlignX::LEFT:
			textInfo.alignX = TextAlignX::CENTER;
			textBox->SetTextInfo(textInfo);
			break;

		case TextAlignX::CENTER:
			textInfo.alignX = TextAlignX::RIGHT;
			textBox->SetTextInfo(textInfo);
			break;

		case TextAlignX::RIGHT:
			textInfo.alignX = TextAlignX::LEFT;
			textBox->SetTextInfo(textInfo);
			break;
		}
		break;

	case Input::KeyboardButton::Up:
		++textInfo.doodleTextSize;
		textBox->SetTextInfo(textInfo);
		break;

	case Input::KeyboardButton::Down:
		--textInfo.doodleTextSize;
		textBox->SetTextInfo(textInfo);
		break;

	case Input::KeyboardButton::Backspace:
		
		str.resize(str.size() - 1);
		textBox->SetText(str);
		break;

	case Input::KeyboardButton::A:
	case Input::KeyboardButton::B:
	case Input::KeyboardButton::C:
	case Input::KeyboardButton::D:
	case Input::KeyboardButton::E:
	case Input::KeyboardButton::F:
	case Input::KeyboardButton::G:
	case Input::KeyboardButton::H:
	case Input::KeyboardButton::I:
	case Input::KeyboardButton::J:
	case Input::KeyboardButton::K:
	case Input::KeyboardButton::L:
	case Input::KeyboardButton::M:
	case Input::KeyboardButton::N:
	case Input::KeyboardButton::O:
	case Input::KeyboardButton::P:
	case Input::KeyboardButton::Q:
	case Input::KeyboardButton::R:
	case Input::KeyboardButton::S:
	case Input::KeyboardButton::T:
	case Input::KeyboardButton::U:
	case Input::KeyboardButton::V:
	case Input::KeyboardButton::W:
	case Input::KeyboardButton::X:
	case Input::KeyboardButton::Y:
	case Input::KeyboardButton::Z:
		textBox->SetText(textBox->GetText() + char(int(key) - int(Input::KeyboardButton::A) + 'a'));
		break;

	case Input::KeyboardButton::_0:
	case Input::KeyboardButton::_1:
	case Input::KeyboardButton::_2:
	case Input::KeyboardButton::_3:
	case Input::KeyboardButton::_4:
	case Input::KeyboardButton::_5:
	case Input::KeyboardButton::_6:
	case Input::KeyboardButton::_7:
	case Input::KeyboardButton::_8:
	case Input::KeyboardButton::_9:
		textBox->SetText(textBox->GetText() + char(int(key) - int(Input::KeyboardButton::_0) + '0'));
		break;

	case Input::KeyboardButton::NumPad_0:
	case Input::KeyboardButton::NumPad_1:
	case Input::KeyboardButton::NumPad_2:
	case Input::KeyboardButton::NumPad_3:
	case Input::KeyboardButton::NumPad_4:
	case Input::KeyboardButton::NumPad_5:
	case Input::KeyboardButton::NumPad_6:
	case Input::KeyboardButton::NumPad_7:
	case Input::KeyboardButton::NumPad_8:
	case Input::KeyboardButton::NumPad_9:
		textBox->SetText(textBox->GetText() + char(int(key) - int(Input::KeyboardButton::NumPad_0) + '0'));
		break;
	}
}

void Prototype_textBox::Draw()
{
	Engine::GetWindow().Clear({ 0, 0, 0, 255 });
	textBox->Draw();

	doodle::no_fill();
	doodle::set_outline_color(doodle::Color(255, 255));

	const Vector2D& TBPos = textBox->GetPosition();
	const Vector2D& TBSize = textBox->GetSize();
	doodle::draw_rectangle(TBPos.x, TBPos.y, TBSize.x, TBSize.y );

	doodle::set_fill_color(doodle::Color(255, 255));
	doodle::no_outline();

	doodle::set_font_size(infoInterface.fontSize);
	doodle::draw_text("Text Font Size : " + std::to_string(textBox->GetTextInfo().doodleTextSize), infoInterface.positionX, infoInterface.positionY);
	doodle::draw_text("Text Length : " + std::to_string(textBox->GetText().size()), infoInterface.positionX, infoInterface.positionY - infoInterface.fontSize);
	doodle::draw_text("TextBox Position : {" + std::to_string(int(textBox->GetPosition().x)) + ", " + std::to_string(int(textBox->GetPosition().y)) + "}", infoInterface.positionX, infoInterface.positionY - infoInterface.fontSize * 2);
	doodle::draw_text("TextBox Size : {" + std::to_string(int(textBox->GetSize().x)) + ", " + std::to_string(int(textBox->GetSize().y)) + "}", infoInterface.positionX, infoInterface.positionY - infoInterface.fontSize * 3);
	doodle::draw_text("A~Z, 0~9, Space, Enter key: Typing Letter\nBackSpace key : Erasing Single Letter\nUp, Down key : Font Size UP/Down\nLeft key : Change Text Align\nEsc key : Erase All text\n", infoInterface.positionX, infoInterface.positionY - infoInterface.fontSize * 6);
}

void Prototype_textBox::Unload()
{
	delete textBox;
	textBox = nullptr;
}
