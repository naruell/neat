﻿#pragma once
// GAM150
// prototype_battlefield.h
// Team Neat
// Primary : Byeongjun Kim, Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/Background.h"
#include "../Engine/GameState.h"
#include "../Engine/Input.h"
#include "../Engine/Sprite.h"
#include "roster.h"

class Rostermanager : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() { return "Rostermanager"; }
	
	Input::InputKey nextLevelKey = Input::KeyboardButton::Enter;
	Input::InputKey  eventLevelKey = Input::KeyboardButton::Space;

private:

	// mouse things, we should change the engine that texture can has more pictures more than 1
	Sprite mouseNormal;
	Sprite mouseHolding;
	Background background;
};
