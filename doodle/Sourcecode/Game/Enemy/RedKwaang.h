﻿#pragma once
// GAM150
// RedKwaang.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class RedKwaang : public Enemy
{
public:
	using Enemy::Enemy;

	~RedKwaang() {};
	void Load() override;
private:
};