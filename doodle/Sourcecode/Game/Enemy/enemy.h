﻿#pragma once
// GAM150
// Mage.h
// Team Neat
// Primary : Sunghwan Cho made the interface of the class and, Junhyuk Cha made the definition of function.
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../../Engine/Sprite.h"
#include "../../Engine/Geometry/Vector2D.h"
#include "../../Environment/constants.h"
#include "../AI.h"

class Enemy
{
public:
	int current_HP;
	int current_damage;
	int current_adArmor;
	int current_apArmor;

	int healthSectionNum = -1;
protected:
	const std::string enemyName;
	const ENEMYTYPE enemyType;

	int original_HP;
	int original_damage;
	int original_adArmor;
	int original_apArmor;

	bool isHovered = false;
	bool hasAttacked = false;

public:
	Enemy() = default;
	Enemy(std::string enName, int hp, int damage, int adArmor, int apArmor, ENEMYTYPE e_type);
	virtual ~Enemy();
	virtual void Load() = 0;
	void Update(OBJECTSTATE uiState);
	void Draw(Vector2D position);
	void SetExplanation();
	void DealtAdDamage(int value, int offset, Vector2D position);
	void DealtApDamage(int value, int offset, Vector2D position);
	void DealtTrueDamage(int value, int offset, Vector2D position);
	void GetHealed(int value, int offset, Vector2D position);
	OBJECTSTATE GetEnemyUIState();

	AI& GetAI();

	ENEMYTYPE GetEnemyType();
	int GetOriginalHP();
	int GetOriginalDamage();
	int GetOriginalAdArmor();
	int GetOriginalApArmor();

	/*virtual void Pattern() = 0;

	virtual void OnHovered() = 0;*/
	/*virtual void AllocateOnHovered(std::function<void> newFunction) = 0;*/

protected:
	/*std::function<void()> ToBeExecutedOnHovered = nullptr;
	virtual void HPrelatedEquation() = 0;*/
	OBJECTSTATE enemyUIState = OBJECTSTATE::DEFAULT;
	Sprite sprite;
	AI ai;
	inline static doodle::Image* healthImage;
	inline static doodle::Image* damageImage;
	inline static doodle::Image* adArmorImage;
	inline static doodle::Image* apArmorImage;

	inline static bool isIconLoaded = false;
};

