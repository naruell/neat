﻿#pragma once
// GAM150
// Boma.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class Boma : public Enemy
{
public:
	using Enemy::Enemy;

	~Boma() {};
	void Load() override;
private:
};