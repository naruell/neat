﻿// GAM150
// Boma.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Boma.h"
#include "Boma_Anims.h"
#include "../../Environment/constants.h"
#include"../AI.h"

void Boma::Load()
{
	sprite.Load(PATH_BOMA_IMAGE);
	BigBombPattern* pattern1 = new BigBombPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern1, 10);
	sprite.PlayAnimation(static_cast<int>(Boma_Anim::Bomb_Anim));
}