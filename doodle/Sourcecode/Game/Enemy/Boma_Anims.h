﻿#pragma once
// GAM150
// Boma_Anims.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

enum class Boma_Anim
{
	None_Anim,
	Bomb_Anim,
};