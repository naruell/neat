﻿// GAM150
// Enemy.cpp
// Team Neat
// Primary : Sunghwan Cho made the interface of the Class, and Junhyuk Cha made the definition of the function
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

#include <doodle/image.hpp>
#include <doodle/environment.hpp>

#include "../../Engine/Engine.h"

Enemy::Enemy(std::string enName, int hp, int damage, int adArmor, int apArmor, ENEMYTYPE e_type) :
	enemyName(enName), original_HP(hp), current_HP(hp), original_damage(damage), current_damage(damage),
	current_adArmor(adArmor), original_adArmor(adArmor), current_apArmor(apArmor), original_apArmor(apArmor), enemyType(e_type)
{
	if (!isIconLoaded)
	{
		healthImage = new doodle::Image{ PATH_HEALTH_IMAGE };
		damageImage = new doodle::Image{ PATH_ADDAMAGE_IMAGE };
		adArmorImage = new doodle::Image{ PATH_ADARMOR_IMAGE };
		apArmorImage = new doodle::Image{ PATH_APARMOR_IMAGE };
		isIconLoaded = true;
	}

	int value = 0;
	while (value < original_HP)
	{
		value += 5;
		++healthSectionNum;
	}
}
Enemy::~Enemy()
{
	delete healthImage;
	delete damageImage;
	delete adArmorImage;
	delete apArmorImage;
	healthImage = nullptr;
	damageImage = nullptr;
	adArmorImage = nullptr;
	apArmorImage = nullptr;
	isIconLoaded = false;
}


void Enemy::Update(OBJECTSTATE uiState)
{
	enemyUIState = uiState;
	switch (enemyUIState)
	{
	case OBJECTSTATE::PRESSED:
		break;
	case OBJECTSTATE::RELEASED:
		break;
	case OBJECTSTATE::DEFAULT:
		break;
	}
}

void Enemy::Draw(Vector2D position)
{
	sprite.Draw(TranslateMatrix{ position.x, position.y });
	doodle::push_settings();
	doodle::set_rectangle_mode(doodle::RectMode::Corner);
	doodle::draw_rectangle(position.x - doodle::Width / 24, position.y + doodle::Height / 15.4, doodle::Width / 12, 15);
	doodle::set_fill_color(doodle::HexColor{ 0xff0000ff });
	if (current_HP > 0)
	{
		doodle::no_outline();
		doodle::draw_rectangle(position.x - doodle::Width / 24, position.y + doodle::Height / 15.4, static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP) * static_cast<double>(current_HP), 15);
	}
	doodle::set_outline_width(1);
	doodle::set_outline_color(doodle::HexColor{ 0x000000ff });
	double oneSectionLength = static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP);
	for (double i = 1; i <= healthSectionNum; ++i)
	{
		doodle::draw_line(position.x - doodle::Width / 24 + i * 5 * oneSectionLength, position.y + doodle::Height / 15.4, position.x - doodle::Width / 24 + i * 5 * oneSectionLength, position.y + doodle::Height / 15.4 + 15);
	}
	doodle::pop_settings();
	if (enemyUIState == OBJECTSTATE::HOVERED || enemyUIState == OBJECTSTATE::PRESSED || enemyUIState == OBJECTSTATE::RELEASED)
	{
		SetExplanation();
	}
}

void Enemy::SetExplanation()
{
	sprite.Draw(TranslateMatrix{ static_cast<double>(doodle::Width) / 1.09 - static_cast<double>(doodle::Width) / 2, -static_cast<double>(doodle::Height) / 3.33 + static_cast<double>(doodle::Height) / 2 });
	doodle::push_settings();
	doodle::set_rectangle_mode(doodle::RectMode::Corner);
	doodle::draw_rectangle(static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 7.5, doodle::Width / 12, 15);
	doodle::set_fill_color(doodle::HexColor{ 0xff0000ff });
	if (current_HP > 0)
	{
		doodle::no_outline();
		doodle::draw_rectangle(static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 7.5, static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP) * static_cast<double>(current_HP), 15);
	}
	doodle::set_outline_width(1);
	doodle::set_outline_color(doodle::HexColor{ 0x000000ff });
	double oneSectionLength = static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP);
	for (double i = 1; i <= healthSectionNum; ++i)
	{
		doodle::draw_line(static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24 + i * 5 * oneSectionLength, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 7.5,
			static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24 + i * 5 * oneSectionLength, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 22.5);
	}
	doodle::pop_settings();
	doodle::push_settings();
	doodle::set_font_size(15);
	doodle::draw_image(*healthImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(current_HP) + " / " + std::to_string(original_HP), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0);
	doodle::draw_image(*damageImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 40.0, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(current_damage), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 40.0);
	doodle::draw_image(*adArmorImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 20.0, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(current_adArmor), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 20.0);
	doodle::draw_image(*apArmorImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 40.0 * 3, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(current_apArmor), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 40.0 * 3);
	doodle::pop_settings();
}

void Enemy::DealtAdDamage(int value, int offset, Vector2D position)
{
	int valuePlusOffset = value + offset;

	if (valuePlusOffset <= 0)
	{
		valuePlusOffset = 0;
		Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(0), position, doodle::Color(255, 0, 0), 50, 2);
	}
	else
	{
		if (valuePlusOffset <= current_adArmor)
		{
			current_HP -= 1;
			Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(1), position, doodle::Color(255, 0, 0), 50, 2);
		}
		else
		{
			Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(valuePlusOffset - current_adArmor), position, doodle::Color(255, 0, 0), 50, 2);
			current_HP -= (valuePlusOffset - current_adArmor);
		}
	}
}

void Enemy::DealtApDamage(int value, int offset, Vector2D position)
{
	int valuePlusOffset = value + offset;

	if (valuePlusOffset <= 0)
	{
		valuePlusOffset = 0;
		Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(0), position, doodle::Color(255, 0, 0), 50, 2);
	}
	else
	{
		if (valuePlusOffset <= current_apArmor)
		{

			current_HP -= 1;
			Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(1), position, doodle::Color(255, 0, 0), 50, 2);
		}
		else
		{
			Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(valuePlusOffset - current_apArmor), position, doodle::Color(255, 0, 0), 50, 2);
			current_HP -= (valuePlusOffset - current_apArmor);
		}
	}
}

void Enemy::DealtTrueDamage(int value, int offset, Vector2D position)
{
	int valuePlusOffset = value + offset;

	if (valuePlusOffset <= 0)
	{
		valuePlusOffset = 0;
		Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(0), position, doodle::Color(255, 0, 0), 50, 2);
	}
	else
	{
		Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(valuePlusOffset), position, doodle::Color(255, 0, 0), 50, 2);
		current_HP -= valuePlusOffset;
	}
}

void Enemy::GetHealed(int value, int offset, Vector2D position)
{
	if (this == nullptr) { return; }
	int valuePlusOffset = value + offset;
	if (valuePlusOffset < 0)
	{
		valuePlusOffset = 0;
	}
	if ((current_HP + valuePlusOffset) > original_HP)
	{
		Engine::GetEffectSystem().GenerateTextEffect("+" + std::to_string(original_HP - current_HP), position, doodle::Color(144, 238, 144), 50, 2);
		current_HP = original_HP;
	}
	else
	{
		Engine::GetEffectSystem().GenerateTextEffect("+" + std::to_string(valuePlusOffset), position, doodle::Color(144, 238, 144), 50, 2);
		current_HP += (valuePlusOffset);
	}
}

OBJECTSTATE Enemy::GetEnemyUIState()
{
	return enemyUIState;
}

AI& Enemy::GetAI()
{
	return ai;
}

ENEMYTYPE Enemy::GetEnemyType()
{
	return enemyType;
}

int Enemy::GetOriginalHP()
{
	return original_HP;
}

int Enemy::GetOriginalDamage()
{
	return original_damage;
}

int Enemy::GetOriginalAdArmor()
{
	return original_adArmor;
}

int Enemy::GetOriginalApArmor()
{
	return original_apArmor;
}
