﻿// GAM150
// PurpleKwang.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "PurpleKwaang.h"
#include "../../Environment/constants.h"
#include"../AI.h"

void PurpleKwaang::Load()
{
	sprite.Load(PATH_KWAANG_IMAGE);
	AttackRandomOneColumnPattern* pattern1 = new AttackRandomOneColumnPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern1, 10);
	AddApArmorPattern* pattern2 = new AddApArmorPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern2, 15);
	Add40PercentDamagePattern* pattern3 = new Add40PercentDamagePattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern3, 5);
}