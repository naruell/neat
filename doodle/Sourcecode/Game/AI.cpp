﻿// GAM150
// AI.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "AI.h"

#include <random>
#include <vector>
#include <doodle/random.hpp>

#include "battlefield.h"

void AI::SelectNextPattern()
{
	std::vector<int> weights{};
	for (int i = 0; i < patterns.size(); ++i)
	{
		weights.push_back(patterns[i].second);
	}
	std::discrete_distribution<int> dist(std::begin(weights), std::end(weights));
	int getDist = dist(Engine::Instance().gen);
	if (patterns[getDist].first->patternType == PatternType::HEAL)
	{
		int numOfEnemy = 0;
		int numOfMaxHealthEnemy = 0;
		for (auto enemy : battlefield.GetEnemyBattleField())
		{
			if (enemy.GetEnemyData() != nullptr)
			{
				++numOfEnemy;
				if (enemy.GetEnemyData()->current_HP == enemy.GetEnemyData()->GetOriginalHP())
				{
					++numOfMaxHealthEnemy;
				}
			}
		}
		if (numOfMaxHealthEnemy == numOfEnemy)
		{
			while (patterns[getDist].first->patternType == PatternType::HEAL)
			{
				getDist = dist(Engine::Instance().gen);
			}
		}
	}
	patterns[getDist].first->Ready();
	nextPattern = patterns[getDist].first;
}

void AI::PushBackPatterns(Pattern* p, int weight)
{
	patterns.push_back(std::pair(p, weight));
}

Pattern* AI::GetNextPattern()
{
	return nextPattern;
}

AI::~AI()
{
	for (auto P : patterns)
	{
		delete P.first;
		P.first = nullptr;
	}
}

void AttackOneHeroPattern::Ready()
{
	attackRange.push_back(battlefield.GetRandomAllyTarget());
	//Engine::GetLogger().LogError(std::to_string(attackRange[0].x) + ' ' + std::to_string(attackRange[0].y));
}

void AttackOneHeroPattern::Execute()
{
	for (auto attack : attackRange)
	{
		battlefield.GetObject(attack).DealtAdDamage(enemyPtr->current_damage, 0);
	}
	attackRange.clear();
}


void DoNothingPattern::Ready()
{
}

void DoNothingPattern::Execute()
{
}


void AttackRandomTwoTilesPattern::Ready()
{
	attackRange.push_back(battlefield.GetRandomAllyPosNoexcept());
	Vector2DInt secondAllyTile = battlefield.GetRandomAllyPosNoexcept();
	while (attackRange[0] == secondAllyTile)
	{
		secondAllyTile = battlefield.GetRandomAllyPosNoexcept();
	}
	attackRange.push_back(secondAllyTile);
	//Engine::GetLogger().LogError(std::to_string(attackRange[0].x) + ' ' + std::to_string(attackRange[0].y));
	//Engine::GetLogger().LogError(std::to_string(secondAllyTile.x) + ' ' + std::to_string(secondAllyTile.y));
}

void AttackRandomTwoTilesPattern::Execute()
{
	for (auto attack : attackRange)
	{
		if (enemyPtr->current_damage < 2)
		{
			battlefield.GetObject(attack).DealtAdDamage(1, 0);
		}
		else
		{
			battlefield.GetObject(attack).DealtAdDamage(static_cast<int>(enemyPtr->current_damage / 2), 0);
		}
	}
	attackRange.clear();
}


void AttackRandomOneColumnPattern::Ready()
{
	int selectHeroColumn = static_cast<int>(doodle::random() * 2);
	if (selectHeroColumn == 0)
	{
		attackRange.push_back({ 0, 0 });
		attackRange.push_back({ 0, 1 });
		attackRange.push_back({ 0, 2 });
	}
	else
	{
		attackRange.push_back({ 1, 0 });
		attackRange.push_back({ 1, 1 });
		attackRange.push_back({ 1, 2 });
	}
}

void AttackRandomOneColumnPattern::Execute()
{
	for (auto attack : attackRange)
	{
		if (enemyPtr->current_damage < 3)
		{
			battlefield.GetObject(attack).DealtAdDamage(1, 0);
		}
		else
		{
			battlefield.GetObject(attack).DealtAdDamage(static_cast<int>(enemyPtr->current_damage / 3), 0);
		}
	}
	attackRange.clear();
}

void Add40PercentDamagePattern::Ready()
{
	for (auto enemy : battlefield.GetEnemyBattleField())
	{
		if (enemy.GetEnemyData() == enemyPtr)
		{
			preparationRange.push_back(enemy.GetPosition());
		}
	}
}

void Add40PercentDamagePattern::Execute()
{
	for (auto enemy : preparationRange)
	{
		battlefield.GetObject(enemy).GetEnemyData()->current_damage += battlefield.GetObject(enemy).GetEnemyData()->current_damage *2/5;
	}
	preparationRange.clear();
}

void HealLowestHPEnemyPattern::Ready()
{
	Enemy* getEnemyData = nullptr;
	Vector2DInt getEnemyIndex;

	for (auto enemy : battlefield.GetEnemyBattleField())
	{
		if (enemy.GetEnemyData() != nullptr)
		{
			if (getEnemyData == nullptr)
			{
				getEnemyData = enemy.GetEnemyData();
				getEnemyIndex = enemy.GetPosition();
			}
			else if(getEnemyData->current_HP == getEnemyData->GetOriginalHP())
			{
				getEnemyData = enemy.GetEnemyData();
				getEnemyIndex = enemy.GetPosition();
			}
			else if (enemy.GetEnemyData()->current_HP == enemy.GetEnemyData()->GetOriginalHP())
			{
				continue;
			}
			else if(
				static_cast<double>(enemy.GetEnemyData()->current_HP)/ static_cast<double>(enemy.GetEnemyData()->GetOriginalHP()) < 
				static_cast<double>(getEnemyData->current_HP) / static_cast<double>(getEnemyData->GetOriginalHP()))
			{
				getEnemyData = enemy.GetEnemyData();
				getEnemyIndex = enemy.GetPosition();
			}
			else if (static_cast<double>(enemy.GetEnemyData()->current_HP) / static_cast<double>(enemy.GetEnemyData()->GetOriginalHP()) >
				static_cast<double>(getEnemyData->current_HP) / static_cast<double>(getEnemyData->GetOriginalHP()))
			{
				continue;
			}
			else
			{
				int randomValue = doodle::random(static_cast<int>(doodle::random() * 2));
				switch (randomValue)
				{
				case 0:
					break;
				case 1:
					getEnemyData = enemy.GetEnemyData();
					getEnemyIndex = enemy.GetPosition();
					break;
				}
			}
		}
	}
	healRange.push_back({ getEnemyIndex, enemyPtr->current_damage });

}

void HealLowestHPEnemyPattern::Execute()
{
	for (auto enemy : healRange)
	{
		battlefield.GetObject(enemy.first).GetEnemyData()->GetHealed(enemy.second, 0, enemy.first);
	}
	healRange.clear();
}

void BigBombPattern::Ready()
{
	if (leftTurn > 0)
	{
		for (auto enemy : battlefield.GetEnemyBattleField())
		{
			if (enemy.GetEnemyData() == enemyPtr)
			{
				preparationRange.push_back(enemy.GetPosition());
			}
		}
	}
	else
	{
		patternType = PatternType::ATTACK;
		for (auto hero : battlefield.GetAllyBattleField())
		{
			attackRange.push_back({ hero.GetPosition() });
		}
	}
}

void BigBombPattern::Execute()
{
	if (leftTurn > 0)
	{
		--leftTurn;
		preparationRange.clear();
	}
	else
	{
		for (auto attack : attackRange)
		{
			battlefield.GetObject(attack).DealtAdDamage(static_cast<int>(enemyPtr->current_damage), 0);
		}
		patternType = PatternType::PREPARATION;
		leftTurn = 2;
		attackRange.clear();
	}
}

void AddApArmorPattern::Ready()
{
	for (auto enemy : battlefield.GetEnemyBattleField())
	{
		if (enemy.GetEnemyData() == enemyPtr)
		{
			preparationRange.push_back(enemy.GetPosition());
		}
	}
}

void AddApArmorPattern::Execute()
{
	for (auto enemy : preparationRange)
	{
		battlefield.GetObject(enemy).GetEnemyData()->current_apArmor += apArmorAmount;
	}
	preparationRange.clear();
}

void AddAdArmorPattern::Ready()
{
	for (auto enemy : battlefield.GetEnemyBattleField())
	{
		if (enemy.GetEnemyData() == enemyPtr)
		{
			preparationRange.push_back(enemy.GetPosition());
		}
	}
}

void AddAdArmorPattern::Execute()
{
	for (auto enemy : preparationRange)
	{
		battlefield.GetObject(enemy).GetEnemyData()->current_adArmor += adArmorAmount;
	}
	preparationRange.clear();
}
