﻿// GAM150
// EventRoom.cpp
// Team Neat
// Primary : Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "EventRoom.h"

#include <iostream>
#include <doodle/random.hpp>

#include "battlefield.h"
#include "CardManager.h"
#include "cardPile.h"
#include "Reward.h"

void Event::Load(int eventType)
{
	eventRoomType = static_cast<EVENTROOMTYPE>(eventType);
	eventPhase = 0;
	isEventEnd = false;

	eventText = std::make_unique<TextBox>(3, "", Vector2D{ 300,0 }, Vector2D{ 600,600 },
		TextInfo{ 48, TextAlignX::CENTER, TextAlignY::TOP, doodle::Color{0x000000FF}, doodle::Color{0x000000FF} }, AutoNextLine::BY_WORD);

	// Load
	switch (eventRoomType)
	{
	case EVENTROOMTYPE::CardDeleting:
		cardManager.Load(1);
		break;
	case EVENTROOMTYPE::CardFinding:
		reward.Load(1);
		break;
	case EVENTROOMTYPE::HeroOffset:
	case EVENTROOMTYPE::HealHerosWithPenalty:
	case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
	case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
	case EVENTROOMTYPE::AddRandomClassCards:
	case EVENTROOMTYPE::Boss_StatIncrease:
		for (int i = 0; i < Battlefield::BATTLEFIELDROW / 2; i++)
		{
			for (int j = 0; j < Battlefield::BATTLEFIELDCOLUMN; j++)
			{
				if (battlefield.battleFieldVector[i][j].GetAllyType() == BattleAllyType::CHARACTER)
				{
					characterTypes.push_back(static_cast<int>(battlefield.battleFieldVector[i][j].GetCharacterData()->GetCharacterType()));
				}
			}
		}
		for (int i = 0; i < static_cast<int>(battlefield.deadCharacterVector.size()); i++)
		{
			characterTypes.push_back(static_cast<int>(battlefield.deadCharacterVector[i].GetCharacterData()->GetCharacterType()));
		}
		break;
	default:
		break;
	}
	std::cout << "[" << static_cast<int>(eventRoomType) << "]\n";
	UpdateText();
}

void Event::Update()
{
	if (isEventEnd == true) { return; }

	switch (eventRoomType)
	{
	case EVENTROOMTYPE::HeroOffset:
		if (eventPhase == 3)
		{
			if (isUpdateEnd == false)
			{
				*targetOffset += 1;
				targetCharacter->SetOriginalAdOffset(*targetOffset);
				isUpdateEnd = true;
			}
		}
		else if (eventPhase == 4)
		{
			*targetOffset -= 1;
			targetCharacter->SetOriginalAdOffset(*targetOffset);
			Unload();
		}
		break;
	case EVENTROOMTYPE::CardDeleting:
		if (event.GetEventPhase() == 1)
		{
			cardManager.Update();
		}
		if (cardManager.IsManagingEnd() == true)
		{
			Unload();
		}
		break;
	case EVENTROOMTYPE::CardFinding:
		if (event.GetEventPhase() == 1)
		{
			reward.Update();
		}
		if (reward.IsRewardEnd() == true)
		{
			Unload();
		}
		break;
	case EVENTROOMTYPE::HealHerosWithPenalty:
		if (eventPhase == 1)
		{
			for (int i = 0; i < battlefield.GetAllyBattleField().size(); i++)
			{
				if (battlefield.GetAllyBattleField()[i].GetAllyType() != BattleAllyType::CHARACTER) { continue; }
				battlefield.GetAllyBattleField()[i].GetCharacterData()->SetOriginalHP(battlefield.GetAllyBattleField()[i].GetCharacterData()->GetOriginalHP() - 1);
				battlefield.GetAllyBattleField()[i].GetCharacterData()->current_HP = battlefield.GetAllyBattleField()[i].GetCharacterData()->GetOriginalHP();
			}
			Unload();
		}
		break;
	case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
		if (eventPhase == 2)
		{
			targetCharacter->SetOriginalHP(targetCharacter->GetOriginalHP() + 2);
			targetCharacter->current_HP = targetCharacter->current_HP + 2;
			Unload();
		}
		break;
	case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
		if (eventPhase == 3)
		{
			*targetOffset += 1;
			targetCharacter->current_HP = targetCharacter->current_HP - 3;
			// 아니 근데 이거 했는데 애 체력 0이하 되면 어캄? 아 ㅋㅋ
			Unload();
		}
		break;
	case EVENTROOMTYPE::AddRandomClassCards:
		if (eventPhase == 1)
		{
			deck.Push_back({ characterTypes[doodle::random(0, static_cast<int>(characterTypes.size()))], (doodle::random(0,3)) });
			Unload();
		}
		break;
	case EVENTROOMTYPE::Boss_StatIncrease:
		if (eventPhase == 0)
		{
			if (isUpdateEnd == false)
			{
				for (int i = 0; i < battlefield.deadCharacterVector.size(); i++)
				{
					for (auto hero : battlefield.GetAllyBattleField())
					{
						if (hero.GetCharacterData() == nullptr)
						{
							battlefield.GetObject(hero.GetPosition()).ChangeObject(battlefield.deadCharacterVector[i]);
							break;
						}
					}
				}
				battlefield.deadCharacterVector.clear();
				isUpdateEnd = true;
			}
			for (auto hero : battlefield.GetAllyBattleField())
			{
				if (hero.GetCharacterData() != nullptr)
				{
					hero.GetCharacterData()->current_HP = hero.GetCharacterData()->GetOriginalHP();
				}
			}
		}
		else if (eventPhase == 1)
		{
			if (isUpdateEnd == false)
			{
				auto heroVector{ battlefield.GetAllyBattleField() };
				int vecSize{ static_cast<int>(heroVector.size()) };
				for (int i = 0; i < vecSize; i++)
				{
					if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
					if (static_cast<int>(heroVector[i].GetCharacterData()->GetCharacterType()) == characterTypes[eventPhase - 1])
					{
						event.SetTargetCharacter(heroVector[i].GetCharacterData());
						Engine::GetLogger().LogEvent("Target character has been set : " + Int2CharacterTypeString(static_cast<int>(heroVector[i].GetCharacterData()->GetCharacterType())));
						break;
					}
				}
				isUpdateEnd = true;
			}
		}
		else if (2 <= eventPhase && eventPhase <= 3) {
			if (isUpdateEnd == false) {
				*targetOffset += 2;
				targetCharacter->SetOriginalHP(targetCharacter->GetOriginalHP() + 2);
				targetCharacter->current_HP = targetCharacter->current_HP + 2;
				auto heroVector{ battlefield.GetAllyBattleField() };
				int vecSize{ static_cast<int>(heroVector.size()) };
				for (int i = 0; i < vecSize; i++)
				{
					if (heroVector[i].GetAllyType() == BattleAllyType::EMPTY) { continue; }
					if (static_cast<int>(heroVector[i].GetCharacterData()->GetCharacterType()) == characterTypes[eventPhase - 1])
					{
						event.SetTargetCharacter(heroVector[i].GetCharacterData());
						Engine::GetLogger().LogEvent("Target character has been set : " + Int2CharacterTypeString(static_cast<int>(heroVector[i].GetCharacterData()->GetCharacterType())));
						break;
					}
				}
				isUpdateEnd = true;
			}
		}
		else if (eventPhase == 4) {
				*targetOffset += 2;
				targetCharacter->SetOriginalHP(targetCharacter->GetOriginalHP() + 2);
				targetCharacter->current_HP = targetCharacter->current_HP + 2;
				Unload();
		}
		break;
	default:
		break;
	}

}

void Event::Draw()
{
	if (isEventEnd == true) { return; }

	switch (eventRoomType)
	{
	case EVENTROOMTYPE::HeroOffset:
		break;
	case EVENTROOMTYPE::CardDeleting:
		if (event.GetEventPhase() == 1)
		{
			cardManager.Draw();
		}
		break;
	case EVENTROOMTYPE::CardFinding:
		if (event.GetEventPhase() == 1)
		{
			reward.Draw();
		}
		break;
	case EVENTROOMTYPE::HealHerosWithPenalty:
		break;
	case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
		break;
	case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
		break;
	case EVENTROOMTYPE::AddRandomClassCards:
		break;
	default:
		break;
	}

	eventText->Draw();
}

void Event::Unload()
{
	switch (eventRoomType)
	{
	case EVENTROOMTYPE::CardDeleting:
		cardManager.Unload();
		break;
	case EVENTROOMTYPE::CardFinding:
		reward.Unload();
		break;
	case EVENTROOMTYPE::HeroOffset:
		break;
	case EVENTROOMTYPE::HealHerosWithPenalty:
		break;
	case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
		break;
	case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
		break;
	case EVENTROOMTYPE::AddRandomClassCards:
		break;
	default:
		break;
	}

	eventRoomType = EVENTROOMTYPE::None;
	isEventEnd = true;
	targetCharacter = nullptr;
	targetOffset = nullptr;
	characterTypes = {};
	eventPhase = 0;

	isUpdateEnd = false;
}

// Text update
void Event::UpdateText()
{
	if (eventText->GetText() != "")
	{
		eventText->SetText("");
	}
	switch (eventRoomType)
	{
	case EVENTROOMTYPE::CardDeleting:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a bright shining stone! You may remove one card from your deck if you touch this stone.");
		}
		break;
	case EVENTROOMTYPE::CardFinding:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a bright glowing stone! You may get one class card if you touch this stone.");
		}
		break;
	case EVENTROOMTYPE::HeroOffset:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a weakly lighting stone! You may increase and decrease one hero's offset if you touch this stone.");
		}
		else if (eventPhase == 1)
		{
			eventText->SetText("Choose the hero who want to increase and decrease the offset.");
		}
		else if (eventPhase == 2)
		{
			eventText->SetText("Choose the offset what you want to increase.");
		}
		else if (eventPhase == 3)
		{
			eventText->SetText("Choose the offset what you want to decrease.");
		}
		break;
	case EVENTROOMTYPE::HealHerosWithPenalty:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a weakly shining stone! If you touch this stone, you may heal all heros, but you will get curse : All hero's HP Max - 1.");
		}
		break;
	case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a bright lighting stone! You may increase one hero's HP offset + 2 if you touch this stone.");
		}
		if (eventPhase == 1)
		{
			eventText->SetText("Choose the character who want to increase the HP max.");
		}
		break;
	case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a weakly glowing stone! If you tuch this stone, you may increase one hero's offset, but you will get curse : Chosen hero's HP get 3 damage.");
		}
		else if (eventPhase == 1)
		{
			eventText->SetText("Choose the hero who want to be the target.");
		}
		else if (eventPhase == 2)
		{
			eventText->SetText("Choose the offset what you want to increase.");
		}
		break;
	case EVENTROOMTYPE::AddRandomClassCards:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a glowing stone! You may get a class card randomly if you touch this stone.");
		}
		break;
	case EVENTROOMTYPE::Boss_StatIncrease:
		if (eventPhase == 0)
		{
			eventText->SetText("Look! You find a glowing stone very brightly! This light revives all of your party! Also, you may increase each heros offset + 2 and HP max + 2 if you touch this stone.");
		}
		else if (eventPhase == 1)
		{
			eventText->SetText("Which offset do you want to increase(+2) for " + Int2CharacterTypeString(event.GetCharacterTypesVector()[eventPhase - 1]));
		}
		else if (eventPhase == 2)
		{
			eventText->SetText("Which offset do you want to increase(+2) for " + Int2CharacterTypeString(event.GetCharacterTypesVector()[eventPhase - 1]));
		}
		else if (eventPhase == 3)
		{
			eventText->SetText("Which offset do you want to increase(+2) for " + Int2CharacterTypeString(event.GetCharacterTypesVector()[eventPhase - 1]));
		}
		break;
	default:
		break;
	}
}

void Event::setIsUpdated(bool update)
{
	isUpdateEnd = update;
}

bool Event::GetIsEventEnd()
{
	return isEventEnd;
}

int Event::GetRoomType()
{
	return static_cast<int>(eventRoomType);
}

void Event::SetRoomType(int value)
{
	eventRoomType = static_cast<EVENTROOMTYPE>(value);
}

int Event::GetEventPhase()
{
	return eventPhase;
}

void Event::SetEventPhase(int value)
{
	eventPhase = value;
}

void Event::SetIsEventEnd(bool EndOrNot)
{
	isEventEnd = EndOrNot;
}

std::vector<int> Event::GetCharacterTypesVector()
{
	return characterTypes;
}

Character* Event::GetTargetCharacter()
{
	return targetCharacter;
}

void Event::SetTargetCharacter(Character* targetChar)
{
	targetCharacter = targetChar;
}

int* Event::GetTargetOffset()
{
	return targetOffset;
}

void Event::SetTargetOffset(int& targetOff)
{
	targetOffset = &targetOff;
}

Event event;