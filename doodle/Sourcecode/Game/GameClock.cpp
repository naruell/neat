﻿// GAM150
// GameClock.cpp
// Team Neat
// Primary : Junhyuk Cha
// Secondary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.
// 
#include "GameClock.h"

#include <cmath>
#include <doodle/drawing.hpp>
#include <doodle/environment.hpp>
#include <doodle/angle.hpp>

#include "../Engine/Engine.h"
#include "../Engine/helper.h"

const double PI = 3.1415926535;

void GameClock::Load()
{
	sprite = std::make_unique<Sprite>();
	sprite->Load(PATH_CLOCK_IMAGE);

	curTimeNeedle = new doodle::Image("Assets/Clock/Needle.png");
	remainingTimeNeedle = new doodle::Image("Assets/Clock/Needle.png");

	CalculateNextLimitedTime();
}

void GameClock::Unload()
{
	delete curTimeNeedle;
	delete remainingTimeNeedle;
}

void GameClock::CalculateNextLimitedTime()
{
	if (currentTime + givenTime >= 12)
	{
		limitedTime = currentTime + givenTime - 12;
	}
	else
	{
		limitedTime = currentTime + givenTime;
	}
}

void GameClock::Update()
{
	if (currentTime >= 12)
	{
		currentTime -= 12;
	}
	
	// 시계바늘 애니메이션
	if(currentMiddleArrowDegree != currentMiddleArrowDegreeTo)
	{
		animationTimer += doodle::DeltaTime;
		if (animationTimer >= animationTimerTo)
		{
			animationTimer = animationTimerTo;
		}
		
		currentMiddleArrowDegree = Map(animationTimer, 0, animationTimerTo, previousMiddleArrowDegree, currentMiddleArrowDegreeTo);

		if(animationTimer == animationTimerTo)
		{
			currentMiddleArrowDegree = currentMiddleArrowDegreeTo;
			animationTimer = 0;
		}
	}
	else
	{
		if (remainingClockVibration != 0)
		{
			currentMiddleArrowDegreeTo = PI / 2 - currentTime * PI / 6;
			
			if (remainingClockVibration % 2 == 0)
			{
				currentMiddleArrowDegreeTo *= 0.97;
				previousMiddleArrowDegree = currentMiddleArrowDegreeTo;
			}
			else
			{
				currentMiddleArrowDegreeTo *= 1.03;
				previousMiddleArrowDegree = currentMiddleArrowDegreeTo;
			}
			animationTimerTo = 0.02;
			--remainingClockVibration;
		}
		else
		{
			currentMiddleArrowDegreeTo = PI / 2 - currentTime * PI / 6;
			currentMiddleArrowDegree = PI / 2 - currentTime * PI / 6;
		}
	}

	
	switch (currentTime)
	{
	case 0:
		currentTimeOfDay = TimeOfDay::NIGHTANDDAY;
		break;
	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
		currentTimeOfDay = TimeOfDay::DAY;
		break;
	case 6:
		currentTimeOfDay = TimeOfDay::NIGHTANDDAY;
		break;
	case 7:
	case 8:
	case 9:
	case 10:
	case 11:
		currentTimeOfDay = TimeOfDay::NIGHT;
		break;
	}

	if (currentTime >= 12)
	{
		currentTime -= 12;
	}
	limitedMiddleArrowDegree = PI / 2 - limitedTime * PI / 6;
}

void GameClock::Draw()
{
	double clockXPos = static_cast<double>(Engine::GetWindow().GetSize().x) / 2.4;
	double clockYPos = static_cast<double>(Engine::GetWindow().GetSize().y) / 5.0 * 2.0;
	doodle::push_settings();
	doodle::set_image_mode(doodle::RectMode::Center);
	sprite->Draw(TranslateMatrix{ clockXPos, clockYPos });

	doodle::apply_translate(clockXPos, clockYPos);

	doodle::push_settings();
	doodle::apply_rotate(currentMiddleArrowDegree - doodle::HALF_PI);
	
	doodle::set_tint_color(0, 255, 0, 255);
	doodle::draw_image(*curTimeNeedle, 0, 44);
	doodle::pop_settings();

	doodle::push_settings();
	doodle::apply_rotate(limitedMiddleArrowDegree - doodle::HALF_PI);
	doodle::set_tint_color(255, 255, 0, 255);
	doodle::draw_image(*remainingTimeNeedle, 0, 44);
	doodle::pop_settings();
	doodle::pop_settings();
}

void GameClock::DrawLimitedTime()
{
	double clockXPos = static_cast<double>(Engine::GetWindow().GetSize().x) / 2.4;
	double clockYPos = static_cast<double>(Engine::GetWindow().GetSize().y) / 5.0 * 2.0;
	Vector2D lineEndedPoint = { clockXPos + middleArrowLength * cos(limitedMiddleArrowDegree), clockYPos + middleArrowLength * sin(limitedMiddleArrowDegree) };
	doodle::push_settings();
	doodle::set_outline_color(doodle::HexColor{ 0xffd300ff });
	doodle::draw_line(lineEndedPoint.x, lineEndedPoint.y, clockXPos, clockYPos);
	doodle::draw_line(lineEndedPoint.x, lineEndedPoint.y, lineEndedPoint.x - shortArrowLength * cos(limitedMiddleArrowDegree + PI / 4), lineEndedPoint.y - shortArrowLength * sin(limitedMiddleArrowDegree + PI / 4));
	doodle::draw_line(lineEndedPoint.x, lineEndedPoint.y, lineEndedPoint.x - shortArrowLength * cos(limitedMiddleArrowDegree - PI / 4), lineEndedPoint.y - shortArrowLength * sin(limitedMiddleArrowDegree - PI / 4));
	doodle::pop_settings();
}

void GameClock::ChangeCurrentTime(int time)
{
	currentTime += time;
	remainingClockVibration = clockVibrationAmount;
	previousMiddleArrowDegree = currentMiddleArrowDegree;
	animationTimerTo = 0.1;
	currentMiddleArrowDegreeTo = PI / 2 - currentTime * PI / 6;
}

void GameClock::ChangeLimitedTime(int time)
{
	limitedTime += time;
}

int GameClock::GetCurrentTime()
{
	return currentTime;
}

int GameClock::GetLimitedTime()
{
	return limitedTime;
}

void GameClock::SetCurrentTime(int time)
{
	currentTime = time;
	remainingClockVibration = clockVibrationAmount;
	previousMiddleArrowDegree = currentMiddleArrowDegree;
	animationTimerTo = 0.1;
	currentMiddleArrowDegreeTo = PI / 2 - currentTime * PI / 6;
}

void GameClock::SetLimitedTime(int time)
{
	limitedTime = time;
}

TimeOfDay GameClock::GetcurrentTimeOfDay()
{
	return currentTimeOfDay;
}

int GameClock::GetRemainingTime()
{
	return limitedTime - currentTime >= 0 ? limitedTime - currentTime : limitedTime + 12 - currentTime;
}
