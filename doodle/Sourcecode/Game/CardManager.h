﻿#pragma once
// GAM150
// CardManager.h
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <map>
#include <vector>
#include <memory>

class Card;

enum class SORTWAY
{
	CHARACTERTYPE,
	CARDTYPE,
	SPENDINGTIME,
	COUNT
};

enum class PageChange
{
	BACK = -1,
	NONE,
	NEXT,
};

class CardManager
{
public:
	CardManager() = default;

	void Load(int RemovableCount = 0);
	void Update();
	void Draw();
	void Unload();

	int GetErasableCount();
	void SetCardManagingIsFinished(bool isFinished);
	bool IsManagingEnd();
	void SetNextPage(int nextPag);

	void Calculate_statistics_allWays();
	void Calculate_statistics(SORTWAY sortWay, int Value);

	static const int cardsCountRow{ 7 }; // 가로 한 줄에 카드 몇 장?
	static const int cardsCountColumn{ 3 }; // 한 페이지에 카드 몇 줄
private:
	 std::vector<std::unique_ptr<Card>> cardList;

	 PageChange nextPage;

	 int collectionPage = 0;
	 int erasableCount = 0;
	 bool isManagingEnd = false;

	 std::map<std::pair<int, int>, int> statistics{};
};

extern CardManager cardManager;