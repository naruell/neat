﻿// GAM150
// alpha_HeroSelect.cpp
// Team Neat
// Primary : Byoengjun Kim
// Secondary : Sunghwan CHo
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "alpha_HeroSelect.h"

#include "../Engine/Engine.h"
#include "HeroSelect.h"
#include "hand.h"
#include "cardPile.h"

void Alpha_HeroSelect::Load()
{
	timer = 0;
	pTimer = 0;
	
	w.Load();
	p.Load();
	m.Load();
	h.Load();
	s.Load();
	v.Load();

	heroSelect.AddCharacters(w_p);
	heroSelect.AddCharacters(p_p);
	heroSelect.AddCharacters(m_p);
	heroSelect.AddCharacters(h_p);
	heroSelect.AddCharacters(s_p);
	heroSelect.AddCharacters(v_p);

	heroSelect.Load();

	background.Add(PATH_HEROSELECTION_BACKGROUND_RAWIMAGE);

	// 덱, 핸드, 무덤을 클리어하고 기본 카드를 생성함.
	deck.Clear();
	graveyard.Clear();
	player_hand.Clear_hand();

	for (std::pair<int, int> id : CardPile::init_deck)
	{
		deck.Push_back(id);
	}
}

void Alpha_HeroSelect::Update([[maybe_unused]]double deltaTime)
{
	pTimer = timer;
	timer += deltaTime;

	if (pTimer <= 0.2 && 0.2 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 6 });
	}
	if (pTimer <= 0.4 && 0.4 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 11 });
	}
	player_hand.Update();
	
	heroSelect.Update();
}

void Alpha_HeroSelect::Draw()
{
	doodle::push_settings();
	doodle::apply_scale(Engine::GetWindow().GetSize().x / static_cast<double>(background[0].texturePtr->GetSize().x),
		Engine::GetWindow().GetSize().y / static_cast<double>(background[0].texturePtr->GetSize().y));
	background.Draw();
	doodle::pop_settings();

	heroSelect.Draw();

	player_hand.Draw();
}

void Alpha_HeroSelect::Unload()
{
	int selectingSize{ static_cast<int>(heroSelect.getSelectingVector().size()) };
	int selectableSize{ static_cast<int>(heroSelect.getSelectableVector().size()) };
	for (int i = 0; i < selectingSize; i++) 
	{
		if (heroSelect.getSelectingVector()[i].x + (heroSelect.getSelectingVector()[i].y * HeroSelect::rowsPicking) >= selectableSize) 
		{
			continue;
		}
		Battlefield::BattleObject characterTemp{ heroSelect.getSelectableVector()[heroSelect.getSelectingVector()[i].x + (heroSelect.getSelectingVector()[i].y * HeroSelect::rowsPicking)], {-1,-1} };
		switch (i)
		{
		case 0:
			battlefield.GetObject({ 0,0 }).ChangeObject(characterTemp);
			break;
		case 1:
			battlefield.GetObject({ 0,1 }).ChangeObject(characterTemp);
			break;
		case 2:
			battlefield.GetObject({ 0,2 }).ChangeObject(characterTemp);
			break;
		}
	}
	Engine::GetLogger().LogEvent("Completed to Picking heros!");
	
	player_hand.Clear_hand();
	graveyard.Clear();
	heroSelect.Unload();
}
