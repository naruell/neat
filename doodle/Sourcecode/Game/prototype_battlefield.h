﻿#pragma once
// GAM150
// prototype_battlefield.h
// Team Neat
// Primary : Byeonjun Kim. Junhyuk Cha
// Secondary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/Input.h"
#include "../Engine/Sprite.h"
#include "../Engine/text.h"
#include "../Engine/Background.h"

#include "battlefield.h"

class Prototype_battlefield : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() { return "Prototype_battlefield"; }

	bool doesStartDrawDone = false;
	const int startHandSize = 3;

	Sprite graveStoneSprite;

private:
	std::vector<Enemy*> enemyPtrLoadVector;
	std::vector<Vector2DInt> enemyLocationLoadVector;

	Sprite mouseSprite;
	Background background;
	Sprite background_darker;

	Input::InputKey kingGodGeneralMajestyDrawKey = Input::KeyboardButton::_1;

	TextBox textBox = TextBox(1, "You Lose\nGood bye", { 0, 0 }, { static_cast<double>(Engine::GetWindow().GetSize().x) / 2, static_cast<double>(Engine::GetWindow().GetSize().y) / 2 });
};