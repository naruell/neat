﻿#pragma once
// GAM150
// prototype_effectSystem.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/Input.h"
#include "../Engine/Particle/effectHeader.h"

class Prototype_effectSystem : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() { return "Prototype_effectSystem"; }

private:
	Input::InputKey removeEffectKey = Input::KeyboardButton::Backspace;
	Input::InputKey testEffectGenerateKey = Input::KeyboardButton::_1;
	Input::InputKey image1EffectGenerateKey = Input::KeyboardButton::_2;
	Input::InputKey nextLevelKey = Input::KeyboardButton::Enter;
};