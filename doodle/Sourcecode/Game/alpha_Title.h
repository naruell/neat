﻿#pragma once
// GAM150
// alpha_Title.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/helper.h"
#include "../Engine/Background.h"
#include "hand.h"

class Alpha_Title : public GameState
{

public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() override { return "Alpha_Title"; }

private:
	std::unique_ptr<TextBox> titleBox;

	std::vector<CustomButton*> buttonVector;
	Background 	background;
};