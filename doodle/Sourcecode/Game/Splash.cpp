﻿// GAM150
// Splash.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Splash.h"
#include "../Engine/Engine.h"

Splash::Splash() {}

void Splash::Load()
{
	pTimer = 0;
	timer = 0;

#ifdef _DEBUG
	Engine::GetGameStateManager().SetNextState(GAMESTATE::TITLE);

#else
	Engine::GetEffectSystem().GenerateImage1Effect({ -512, 123 }, PATH_SPLASHLOGO, 3);

#endif // DEBUG
}

void Splash::Update(double deltaTime)
{
	timer += deltaTime;
	if (timer >= SplashDuration)
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::TITLE);
	}
}

void Splash::Draw()
{
	Engine::GetWindow().Clear({ 50,50,50,255 });
	Engine::GetEffectSystem().Run();
	/*
	Vector2D logoPosition{0, 0};
	splashSprtie.Draw(logoPosition);
	*/
}

void Splash::Unload()
{
}