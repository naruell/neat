﻿// GAM150
// CardManager.cpp
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "CardManager.h"

#include <algorithm>
#include <string>

#include "../Engine/Engine.h"
#include "../Engine/helper.h"
#include "cardPile.h"
#include "card.h"

void CardManager::Load(int RemovableCount)
{
	deck.Sort();
	collectionPage = 0;
	erasableCount = RemovableCount;
	isManagingEnd = false;

	int deckSize{ deck.GetSize() };
	for (int i = 0; i < deckSize;i++) {
		cardList.emplace_back(Vector2Card(deck.GetCardID(i)));
		cardList[i]->card_state = Card_State::NORMAL;
		cardList[i]->geometry.angle = 0;
		cardList[i]->geometry.cur_posX = 0;
		cardList[i]->geometry.cur_posY = Engine::GetWindow().GetSize().y / 2 * i;
	}

	deck.Clear();
	Calculate_statistics_allWays();

	int cardListSize{ static_cast<int>(cardList.size()) };
	for (int i = 0, posHelper; i < cardListSize; i++) {
		posHelper = i % (cardsCountRow * cardsCountColumn);
		cardList[i]->geometry.posX = -Engine::GetWindow().GetSize().x / 2 + cardList[i]->geometry.width + ((cardList[i]->geometry.width * 1.2) * ((posHelper % cardsCountRow) + 0.5));
		cardList[i]->geometry.posY = Engine::GetWindow().GetSize().y / 2 - cardList[i]->geometry.height * 2 - (cardList[i]->geometry.height * 1.2 * ((posHelper / cardsCountRow) + (1 / 3)));
	}
}

void CardManager::Update()
{
	if (nextPage == PageChange::NEXT)
	{
		nextPage = PageChange::NONE;
		if (static_cast<float>(cardList.size()) / (cardsCountRow * cardsCountColumn) > (collectionPage + 1)) {
			collectionPage++;
			int cardListSize{ static_cast<int>(cardList.size()) };
			for (int i = (collectionPage * cardsCountRow * cardsCountColumn); i < std::min(cardListSize, ((collectionPage + 1) * cardsCountRow * cardsCountColumn)); i++) {
				cardList[i]->geometry.cur_posY = doodle::random(Engine::GetWindow().GetSize().y, Engine::GetWindow().GetSize().y / 2);
			}
		}
	}
	else if (nextPage == PageChange::BACK)
	{
		nextPage = PageChange::NONE;
		if (collectionPage > 0) {
			collectionPage--;
			int cardListSize{ static_cast<int>(cardList.size()) };
			for (int i = (collectionPage * cardsCountRow * cardsCountColumn); i < std::min(cardListSize, ((collectionPage + 1) * cardsCountRow * cardsCountColumn)); i++) {
				cardList[i]->geometry.cur_posY = doodle::random(Engine::GetWindow().GetSize().y, Engine::GetWindow().GetSize().y / 2);
			}
		}
	}

	int cardListSize{ static_cast<int>(cardList.size()) };
	for (int i = (collectionPage * cardsCountRow * cardsCountColumn); i < std::min(cardListSize, ((collectionPage + 1) * cardsCountRow * cardsCountColumn)); i++) {
		cardList[i]->SetExplanation();
		cardList[i]->Update();

		if (isRectCollided(Vector2D{ Engine::GetInput().GetMousePos() },
			{ cardList[i]->geometry.posX - cardList[i]->geometry.width / 2.0,cardList[i]->geometry.posY - cardList[i]->geometry.height / 2.0 },
			{ cardList[i]->geometry.posX + cardList[i]->geometry.width / 2.0,cardList[i]->geometry.posY + cardList[i]->geometry.height / 2.0 }))
		{
			cardList[i]->card_state = Card_State::HOVERED;
			cardList[i]->pCard_state = Card_State::HOVERED;

			if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Right)) {
				if (erasableCount != 0) {
					erasableCount--;
					cardList.erase(cardList.begin() + i);
					cardList.resize(cardList.size());
					cardListSize--;
					Calculate_statistics_allWays();
					for (int k = i, posHelper; k < cardListSize; k++) {
						posHelper = k % (cardsCountRow * cardsCountColumn);
						cardList[k]->geometry.posX = -Engine::GetWindow().GetSize().x / 2 + cardList[k]->geometry.width + ((cardList[k]->geometry.width * 1.2) * ((posHelper % cardsCountRow) + 0.5));
						cardList[k]->geometry.posY = Engine::GetWindow().GetSize().y / 2 - cardList[k]->geometry.height * 2 - (cardList[k]->geometry.height * 1.2 * ((posHelper / cardsCountRow) + (1 / 3)));
					}
				}
				else {
					Engine::GetLogger().LogError("You do not have enough erasable chance to remove a card!");
				}
			}
		}
	}
}

void CardManager::Draw()
{
	int cardListSize{ static_cast<int>(cardList.size()) };
	for (int i = (collectionPage * cardsCountRow * cardsCountColumn); i < std::min(cardListSize, ((collectionPage + 1) * cardsCountRow * cardsCountColumn)); i++) {
		if (cardList[i]->card_state == Card_State::HOVERED)
		{
			continue;
		}
		cardList[i]->Draw();
	}
	for (int i = (collectionPage * cardsCountRow * cardsCountColumn); i < std::min(cardListSize, ((collectionPage + 1) * cardsCountRow * cardsCountColumn)); i++) {
		if (cardList[i]->card_state == Card_State::HOVERED)
		{
			cardList[i]->Draw();
		}
	}
	
	// 통계 적기
	doodle::push_settings();
	doodle::set_font_size(24);
	doodle::set_fill_color(doodle::Color{ 0x000000FF });
	doodle::set_outline_color(doodle::Color{ 0xFFFFFFFF });
	int TextXPos{ 220 };
	int TextYPos{ 300 };
	// Character Type Arrange
	for (int i = 0; i < 8; i++) 
	{
		if (statistics[{0, i}] > 0)
		{
			doodle::draw_text(Int2CharacterTypeString(i) + " cards: " + std::to_string(statistics[{0, i}]), TextXPos, TextYPos);
			TextYPos -= 30;
		}
	}
	TextYPos -= 30;

	// Card Type arrange
	for (int i = 0; i < 4; i++)
	{
		if (statistics[{1, i}] > 0)
		{
			doodle::draw_text(Int2CardTypeString(i) + " cards: " + std::to_string(statistics[{1, i}]), TextXPos, TextYPos);
			TextYPos -= 30;
		}
	}
	TextYPos -= 30;

	// Cost arrange
	for (int i = 0; i < 4; i++)
	{
		if (statistics[{2, i}] > 0)
		{
			doodle::draw_text(std::to_string(i) + " Hour cards: " + std::to_string(statistics[{2, i}]), TextXPos, TextYPos);
			TextYPos -= 30;
		}
	}
	/*
	doodle::draw_text("NEUTRAL: " + std::to_string(statistics[{static_cast<int>(SORTWAY::CHARACTERTYPE), static_cast<int>(CHARACTERTYPE::NEUTRAL)}]), 250, 300);
	doodle::draw_text("WARRIOR: " + std::to_string(statistics[{static_cast<int>(SORTWAY::CHARACTERTYPE), static_cast<int>(CHARACTERTYPE::WARRIOR)}]), 250, 250);
	doodle::draw_text("MAGE: " + std::to_string(statistics[{static_cast<int>(SORTWAY::CHARACTERTYPE), static_cast<int>(CHARACTERTYPE::MAGE)}]), 250, 200);
	doodle::draw_text("COST 0: " + std::to_string(statistics[{static_cast<int>(SORTWAY::SPENDINGTIME), 0}]), 250, 100);
	doodle::draw_text("COST 1: " + std::to_string(statistics[{static_cast<int>(SORTWAY::SPENDINGTIME), 1}]), 250, 50);
	doodle::draw_text("COST 2: " + std::to_string(statistics[{static_cast<int>(SORTWAY::SPENDINGTIME), 2}]), 250, 0);
	doodle::draw_text("MOVE: " + std::to_string(statistics[{static_cast<int>(SORTWAY::CARDTYPE), static_cast<int>(Card_Subtype::MOVE)}]), 250, -100);
	doodle::draw_text("PHYSICAL: " + std::to_string(statistics[{static_cast<int>(SORTWAY::CARDTYPE), static_cast<int>(Card_Subtype::PHYSICAL)}]), 250, -150);
	doodle::draw_text("MAGICAL: " + std::to_string(statistics[{static_cast<int>(SORTWAY::CARDTYPE), static_cast<int>(Card_Subtype::MAGICAL)}]), 250, -200);
	*/
	doodle::pop_settings();
}

void CardManager::Unload()
{
	isManagingEnd = true;
	int cardListSize{ static_cast<int>(cardList.size()) };
	for (int i = 0; i < cardListSize; i++) {
		deck.Push_back(cardList[0].get()->GetCardID());
		cardList.erase(cardList.begin());
	}
	cardList.clear();
	statistics.clear();
}

void CardManager::Calculate_statistics_allWays()
{
	statistics.clear();
	int cardListSize{ static_cast<int>(cardList.size()) };
	for (int i = 0; i < cardListSize; i++) {
		Calculate_statistics(SORTWAY::CHARACTERTYPE, cardList[i].get()->GetCardID().first);
		Calculate_statistics(SORTWAY::CARDTYPE, static_cast<int>(cardList[i].get()->current_subtype[0]));
		Calculate_statistics(SORTWAY::SPENDINGTIME, cardList[i].get()->current_spendingTime);
	}
}

void CardManager::Calculate_statistics(SORTWAY sortWay, int value)
{
	auto location = std::find_if(begin(statistics), end(statistics), [=](auto ele) { return (ele.first.first == static_cast<int>(sortWay) && ele.first.second == value); });
	if (location != statistics.end()) {
		location->second++;
	}
	else {
		// std::cout << "없어 십덕아";
		statistics.insert({ {static_cast<int>(sortWay), value}, 1 });
	}
}

int CardManager::GetErasableCount()
{
	return erasableCount;
}

void CardManager::SetCardManagingIsFinished(bool isFinished)
{
	isManagingEnd = isFinished;
}

bool CardManager::IsManagingEnd()
{
	return isManagingEnd;
}

void CardManager::SetNextPage(int nextPag)
{
	if (nextPag > 1 || nextPag < -1) { return; }
	nextPage = static_cast<PageChange>(nextPag);
}

CardManager cardManager;