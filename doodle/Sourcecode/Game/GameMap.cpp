﻿// GAM150
// Team Neat
// GameMap.cpp
// Primary : Sunghwan Cho
// 5/27/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "GameMap.h"

#include <fstream>
#include <string>
#include <stdexcept>
#include <doodle/drawing.hpp>

#include "../Engine/Geometry/Graph.h"
#include "../Engine/Engine.h"
#include "../Environment/constants.h"

void GameMap::Load() {}

void GameMap::Load(std::string mapInputFile)
{
    if (mapInputFile.substr(mapInputFile.find_last_of('.')) != ".neat")
    {
        throw std::runtime_error("Bad Filetype.  " + mapInputFile + " not a neat info file (.neat)");
    }
    std::ifstream inFile(mapInputFile);

    if (inFile.is_open() == false)
    {
        throw std::runtime_error("Failed to load " + mapInputFile);
    }

    if (mapInputFile == PATH_FIRST_MAP)
    {
        mapIndex = 1;
        background = std::make_unique<doodle::Image>(doodle::Image{PATH_FIRST_MAP_BACKGROUND});
    }
	else if(mapInputFile == PATH_SECOND_MAP)
	{
        mapIndex = 2;
        background = std::make_unique<doodle::Image>(doodle::Image{ PATH_SECOND_MAP_BACKGROUND });
	}
	
    std::string text;
    int indexForMap = 0;
    int indexForRoomPos = 0;
    while (inFile >> text)
    {
    	if(indexForMap < MAXIMUM)
    	{
            for (int j = 0; j < MAXIMUM; ++j)
            {
                if (text[j] == '0')
                {
                    gameMap.RemoveEdge(indexForMap, j);
                }
                else if (text[j] == '1')
                {
                    gameMap.AddEdge(indexForMap, j);
                }
                else
                {
                    gameMap.RemoveEdge(indexForMap, j);
                }
            }
            ++indexForMap;
    	}
        else if(indexForRoomPos < MAXIMUM)
        {
            double xPos, yPos;
            int battleRoomType;
        	inFile >> xPos;
            inFile >> yPos;
            inFile >> battleRoomType;
        	
            if (text == "Battleroom")
            {
                gameRoomList.emplace_back(new BattleRoom(Vector2D{xPos,yPos}, battleRoomType));
            	if(indexForRoomPos == 0)
            	{
					gameRoomList[indexForRoomPos]->SetIsAbleToHover(true);
            	}
            }
            else if(text == "Eventroom")
            {
                gameRoomList.emplace_back(new EventRoom(Vector2D{ xPos,yPos }));
                if (indexForRoomPos == 0)
                {
                    gameRoomList[indexForRoomPos]->SetIsAbleToHover(true);
                }
            }
            ++indexForRoomPos;
        }
        
    }
	for(auto& room : gameRoomList)
	{
        room->Load();
	}
}

void GameMap::Update([[maybe_unused]] double deltaTime)
{
    for (int i = 0; i < static_cast<int>(gameRoomList.size()); ++i)
    {
        gameRoomList[i]->Update();
        if (gameRoomList[i]->GetIsOnClicked() == true && gameRoomList[i]->GetIsCleared() == true)
        {
            for (auto& room : gameRoomList)
            {
                room->SetIsAbleToHover(false);
                room->SetIsOnHovered(false);
                room->SetIsOnClicked(false);
            }
            std::set<int> neighbors = gameMap.neighbors(i);
            for (int iter : neighbors)
            {
                gameRoomList[iter]->SetIsAbleToHover(true);
            }

            gameRoomList[i]->SetHasCleared(true);
            gameRoomList[i]->SetIsCleared(false);
        }
    	if(gameRoomList[i]->GetIsOnClicked() == true && gameRoomList[i]->GetIsCleared() == false)
    	{
    		gameRoomList[i]->ToBeExecuteOnClicked();
    	}
    }
}

void GameMap::Draw()
{
    doodle::draw_image(*background, 0, 0, Engine::GetWindow().GetSize().x, Engine::GetWindow().GetSize().y);
    for (int i = 0; i < static_cast<int>(gameRoomList.size()); ++i)
    {
        std::set<int> neighbors = gameMap.neighbors(i);
        for (int iter : neighbors)
        {
            if (gameRoomList[iter]->GetIsAbleToHover() == true && gameRoomList[i]->GetHasCleared() == true)
            {
                doodle::push_settings();
                doodle::set_outline_color(87, 65, 170, 255);
                doodle::set_outline_width(5);
                doodle::draw_line(gameRoomList[i]->GetPosition().x, gameRoomList[i]->GetPosition().y, gameRoomList[iter]->GetPosition().x, gameRoomList[iter]->GetPosition().y);
                doodle::pop_settings();
            }
            else if(gameRoomList[iter]->GetHasCleared() == true && gameRoomList[i]->GetHasCleared() == true)
            {
                doodle::push_settings();
                doodle::set_outline_color(0, 0, 0, 255);
                doodle::set_outline_width(5);
                doodle::draw_line(gameRoomList[i]->GetPosition().x, gameRoomList[i]->GetPosition().y, gameRoomList[iter]->GetPosition().x, gameRoomList[iter]->GetPosition().y);
                doodle::pop_settings();
            }
            else
            {
                doodle::push_settings();
                doodle::set_outline_color(255, 255, 255, 255);
                doodle::set_outline_width(3);
                doodle::draw_line(gameRoomList[i]->GetPosition().x, gameRoomList[i]->GetPosition().y, gameRoomList[iter]->GetPosition().x, gameRoomList[iter]->GetPosition().y);
                doodle::pop_settings();
            }
        }
    }
	
	for(unsigned int i = 0; i < 31; ++i)
	{
		if(i == 30)
		{
            gameRoomList[i]->Draw(90);
		}
        else
        {
            gameRoomList[i]->Draw();
        }
	}
}

void GameMap::Unload()
{
	for(unsigned i = 0; i < MAXIMUM; ++i)
	{
		for(unsigned j = 0; j < MAXIMUM; ++j)
		{
            gameMap.RemoveEdge(i, i);
		}
	}
    gameRoomList.clear();
    background = nullptr;
}

int GameMap::GetNeighborRoomNumbers(int index)
{
    return static_cast<int>(gameMap.neighbors(index).size());
}

int GameMap::GetCurrentRoomNum()
{
    int currentRoomNum = 0;
    for (int i = 0; i < static_cast<int>(gameRoomList.size()); ++i)
    {
        if (gameRoomList[i]->GetIsOnClicked() == true && gameRoomList[i]->GetIsCleared() == false)
        {
            currentRoomNum = i;
        }
    }
    return currentRoomNum;
}

int GameMap::GetCurrentRoomID()
{
    int currentRoomID = 0;
    for (int i = 0; i < static_cast<int>(gameRoomList.size()); ++i)
    {
        if (gameRoomList[i]->GetIsOnClicked() == true && gameRoomList[i]->GetIsCleared() == false)
        {
            currentRoomID = static_cast<int>(gameRoomList[i]->GetBattleRoomType());
        }
    }
    return currentRoomID;
}

bool GameMap::GetIsMapCleared()
{
    return isMapCleared;
}

void GameMap::SetIsMapCleared(bool set)
{
    isMapCleared = set;
}

void GameMap::SetIsRoomCleared(int roomNum, bool set)
{
    gameRoomList[roomNum]->SetIsCleared(set);
}

int GameMap::GetIndex() const
{
    return mapIndex;
}
