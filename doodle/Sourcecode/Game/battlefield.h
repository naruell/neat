﻿#pragma once
// GAM150
// battlefield.h
// Team Neat
// Primary : Byeongjun Kim, Junhyuk Cha
// Secondary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/Engine.h"
#include "../Game/Enemy/enemy.h"
#include "../Game/Character/Character.h"
#include "../Engine/Geometry/Vector2D.h"
#include "Reward.h"

enum class BattleAllyType { NONE, EMPTY, CHARACTER, ENEMY, COUNT }; // 옵젝 종류

class Battlefield {
public:
	Battlefield() = default;
	// 전장 필드 기본값
	static const int BATTLEFIELDROW = 4; // 전장 가로
	static const int BATTLEFIELDCOLUMN = 3; // 전장 세로

	Vector2DInt releasedBattleObjectIndex = {-1, -1};
	Vector2DInt hoveredBattleObjectIndex = {-1, -1};

	Vector2DInt currentMouseIndex;
	Vector2DInt mousePressedIndex;

	enum class SelectMode { Single, Line };
	SelectMode selectMode{ SelectMode::Single };

	struct BattleObject
	{
	private:
		BattleAllyType type{ BattleAllyType::EMPTY };
		Character* characterdata{ nullptr };
		Enemy* enemydata{ nullptr };
		Vector2DInt index{ -1, -1 };
		bool isSelectAble = false;
	public:
		BattleObject(Character* Char, Vector2DInt Pos) : type(BattleAllyType::CHARACTER), characterdata(Char), index(Pos) {}
		BattleObject(Enemy* Ene, Vector2DInt Pos) : type(BattleAllyType::ENEMY), enemydata(Ene), index(Pos) {}
		BattleObject(Vector2DInt Pos) : type(BattleAllyType::EMPTY), index(Pos) {}
		BattleObject() : type(BattleAllyType::NONE), index({ -1, -1 }) {}

		BattleAllyType GetAllyType() const;
		void SetAllyType(BattleAllyType allyType);
		Character* GetCharacterData();
		Enemy* GetEnemyData();
		Vector2DInt GetPosition();
		void SetPosition(Vector2DInt nextIndex);
		void ChangeObject(BattleObject newObject);

		void Update();
		void DrawField();
		void DrawCharacter();
		void Draw();

		// 오브젝트 삭제
		void DeleteObject();

		void SetIsSelectAble(bool selectAble);
		bool GetIsSelectAble();

		void DealtAdDamage(int value, int offset);
		void DealtApDamage(int value, int offset);
		void DealtTrueDamage(int value, int offset);
		void GetHealed(int value, int offset);
	};

	void Load();
	void Update(double pTimer, double timer);
	void Draw();
	void UpdateCurrentMouseLocation();
	void DrawEnemyPatternRange();

	// 전장 전체 벡터
	std::vector<std::vector<BattleObject>> battleFieldVector = {};

	// 죽은 캐릭터 벡터
	std::vector<BattleObject> deadCharacterVector = {};

	// 죽은 적 벡터
	std::vector<BattleObject> deadEnemyVector = {};

	// 전장 벡터 이니셜라이징 #help function
	void initializeBattleFieldVector();

	// 포지션 넣어서 서로 스왑
	void SwapObject(Vector2DInt Pos1, Vector2DInt Pos2);

	// 포지션 넣어서 원래 뭔지 뽑는 함수 #help function
	BattleObject& GetObject(Vector2DInt Pos);

	// 포지션 넣어서 원래 뭔지 뽑는 함수 (reference 말고) #help function
	BattleObject GetObjectData(Vector2DInt Pos);

	// 적 전장 벡터 뽑음 #help function
	std::vector<BattleObject> GetEnemyBattleField();

	// 아군 전장 벡터 뽑음 #help function
	std::vector<BattleObject> GetAllyBattleField();

	// 빈 칸 포함 랜덤 위치 뽑음. Noexcept 오타 아님. Noexcept = 빈칸 포함
	Vector2DInt GetRandomPosNoexcept();

	// 빈 칸 포함 적 위치 중에 하나 뽑음
	Vector2DInt GetRandomEnemyPosNoexcept();

	// 빈 칸 포함 아군 위치 중에 하나 뽑음
	Vector2DInt GetRandomAllyPosNoexcept();

	// 랜덤 대상 지정, 아군/적 포함
	Vector2DInt GetRandomTarget();

	// 랜덤 아군 대상 지정
	Vector2DInt GetRandomAllyTarget();

	// 랜덤 적 대상 지정
	Vector2DInt GetRandomEnemyTarget();

	// 랜덤 빈 칸 지정
	Vector2DInt GetRandomEmptyTarget();

	Vector2DInt getSelection();

	void DrawSelection(Vector2DInt index);

	void DrawNextTurnButton();

	void CheckIfMouseIsClickedTurnButton();

	bool GetIsPlayerTurn();

	//겜끝났는지 (임시)
	bool isGameEnded = false;
	bool isHeroAllDead = false;

	bool isPlayerDrawCard = false;
private:
	Vector2DInt selection{ 0,0 };
	Vector2DInt pressedPos{ 0,0 };
	Input::InputKey moveUpKey = Input::KeyboardButton::Up;
	Input::InputKey moveDownKey = Input::KeyboardButton::Down;
	Input::InputKey moveLeftKey = Input::KeyboardButton::Left;
	Input::InputKey moveRightKey = Input::KeyboardButton::Right;

	bool isHeroTurn = true;
	bool isEnemySelectTarget = true;
	bool isLimitedTimeUpdated = true;
	bool isTurnButtonPressed = false;
	bool isHeroGetActPower = true;
	bool isFirstTurn = true;
};

extern Battlefield battlefield;

// 타겟 힐
void HealTarget(Vector2DInt caster, std::vector<Vector2DInt> target, int value);

// Ad 공격
void AdAttackTarget(Vector2DInt caster, std::vector<Vector2DInt> target, int value);

// Ap 공격
void ApAttackTarget(Vector2DInt caster, std::vector<Vector2DInt> target, int value);


