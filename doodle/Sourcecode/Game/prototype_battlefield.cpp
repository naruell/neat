﻿// GAM150
// prototype_battlefield.cpp
// Team Neat
// Primary : Byeongjun Kim, Junhyuk Cha
// Secondary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "prototype_battlefield.h"

#include <iostream>
#include <doodle/window.hpp>
#include <doodle/input.hpp>

#include "../Engine/Engine.h"

#include "cardPile.h"
#include "GameClock.h"
#include "GameMap.h"
#include "hand.h"

#include "Enemy/Slime.h"
#include "Enemy/PoisonedSlime.h"
#include "Enemy/Lich.h"
#include "Enemy/PurpleKwaang.h"
#include "Enemy/RedKwaang.h"
#include "Enemy/Boma.h"
#include "Enemy/BomberGorilla.h"

void Prototype_battlefield::Load()
{
	timer = 0;
	pTimer = 0;

	player_hand.MoveCardActivateZone({ 0, -365 });
	doodle::show_cursor(false);
	mouseSprite.Load(space_roster::PATH_MOUSE_NORMAL);
	mouseSprite.SetHotSpot({ -15,25 });
	background.Add(PATH_BATTLEFIELD_BACKGROUND_RAWIMAGE);
	background_darker.Load(PATH_BATTLEFIELD_BLACK_IMAGE);

	int stageNum = Engine::GetGameMap().GetIndex();
	int roomNum = Engine::GetGameMap().GetCurrentRoomID();
	switch (Engine::GetGameMap().GetCurrentRoomID())
	{
	case 1:
		enemyPtrLoadVector.emplace_back(new Slime{ "slime", 10 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 2, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new Slime{ "slime", 10 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 2, ENEMYTYPE::SLIME });

		enemyLocationLoadVector.push_back({ 2,0 });
		enemyLocationLoadVector.push_back({ 2,2 });
		break;
	case 2:
		enemyPtrLoadVector.emplace_back(new Slime{ "slime", 10 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new Slime{ "slime", 10 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new PoisonedSlime{ "slime", 14 + roomNum * stageNum / 2, 6 + roomNum * stageNum / 2, 2 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });

		enemyLocationLoadVector.push_back({ 2,0 });
		enemyLocationLoadVector.push_back({ 2,1 });
		enemyLocationLoadVector.push_back({ 3,2 });
		break;
	case 3:
		enemyPtrLoadVector.emplace_back(new Slime{ "slime", 10 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new PoisonedSlime{ "slime", 14 + roomNum * stageNum / 2, 6 + roomNum * stageNum / 2, 2 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new Lich{ "lich", 12 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::LICH });

		enemyLocationLoadVector.push_back({ 2,1 });
		enemyLocationLoadVector.push_back({ 3,1 });
		enemyLocationLoadVector.push_back({ 3,2 });
		break;
	case 4:
		enemyPtrLoadVector.emplace_back(new Slime{ "slime", 10 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new RedKwaang{ "kwaang", 16 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 1 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::KWAANG });
		enemyPtrLoadVector.emplace_back(new Lich{ "lich", 12 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::LICH });

		enemyLocationLoadVector.push_back({ 2,0 });
		enemyLocationLoadVector.push_back({ 2,2 });
		enemyLocationLoadVector.push_back({ 3,1 });
		break;
	case 5:
		enemyPtrLoadVector.emplace_back(new PurpleKwaang{ "kwaang", 16 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 1 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::KWAANG });
		enemyPtrLoadVector.emplace_back(new PoisonedSlime{ "slime", 14 + roomNum * stageNum / 2, 6 + roomNum * stageNum / 2, 2 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });
		enemyPtrLoadVector.emplace_back(new Boma{ "boma", 10 + roomNum * stageNum / 2, 5 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::BOMA });

		enemyLocationLoadVector.push_back({ 2,0 });
		enemyLocationLoadVector.push_back({ 2,1 });
		enemyLocationLoadVector.push_back({ 3,0 });
		break;
	case 6:
		enemyPtrLoadVector.emplace_back(new Boma{ "boma", 10 + roomNum * stageNum / 2, 5 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::BOMA });
		enemyPtrLoadVector.emplace_back(new Lich{ "lich", 12 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::LICH });
		enemyPtrLoadVector.emplace_back(new PoisonedSlime{ "slime", 14 + roomNum * stageNum / 2, 6 + roomNum * stageNum / 2, 2 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::SLIME });

		enemyLocationLoadVector.push_back({ 3,0 });
		enemyLocationLoadVector.push_back({ 3,1 });
		enemyLocationLoadVector.push_back({ 3,2 });
		break;
	case 7:
		break;
	case 8:
		enemyPtrLoadVector.emplace_back(new RedKwaang{ "kwaang", 16 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 1 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::KWAANG });
		enemyPtrLoadVector.emplace_back(new Lich{ "lich", 12 + roomNum * stageNum / 2, 4 + roomNum * stageNum / 2, 0 + roomNum * stageNum / 3, 0 + roomNum * stageNum / 3, ENEMYTYPE::LICH });
		enemyPtrLoadVector.emplace_back(new BomberGorilla{ "bomberGorilla", 25 + roomNum * stageNum / 2, 5 + roomNum * stageNum / 2, 1 + roomNum * stageNum / 3, 1 + roomNum * stageNum / 3, ENEMYTYPE::BOMBERGORILLA });

		enemyLocationLoadVector.push_back({ 2,0 });
		enemyLocationLoadVector.push_back({ 3,1 });
		enemyLocationLoadVector.push_back({ 3,2 });
		break;
	}

	GameClock::Load();
	graveStoneSprite.Load(PATH_GRAVESTONE_IMAGE);
	battlefield.Load();
	battlefield.isGameEnded = false;
	battlefield.isHeroAllDead = false;

	for (int i = 0; i < enemyPtrLoadVector.size(); ++i)
	{
		enemyPtrLoadVector[i]->Load();
		battlefield.GetObject(enemyLocationLoadVector[i]).ChangeObject({ enemyPtrLoadVector[i] , { -1, -1 } });
	}

	enemyPtrLoadVector.clear();
	enemyLocationLoadVector.clear();

	for (auto& enemy : battlefield.GetEnemyBattleField())
	{
		if (enemy.GetEnemyData() != nullptr)
		{
			enemy.GetEnemyData()->GetAI().SelectNextPattern();
		}
	}

	// 덱을 섞고 드로우하기
	deck.Shuffle();

	doesStartDrawDone = false;
	battlefield.isGameEnded = false;
	battlefield.isPlayerDrawCard = false;
}

void Prototype_battlefield::Update(double deltaTime)
{
	if (battlefield.isGameEnded == false && battlefield.isHeroAllDead == false)
	{
		pTimer = timer;
		timer += deltaTime;

		if (doesStartDrawDone == false)
		{
			if (pTimer <= 0.2 && 0.2 < timer)
			{
				player_hand.Draw_from_deck(1);
			}

			if (pTimer <= 0.4 && 0.4 < timer)
			{
				player_hand.Draw_from_deck(1);
			}

			if (pTimer <= 0.6 && 0.6 < timer)
			{
				player_hand.Draw_from_deck(1);
				doesStartDrawDone = true;
			}
		}

		GameClock::Update();

		//set isSelectAble = false to all battleObject
		int battlefieldRowSize = 4;
		int battlefieldColumnSize = 3;
		for (int i = 0; i < battlefieldRowSize; i++)
		{
			for (int j = 0; j < battlefieldColumnSize; j++)
			{
				battlefield.battleFieldVector[i][j].SetIsSelectAble(false);
			}
		}

		battlefield.selectMode = Battlefield::SelectMode::Single;
		player_hand.Update();
		battlefield.Update(pTimer, timer);

		if (kingGodGeneralMajestyDrawKey.IsKeyReleased())
		{
			int x, y;
			std::cin >> x >> y;
			player_hand.Draw_card({ x, y });
		}
	}
	else if (battlefield.isHeroAllDead == true)
	{
		if (doodle::MouseIsPressed == true)
		{
			Engine::GetGameStateManager().Shutdown();
			return;
		}
	}
	else if (battlefield.isGameEnded == true && battlefield.isHeroAllDead == false)
	{
		const int stageNum = Engine::GetGameMap().GetIndex();
		const int roomNum = Engine::GetGameMap().GetCurrentRoomNum();
		if (stageNum == 2 && roomNum == 30)
		{
			Engine::GetGameStateManager().SetNextState(GAMESTATE::ENDING);
			return;
		}
		reward.Update();
		if (reward.IsRewardEnd() == true)
		{
			Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_MAPANDROOM);
			Engine::GetGameMap().SetIsRoomCleared(roomNum, true);
			if (Engine::GetGameMap().GetNeighborRoomNumbers(roomNum) == 0)
			{
				Engine::GetGameMap().SetIsMapCleared(true);
			}
		}
	}

}

void Prototype_battlefield::Draw()
{
	doodle::push_settings();
	doodle::apply_scale(Engine::GetWindow().GetSize().x / static_cast<double>(background[0].texturePtr->GetSize().x),
		Engine::GetWindow().GetSize().y / static_cast<double>(background[0].texturePtr->GetSize().y));
	background.Draw();
	doodle::pop_settings();

	doodle::push_settings();
	switch (GameClock::GetcurrentTimeOfDay())
	{
	case TimeOfDay::NIGHT:
		doodle::set_tint_color(0, 180);
		break;
	case TimeOfDay::DAY:
		doodle::set_tint_color(0, 30);
		break;
	case TimeOfDay::NIGHTANDDAY:
		doodle::set_tint_color(0, 110);
		break;
	}
	background_darker.Draw(TranslateMatrix{ 0,0 });
	doodle::pop_settings();

	battlefield.Draw();
	GameClock::Draw();
	deck.Draw(-2.14);
	graveyard.Draw(2.14);

	for (auto character : battlefield.deadCharacterVector)
	{
		double width_divide6 = Engine::GetWindow().GetSize().x / 6.0;
		double height_divide5 = Engine::GetWindow().GetSize().y / 5.0;
		double posX{ width_divide6 * (2.0 + static_cast<double>(character.GetPosition().x)) - width_divide6 / 4 + width_divide6 / 11 };
		double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (1.0 + static_cast<double>(character.GetPosition().y))) - height_divide5 / 4 * 3 + height_divide5 / 8 };

		graveStoneSprite.Draw(TranslateMatrix{ posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2 });
	}
	for (auto enemy : battlefield.deadEnemyVector)
	{
		double width_divide6 = Engine::GetWindow().GetSize().x / 6.0;
		double height_divide5 = Engine::GetWindow().GetSize().y / 5.0;

		double posX{ width_divide6 * (2.0 + static_cast<double>(enemy.GetPosition().x)) - width_divide6 / 4 + width_divide6 / 11 };
		double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (1.0 + static_cast<double>(enemy.GetPosition().y))) - height_divide5 / 4 * 3 + height_divide5 / 8 };

		graveStoneSprite.Draw(TranslateMatrix{ posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2 });
	}
	Engine::GetEffectSystem().Run();
	player_hand.Draw();

	if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
	{
		Engine::GetEffectSystem().GenerateClick1Effect();
	}
	//겜끝났는지 (임시)
	if (battlefield.isGameEnded)
	{
		doodle::push_settings();
		doodle::set_tint_color(0, 200);
		background_darker.Draw(TranslateMatrix{ 0,0 });
		doodle::pop_settings();
		reward.Draw();
	}
	//영웅 다 죽었는지(임시)

	int count = 0;
	for (auto hero : battlefield.GetAllyBattleField())
	{
		if (hero.GetCharacterData() != nullptr)
		{
			++count;
		}
	}
	if (count == 0)
	{
		battlefield.isHeroAllDead = true;
		doodle::push_settings();
		doodle::set_tint_color(0, 200);
		background_darker.Draw(TranslateMatrix{ 0,0 });
		textBox.SetTextSize(50);

		textBox.Draw();
		doodle::pop_settings();
	}


	// mouse drawing
	mouseSprite.Draw(TranslateMatrix{ static_cast<double>(doodle::get_mouse_x()), static_cast<double>(doodle::get_mouse_y()) });
}

void Prototype_battlefield::Unload()
{
	background.Unload();
	reward.Unload();
	for (auto enemy : battlefield.deadEnemyVector)
	{
		delete enemy.GetEnemyData();
	}
	battlefield.deadEnemyVector.clear();

	for (auto deadCharacter : battlefield.deadCharacterVector)
	{
		deadCharacter.SetPosition({ -3, -3 });
	}


	if (battlefield.isHeroAllDead == true)
	{
		battlefield.deadCharacterVector.clear();
	}
	GameClock::Unload();
	int battlefieldRowSize = 4;
	int battlefieldColumnSize = 3;
	for (int i = 0; i < battlefieldRowSize; i++)
	{
		for (int j = 0; j < battlefieldColumnSize; j++)
		{
			if (battlefield.battleFieldVector[i][j].GetCharacterData() != nullptr)
			{
				battlefield.battleFieldVector[i][j].GetCharacterData()->SetActableCount(1);
				battlefield.battleFieldVector[i][j].GetCharacterData()->Update(OBJECTSTATE::DEFAULT);
			}
		}
	}
	doodle::show_cursor(true);

	// 손과 무덤에 있는 카드들 전부 덱으로 옮기고 정렬하기.
	int graveyardSize = graveyard.GetSize();
	for (int i = 0; i < graveyardSize; ++i)
	{
		deck.Push_back(graveyard.MoveCard(0));
	}

	int handSize = player_hand.Get_real_handSize();
	if (handSize > 1)
	{
		for (int i = handSize - 1; i > 0; --i)
		{
			std::pair<int, int> id = player_hand.GetCard(i)->GetCardID();
			deck.Push_back(id);

			player_hand.Remove_card(i);
		}
	}
	deck.Sort();
}