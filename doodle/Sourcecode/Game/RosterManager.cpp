﻿// GAM150
// RosterManager.cpp
// Team Neat
// Primary : Byeongjun Kim, Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "RosterManager.h"

#include <doodle/input.hpp>
#include <doodle/window.hpp>

#include "../Engine/helper.h"

#include "cardPile.h"
#include "hand.h"

void Rostermanager::Load()
{
	timer = 0;
	pTimer = 0;
	
	background.Add(PATH_ROSTER_BACKGROUND_RAWIMAGE);
	mouseNormal.Load(space_roster::PATH_MOUSE_NORMAL);
	mouseNormal.SetHotSpot({ -15,25 });
	mouseHolding.Load(space_roster::PATH_MOUSE_HOLDING);
	mouseHolding.SetHotSpot({ -15,25 });
	roster.Load();
	doodle::show_cursor(false);
}

void Rostermanager::Update(double deltaTime)
{
	pTimer = timer;
	timer += deltaTime;

	if (pTimer <= 0.2 && 0.2 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 7 });
	}

	if (pTimer <= 0.4 && 0.4 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 8 });
	}

	player_hand.Update();
	roster.Update();

	
	if (nextLevelKey.IsKeyReleased())
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_BATTLEFIELD);
	}
	if (eventLevelKey.IsKeyReleased())
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::FINAL_EVENTROOM);
	}

}

void Rostermanager::Draw()
{
	doodle::push_settings();
	doodle::apply_scale(Engine::GetWindow().GetSize().x / static_cast<double>(background[0].texturePtr->GetSize().x),
		Engine::GetWindow().GetSize().y / static_cast<double>(background[0].texturePtr->GetSize().y));
	background.Draw();
	doodle::pop_settings();

	roster.Draw();
	player_hand.Draw();
	{
		if (roster.getIsHoldingCharacter()) {
			mouseHolding.Draw(TranslateMatrix{ static_cast<double>(doodle::get_mouse_x()), static_cast<double>(doodle::get_mouse_y()) });
		}
		else {
			mouseNormal.Draw(TranslateMatrix{ static_cast<double>(doodle::get_mouse_x()), static_cast<double>(doodle::get_mouse_y()) });
		}
	}

}

void Rostermanager::Unload()
{
	player_hand.Clear_hand();
	graveyard.Clear();
	doodle::show_cursor(true);
}
