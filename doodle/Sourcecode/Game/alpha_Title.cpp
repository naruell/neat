﻿// GAM150
// alpha_Title.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "alpha_Title.h"

#include "../Engine/helper.h"
#include "cardPile.h"

void Alpha_Title::Load()
{
	timer = 0;
	pTimer = 0;
	
	background.Add(PATH_MENU_BACKGROUND_RAWIMAGE);

	buttonVector.push_back(MakeButton({ 300,100 }, { 100,100 }, [&]() {buttonVector[0]->Load(Spt2Png(PATH_GRAVESTONE_IMAGE)); }));
	buttonVector.push_back(MakeButton({ 400,190 }, { 100,100 }, [&]() {buttonVector[1]->Load(Spt2Png(PATH_GRAVESTONE_IMAGE)); }));
	buttonVector.push_back(MakeButton({ 500,0 }, { 100,100 }, [&]() {buttonVector[2]->Load(Spt2Png(PATH_GRAVESTONE_IMAGE)); }));
	buttonVector.push_back(MakeButton({ -300,130 }, { 100,100 }, [&]() {buttonVector[3]->Load(Spt2Png(PATH_GRAVESTONE_IMAGE)); }));
	buttonVector.push_back(MakeButton({ -400,210 }, { 100,100 }, [&]() {buttonVector[4]->Load(Spt2Png(PATH_GRAVESTONE_IMAGE)); }));
	buttonVector.push_back(MakeButton({ -500,-50 }, { 100,100 }, [&]() {buttonVector[5]->Load(Spt2Png(PATH_GRAVESTONE_IMAGE)); }));
	int Vecsize{ static_cast<int>(buttonVector.size()) };
	for (int i = 0; i < Vecsize; i++)
	{
		if (i < Vecsize / 2) 
		{
			if (i % 2 == 1)
			{
				buttonVector[i]->Load(Spt2Png(PATH_SLIME_IMAGE));
			}
			else
			{
				buttonVector[i]->Load(Spt2Png(PATH_POISONEDSLIME_IMAGE));
			}
		}
		else 
		{
			if (i % 2 == 1)
			{
				buttonVector[i]->Load(Spt2Png(PATH_SLIME_HORI_IMAGE));
			}
			else
			{
				buttonVector[i]->Load(Spt2Png(PATH_SLIME2_HORI_IMAGE));
			}
		}
	}

	titleBox = std::make_unique<TextBox>(1, "Not Bomb", Vector2D{ 0, 200 }, Vector2D{ 400, 80 });
	titleBox->SetTextSize(50);

	Engine::GetSFML().ChangeCurrentBgm(E_BGM::NONBATTLEBGM);
	Engine::GetSFML().ChangeMusicVolume(20);
	Engine::GetSFML().PlayMusic(true);
}

void Alpha_Title::Update(double deltaTime)
{
	pTimer = timer;
	timer += deltaTime;
	
	if (pTimer <= 0.2 && 0.2 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 5 });
	}
	if (pTimer <= 0.4 && 0.4 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 1 });
	}
	if (pTimer <= 0.6 && 0.6 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 22 });
	}
	if (pTimer <= 0.8 && 0.8 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 2 });
	}
	if (pTimer <= 1.0 && 1.0 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 3 });
	}
	if (pTimer <= 1.2 && 1.2 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 5 });
	}

	player_hand.Update();

	int Vecsize{ static_cast<int>(buttonVector.size()) };
	for (int i = 0; i < Vecsize; i++) { buttonVector[i]->Update(); }
}

void Alpha_Title::Draw()
{
	//Engine::GetWindow().Clear({ 0, 0, 0, 255 });

	doodle::push_settings();
	doodle::apply_scale(Engine::GetWindow().GetSize().x / static_cast<double>(background[0].texturePtr->GetSize().x),
		Engine::GetWindow().GetSize().y / static_cast<double>(background[0].texturePtr->GetSize().y));
	background.Draw();
	doodle::pop_settings();

	Engine::GetEffectSystem().Run();
	player_hand.Draw();
	//titleBox->Draw();

	int Vecsize{ static_cast<int>(buttonVector.size()) };
	for (int i = 0; i < Vecsize; i++)
	{ 
		if (i >= Vecsize / 2) { buttonVector[i]->Draw(TranslateMatrix{ -1,1 }); } // 왜 안 되징 반대로 보게 할려는데
		else { buttonVector[i]->Draw(); }
	}
}

void Alpha_Title::Unload()
{
	player_hand.Clear_hand();
	graveyard.Clear();
	
	for (auto& effect : Engine::GetEffectSystem().effectList)
	{
		effect->setEndCondition(true);
	}

	int selectableSize{ static_cast<int>(buttonVector.size()) };
	for (int i = 0; i < selectableSize; i++) 
	{
		delete buttonVector[i];
	}
	buttonVector.clear();
}
