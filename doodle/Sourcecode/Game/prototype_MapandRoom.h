﻿#pragma once
// GAM150
// prototype_MapandRoom.h
// Team Neat
// Primary : Sunghwan Cho
// 5/24/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>

#include "../Engine/GameState.h"
#include "../Engine/Input.h"

class Prototype_MapandRoom : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;
	std::string GetName() override { return "Prototype_MapandRoom"; };

private:
	Input::InputKey nextLevelKey = Input::KeyboardButton::Enter;

	bool isLoaded = false;
	bool isFirstMapLoaded = false;
	bool isSecondMapLoaded = false;
};