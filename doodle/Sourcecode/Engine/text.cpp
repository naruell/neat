﻿// GAM150
// text.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "text.h"
#include "Engine.h"

TextBox::TextBox(int fontID, std::string text, Vector2D pos, Vector2D size) : TextBox(fontID, text, pos, size, { 15, TextAlignX::CENTER, TextAlignY::TOP, doodle::Color(255, 255),doodle::Color(255, 255) }, AutoNextLine::BY_WORD)
{
	AutoWrap();
	shouldBeAutoWrappped = false;
}

TextBox::TextBox(int fontID, std::string text, Vector2D pos, Vector2D size, TextInfo textInfo, AutoNextLine autoNextLine) : fullText(text), position(pos), size(size), textInfo(textInfo), autoNextLine(autoNextLine), fontInfo(Engine::GetTextManager().getFontInfo(fontID)), characterInfo(Engine::GetTextManager().getCharacterInfo(fontID))
{
	AutoWrap();
	shouldBeAutoWrappped = false;
}

void TextBox::AutoWrap()
{
	dividedText.clear();
	dividedTextWidth.clear();

	if (autoNextLine == AutoNextLine::BY_WORD)
	{
		std::vector<std::string> dividedWord;
		std::vector<double> dividedWordWidth;

		std::string wordBuffer = "";

		// Divide String to each words
		for (char c : fullText)
		{
			if (c == ' ')
			{
				if (wordBuffer != "")
				{
					dividedWord.push_back(wordBuffer);
				}
				wordBuffer = "";
			}
			if (c == '\n')
			{
				dividedWord.push_back(wordBuffer);
				dividedWord.push_back("\n");
				wordBuffer = "";
			}
			else
			{
				wordBuffer += c;
			}
		}
		// Last word
		if (wordBuffer != "")
		{
			dividedWord.push_back(wordBuffer);
		}

		// Calculate each word's width
		for (std::string str : dividedWord)
		{
			dividedWordWidth.push_back(Engine::GetTextManager().CalculateStringWidth(*this, str));
		}
		std::string whiteSpace = " ";
		const double whiteSpaceWidth = Engine::GetTextManager().CalculateStringWidth(*this, whiteSpace);

		std::string dividedLine = "";
		double dividedLineWidth = 0;

		int index = 0;
		for (std::string word : dividedWord)
		{
			if (word == "\n")
			{
				if (dividedLine == "" || dividedLine == " ")
				{
					dividedText.push_back("");
					dividedTextWidth.push_back(0);
					dividedLine = "";
					dividedLineWidth = 0;
				}

				else 
				{
					dividedText.push_back(dividedLine);
					dividedTextWidth.push_back(dividedLineWidth);
					dividedLine = "";
					dividedLineWidth = 0;
				}
			}

			else if (dividedLineWidth + dividedWordWidth[index] + whiteSpaceWidth > size.x)
			{
				if (dividedWordWidth[index] > size.x && dividedLine == "")
				{
					Engine::GetLogger().LogError("Width of the word \"" + word + "\" is larger than textBox size!!");
					return;
				}

				// 공백을 제외한 단어가 사이즈에 맞으면 단어만 그대로 넣음
				if (dividedLineWidth + dividedWordWidth[index] < size.x)
				{
					dividedText.push_back(dividedLine + word);
					dividedTextWidth.push_back(dividedLineWidth + dividedWordWidth[index]);
					dividedLine = "";
					dividedLineWidth = 0;
				}
				else
				{
					dividedText.push_back(dividedLine);
					dividedTextWidth.push_back(dividedLineWidth);
					dividedLine = word + " ";
					dividedLineWidth = dividedWordWidth[index];
					dividedLineWidth += whiteSpaceWidth;
				}
			}

			else
			{
				dividedLine += word;
				dividedLine += " ";

				dividedLineWidth += dividedWordWidth[index];
				dividedLineWidth += whiteSpaceWidth;
			}
			++index;
		}
		// Last line
		if (dividedLine != "")
		{
			dividedText.push_back(dividedLine);
			dividedTextWidth.push_back(dividedLineWidth);
		}
		dividedLine = "";
		dividedLineWidth = 0;
	}

	else if (autoNextLine == AutoNextLine::BY_CHARACTER)
	{
		std::string dividedLine = "";
		for (char c : fullText)
		{
			std::string tempStr = dividedLine + c;
			if (Engine::GetTextManager().CalculateStringWidth(*this, tempStr) > size.x)
			{
				dividedText.push_back(dividedLine);
				dividedTextWidth.push_back(size.x);
				dividedLine = c;
			}
			else
			{
				dividedLine += c;
			}
		}
		// Last line
		if (dividedLine != "")
		{
			dividedText.push_back(dividedLine);
			dividedTextWidth.push_back(Engine::GetTextManager().CalculateStringWidth(*this, dividedLine));
		}
		dividedLine = "";
	}
}

void TextBox::Draw() noexcept
{
	if (shouldBeAutoWrappped == true)
	{
		AutoWrap();
		shouldBeAutoWrappped = false;
	}

	while(static_cast<int>(dividedText.size()) * textInfo.doodleTextSize / fontInfo.size * fontInfo.height > size.y)
	{
		textInfo.doodleTextSize -= 1;
		AutoWrap();
	}

	doodle::push_settings();

	doodle::no_fill();
	doodle::no_outline();

#ifdef _DEBUG
	doodle::set_outline_color(doodle::Color(255, 255));
	doodle::draw_rectangle(position.x, position.y, size.x, size.y );
#endif
	
	doodle::set_font(fontInfo.id);
	doodle::set_font_size(textInfo.doodleTextSize);
	doodle::set_fill_color(textInfo.fillColor);
	doodle::set_outline_color(textInfo.outlineColor);

	double lineHight = 1.0 * textInfo.doodleTextSize / fontInfo.size * fontInfo.height;


	double yOffset = 0;
	switch (textInfo.alignY)
	{
	case TextAlignY::TOP:
		break;

	case TextAlignY::CENTER:
		yOffset = -size.y / 2 + lineHight * dividedText.size() / 2;
		break;
	}
	for (int i = 0; i < dividedText.size(); ++i)
	{
		double xOffset = 0;
		switch (textInfo.alignX)
		{
		case TextAlignX::LEFT:
			break;

		case TextAlignX::CENTER:
			xOffset = size.x / 2 - (dividedTextWidth[i] / 2);
			break;

		case TextAlignX::RIGHT:
			xOffset = size.x - dividedTextWidth[i];
			break;
		}

		doodle::draw_text(dividedText[i], position.x + xOffset - size.x / 2, position.y + yOffset - lineHight * (i + 1) + size.y / 2 );
	}
	doodle::pop_settings();
}

void TextBox::SetText(std::string textTo)
{
	fullText = textTo;
	shouldBeAutoWrappped = true;
}

const std::string& TextBox::GetText() const noexcept
{
	return fullText;
}

const std::string& TextBox::GetText(int lineIndex) const noexcept
{
	// 벡터 사이즈 체크
	if (dividedText.size() <= lineIndex || lineIndex < 0)
	{
		Engine::GetLogger().LogError("Text line : " + std::to_string(lineIndex) + " Out of Range!");
	}
	return dividedText[lineIndex];
}

const Vector2D& TextBox::GetSize() const noexcept
{
	return size;
}

void TextBox::SetSize(Vector2D sizeTo)
{
	size = sizeTo;
	shouldBeAutoWrappped = true;
}

const Vector2D& TextBox::GetPosition() const noexcept
{
	return position;
}

void TextBox::setPosition(Vector2D positionTo)
{
	position = positionTo;
}

const FontInfo& TextBox::GetFontInfo() const noexcept
{
	return fontInfo;
}

const std::map<int, CharacterInfo>& TextBox::GetCharacterInfo() const noexcept
{
	return characterInfo;
}

void TextBox::SetTextInfo(TextInfo textInfoTo)
{
	textInfo = textInfoTo;
	shouldBeAutoWrappped = true;
}

void TextBox::SetTextSize(double textSizeTo)
{
	textInfo.doodleTextSize = textSizeTo;
	shouldBeAutoWrappped = true;
}

void TextBox::SetTextAlignX(TextAlignX alignTo)
{
	textInfo.alignX = alignTo;
}

void TextBox::SetTextAlignY(TextAlignY alignTo)
{
	textInfo.alignY = alignTo;
}

void TextBox::SetTextFillColor(doodle::Color colorTo)
{
	textInfo.fillColor = colorTo;
}

const TextInfo& TextBox::GetTextInfo() const noexcept
{
	return textInfo;
}

void TextBox::SetAutoNextLine(AutoNextLine autoNextLineTo)
{
	if (autoNextLine != autoNextLineTo)
	{
		autoNextLine = autoNextLineTo;
		shouldBeAutoWrappped = true;
	}
}

const AutoNextLine& TextBox::GetAutoNextLine() const noexcept
{
	return autoNextLine;
}
