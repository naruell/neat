﻿#pragma once
// GAM150
// Graph.inl
// Team Neat
// Primary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <cassert>

namespace CDATASTRUCTURE
{
	template<typename Type>
	void CTECGraph<Type>::AddVertex(const Type& value)
	{
		assert(Size() < MAXIMUM && "Size is more than Maximum!!!");
		int newVertexNumber = manyVertices;
		manyVertices++;

		for (int otherVertexNumber = 0; otherVertexNumber < manyVertices; otherVertexNumber++)
		{
			adjacencyMatrix[otherVertexNumber][newVertexNumber] = false;
			adjacencyMatrix[newVertexNumber][otherVertexNumber] = false;
		}

		lables[newVertexNumber] = value;
	}

	template<typename Type>
	void CTECGraph<Type>::AddEdge(int source, int target)
	{
		assert(source < Size() && target < Size());
		adjacencyMatrix[source][target] = true;
	}

	template<typename Type>
	void CTECGraph<Type>::RemoveEdge(int source, int target)
	{
		assert(source < Size() && target < Size());
		adjacencyMatrix[source][target] = false;
	}

	template<typename Type>
	Type& CTECGraph<Type>::operator[](int vertex)
	{
		assert(vertex < Size());
		return lables[vertex];
	}

	template<typename Type>
	Type CTECGraph<Type>::operator[](int vertex) const
	{
		assert(vertex < Size());
		return lables[vertex];
	}

	template<typename Type>
	int CTECGraph<Type>::Size() const
	{
		return MAXIMUM;
	}

	template<typename Type>
	bool CTECGraph<Type>::IsEdge(int source, int target) const
	{
		assert(source < Size() && target < Size());
		bool isAnEdge = false;
		isAnEdge = adjacencyMatrix[source][target];
		return isAnEdge;
	}

	template<typename Type>
	std::set<int> CTECGraph<Type>::neighbors(int vertex) const
	{
		assert(vertex < Size());
		std::set<int> vertexNeighbors;
		for (int i = 0; i < Size(); ++i)
		{
			if (adjacencyMatrix[vertex][i])
			{
				vertexNeighbors.insert(i);
			}
		}
		return vertexNeighbors;
	}
}
