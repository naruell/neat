﻿// GAM150
// prototype_battlefield.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Texture.h"

#include <doodle/drawing.hpp>		// doodle::Image, draw_image

Texture::Texture() { }

Texture::Texture(const std::filesystem::path& filePath) : image(filePath)
{
	size = { image.GetWidth(), image.GetHeight() };
}

void Texture::Load(const std::filesystem::path& filePath)
{
	image = doodle::Image{ filePath };
	size = { image.GetWidth(), image.GetHeight() };
}

// Draw texture using TransformMatrix
void Texture::Draw(TransformMatrix displayMatrix, Vector2DInt texelPos, Vector2DInt frameSize)
{
	doodle::push_settings();
	doodle::apply_matrix(displayMatrix[0][0], displayMatrix[1][0], displayMatrix[0][1], displayMatrix[1][1], displayMatrix[0][2], displayMatrix[1][2]);
	doodle::draw_image(image, 0, 0, static_cast<double>(frameSize.x), static_cast<double>(frameSize.y), texelPos.x, texelPos.y);
	doodle::pop_settings();
}

Vector2DInt Texture::GetSize() { return size; }

// Get Pixel by Vector2D 
Color Texture::GetPixel(Vector2DInt pos)
{
	int index = pos.y * size.x + pos.x;
	return image[index].red << 24 | image[index].green << 16 | image[index].blue << 8 | image[index].alpha;
}