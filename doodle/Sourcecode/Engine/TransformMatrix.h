﻿#pragma once
// GAM150
// TransformMatrix.h
// Team Neat
// Primary : Kevin Wright
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Geometry/Vector2D.h"

class TransformMatrix
{
public:
	TransformMatrix();

	double* operator[](int index) { return matrix[index]; }
	const double* operator[](int index) const { return matrix[index]; }
	TransformMatrix operator * (TransformMatrix rhs) const;
	Vector2D operator * (Vector2D rhs) const;
	TransformMatrix& operator *= (TransformMatrix rhs);
	void Reset();
protected:
	double matrix[3][3];
};

class TranslateMatrix : public TransformMatrix
{
public:
	TranslateMatrix(Vector2DInt translate);
	TranslateMatrix(Vector2D translate);
	TranslateMatrix(double translate1, double translate2);
};

class RotateMatrix : public TransformMatrix
{
public:
	RotateMatrix(double radians);
};

class ScaleMatrix : public TransformMatrix
{
public:
	ScaleMatrix(Vector2D scale);
};