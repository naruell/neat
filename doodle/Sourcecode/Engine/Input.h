﻿#pragma once
// GAM150
// Input.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>

#include "Geometry/Vector2D.h"

class Input 
{
public:
	enum class KeyboardButton : int
	{
		None,
		Enter, Escape, Space, Left, Up, Right, Down, Backspace,
		A, B, C, D, E, F, G, H, I, J,
		K, L, M, N, O, P, Q, R, S, T,
		U, V, W, X, Y, Z,
		_0, _1, _2, _3, _4, _5, _6, _7, _8, _9,
		NumPad_0, NumPad_1, NumPad_2, NumPad_3, NumPad_4, NumPad_5, NumPad_6, NumPad_7, NumPad_8, NumPad_9,
		Count
	};

	enum class MouseButton
	{
		None,
		Left, Right, Middle,
		Count
	};

	class InputKey
	{
	public:
		InputKey(KeyboardButton button);
		bool IsKeyDown() const;
		bool IsKeyUp() const;
		bool IsKeyPressed() const;
		bool IsKeyReleased() const;

		void Reassign(KeyboardButton buttonTo);
	private:
		KeyboardButton button;
	};

	Input();
	bool IsKeyDown(KeyboardButton key) const;
	bool IsKeyUp(KeyboardButton key) const;
	bool IsKeyPressed(KeyboardButton key) const;
	bool IsKeyReleased(KeyboardButton key) const;

	bool IsMouseDown(MouseButton mouse) const;
	bool IsMouseUp(MouseButton mouse) const;
	bool IsMousePressed(MouseButton mouse) const;
	bool IsMouseReleased(MouseButton mouse) const;

	void SetKeyDown(KeyboardButton key, bool value);
	void SetMouseDown(MouseButton mouse, bool value);
	
	KeyboardButton GetPressedKey() const noexcept;
	Vector2DInt GetMousePos() const noexcept;
	Vector2DInt GetPMousePos() const noexcept;
	Vector2DInt GetMouseDelta() const noexcept;

	void Update();
private:
	std::vector<bool> keyDown;
	std::vector<bool> wasKeyDown;

	std::vector<bool> mousePressed;
	std::vector<bool> wasMousePressed;

	int pMouseX = 0;
	int mouseX = 0;

	int pMouseY = 0;
	int mouseY = 0;

	Vector2DInt pMousePos;
	Vector2DInt mousePos;
};