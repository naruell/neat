﻿#pragma once
// GAM150
// GameState.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.


#include <string>

class GameState 
{
public:
	virtual void Load() = 0;
	virtual void Update(double deltaTime) = 0;
	virtual void Draw() = 0;
	virtual void Unload() = 0;
	virtual std::string GetName() = 0;

	double pTimer = 0;
	double timer = 0;
private:
};

const enum class GAMESTATE
{
	SPLASH,
	TITLE,
	CREDIT,
	ALPHA_HEROSELECT,
	PROTOTYPE_ROSTER,
	PROTOTYPE_BATTLEFIELD,
	PROTOTYPE_EFFECTSYSTEM,
	PROTOTYPE_CARDDRAW,
	PROTOTYPE_MAPANDROOM,
	PROTOTYPE_TEXTBOX,
	ALPHA_CARDMANAGING,
	FINAL_EVENTROOM,
	FINAL_TUTORIAL,
	ENDING,
	COUNT
};