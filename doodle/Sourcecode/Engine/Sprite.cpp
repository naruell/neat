﻿// GAM150
// Sprite.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Sprite.h"
#include "../Engine/Engine.h"

Sprite::Sprite()
{
}

Sprite::~Sprite()
{
	for (Animation* anim : animations)
	{
		delete anim;
	}
	animations.clear();
}

// Load with file path
void Sprite::Load(std::string spriteInfoFile)
{
	animations.clear();
	hotspots.clear();

	if (spriteInfoFile.substr(spriteInfoFile.find_last_of('.')) != ".spt") 
	{
		throw std::runtime_error("Bad Filetype.  " + spriteInfoFile + " not a sprite info file (.spt)");
	}
	std::ifstream inFile(spriteInfoFile);

	if (inFile.is_open() == false) 
	{
		throw std::runtime_error("Failed to load " + spriteInfoFile);
	}

	std::string text;
	inFile >> text;
	texture.Load(text);
	frameSize = texture.GetSize();

	while (inFile.eof() == false) 
	{
		inFile >> text;
		if (text == "FrameSize") {
			inFile >> frameSize.x;
			inFile >> frameSize.y;
		}
		else if (text == "Frame") 
		{
			int frameLocationX, frameLocationY;
			inFile >> frameLocationX;
			inFile >> frameLocationY;
			frameTexel.push_back({ frameLocationX, frameLocationY });
		}
		else if (text == "HotSpot") 
		{
			int hotSpotX, hotSpotY;
			inFile >> hotSpotX;
			inFile >> hotSpotY;
			hotspots.push_back({ hotSpotX, hotSpotY });
		}
		else if (text == "Anim") 
		{
			inFile >> text;
			animations.push_back(new Animation(text));
		}
		else 
		{
			Engine::GetLogger().LogError("Unknown spt command " + text);
		}
	}
	if (frameTexel.empty() == true) 
	{
		frameTexel.push_back({ 0,0 });
	}
	if (animations.empty() == true) 
	{
		animations.push_back(new Animation());
		PlayAnimation(0);
	}
}

// Update
void Sprite::Update(double dt)
{
	animations[currAnim]->Update(dt);
}

// Drawing with Matrix
void Sprite::Draw(TransformMatrix displayMatrix)
{
	texture.Draw(displayMatrix * TranslateMatrix(-hotspots[0]), GetFrameTexel(animations[currAnim]->GetDisplayFrame()), GetFrameSize());
}

// sets the current animation to play
void Sprite::PlayAnimation(int anim)
{
	if (anim >= animations.size() || anim < 0) 
	{
		Engine::GetLogger().LogError("bad index!");
		return;
	}
	animations[anim]->Reset();
	currAnim = anim;
}

// checks the current animation to see if it is done playing
bool Sprite::IsAnimationDone()
{
	return animations[currAnim]->IsAnimationDone();
}

// Helper function, help to set the hotspots
void Sprite::SetHotSpot(Vector2DInt position, int index )
{
	int hotspotSize = static_cast<int>(hotspots.size());
	if (index < 0 || index >= hotspotSize) 
	{
		Engine::GetLogger().LogError("Invalid hotspot index. Failed to SetHotspot at {" + std::to_string(position.x) + ", " + std::to_string(position.y) + "}");
		return;
	}
	hotspots[index] = position;
}

// Helper function, help to add a hotspot
void Sprite::AddHotSpot(Vector2DInt position)
{
	hotspots.push_back(position);
}

// Helper function, help to get the hotspots
Vector2DInt Sprite::GetHotSpot(int index)
{
	int hotspotSize = static_cast<int>(hotspots.size());
	if (index < 0 || index >= hotspotSize) 
	{
		Engine::GetLogger().LogError("Invalid hotspot index. Returns [0] instead.");
		return hotspots[0];
	}
	return hotspots[index];
}

// return the frame size which is set in our .spt file
Vector2DInt Sprite::GetFrameSize() const
{
	return frameSize;
}

//  return the texel (Texture Pixel) start position of the passed in frame index
Vector2DInt Sprite::GetFrameTexel(int frameNum) const
{
	int frametexelSize = static_cast<int>(frameTexel.size());
	if (frameNum < 0 || frameNum >= frametexelSize) 
	{
		Engine::GetLogger().LogError("Invalid frameTexel index. Returns [0] instead.");
		return frameTexel[0];
	}
	return frameTexel[frameNum];
}
