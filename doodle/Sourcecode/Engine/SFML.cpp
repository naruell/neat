﻿// GAM150
// SFML.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "SFML.h"
#include "helper.h"
#include "../Game/battlefield.h"

void SFML::Load()
{
	cardSound1.setBuffer(cardSoundBuffer1);
	cardSound2.setBuffer(cardSoundBuffer2);
	enemyDeadSound1.setBuffer(enemyDeadSoundBuffer1);
	enemyDeadSound2.setBuffer(enemyDeadSoundBuffer2);
	enemyDeadSound3.setBuffer(enemyDeadSoundBuffer3);
}

void SFML::PlayMusic(bool isSetLoopTrue)
{
	music.play();
	music.setLoop(isSetLoopTrue);
}

void SFML::ChangeCurrentBgm(E_BGM bgm)
{
	if (currentBgm == bgm)
	{
		return;
	}

	switch (bgm)
	{
	case E_BGM::BATTLEBGM:
		music.openFromFile("Assets/Music/battle_bgm.wav");
		currentBgm = bgm;
		break;

	case E_BGM::NONBATTLEBGM:
		music.openFromFile("Assets/Music/non_battle_bgm.wav");
		currentBgm = bgm;
		break;
	}
}

void SFML::ChangeMusicVolume(float volume)
{
	music.setVolume(volume);
}

E_BGM SFML::GetCurrentBgm()
{
	return currentBgm;
}

void SFML::PlaySound(std::pair<int, int> cardID)
{
	if (cardIDToSound[cardID] == "")
	{
		return;
	}
	switch (currentCardSoundBufferNum)
	{
	case 1:
		cardSoundBuffer1.loadFromFile(cardIDToSound[cardID]);
		cardSound1.play();
		currentCardSoundBufferNum = 2;
		break;
	case 2:
		cardSoundBuffer2.loadFromFile(cardIDToSound[cardID]);
		cardSound2.play();
		currentCardSoundBufferNum = 1;
		break;
	}
}

void SFML::PlaySound(ENEMYTYPE enemyType)
{
	if (enemyTypeToSound[enemyType] == "")
	{
		return;
	}
	switch (currentEnemyDeadSoundBufferNum)
	{
	case 1:
		enemyDeadSoundBuffer1.loadFromFile(enemyTypeToSound[enemyType]);
		enemyDeadSound1.play();
		currentEnemyDeadSoundBufferNum = 2;
		break;
	case 2:
		enemyDeadSoundBuffer2.loadFromFile(enemyTypeToSound[enemyType]);
		enemyDeadSound2.play();
		currentEnemyDeadSoundBufferNum = 3;
		break;
	case 3:
		enemyDeadSoundBuffer3.loadFromFile(enemyTypeToSound[enemyType]);
		enemyDeadSound3.play();
		currentEnemyDeadSoundBufferNum = 1;
		break;
	}
}
