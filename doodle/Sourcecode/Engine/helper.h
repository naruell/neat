﻿#pragma once
// GAM150
// GameState.h
// Team Neat
// Primary : Duhwan Kim, Byeongjun Kim
// Secondary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.


#include <functional>
#include <map>
#include <doodle/drawing.hpp>

#include "Geometry/Vector2D.h"
#include "TransformMatrix.h"
#include "../Environment\constants.h"


double Map(double value, double low1, double high1, double low2, double high2);

// 스타트 포지션을 시작으로 우측상단으로 그림
template <class VecType1 = Vector2D, class VecType2 = VecType1>
void DrawGrid(VecType1 StartPos, int widthCount, int heightCount, VecType2 gridSize) {
	using namespace doodle;
	for (int i = 0; i < (widthCount + 1); i++)
	{
		draw_line(StartPos.x + (gridSize.x * i), StartPos.y, StartPos.x + (gridSize.x * i), StartPos.y + (gridSize.y * (heightCount)));
	}
	for (int i = 0; i < (heightCount + 1); i++)
	{
		draw_line(StartPos.x, StartPos.y + (gridSize.y * i), StartPos.x + (gridSize.x * (widthCount)), StartPos.y + (gridSize.y * i));
	}

	return;
}
//void DrawGrid(Vector2DInt StartPos, int widthCount, int heightCount, Vector2DInt gridSize);

class Texture;

class CustomButton {
public:
	CustomButton(Vector2DInt buttonPos, Vector2DInt buttonSize, std::function<void()> whatItWillDo);
	~CustomButton();
	virtual void Load(std::string filepath);
	virtual void Update();
	virtual void Draw(TransformMatrix transformMatrix = TranslateMatrix{ 1,1 });

	Texture* getTexturePtr();
	
private:
	bool pressing = false;
	Vector2DInt position;
	Vector2DInt size;
	std::function<void()> func;
	Texture* TexturePtr = nullptr;
};

CustomButton* MakeButton(Vector2DInt StartPos, Vector2D ButtonSize, std::function<void()> thing);

bool isRectCollided(Vector2D beChecked, Vector2D bottemLeft, Vector2D topRight);

std::string Spt2Png(std::string SpriteFilePath);


class Card;

Card* Vector2Card(std::pair<int, int> cardID);

std::string Int2CharacterTypeString(int num);

std::string Int2CardTypeString(int num);

doodle::Color GetHeroColor(CHARACTERTYPE type);

extern std::map<std::pair<int, int>, std::string> cardIDToSound;

extern std::unordered_map<ENEMYTYPE, std::string> enemyTypeToSound;