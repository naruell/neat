﻿#pragma once
// GAM150
// SFML.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <SFML/Audio.hpp>
#include <vector>
#include "../Environment/constants.h"

class SFML
{
public:
	void Load();
	void PlayMusic(bool isSetLoopTrue);
	void ChangeCurrentBgm(E_BGM bgm);
	void ChangeMusicVolume(float volume);
	E_BGM GetCurrentBgm();
	void PlaySound(std::pair<int, int> cardID);
	void PlaySound(ENEMYTYPE enemyType);

private:
	E_BGM currentBgm;

	sf::Music music;

	sf::SoundBuffer cardSoundBuffer1;
	sf::SoundBuffer cardSoundBuffer2;

	sf::Sound cardSound1;
	sf::Sound cardSound2;

	int currentCardSoundBufferNum = 1;

	sf::SoundBuffer enemyDeadSoundBuffer1;
	sf::SoundBuffer enemyDeadSoundBuffer2;
	sf::SoundBuffer enemyDeadSoundBuffer3;

	sf::Sound enemyDeadSound1;
	sf::Sound enemyDeadSound2;
	sf::Sound enemyDeadSound3;

	int currentEnemyDeadSoundBufferNum = 1;

};