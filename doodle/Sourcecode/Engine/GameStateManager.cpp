﻿// GAM150
// GameStateManager.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "GameStateManager.h"
#include "Engine.h"

GameStateManager::GameStateManager() : currGameState(nullptr), nextGameState(nullptr), state(State::START) {}

void GameStateManager::AddGameState(GameState& gameState) 
{
	gameStates.push_back(&gameState);
}

void GameStateManager::Update(double deltaTime) 
{
	switch (state)
	{
	case State::START:

		nextGameState = gameStates[0];
		state = State::LOAD;
		break;

	case State::LOAD:

		currGameState = nextGameState;

		Engine::GetLogger().LogEvent("Load " + currGameState->GetName());
		currGameState->Load();
		Engine::GetLogger().LogEvent("Load Complete");
		state = State::RUNNING;

		break;

	case State::RUNNING:

		if (currGameState == nextGameState)
		{
			Engine::GetLogger().LogVerbose("Update" + currGameState->GetName());
			currGameState->Update(deltaTime);
			currGameState->Draw();
		}
		else { state = State::UNLOAD; }

		break;

	case State::UNLOAD:

		Engine::GetLogger().LogEvent("Unload " + currGameState->GetName());
		currGameState->Unload();

		if (nextGameState == nullptr) { state = State::SHUTDOWN; }
		else { state = State::LOAD; }

		break;

	case State::SHUTDOWN:

		state = State::EXIT;
		break;

	case State::EXIT:
		break;
	}
}

void GameStateManager::SetNextState(GAMESTATE initState) 
{
	nextGameState = gameStates[static_cast<int>(initState)];
}

void GameStateManager::ReloadState() 
{
	state = State::UNLOAD;
}

void GameStateManager::Shutdown() 
{
	state = State::UNLOAD;
	nextGameState = nullptr;
}