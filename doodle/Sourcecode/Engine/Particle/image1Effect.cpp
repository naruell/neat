﻿// GAM150
// inamge1Effect.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "image1Effect.h"

#include <iostream>
#include <doodle/image.hpp>
#include <doodle/noise.hpp>
#include <doodle/random.hpp>
#include <doodle/environment.hpp>

#include "../helper.h"

void PixelParticle::MemberSetup()
{
	noiseSeedX = static_cast<double>(doodle::random(0, 3000));
	noiseSeedY = static_cast<double>(doodle::random(0, 3000));
}

void PixelParticle::MemberUpdate()
{
	noiseSeedX += doodle::DeltaTime;
	noiseSeedY += doodle::DeltaTime;
	if (fillColor.alpha == 0) { endCondition = true; }
}

void PixelParticle::MemberDraw()
{
	if (subtype == "nullPixel") { return; }

	doodle::set_fill_color(fillColor);
	doodle::no_outline();
	doodle::draw_rectangle(position.x, position.y, static_cast<double>(pixelSize), static_cast<double>(pixelSize));
}

void Image1Effect::GenerateParticle()
{
	doodle::Image image(imagePath);

	imageSize = Vector2DInt{ image.GetWidth(), image.GetHeight() };

	imageWidth = image.GetWidth();
	imageHeight = image.GetHeight();

	const doodle::Image::color* p_colors = image.begin();

	for (int y = 0; y < imageHeight; y += pixelSize)
	{
		for (int x = 0; x < imageWidth; x += pixelSize)
		{
			if (p_colors[x + imageWidth * y].alpha == 0)
			{
				//particleList.emplace_back(new PixelParticle());
			}
			else
			{
				particleList.emplace_back(new PixelParticle( x + imageWidth * y, Vector2D{ origin.x + x, origin.y - y }, pixelSize));
				particleList[particleList.size() - 1]->fillColor = p_colors[x + imageWidth * y];
			}
		}
	}
}

void Image1Effect::MemberSetup()
{
	GenerateParticle();
	effectrangeX = static_cast<double>(imageWidth);
	effectrangeY = 0;
}

void Image1Effect::MemberRun()
{
	if (GetParticlesSize() == 0)
	{
		endCondition = true;
	}

	effectrangeX = Map(showedTime, 3.0, 5.5, static_cast<double>(imageWidth), 0);	// 사라지게 하는 경계선

	for (std::unique_ptr<Particle> &p : particleList)
	{
		PixelParticle* pp = static_cast<PixelParticle*>(p.get());
		if (pp && pp->particleIndex % imageWidth >= effectrangeX)
		{
			pp->acceleration = Vector2D{ 2, 2 };

			double n1 = doodle::noise(pp->noiseSeedX);
			double n2 = doodle::noise(pp->noiseSeedY);
			pp->position.x += Map(n1, 0, 1, -1, 3) * 120 * (showedTime - pShowedTime);	// 흩날리는 방향
			pp->position.y += Map(n2, 0, 1, -1, 2) * 120 * (showedTime - pShowedTime);

			double alpha = Map(pp->particleIndex % imageWidth - effectrangeX, 0.0f, 500.0f, 255, 0);
			if (alpha <= 0) { alpha = 0; }
			pp->fillColor.alpha = alpha;
		}
	}

	for (int i = static_cast<int>(particleList.size()) - 1; i >= 0; --i)
	{
		particleList[i]->Run();

		if (particleList[i]->isEnded())
		{
			particleList.erase(particleList.begin() + i);
		}
	}
}

Vector2DInt Image1Effect::GetSize()
{
	return imageSize;
}

Image1Effect::~Image1Effect()
{

}