﻿#pragma once
// GAM150
// click1Effect.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "ParticleSystem.h"

#include <doodle/random.hpp>

struct Click1Circle : public Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;
	
	double outlineWidth{ 0 };

	Click1Circle(Vector2D pos) : Particle("Click1Circle", pos, Vector2D{ 0.0, 0.0 }, Vector2D{ 0.0, 0.0 }) {};
};

struct Click1Line : public Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;

	double angle;
	double length;
	Vector2D outPos{ 0, 0 };
	Click1Line(Vector2D pos, double angle) : angle(angle), length(static_cast<float>(doodle::random(40, 100))), Particle("Click1Line", pos, Vector2D{ 0.0f, 0.0f }, Vector2D{ 0.0f, 0.0f }) {};
};

struct Click1Twinkle : public Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;

	Click1Twinkle(Vector2D pos) : Particle("Click1Twinkle", pos, Vector2D{ 0.0f, 0.0f }, Vector2D{ 0.0f, 0.0f }) {};
};

class Click1Effect : public Effect
{
public:
	void GenerateParticle() override;
	void GenerateTwinkle(Vector2D pos);
	void MemberSetup() override;
	void MemberRun() override;

	Click1Effect(Vector2D origin) : Effect(origin) {};
	~Click1Effect() override;
};