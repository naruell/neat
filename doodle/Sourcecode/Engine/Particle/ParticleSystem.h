﻿#pragma once
// GAM150
// ParticleSystem.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>
#include <vector>

#include <doodle/color.hpp>
#include <doodle/drawing.hpp>

#include "../Geometry/Vector2D.h"

struct Particle
{
	/**
	 * \essential code guideline
	 * \code
	 *	void setup()
	 *	{
	 *		generatedTime = doodle::ElapsedTime;
	 *		if(duration != -1) {endTime = generatedTime + duration;}
	 *		
	 *		MemberSetup(); 
	 *	
	 *		isSetup = true;
	 *	}
	 */
	void Setup();
	virtual void MemberSetup() = 0;
	
	/**
	 * \essential code guideline
	 * \code 
	 *	void Update()
	 *	{
	 *		if (!isSetup) { Setup(); }
	 *		pShowedTime = showedTime;
	 *		showedTime = doodle::ElapsedTime - generatedTime;
	 *	
	 *		MemberUpdate();
	 *		// If there are no time limit, then you have to set endCondtion true to remove particle
	 *	}
	 */
	void Update();
	virtual void MemberUpdate() = 0;

	/**
	 * \essential code guideline
	 * \code 
	 * void Draw()
	 *	{
	 *		doodle::push_settings();
	 *	
	 *		MemberDraw();
	 *	
	 *		doodle::pop_settings();
	 *	}
	 */
	void Draw();
	virtual void MemberDraw() = 0;

	void Run();
	bool isEnded();	// 파티클이 사라져야 할 지 여부 ((재생시간 >= 수명) 혹은 (endCondition이 true 일 때))

	bool isSetup{ false };
	std::string subtype;

	Vector2D position;
	Vector2D velocity;
	Vector2D acceleration;

	double mass{ 1 };				// 질량
	bool isInfbyForce{ false };

	double magnetism{ 0 };			//자성
	double ferromagnetic{ 0 };		// 강자성
	bool isInfbyMagnet{ false };

	double elasticity{ 1 };			// 탄성
	bool isInfbyCollision{ false };

	double generatedTime{ 0 };
	double endTime{ 0 };
	double showedTime{ 0 };
	double pShowedTime{ 0 };
	double duration{ -1 };
	bool endCondition{ false };

	double size_w{ 0 };
	double size_h{ 0 };
	doodle::Color fillColor{ 255, 255, 255, 255 };
	doodle::Color outlineColor{ 255, 255, 255, 255 };
	
	Particle(std::string subtype, Vector2D pos, Vector2D vel, Vector2D acc) : subtype(subtype), position(pos), velocity(vel), acceleration(acc) {};
	Particle(std::string subtype, Vector2D pos, Vector2D vel, Vector2D acc, double dur) : subtype(subtype), position(pos), velocity(vel), acceleration(acc), duration(dur) {};
	virtual ~Particle();
};

class Effect
{
public:
	virtual void GenerateParticle() = 0;

	void Setup();
	virtual void MemberSetup() = 0;

	void Run();
	virtual void MemberRun() = 0;

	bool isEnded();
	void setEndCondition(bool b);

	std::string GetType();
	void SetType(std::string typeTo);

	Vector2D GetOrigin();
	void SetOrigin(Vector2D originTo);

	void SetDuration(double durationTo);

	int GetParticlesSize();

	Effect(Vector2D origin) : origin(origin) {}
	Effect(Vector2D origin, double dur) : origin(origin), duration(dur) {}
	virtual ~Effect();

protected:
	Vector2D origin;
	std::vector<std::unique_ptr<Particle>> particleList;
	std::string type;

	bool isSetup{ false };

	double generatedTime{ 0 };
	double endTime{ 0 };
	double showedTime{ 0 };
	double pShowedTime{ 0 };
	double duration{ -1 };
	bool endCondition{ false };
};

class EffectSystem
{
public:
	void Run();

	// 이펙트 생성 명령어
	void GenerateClick1Effect();
	void GenerateTestEffect(Vector2D pos);
	void GenerateImage1Effect(Vector2D pos, std::string filePath, int pixelSize);
	void GenerateTextEffect(std::string text, Vector2D pos, doodle::Color textColor, double textSize, double dur);

	std::vector<std::unique_ptr<Effect>> effectList;

	~EffectSystem();
private:
};

/*	#### GUIDLINES FOR OVERRIDING FUNCTIONS ####
 *	
 *	 ## Particle ##
 *	
 *	void setup()
 *	{
 *		generatedTime = doodle::ElapsedTime;
 *		if(duration != -1) {endTime = generatedTime + duration;}
 *		
 *		// setup code here
 *	
 *		isSetup = true;
 *	}
 *	
 *	void Update()
 *	{
 *		if (!isSetup) { setup(); }
 *		pshowedTime = showedTime;
 *		showedTime = doodle::ElapsedTime - generatedTime;
 *	
 *		// Update code here
 *		// If there are no time limit, then you have to set endCondtion true to remove particle
 *	}
 *	
 *	void Draw()
 *	{
 *		doodle::push_settings();
 *	
 *		// Draw code here
 *	
 *		doodle::pop_settings();
 *	}
 */