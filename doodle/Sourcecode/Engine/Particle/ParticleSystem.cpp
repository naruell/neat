﻿// GAM150
// ParticleSystem.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "ParticleSystem.h"

#include <fstream>
#include <doodle/environment.hpp>
#include <doodle/input.hpp>

#include "effectHeader.h"
#include "../helper.h"

void Particle::Setup()
{
	{
		generatedTime = doodle::ElapsedTime;
		if (duration != -1) { endTime = generatedTime + duration; }

		MemberSetup();

		isSetup = true;
	};
}

void Particle::Update()
{
	if (!isSetup) { Setup(); }
	pShowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	MemberUpdate();
}

void Particle::Draw()
{
	doodle::push_settings();
	MemberDraw();
	doodle::pop_settings();
}

void Particle::Run()
{
	Update();
	Draw();
}

bool Particle::isEnded()
{
	if (duration != -1 && (showedTime >= duration)) { return true; }
	if (endCondition == true) { return true; }

	return false;
}

Particle::~Particle()
{

}

void Effect::Setup()
{
	generatedTime = doodle::ElapsedTime;
	if (duration != -1) { endTime = generatedTime + duration; }

	MemberSetup();

	isSetup = true;
}

void Effect::Run()
{
	if (!isSetup) { Setup(); }
	pShowedTime = showedTime;
	showedTime = doodle::ElapsedTime - generatedTime;

	MemberRun();
}

bool Effect::isEnded()
{
	if (duration != -1 && (showedTime >= duration)) { return true; }
	if (endCondition == true) { return true; }

	return false;
}

void Effect::setEndCondition(bool b)
{
	endCondition = b;
}

std::string Effect::GetType()
{
	return type;
}

void Effect::SetType(std::string typeTo)
{
	type = typeTo;
}

Vector2D Effect::GetOrigin()
{
	return origin;
}

void Effect::SetOrigin(Vector2D originTo)
{
	origin = originTo;
}

void Effect::SetDuration(double durationTo)
{
	duration = durationTo;
}

int Effect::GetParticlesSize()
{
	return static_cast<int>(particleList.size());
}

Effect::~Effect()
{
}

void EffectSystem::Run()
{
	for (int i = static_cast<int>(effectList.size()) - 1; i >= 0; --i)
	{
		effectList[i]->Run();
		
		if (effectList[i]->isEnded())
		{
			effectList.erase(effectList.begin() + i);
		}
	}
}

void EffectSystem::GenerateClick1Effect()
{
	double mouseX = doodle::get_mouse_x() * 1.0;
	double mouseY = doodle::get_mouse_y() * 1.0;
	effectList.emplace_back(new Click1Effect(Vector2D{mouseX, mouseY}));
	effectList[effectList.size() - 1]->GenerateParticle();
}

void EffectSystem::GenerateTestEffect(Vector2D pos)
{
	effectList.emplace_back(new TestEffect(pos));
}

void EffectSystem::GenerateImage1Effect(Vector2D pos, std::string filePath, int pixelSize)
{
	effectList.emplace_back(new Image1Effect(pos, filePath, pixelSize));
}

void EffectSystem::GenerateTextEffect(std::string text, Vector2D pos, doodle::Color textColor, double textSize, double dur)
{
	effectList.emplace_back(new TextEffect(text, pos, textColor, textSize, dur));
}

EffectSystem::~EffectSystem()
{

}