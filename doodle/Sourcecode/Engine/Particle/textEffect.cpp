﻿// GAM150
// textEffect.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "textEffect.h"
#include "../helper.h"

void TextParticle::MemberSetup()
{
}

void TextParticle::MemberUpdate()
{
	if (showedTime >= duration - 0.4) 
	{
		textColor.alpha = Map(showedTime, duration - 0.4, duration, 255, 0);
	}
}

void TextParticle::MemberDraw()
{
	doodle::push_settings();
	doodle::set_fill_color(textColor);
	doodle::no_outline();
	doodle::set_font_size(textSize);
	doodle::draw_text(text, position.x, position.y);
	doodle::pop_settings();
}

void TextEffect::GenerateParticle()
{
	particleList.emplace_back(new TextParticle(text, origin, textColor, textSize, duration));
}

void TextEffect::MemberSetup()
{
	GenerateParticle();
}

void TextEffect::MemberRun()
{
	if (GetParticlesSize() == 0)
	{
		endCondition = true;
	}

	for (int i = static_cast<int>(particleList.size()) - 1; i >= 0; --i)
	{
		particleList[i]->Run();

		if (particleList[i]->isEnded())
		{
			particleList.erase(particleList.begin() + i);
		}
	}
}

TextEffect::~TextEffect()
{
}
