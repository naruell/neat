﻿#pragma once
// GAM150
// testEffect.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <doodle/random.hpp>

#include "ParticleSystem.h"

struct TestParticle_circle : Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;

	TestParticle_circle(Vector2D pos) : Particle("TestParticle_circle", pos, Vector2D{ doodle::random(-1.0, 1.0), doodle::random(0.0, 1.0) }, Vector2D{ 0.0, -0.05 }, 255.0) {};
};

struct TestParticle_rect : Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;

	TestParticle_rect(Vector2D pos) : Particle("TestParticle_rect", pos, Vector2D{ doodle::random(-1.0, 1.0), doodle::random(0.0, 1.0) }, Vector2D{ 0.0, -0.05 }, 255.0) {};
};

class TestEffect : public Effect
{
public:
	void GenerateParticle() override;
	void MemberSetup() override;
	void MemberRun() override;

	TestEffect(Vector2D origin) : Effect(origin) {};
	~TestEffect() override;
};