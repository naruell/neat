﻿// GAM150
// Animation.cpp
// Team Neat
// Primary : 
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Animation.h"

#include <fstream>    // file input stuff & runtime_error

#include "Engine.h"    // Engine::GetLogger

Animation::Animation() : Animation("assets/none.anm") { }

// Constructor with file path
Animation::Animation(std::string fileName) : animCommandIndex(0)
{
	std::ifstream inFile(fileName);
	std::string label;

	if (fileName.substr(fileName.find_last_of('.')) != ".anm") 
	{
		throw std::runtime_error("Bad Filetype.  " + fileName + " not a sprite info file (.anm)");
	}
	if (inFile.is_open() == false) 
	{
		throw std::runtime_error("Failed to load " + fileName);
	}

	while (inFile.eof() == false) 
	{
		inFile >> label;
		if (label == "PlayFrame") 
		{
			int frame;
			float targetTime;
			inFile >> frame;
			inFile >> targetTime;

			animation.push_back(new PlayFrame(frame, targetTime));
		}
		else if (label == "Loop") 
		{
			int loopToIndex;
			inFile >> loopToIndex;
			animation.push_back(new Loop(loopToIndex));
		}
		else if (label == "End") 
		{
			animation.push_back(new End());
		}
		else 
		{
			Engine::GetLogger().LogError("Unknown command " + label + " in anm file " + fileName);
		}
	}
	Reset();
}

Animation::~Animation()
{
	for (Animation::CommandData* commandData : animation)
	{
		delete commandData;
	}
	animation.clear();
	Engine::Instance().GetLogger().LogEvent("All of the animation has been erased");
}

// update our current animation command
void Animation::Update(double dt)
{
	currPlayFrameData->Update(dt);
	if (currPlayFrameData->IsFrameDone() == true) 
	{
		currPlayFrameData->Reset();
		++animCommandIndex;
		if (animation[animCommandIndex]->GetType() == Command::PlayFrame) 
		{
			currPlayFrameData = static_cast<PlayFrame*>(animation[animCommandIndex]);
		}
		else if (animation[animCommandIndex]->GetType() == Command::Loop) 
		{
			Loop* loopData = static_cast<Loop*>(animation[animCommandIndex]);
			animCommandIndex = loopData->GetLoopToIndex();
			if (animation[animCommandIndex]->GetType() == Command::PlayFrame) 
			{
				currPlayFrameData = static_cast<PlayFrame*>(animation[animCommandIndex]);
			}
			else 
			{
				Engine::GetLogger().LogError("Loop does not go to PlayFrame");
				Reset();
			}
		}
		else if (animation[animCommandIndex]->GetType() == Command::End) 
		{
			IsAnimationDone_bool = true;
			return;
		}
	}
}

// return which of the frames in our sprite sheet we want to display
int Animation::GetDisplayFrame()
{
	return currPlayFrameData->GetFrameNum();
}

// reset our animation command
void Animation::Reset()
{
	IsAnimationDone_bool = false;
	animCommandIndex = 0;
	currPlayFrameData = static_cast<PlayFrame*>(animation[animCommandIndex]);
}

// return if animation (all of the commands) is done or not
bool Animation::IsAnimationDone()
{
	return IsAnimationDone_bool;
}

// Constructor
Animation::Loop::Loop(int loopToIndex) : loopToIndex(loopToIndex)
{
}

// returns the index to jump to 
int Animation::Loop::GetLoopToIndex()
{
	return loopToIndex;
}

// gets a frame to show and an amount of time to show it, this will also need to initialize the timer = 0
Animation::PlayFrame::PlayFrame(int frame, double duration) : frame(frame), targetTime(duration)
{
	timer = 0;
}

// advance our timer
void Animation::PlayFrame::Update(double dt)
{
	timer += dt;
}

// checks if timer >= targetTime
bool Animation::PlayFrame::IsFrameDone()
{
	return targetTime <= timer;
}

// sets our timer back to 0
void Animation::PlayFrame::Reset()
{
	timer = 0;
}

// returns the frame which is being shown
int Animation::PlayFrame::GetFrameNum()
{
	return frame;
}
