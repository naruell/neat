﻿#pragma once
// GAM150
// Animation.h
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>  //std::vector
#include <string>  //std::string

class Animation
{
public:
	Animation();
	Animation(std::string file);
	~Animation();
	void Update(double dt);
	int GetDisplayFrame();
	void Reset();
	bool IsAnimationDone();

private:

	enum class Command
	{
		PlayFrame,
		Loop,
		End,
	};

	class CommandData
	{
	public:
		virtual Command GetType() = 0;
		virtual ~CommandData() = default;
	};

	class PlayFrame : public CommandData
	{
	public:
		PlayFrame(int frame, double duration);
		Command GetType() override { return Command::PlayFrame; }
		void Update(double dt);
		bool IsFrameDone();
		void Reset();
		int GetFrameNum();
	private:
		int frame;
		double targetTime;
		double timer;
	};

	class Loop : public CommandData
	{
	public:
		Loop(int loopToIndex);
		Command GetType() override { return Command::Loop; }
		int GetLoopToIndex();
	private:
		int loopToIndex;
	};

	class End : public CommandData
	{
	public:
		Command GetType() override { return Command::End; }
	private:
	};

	bool IsAnimationDone_bool;
	int animCommandIndex;
	PlayFrame* currPlayFrameData;
	std::vector<CommandData*> animation;
};
