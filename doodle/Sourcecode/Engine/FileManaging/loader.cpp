﻿// GAM150
// loader.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "loader.h"

#include <iostream>
#include <fstream>

const unsigned int encryptingSeed = 2020;
const unsigned int randRange = 77;

void EncryptingData(std::string filePath)
{
	std::ifstream readStream(filePath);
	if (!readStream) { std::cerr << "Failed to open file!" << std::endl; }

	srand(encryptingSeed);

	char data_char;
	std::string encryptedData = "";

	while (readStream >> data_char)
	{
		encryptedData += static_cast<char>(static_cast<int>(data_char) + rand() % randRange);
	}

	std::cout << "File has been successfully encrypted!\n";

	std::ofstream writeStream;
	writeStream.open(filePath, std::ios::trunc);
	writeStream << encryptedData;
	writeStream.close();
}

void DecryptingData(std::string filePath)
{
	std::ifstream readStream(filePath);
	if (!readStream) { std::cerr << "Failed to open file!" << std::endl; }

	srand(encryptingSeed);

	char data_char;
	std::string decryptedData = "";

	while (readStream >> data_char)
	{
		decryptedData += static_cast<char>(static_cast<int>(data_char) - rand() % randRange);
	}

	std::cout << "File has been successfully decrypted!\n";

	std::ofstream writeStream;
	writeStream.open(filePath, std::ios::trunc);
	writeStream << decryptedData;
	writeStream.close();
}

void LoadGameData(std::string filePath)
{
	DecryptingData(filePath);

	std::ifstream jsonPath(filePath);
	CharReaderBuilder builder;
	builder["collectComments"] = false;
	Value value;

	JSONCPP_STRING errs;
	bool isParsed = parseFromStream(builder, jsonPath, &value, &errs);

	if (isParsed == true)
	{
		// variable = value["key1"]["key2"].asType();
		// asType  =>> asUInt, asInt, asFloat, asDouble, asString, asBool
		
		// Example
		std::cout << "\n" << value["currentInfo"]["roomsInfo"][2]["isCleared"] << "\n";
	}
	else
	{
		std::cout << "Failed to load mapData. Check your file";
		abort();
	}

	// Temparary File Reading Start
	std::string data;
	std::ifstream TEMPREADING(filePath);
	TEMPREADING >> data;
	std::cout << "\n" << data << "\n\n";
	// Temparary File Reading End

	EncryptingData(filePath);
}