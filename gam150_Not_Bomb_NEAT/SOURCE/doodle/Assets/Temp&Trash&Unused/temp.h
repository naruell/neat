#pragma once
#include <vector>
#include <string>

using std::string;

enum class ObjectType { Error, Character, NPC, Monster, Item, Structure, count }; // 옵젝 타입.
enum class Class { Error, None, Warrior, Priest, Rogue, Paladin, Hunter, Shaman, Mage, Warlock, Druid, Monk, count };
// 클래스. 플레이어 또는 NPC 등 특별한 애한테만 쓸 듯. 직업 이름은 임시로 적어둠. (와! 월드 오브 워크래프트!)
enum class MonsterType { Error, None, Human, Undead, Beast, Spirit, Dragon, Demon, Duhwan, count }; // 몹 타입. 일단 임시로 적어둠 (와! 두환 타입!)
enum class StageType { Error, Menu, Store, Story, Battle, count }; // 스테이지 타입.
enum class StagesubType { Error, None, Normal, Boss, Event, count }; // 스테이지 서브 타입. 이 값은 type이 Battle일 경우에만 사용하지 싶음

// 캐릭터, NPC, 템 전부 이걸로 쓰면 되지 않을까 싶은데 어떨진 모르겠다
class Object
{
public:
	// 여긴 일단 안 건드릴게, 옵젝 어캐할지 모르니까
private:
	ObjectType type = static_cast<ObjectType>(0); // 옵젝 기본 타입. 기본값 error

	// 포지션 struct로 할 거야 아니면 이렇게 개인 변수로 끼울 거야?
	float posX = 0.0f;
	float posY = 0.0f;

	float HP = 0.0f; // 체력
	// float MP = 0.0f; // 안 쓸 거 같음
	float ATK = 0.0f; // 아마 공격 보정치
	float HEAL = 0.0f; // 아마 힐 보정치
	float DEF = 0.0f; // 방어력
};

enum class RoomType { Error, StartPoint, EndPoint, Battle, Trap, Treasure, Event, Boss, count }; // 방 타입.

// 스테이지, 번째 & 특별 스테이지 및 이벤트 맵은 스테이지 0번으로 합시다
struct RoomNumber 
{
	int stage = 0;
	int number = 0;
};

struct Room
{
public:
	void setName(string inputName);
	string getName();
	void setType(RoomType inputType);
	//Room(string name, RoomNumber RoomNumbers, RoomType RoomType, std::vector<RoomNumber>roomTo) // 초기화
	//	:Roomname(name), roomNumber(RoomNumbers), type(RoomType), roomTo(roomTo) {}
private:
	std::string Roomname = "Error : Untitled Name"; // 방 이름, 기본값 error
	RoomNumber roomNumber = { -1,-1 }; // 방 고유 식별 번호
	RoomType type = static_cast<RoomType>(0); // 방의 타입. 기본값 error
	std::vector<RoomNumber>roomTo = { }; // 이어지는 방 목록
	std::vector<Object>monsterList = { }; // 방에 존재하는 몬스터 목록
	// std::vector<Object>reward = { }; // 해당 방의 보상, 아마 treasure 방 같은 데서만 쓸 듯
	// bool isCleared = false; // 그 방이 클리어 됐는지
};

struct Stage
{
public:
	 void setName(string inputName);
	 string getName();
	 void setType(StageType inputType);
	void setsubType(StagesubType inputType);
	Stage(string name, StageType StageType, StagesubType StagesubType) // 초기화
		:Stagename(name), type(StageType), subType(StagesubType) {}
private:
	string Stagename = "Error : Untitled Name"; // 스테이지 이름, 기본값 error
	StageType type = static_cast<StageType>(0); // 스테이지의 타입. 기본값 error
	StagesubType subType = static_cast<StagesubType>(0); // 스테이지의 서브 타입.기본값 error
	std::vector<Room> Rooms = {  };
	std::vector<Object>reward = { }; // 해당 스테이지의 보상	
	bool isCleared = false; // 그 스테이지가 클리어 됐는지
};



std::vector<Stage> Stages = {}; // 스테이지 목록들.