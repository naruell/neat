﻿// Gam150
// Room.cpp
// Team NEAT
// Primary: Sunghwan Cho
// 5/20/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Room.h"

#include <doodle/drawing.hpp>
#include <doodle/input.hpp>

#include "../Engine/Engine.h"

Room::Room(Vector2D roomPos) : position(roomPos)
{
}

Room::~Room()	{}

BattleRoomType Room::GetBattleRoomType()
{
	return BattleRoomType::NONE;
}

RoomType Room::GetType()
{
	return RoomType::None;
}

void Room::Load()
{
	unclearedRoom = std::make_unique<doodle::Image>(doodle::Image{ PATH_DEFAULT_ROOM });
	clearedRoom = std::make_unique<doodle::Image>(doodle::Image{ PATH_DEFAULT_ROOM });
}

void Room::Draw()
{
	doodle::draw_image(*clearedRoom,position.x,position.y,length,length);
	if (isAbleToHover == true)
	{
		doodle::draw_image(*unclearedRoom, position.x, position.y, length + 1, length + 1);
	}
	if (isOnHovered == true)
	{
		doodle::draw_image(*unclearedRoom, position.x, position.y, length + 4, length + 4);
	}
	if (isOnClicked == true)
	{
		doodle::draw_image(*clearedRoom, position.x, position.y, length + 4, length + 4);
	}
	if (hasCleared == true)
	{
		doodle::draw_image(*clearedRoom, position.x, position.y, length + 5, length + 5);
	}
}

void Room::Draw(const double size)
{
	doodle::draw_image(*clearedRoom, position.x, position.y, size, size);
	if (isAbleToHover == true)
	{
		doodle::draw_image(*unclearedRoom, position.x, position.y, size + 1.0, size + 1.0);
	}
	if (isOnHovered == true)
	{
		doodle::draw_image(*unclearedRoom, position.x, position.y, size + 4.0, size + 4.0);
	}
	if (isOnClicked == true)
	{
		doodle::draw_image(*clearedRoom, position.x, position.y, size + 4.0, size + 4.0);
	}
	if (hasCleared == true)
	{
		doodle::draw_image(*clearedRoom, position.x, position.y, size + 5.0, size + 5.0);
	}
}

void Room::Update()
{
	OnHovered();
	OnClicked();
}

void Room::OnHovered()
{
	const int mouseX = doodle::get_mouse_x();
	const int mouseY = doodle::get_mouse_y();
	if( mouseX < position.x + length / 2 && mouseX > position.x - length/2 && mouseY < position.y + length / 2 && mouseY > position.y - length / 2 && isAbleToHover == true )
	{
		isOnHovered = true;
	}
	else
	{
		isOnHovered = false;
	}
}

void Room::OnClicked()
{
	if(isOnHovered == true && isOnClicked == false && Engine::GetInput().IsMouseReleased(Input::MouseButton::Left))
	{
		isOnClicked = true;
	}
}

Vector2D Room::GetPosition()
{
	return position;
}

void Room::SetIsOnClicked(bool set)
{
	isOnClicked = set;
}

bool Room::GetIsOnClicked()
{
	return isOnClicked;
}

void Room::SetIsAbleToHover(bool set)
{
	isAbleToHover = set;
}

bool Room::GetIsAbleToHover()
{
	return isAbleToHover;
}

void Room::SetIsOnHovered(bool set)
{
	isOnHovered = set;
}

bool Room::GetIsOnHovered()
{
	return isOnHovered;
}

void Room::SetIsCleared(bool set)
{
	isCleared = set;
}

bool Room::GetIsCleared()
{
	return isCleared;
}

void Room::SetHasCleared(bool set)
{
	hasCleared = set;
}

bool Room::GetHasCleared()
{
	return hasCleared;
}

void Room::ToBeExecuteOnClicked()
{
}

BattleRoom::BattleRoom(Vector2D roomPos, int type) : type(static_cast<BattleRoomType>(type))
{
	position = roomPos;
}

BattleRoom::~BattleRoom() {}

void BattleRoom::Load()
{
	unclearedRoom = std::make_unique<doodle::Image>(doodle::Image{PATH_UNCLEARED_BATTLE_ROOM});
	clearedRoom = std::make_unique<doodle::Image>(doodle::Image{ PATH_CLEARED_BATTLE_ROOM });

}

void BattleRoom::ToBeExecuteOnClicked()
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_ROSTER);
}

BattleRoomType BattleRoom::GetBattleRoomType()
{
	return type;
}

EventRoom::EventRoom(Vector2D roomPos)
{
	position = roomPos;
}

EventRoom::~EventRoom() {}

void EventRoom::Load()
{
	unclearedRoom = std::make_unique<doodle::Image>(doodle::Image{ PATH_UNCLEARED_EVENT_ROOM });
	clearedRoom = std::make_unique<doodle::Image>(doodle::Image{ PATH_CLEARED_EVENT_ROOM });
}

void EventRoom::ToBeExecuteOnClicked()
{
	Engine::GetGameStateManager().SetNextState(GAMESTATE::FINAL_EVENTROOM);
}
