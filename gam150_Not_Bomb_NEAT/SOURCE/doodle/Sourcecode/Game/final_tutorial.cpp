﻿// GAM150
// final_tutotial.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "final_tutorial.h"

#include <doodle/drawing.hpp>
#include <doodle/window.hpp>

#include "../Engine/Engine.h"
#include "../Environment/constants.h"
#include "cardPile.h"
#include "hand.h"

void Final_Tutorial::Load()
{
	timer = 0;
	pTimer = 0;

	for(std::string path : space_tutorial::tutorial_image_path)
	{
		tutorial_images.emplace_back(std::make_unique<doodle::Image>(path));
	}
	index = 0;

	prev_mouse_image = std::make_unique<doodle::Image>(space_tutorial::PATH_MOUSE_PREV);
	next_mouse_image = std::make_unique<doodle::Image>(space_tutorial::PATH_MOUSE_NEXT);

	doodle::show_cursor(false);
}

void Final_Tutorial::Update(double deltaTime)
{
	pTimer = timer;
	timer += deltaTime;
	
	if(Engine::GetInput().IsMousePressed(Input::MouseButton::Left) == true)
	{
		if(Engine::GetInput().GetMousePos().x >= 0)
		{
			index++;
			if (index >= static_cast<int>(tutorial_images.size()))
			{
				index--;
				Engine::GetGameStateManager().SetNextState(GAMESTATE::TITLE);
				return;
			}
		}
		else
		{
			index--;
			if (index < 0)
			{
				index = 0;
			}
		}
	}

	player_hand.Update();
}

void Final_Tutorial::Draw()
{
	doodle::push_settings();
	doodle::draw_image(*tutorial_images[index], 0, 0, Engine::GetWindow().GetSize().x, Engine::GetWindow().GetSize().y);
	player_hand.Draw();

	if (Engine::GetInput().GetMousePos().x >= 0)
	{
		doodle::draw_image(*next_mouse_image, Engine::GetInput().GetMousePos().x, Engine::GetInput().GetMousePos().y);
	}
	else
	{
		doodle::draw_image(*prev_mouse_image, Engine::GetInput().GetMousePos().x, Engine::GetInput().GetMousePos().y);
	}
	doodle::pop_settings();
}

void Final_Tutorial::Unload()
{
	player_hand.Clear_hand();
	graveyard.Clear();
	index = 0;
	
	doodle::show_cursor(true);
}