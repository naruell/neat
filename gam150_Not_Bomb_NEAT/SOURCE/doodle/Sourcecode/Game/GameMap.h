﻿#pragma once
// GAM150
// Team Neat
// GameMap.h
// Primary :Sunghwan Cho
// 5/27/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>
#include <doodle/drawing.hpp>

#include "../Engine/Geometry/Graph.h"
#include "Room.h"

class GameMap
{
public:
	GameMap() = default;
	~GameMap() = default;
	
	void Load();
	void Load(std::string mapInputFile);
	void Update(double deltaTime);
	void Draw();
	void Unload();

	int GetNeighborRoomNumbers(int index);
	int GetCurrentRoomNum();
	int GetCurrentRoomID();

	bool GetIsMapCleared();
	void SetIsMapCleared(bool set);
	void SetIsRoomCleared(int roomNum, bool set);
	int GetIndex() const;
private:

	static constexpr int MAXIMUM = 31;
	CDATASTRUCTURE::CTECGraph<Room> gameMap;
	std::vector<std::unique_ptr<Room>> gameRoomList;
	bool isMapCleared = false;
	int mapIndex = 0;
	std::unique_ptr<doodle::Image> background = nullptr;
public:
	double timer = 0;
};