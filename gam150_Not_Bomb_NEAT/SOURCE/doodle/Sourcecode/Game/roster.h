﻿#pragma once
// GAM150
// roster.h
// Team Neat
// Primary : Sunghwan Cho, Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>

#include "../Game/battlefield.h"
#include "../Game/Character/Character.h"

class Roster
{
public:
	Roster();

	void Load();
	void Update();
	void Draw();

	bool getIsHoldingCharacter();
	
private:
	std::vector<Character> characterVector{};
	Vector2DInt ClickedPos{};
	bool isHoldingCharacter { false };
};

extern Roster roster;
