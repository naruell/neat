﻿#pragma once
// GAM150
// AI.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <functional>
#include <vector>

#include "../Engine/Geometry/Vector2D.h"

enum class PatternType { ATTACK, PREPARATION, HEAL };

class Enemy;

class Pattern
{
public:
    Pattern(Enemy* enemyPtr, PatternType patternType) : enemyPtr(enemyPtr), patternType(patternType) {}

    virtual void Ready() = 0;
    virtual void Execute() = 0;
    virtual ~Pattern() {};
    std::vector<Vector2DInt> attackRange;
    //first is heal target, and second is amount of healing
    std::vector<std::pair<Vector2DInt, int>> healRange;
    std::vector<Vector2DInt> preparationRange;
    Enemy* enemyPtr;
    PatternType patternType;
};

class AI
{
public:
    void SelectNextPattern();
    void PushBackPatterns(Pattern* p, int weight);
    Pattern* GetNextPattern();
    ~AI();
private:
    Pattern* nextPattern;
    std::vector<std::pair<Pattern*, int>> patterns;
};

class AttackOneHeroPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
};

class DoNothingPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
};

class AttackRandomTwoTilesPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
};

class AttackRandomOneColumnPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
};

class Add40PercentDamagePattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
};

class HealLowestHPEnemyPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
};

class BigBombPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
private:
    int leftTurn = 2;
};

class AddApArmorPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
private:
    int apArmorAmount = 3;
};

class AddAdArmorPattern : public Pattern
{
public:
    using Pattern::Pattern;
    void Ready() override;
    void Execute() override;
private:
    int adArmorAmount = 3;
};