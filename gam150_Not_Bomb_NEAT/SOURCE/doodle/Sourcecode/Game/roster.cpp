﻿// GAM150
// roster.cpp
// Team Neat
// Primary : Byeongjun Kim, Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "roster.h"

#include <doodle/input.hpp>
#include <doodle/environment.hpp>

#include "../Engine/helper.h"
#include "hand.h"

Roster::Roster()
{
	battlefield.initializeBattleFieldVector();
	// battlefield.battleFieldVector[0][0] = new Battlefield::BattleObject(characterVector[0],)
}

void Roster::Load()
{

}

void Roster::Update()
{
	{
		using namespace doodle;
		push_settings();

		double width_divide6 = Width / 6.0;
		double height_divide5 = Height / 5.0;
		int ClickedPosX{ doodle::get_mouse_x() + Engine::GetWindow().GetSize().x / 2 };
		int ClickedPosY{ Engine::GetWindow().GetSize().y - (doodle::get_mouse_y() + Engine::GetWindow().GetSize().y / 2) };
		if (isHoldingCharacter == false) 
		{
			ClickedPos = { -1,-1 };
		}

		if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
		{
			if (ClickedPosX >= width_divide6 * 1 && ClickedPosX <= width_divide6 * 3 &&
				ClickedPosY >= height_divide5 * 1 && ClickedPosY <= height_divide5 * 4) {
				ClickedPos = { static_cast<int>(ClickedPosX / width_divide6) - 1, static_cast<int>(ClickedPosY / height_divide5) - 1 };
				if (battlefield.battleFieldVector[ClickedPos.x][ClickedPos.y].GetAllyType() == BattleAllyType::CHARACTER && player_hand.DoesAnyCardClicked() == false) 
				{
					isHoldingCharacter = true;
					// Engine::GetLogger().LogEvent("You start to pick an object at {" + std::to_string(ClickedPos.x) + ", " + std::to_string(ClickedPos.y) + '}');
				}
			}
		}
		if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left))
		{
			if (ClickedPosX >= width_divide6 * 1 && ClickedPosX <= width_divide6 * 3 &&
				ClickedPosY >= height_divide5 * 1 && ClickedPosY <= height_divide5 * 4) 
			{
				if (isHoldingCharacter) 
				{
					int ReleasedX = static_cast<int>((doodle::get_mouse_x() + Engine::GetWindow().GetSize().x / 2) / width_divide6) - 1;
					int ReleasedY = static_cast<int>((Engine::GetWindow().GetSize().y - (doodle::get_mouse_y() + Engine::GetWindow().GetSize().y / 2)) / height_divide5)-1;
					if (ClickedPos != Vector2DInt{ ReleasedX, ReleasedY }) 
					{
						battlefield.SwapObject(ClickedPos, { ReleasedX, ReleasedY });
					}
					Engine::GetLogger().LogEvent("Object what at {" + std::to_string(ClickedPos.x) + ", " + std::to_string(ClickedPos.y) + "} is swapped with "+
					"object what at {"+ std::to_string(ReleasedX) + ", " + std::to_string(ReleasedY) + '}');

				}
			}
			isHoldingCharacter = false;
		}

		pop_settings();
	}

}

void Roster::Draw()
{
	// Engine::GetWindow().Clear({ 40, 213, 20, 255 });
	{
		using namespace doodle;
		push_settings();
		set_outline_color(HexColor(0xFFFFFF99));

		double width_divide6 = Width / 6.0;
		double height_divide5 = Height / 5.0;
		DrawGrid({ width_divide6 - Engine::GetWindow().GetSize().x / 2 ,height_divide5 - Engine::GetWindow().GetSize().y / 2 },
			2, 3, { width_divide6 ,height_divide5 });

		doodle::set_rectangle_mode(RectMode::Corner);
		set_fill_color(HexColor(0x000000B9));
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 3; j++) {
				if (i == ClickedPos.x && j == ClickedPos.y && isHoldingCharacter) {
					battlefield.battleFieldVector[i][j].GetCharacterData()->DrawImage({ static_cast<double>(get_mouse_x()),static_cast<double>(get_mouse_y()) });
					battlefield.battleFieldVector[i][j].GetCharacterData()->DrawHP({ static_cast<double>(get_mouse_x()),static_cast<double>(get_mouse_y()) });
					continue;
				}
				battlefield.battleFieldVector[i][j].DrawCharacter();
			}
		}

		pop_settings();
	}
}

bool Roster::getIsHoldingCharacter()
{
	return isHoldingCharacter;
}

Roster roster;