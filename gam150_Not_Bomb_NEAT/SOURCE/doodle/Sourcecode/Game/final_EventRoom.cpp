﻿// GAM150
// final_eventroom.cpp
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "final_EventRoom.h"
#include "cardPile.h"
#include "EventRoom.h"
#include "GameMap.h"
#include "hand.h"

void Final_EventRoom::Load()
{
	timer = 0;
	pTimer = 0;

	pEventPhase = event.GetEventPhase();

	if (event.GetRoomType() == -1) 
	{
		event.Load(doodle::random(0, 7));
		//event.Load(?);
	}
	else 
	{
		event.Load(event.GetRoomType());
	}

	LoadBackground();
}

void Final_EventRoom::Update([[maybe_unused]]double deltaTime)
{
	pTimer = timer;
	timer += deltaTime;

	if (event.GetEventPhase() != pEventPhase) 
	{
		pEventPhase = event.GetEventPhase();
		player_hand.Clear_hand();
		event.UpdateText();
		ResetTimer();
	}

	if (event.GetEventPhase() == 0) 
	{
		if (pTimer <= 0.2 && 0.2 < timer)
		{
			player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 13 });
		}

		if (pTimer <= 0.4 && 0.4 < timer)
		{
			player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 14 });
		}
	}
	else 
	{
		switch (static_cast<EVENTROOMTYPE>(event.GetRoomType()))
		{
		case EVENTROOMTYPE::HeroOffset:
			if (event.GetEventPhase() == 1) 
			{
				int i;
				for (i= 0; i < event.GetCharacterTypesVector().size(); i++) 
				{
					if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
					{
						player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), -event.GetCharacterTypesVector()[i] });
					}
				}
			}
			else if (event.GetEventPhase() == 2) 
			{
				int i;
				for (i = 0; i < 3; i++) 
				{
					if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
					{
						player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), i + 16 });
					}
				}
			}
			else if (event.GetEventPhase() == 3) 
			{
				int i;
				for (i = 0; i < 3; i++) 
				{
					if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
					{
						player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), i + 16 });
					}
				}
			}
			break;
		case EVENTROOMTYPE::CardDeleting:
			if (event.GetEventPhase() == 1) 
			{
				if (pTimer <= 0.2 && 0.2 < timer)
				{
					player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 12 });
				}
				else if (pTimer <= 0.4 && 0.4 < timer)
				{
					player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 10 });
				}
				else if (pTimer <= 0.6 && 0.6 < timer)
				{
					player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 20 });
				}
				else if (pTimer <= 0.8 && 0.8 < timer)
				{
					player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 21 });
				}
			}
			break;
		case EVENTROOMTYPE::CardFinding:
			break;
		case EVENTROOMTYPE::HealHerosWithPenalty:
			break;
		case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
			for (int i = 0; i < event.GetCharacterTypesVector().size(); i++) {
				if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
				{
					player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), -event.GetCharacterTypesVector()[i] });
				}
			}
			break;
		case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
			if (event.GetEventPhase() == 1) 
			{
				int i;
				for (i = 0; i < event.GetCharacterTypesVector().size(); i++) 
				{
					if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
					{
						player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), -event.GetCharacterTypesVector()[i] });
					}
				}
			}
			else if (event.GetEventPhase() == 2) 
			{
				int i;
				for (i = 0; i < 3; i++) 
				{
					if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
					{
						player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), i + 16 });
					}
				}
			}
			break;
		case EVENTROOMTYPE::AddRandomClassCards:
			break;
		case EVENTROOMTYPE::Boss_StatIncrease:
			int i;
			for (i = 0; i < 3; i++) 
			{
				if (pTimer <= (0.2 + i * 0.2) && (0.2 + i * 0.2) < timer)
				{
					player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), i + 16 });
				}
			}
		default:
			break;
		}
	}

	event.Update();
	player_hand.Update();

	if (event.GetIsEventEnd() == true) 
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::PROTOTYPE_MAPANDROOM);
		const int roomNum = Engine::GetGameMap().GetCurrentRoomNum();
		Engine::GetGameMap().SetIsRoomCleared(roomNum, true);
		if (Engine::GetGameMap().GetNeighborRoomNumbers(roomNum) == 0)
		{
			Engine::GetGameMap().SetIsMapCleared(true);
		}
	}
}

void Final_EventRoom::Draw()
{
	Engine::GetWindow().Clear({ 77, 77, 77, 255 });

	doodle::push_settings();
	doodle::apply_scale(Engine::GetWindow().GetSize().x / static_cast<double>(background1[0].texturePtr->GetSize().x),
		Engine::GetWindow().GetSize().y / static_cast<double>(background1[0].texturePtr->GetSize().y));
	background1.Draw();
	if (event.GetEventPhase() >= 1) 
	{
		background2.Draw(); // This means that if background2 does not have image, it does not draw anything
	}
	doodle::pop_settings();

	event.Draw();
	player_hand.Draw();
}

void Final_EventRoom::Unload()
{
	event.Unload();
	player_hand.Clear_hand();
	graveyard.Clear();
	background1.Unload();
	background2.Unload();
}

void Final_EventRoom::LoadBackground()
{
	background1.Unload();
	background2.Unload();

	background1.Add(PATH_EVENT_BACKGROUND_RAWIMAGE);
	switch (static_cast<EVENTROOMTYPE>(event.GetRoomType()))
	{
	case EVENTROOMTYPE::HeroOffset:
		break;
	case EVENTROOMTYPE::CardDeleting:
		background2.Add(PATH_CARDMANAGER_BACKGROUND_RAWIMAGE);
		break;
	case EVENTROOMTYPE::CardFinding:
		background2.Add(PATH_BATTLEFIELD_BACKGROUND_RAWIMAGE);
		break;
	case EVENTROOMTYPE::HealHerosWithPenalty:
		break;
	case EVENTROOMTYPE::IncreaseOneHerosMaxHP:
		break;
	case EVENTROOMTYPE::OffsetIncreaseWithPenalty:
		break;
	case EVENTROOMTYPE::AddRandomClassCards:
		break;
	default:
		break;
	}
}

void Final_EventRoom::ResetTimer()
{
	timer = 0;
	pTimer = 0;
}
