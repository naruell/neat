﻿// GAM150
// Ending.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#pragma once
#include "../Engine/GameState.h"

#include <memory>

struct TextBox;

class Ending : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() override { return "Ending"; }
private:
	std::unique_ptr<TextBox> endingBox;
};