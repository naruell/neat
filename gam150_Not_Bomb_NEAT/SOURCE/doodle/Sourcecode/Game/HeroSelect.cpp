﻿// GAM150
// HeroSelect.cpp
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "HeroSelect.h"
#include "../Engine/helper.h"
#include "card.h"

void HeroSelect::Load()
{
	characterCardSpt = std::make_unique<doodle::Image>("Assets/Graphics/HeroSelection/characterCard.png");

	characterHPSpt = std::make_unique<doodle::Image>("Assets/Characters/CharacterStats/Heart.png");
	characterADSpt = std::make_unique<doodle::Image>("Assets/Characters/CharacterStats/Sword.png");
	characterAPSpt = std::make_unique<doodle::Image>("Assets/Characters/CharacterStats/Rod.png");
	characterMovementSpt = std::make_unique<doodle::Image>("Assets/Characters/CharacterStats/Shoes.png");
	characterHealSpt = std::make_unique<doodle::Image>("Assets/Characters/CharacterStats/heal.png");
}

void HeroSelect::Update()
{
	int width_divide{ Engine::GetWindow().GetSize().x / (windowSeparatorX + 2) };
	int height_divide{ Engine::GetWindow().GetSize().y / (windowSeparatorY + 2) };

	// 인포창 띄워주는 상태(호버드)로 바꾸는 함수는 밑으로
	int VecSize = static_cast<int>(selectable.size());
	for (int i = 0; i < VecSize; i++) {
		selectable[i]->Update(OBJECTSTATE::DEFAULT);
	}

	if (((Engine::GetInput().GetMousePos().x + Engine::GetWindow().GetSize().x / 2 - (width_divide / 2)) * width_divide) < 0 ||
		((Engine::GetInput().GetMousePos().y - Engine::GetWindow().GetSize().y / 2 + height_divide / 2) * -height_divide) < 0)
	{
		return;
	}
	int ClickedPosX{ (Engine::GetInput().GetMousePos().x + Engine::GetWindow().GetSize().x / 2 - (width_divide / 2)) / width_divide };
	int ClickedPosY{ (Engine::GetInput().GetMousePos().y - Engine::GetWindow().GetSize().y / 2 + height_divide / 2) / -height_divide };
	if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left)) 
	{
		// Engine::GetLogger().LogEvent(std::to_string(ClickedPosX) + "  " + std::to_string(ClickedPosY));
		if (0 <= ClickedPosX && ClickedPosX < rowsPicking && 0 <= ClickedPosY && ClickedPosY < columnsPicking) {
			bool isMaking = true;
			int selectingSize{ static_cast<int>(selecting.size()) };
			for (int i = 0; i < selectingSize; i++) {
				if (selecting[i] == Vector2DInt{ ClickedPosX, ClickedPosY }) {
					selecting.erase(selecting.begin() + i);
					// Engine::GetLogger().LogEvent("Deleted");
					isMaking = false;
					break;
				}
			}
			if (isMaking && selectingSize < maxHeroPicking) {
				selecting.push_back({ ClickedPosX ,ClickedPosY });
				// Engine::GetLogger().LogEvent("Added");
			}
		}
	}
	else if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Right))
	{
		if (0 <= ClickedPosX && ClickedPosX < rowsPicking && 0 <= ClickedPosY && ClickedPosY < columnsPicking) {
			isDrawingInfo = true;
			indexWhereDrawingInfo = { ClickedPosX ,ClickedPosY };
			UpdateCardList();
		}
		else { isDrawingInfo = false; }
	}

	int cardListSize{ static_cast<int>(cardList.size()) };
	for (int i = 0; i < cardListSize; i++)
	{
		cardList[i]->SetExplanation();
		cardList[i]->Update();

		if (isRectCollided(Vector2D{ Engine::GetInput().GetMousePos() },
			{ cardList[i]->geometry.posX - cardList[i]->geometry.width / 2.0,cardList[i]->geometry.posY - cardList[i]->geometry.height / 2.0 },
			{ cardList[i]->geometry.posX + cardList[i]->geometry.width / 2.0,cardList[i]->geometry.posY + cardList[i]->geometry.height / 2.0 }))
		{
			cardList[i]->card_state = Card_State::HOVERED;
			cardList[i]->pCard_state = Card_State::HOVERED;
		}
	}
}

void HeroSelect::Draw()
{
	auto width_divide{ Engine::GetWindow().GetSize().x / (windowSeparatorX + 2) };
	auto height_divide{ Engine::GetWindow().GetSize().y / (windowSeparatorY + 2) };

	using namespace doodle;

	// Draw grid
	push_settings();
	set_outline_color(HexColor(0xFFFFFFFF));
	DrawGrid<Vector2DInt>({ -Engine::GetWindow().GetSize().x / 2 + (width_divide / 2), -Engine::GetWindow().GetSize().y / 2 + static_cast<int>((height_divide * 2.5)) },
		rowsPicking, columnsPicking, { width_divide, height_divide });
	pop_settings();

	// Drawing selection
	push_settings();
	set_fill_color(HexColor(0xFFFFFF99));
	int selectingSize{ static_cast<int>(selecting.size()) };
	for (int i = 0; i < selectingSize; i++) {
		doodle::set_rectangle_mode(doodle::RectMode::Corner);
		doodle::draw_rectangle(-Engine::GetWindow().GetSize().x / 2 + (width_divide/2) + (width_divide * selecting[i].x),
			Engine::GetWindow().GetSize().y / 2 - ((height_divide * 1.5) + (height_divide * selecting[i].y)),
			width_divide, height_divide);
	}
	pop_settings();

	// Draw characters
	int VecSize = static_cast<int>(selectable.size());
	for (int i = 0; i < VecSize; i++) {
		selectable[i]->DrawImage({ static_cast<double>(width_divide * ((i% HeroSelect::rowsPicking) + 1)) - Engine::GetWindow().GetSize().x / 2,
			Engine::GetWindow().GetSize().y / 2 - static_cast<double>(height_divide * ((i / (HeroSelect::rowsPicking)) + 1)) });
	}

	// Drawing Character Info
	if (isDrawingInfo == true)
	{
		//배경

		doodle::push_settings();
		doodle::apply_scale(Engine::GetWindow().GetSize().x / 1920.0, Engine::GetWindow().GetSize().y / 1080.0);
		doodle::draw_image(*characterCardSpt, 379.0, 165.0);
		doodle::pop_settings();

		doodle::set_font_size(30);
		double magniX{ Engine::GetWindow().GetSize().x / (1920.0 / 1.25) };
		double magniY{ Engine::GetWindow().GetSize().y / (1080.0 / 1.25) };
		doodle::draw_text(Int2CharacterTypeString(static_cast<int>(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->GetCharacterType())), 289 * magniX, 280 * magniY);
		doodle::set_font_size(20);
		int Xpos{ 285 };
		int Xpos2{ 290 };
		int Ypos{ 185 };
		int Ypos2{ 237 };
		int adder{ 92 };
		selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->DrawImage({ 0, 270 * magniY });

		doodle::draw_text(std::to_string(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->original_HP), Xpos * magniX, Ypos * magniY);
		doodle::draw_image(*characterHPSpt, Xpos2 * magniX, Ypos2 * magniY);
		Xpos += adder;
		Xpos2 += adder;
		doodle::draw_text(std::to_string(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->currentOffset.adOffset), Xpos * magniX, Ypos * magniY);
		doodle::draw_image(*characterADSpt, Xpos2 * magniX, Ypos2 * magniY);
		Xpos += adder;
		Xpos2 += adder;
		doodle::draw_text(std::to_string(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->currentOffset.apOffset), Xpos * magniX, Ypos * magniY);
		doodle::draw_image(*characterAPSpt, Xpos2 * magniX, Ypos2 * magniY);
		Xpos += adder;
		Xpos2 += adder;
		doodle::draw_text(std::to_string(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->currentOffset.movementOffset), Xpos * magniX, Ypos * magniY);
		doodle::draw_image(*characterMovementSpt, Xpos2 * magniX, Ypos2 * magniY);
		Xpos += adder;
		Xpos2 += adder;
		doodle::draw_text(std::to_string(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->currentOffset.healOffset), Xpos * magniX, Ypos * magniY);
		doodle::draw_image(*characterHealSpt, Xpos2 * magniX, Ypos2 * magniY);

		int cardListSize{ static_cast<int>(cardList.size()) };
		for (int i = 0; i < cardListSize; i++) {
			if (cardList[i].get()->card_state == Card_State::HOVERED)
			{
				continue;
			}
			cardList[i].get()->Draw();
		}
		for (int i = 0; i < cardListSize; i++) {
			if (cardList[i].get()->card_state == Card_State::HOVERED)
			{
				cardList[i].get()->Draw();
			}
		}
	}
}

void HeroSelect::Unload()
{
	selecting.clear();

	int selectableSize{ static_cast<int>(selectable.size()) };
	for (int i = 0; i < selectableSize; i++) {
		selectable[i] = nullptr;
	}
	selectable.clear();
}

std::vector<Character*> HeroSelect::getSelectableVector()
{
	return selectable;
}

std::vector<Vector2DInt> HeroSelect::getSelectingVector()
{
	return selecting;
}

void HeroSelect::AddCharacters(Character* add)
{
	selectable.push_back(add);
}

void HeroSelect::UpdateCardList()
{
	cardList.clear();
	double magniX{ Engine::GetWindow().GetSize().x / (1920.0 / 1.25) };
	double magniY{ Engine::GetWindow().GetSize().y / (1080.0 / 1.25) };
	for (int i = 0; i < 3; i++) {
		cardList.emplace_back(Vector2Card({ static_cast<int>(selectable[indexWhereDrawingInfo.x + (indexWhereDrawingInfo.y * rowsPicking)]->GetCharacterType()), i }));
		cardList[i].get()->geometry.posX = i * (cardList[i].get()->geometry.width * 1.2) * magniX;
		cardList[i].get()->geometry.posY = 47 * magniY;
		cardList[i].get()->geometry.angle = 0;
		cardList[i].get()->geometry.cur_posX = 0;
		cardList[i].get()->geometry.cur_posY = 0;
		cardList[i].get()->card_state = Card_State::NORMAL;
	}
}

HeroSelect heroSelect;