﻿// GAM150
// hand.cpp
// Team Neat
// Primary : Duhwan Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "hand.h"

#include <iostream>
#include <doodle/angle.hpp>
#include <doodle/input.hpp>
#include <doodle/drawing.hpp>

#include "../Engine/Engine.h"
#include "../Engine/Input.h"
#include "../Engine/helper.h"
#include "../Engine/Geometry/Vector2D.h"

#include "battlefield.h"
#include "cardPile.h"
#include "GameClock.h"

// 코딩스탠다드 예외 케이스
#include "cardEffectFunction.h"

Player_Hand::Player_Hand()
{
	cardList.emplace_back(new Card());
	MoveCardActivateZone({0, -365});
}

/*void Player_Hand::Run()
{
	Update();
	Draw();
}*/

void Player_Hand::Draw_card(std::pair<int, int> id, int amount, int index)
{
	cardList.emplace_back(Vector2Card(id));

	++index;
	switch (amount % 2)
	{
	case 0:
		if (index <= amount / 2)
		{
			cardList.back()->geometry.draw_checkPoint_posX = (amount / 2 + 1 - index) * cardList.back()->geometry.width * cardList.back()->geometry.hoveredTextScale * -1.0f + cardList.back()->geometry.width * cardList.back()->geometry.hoveredTextScale / 2.0f;
		}

		else
		{
			cardList.back()->geometry.draw_checkPoint_posX = (index - amount / 2) * cardList.back()->geometry.width * cardList.back()->geometry.hoveredTextScale - cardList.back()->geometry.width * cardList.back()->geometry.hoveredTextScale / 2.0f;
		}
		break;

	case 1:
		if (index == amount / 2 + 1)
		{
			cardList.back()->geometry.draw_checkPoint_posX = 0.0f;
		}

		else if (index <= amount / 2)
		{
			cardList.back()->geometry.draw_checkPoint_posX = (amount / 2 + 1 - index) * cardList.back()->geometry.width * cardList.back()->geometry.hoveredTextScale * -1.0f;
		}

		else
		{
			cardList.back()->geometry.draw_checkPoint_posX = (index - amount / 2 - 1) * cardList.back()->geometry.width * cardList.back()->geometry.hoveredTextScale;
		}
		break;
	}

	cardList.back()->card_state = Card_State::DRAWING;
	cardList.back()->drawOffset += drawOffset;
	cardList.back()->geometry.draw_checkPoint_posY = 200.0f;
}

void Player_Hand::Draw_from_deck(int amount)
{
	if (amount <= 0) { return; }
	
	if(deck.GetSize() < amount)
	{
		graveyard.Shuffle();

		int graveyardSize = graveyard.GetSize();

		if(graveyardSize == 0)
		{
			amount = deck.GetSize();
			Engine::GetEffectSystem().GenerateTextEffect("No more cards are remaining in both Deck and Graveyard!!", { 0, 0 }, doodle::Color(255), 20, 3);
		}
		else
		{
			for (int i = graveyardSize - 1; i >= 0; --i)
			{
				deck.Push_back(graveyard.MoveCard(i));
			}
		}
	}

	if(deck.GetSize() < amount)
	{
		amount = deck.GetSize();
		Engine::GetEffectSystem().GenerateTextEffect("No more cards are remaining in both Deck and Graveyard!!", { 0, 0 }, doodle::Color(255), 20, 3);
	}
	
	for (int i = 0; i < amount; ++i)
	{
		std::pair<int, int> id = deck.MoveCard();
		Draw_card(id, amount, i);

		if(activatingCardIndex == -1)
		{
			if (Get_handSize() > current_maxHandSize)
			{
				cardList.back()->doesUsingTime = false;
				cardList.back()->ForceToDiscard();
			}
		}
		else
		{
			if (Get_handSize() > current_maxHandSize + 1)
			{
				cardList.back()->doesUsingTime = false;
				cardList.back()->ForceToDiscard();
			}
		}
	}
}

void Player_Hand::Remove_card(int realIndex)
{
	if (realIndex == 0)
	{
		std::cout << "Warnning! you are trying to remove Criteria card!!\n";
		return;
	}

	else if (realIndex == -1)
	{
		if (cardList.size() <= 1)
		{
			std::cout << "Error : Amount of cards on the hand is zero!\n";
			return;
		}

		if (cardList[cardList.size() - 1]->card_state == Card_State::HOVERED)
		{
			hoveredCardIndex = -1;
		}

		cardList.erase(cardList.end() - 1);
		std::cout << "Last card has been successfully removed!!\n";
		return;
	}

	int handSize = Get_real_handSize();
	if (realIndex >= handSize)
	{
		std::cout << "Error : Out of range!!\n";
		return;
	}

	if (cardList[realIndex]->card_state == Card_State::HOVERED)
	{
		hoveredCardIndex = -1;
	}

	cardList.erase(cardList.begin() + realIndex);
	std::cout << realIndex << "th card has been successfully removed!!\n";
}

void Player_Hand::Clear_hand()
{
	int handSize = Get_handSize();
	for (int i = handSize; i > 0; --i)
	{
		player_hand.Remove_card(i);
	}
	std::cout << "All cards in the hand has been successfully removed!!\n";
}

int Player_Hand::Get_handSize()
{
	return static_cast<int>(cardList.size() - 1);
}

void Player_Hand::SetLastCardCasterIndex(Vector2DInt indexTo)
{
	lastCardCasterIndex = indexTo;
}

Vector2DInt Player_Hand::GetLastCardCasterIndex()
{
	return lastCardCasterIndex;
}

void Player_Hand::MoveCardActivateZone(Vector2D offset)
{
	Vector2D pDrawOffset = drawOffset;
	drawOffset = offset;

	Vector2D changedOffest = drawOffset - pDrawOffset;
	for (auto& card : cardList)
	{
		card->drawOffset += changedOffest;
	}
}

std::unique_ptr<Card>& Player_Hand::GetCard(int index)
{
	return cardList[index];
}

const int Player_Hand::GetLastCardIndex() const noexcept
{
	return static_cast<int>(cardList.size()) - 1;
}

void Player_Hand::Set_current_manxHnadSize(int sizeTo)
{
	current_maxHandSize = sizeTo;
}

void Player_Hand::Unload()
{
	current_maxHandSize = original_maxHandSize;
}

int Player_Hand::Get_real_handSize()
{
	return static_cast<int>(cardList.size());
}

bool Player_Hand::DoesAnyCardHovered() const noexcept
{
	return (hoveredCardIndex != -1);
}

bool Player_Hand::DoesAnyCardClicked() const noexcept
{
	return (clickedCardIndex != -1);
}

bool Player_Hand::DoesAnyCardTargeting() const noexcept
{
	return (targetingCardIndex != -1);
}

void Player_Hand::Arrange()
{
	int handSize = Get_handSize();
	if (handSize <= 0) {
		return;
	}

	Card_Geometry geometry = cardList[0]->geometry;

	switch (handSize % 2)
	{
	case 0:
		for (int i = 1; i <= handSize; ++i)
		{
			if (i <= handSize / 2)
				// x축 보다 왼쪽에 있는 카드들 
			{
				cardList[i]->geometry.posX = cardList[i]->GetOffset().x +  (handSize / 2 + 1 - i) * geometry.width * -1.0 + geometry.width / 2.0;
				cardList[i]->geometry.posY = cardList[i]->GetOffset().y + static_cast<double>(((geometry.width / 2.5 / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) - (((geometry.width / 2.5f / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) * cos(36 / doodle::PI * (handSize / 2 + 1 - i) / 2) / 2)) + 100.0;
				cardList[i]->geometry.angle = (handSize / 2 + 1 - i) / 6.0;
				cardList[i]->geometry.width = geometry.width;
				cardList[i]->geometry.height = geometry.height;
			}

			else
				// x축 보다 오른쪽에 있는 카드들
			{
				cardList[i]->geometry.posX = cardList[i]->GetOffset().x + (i - handSize / 2) * geometry.width - geometry.width / 2.0;
				cardList[i]->geometry.posY = cardList[i]->GetOffset().y + static_cast<double>(((geometry.width / 2.5f / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) - (((geometry.width / 2.5f / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) * cos(36 / doodle::PI * (i - handSize / 2) / 2) / 2)) + 100.0;
				cardList[i]->geometry.angle = -(i - handSize / 2) / 5.0;
				cardList[i]->geometry.width = geometry.width;
				cardList[i]->geometry.height = geometry.height;
			}
		}
		break;

	case 1:
		for (int i = 1; i <= handSize; ++i)
		{
			if (i == handSize / 2 + 1)
			{
				cardList[i]->geometry.posX = cardList[i]->GetOffset().x + 0.0;
				cardList[i]->geometry.posY = cardList[i]->GetOffset().y + -25.0 + 100.0;
				cardList[i]->geometry.width = geometry.width;
				cardList[i]->geometry.height = geometry.height;
				cardList[i]->geometry.angle = 0.0;
			}

			else if (i <= handSize / 2)
			{
				cardList[i]->geometry.posX = cardList[i]->GetOffset().x + (handSize / 2 + 1 - i) * geometry.width * -1.0;
				cardList[i]->geometry.posY = cardList[i]->GetOffset().y + static_cast<double>(((geometry.width / 2.5 / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) - (((geometry.width / 2.5f / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) * cos(36 / doodle::PI * (handSize / 2 + 1 - i) / 2) / 2)) + 100.0;
				cardList[i]->geometry.width = geometry.width;
				cardList[i]->geometry.height = geometry.height;
				cardList[i]->geometry.angle = (handSize / 2 + 1 - i) / 5.0;
			}

			else
			{
				cardList[i]->geometry.posX = cardList[i]->GetOffset().x + (i - handSize / 2 - 1) * geometry.width;
				cardList[i]->geometry.posY = cardList[i]->GetOffset().y + static_cast<double>(((geometry.width / 2.5f / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) - (((geometry.width / 2.5f / cos(36 / doodle::PI)) / sin(36 / doodle::PI)) * cos(36 / doodle::PI * (i - handSize / 2 - 1) / 2) / 2)) + 100.0;
				cardList[i]->geometry.width = geometry.width;
				cardList[i]->geometry.height = geometry.height;
				cardList[i]->geometry.angle = -(i - handSize / 2 - 1) / 5.0;
			}
		}
		break;
	}

	if (clickedCardIndex != -1)
	{
		for (int i = 1; i <= handSize; ++i)
		{
			cardList[i]->geometry.posY -= 60.0;
		}
	}
}

void Player_Hand::Update()
{
	// Prevent Criteria Card Being Removed
	if (cardList[0]->current_name != "Noname")
	{
		Engine::GetLogger().LogError("Warnning! Criteria Card has been removed, or moved into another index!!");
	}
	hoveredCardIndex = -1;
	clickedCardIndex = -1;
	targetingCardIndex = -1;
	activatingCardIndex = -1;

	int handSize = Get_handSize();
	// 삭제할 카드 먼저 체크
	for (int i = handSize; i > 0; --i)
	{
		if (cardList[i]->card_state == Card_State::ACTIVATED)
		{
			graveyard.Push_back(cardList[i]->GetCardID());
			Remove_card(i);
			handSize = Get_handSize();
		}
	}

	// Check Index of Specific Card States
	for (int i = handSize; i > 0; --i)
	{
		if (cardList[i]->card_state == Card_State::HOVERED)
		{
			if (hoveredCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are being hovered!");
			}
			hoveredCardIndex = i;
		}
		else if (cardList[i]->card_state == Card_State::CLICKED)
		{
			if (clickedCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are being clicked!");
			}
			clickedCardIndex = i;
		}
		else if (cardList[i]->card_state == Card_State::TARGETING)
		{
			if (targetingCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are on targeting!");
			}
			targetingCardIndex = i;
		}
		else if (cardList[i]->card_state == Card_State::ACTIVATING)
		{
			if (activatingCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are on activating!");
			}
			activatingCardIndex = i;
		}

	}
	if (targetingCardIndex == -1)
	{
		lastCardCasterIndex = { -1, -1 };
	}

	int mouseX = doodle::get_mouse_x();
	int mouseY = doodle::get_mouse_y();

	// 개별 카드 업데이트 시작
	for (int i = 1; i <= handSize; ++i)
	{
		std::unique_ptr<Card> &card = cardList[i];

		Battlefield::BattleObject& hero = battlefield.battleFieldVector[battlefield.getSelection().x][battlefield.getSelection().y];
		if(hero.GetCharacterData() != nullptr)
		{
			card->SetExplanation(&hero);
		}
		else
		{
			card->SetExplanation();
		}
		
		card->Update();

		if (card->card_state == Card_State::NORMAL && clickedCardIndex == -1 && targetingCardIndex == -1 &&
			card->geometry.posX - card->geometry.width / 2.0f <= mouseX && mouseX < card->geometry.posX + card->geometry.width / 2.0f &&
			card->geometry.posY - card->geometry.height / 2.0f <= mouseY && mouseY < card->geometry.posY + card->geometry.height / 2.0f)
		{
			card->OnHovered();
		}

		if ((card->card_state == Card_State::HOVERED || card->pCard_state == Card_State::CLICKED) &&
			Engine::GetInput().IsMouseDown(Input::MouseButton::Left) == true)
		{
			card->OnClicked();
		}

		if (card->pCard_state == Card_State::CLICKED && Engine::GetInput().IsMouseUp(Input::MouseButton::Left) == true)
		{
			// Checking Card Activating Zone
			if (cardActivateZone.posX - cardActivateZone.width / 2.0f + drawOffset.x >= mouseX || mouseX > cardActivateZone.posX + cardActivateZone.width / 2.0f + drawOffset.x ||
				cardActivateZone.posY - cardActivateZone.height / 2.0f + drawOffset.y >= mouseY || mouseY > cardActivateZone.posY + cardActivateZone.height / 2.0f + drawOffset.y)
			{
				Engine::GetLogger().LogEvent("Card has been successfully activated!");
				if (card->current_type == Card_Type::UI)
				{
					card->card_state = Card_State::TARGETING;
				}

				// 남아있는 코스트 체크
				else if (card->current_spendingTime > GameClock::GetRemainingTime())
				{
					Engine::GetLogger().LogError("There's no time!");
					Engine::GetEffectSystem().GenerateTextEffect("There's no time!", { 0, 0 }, doodle::Color(255), 20, 3);
					card->card_state = Card_State::NORMAL;
				}

				// 카드 시전하는 영웅 체크
				else if (battlefield.battleFieldVector[battlefield.getSelection().x][battlefield.getSelection().y].GetAllyType() != BattleAllyType::CHARACTER ||
					     battlefield.battleFieldVector[battlefield.getSelection().x][battlefield.getSelection().y].GetCharacterData()->current_HP <= 0)
				{
					Engine::GetLogger().LogError("You have to select the card caster hero");
					Engine::GetEffectSystem().GenerateTextEffect("You have to select the card caster hero", { 0, 0 }, doodle::Color(255), 20, 3);
					card->card_state = Card_State::NORMAL;
				}

				// 선택된 영웅의 행동 가능 여부 체크
				else if (battlefield.battleFieldVector[battlefield.getSelection().x][battlefield.getSelection().y].GetCharacterData()->GetActableCount() <= 0)
				{
					Engine::GetLogger().LogError("That hero cannot do anything more this turn.");
					Engine::GetEffectSystem().GenerateTextEffect("That hero cannot do anything more this turn.", { 0, 0 }, doodle::Color(255), 20, 3);
					card->card_state = Card_State::NORMAL;
				}

				else
				{
					Engine::GetLogger().LogEvent("Card has been successfully activated!");
					card->SetCardCastingHeroIndex({ battlefield.getSelection().x ,  battlefield.getSelection().y });
					SetLastCardCasterIndex({ battlefield.getSelection().x, battlefield.getSelection().y });
					card->card_state = Card_State::TARGETING;
				}
			}

			else
			{
				card->card_state = Card_State::NORMAL;
			}
		}

		///////////////////////////////////////////////////////////////////////// 타겟팅
		if (card->pCard_state == Card_State::TARGETING && card->card_state == Card_State::TARGETING)
		{
			if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Right))
			{
				card->card_state = Card_State::NORMAL;
			}

			if (battlefield.battleFieldVector.size() <= 0)
			{
				continue;
			}

			const int targetAmount = static_cast<int>(card->GetTargetAndEffectMap().size());
			int confirmedTargetAmount = 0;
			BattleAllyType targetType;

			for (auto& targetAndEffect : card->GetTargetAndEffectMap())
			{
				if (targetAndEffect.second.GetAllyType() != BattleAllyType::NONE)
				{
					++confirmedTargetAmount;
				}
				else if (targetAndEffect.first.targetType == Card_Target::NONE)
				{
					++confirmedTargetAmount;
				}

				else
				{
					targetType = targetAndEffect.first.targetAllyType;

					int rowFieldSize{ static_cast<int>(battlefield.battleFieldVector.size()) };
					int columnFieldSize{ static_cast<int>(battlefield.battleFieldVector[0].size()) };

					switch (targetAndEffect.first.targetType)
					{
					case Card_Target::NONE:
						break;

					case Card_Target::EMPTY:
						
						// 이동 가능한 인덱스가 지정되어 있을 때
						if (targetAndEffect.first.moveableIndex.size() != 0)
						{
							// 모든 배틀오브젝트 잠금
							for (int x = 0; x < rowFieldSize; ++x)
							{
								for (int y = 0; y < columnFieldSize; ++y)
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(false);
								}
							}

							std::vector<Vector2DInt> availalbeIndex;
							for (Vector2DInt vec : targetAndEffect.first.moveableIndex) { availalbeIndex.push_back(vec + GetLastCardCasterIndex()); }

							// 이동 가능한 인덱스만 설정 할 수 있게 함
							for (Vector2DInt vec : availalbeIndex)
							{
								if (0 <= vec.x && vec.x < rowFieldSize / 2 &&
									0 <= vec.y && vec.y < columnFieldSize &&
									battlefield.battleFieldVector[vec.x][vec.y].GetAllyType() == BattleAllyType::EMPTY)
								{
									battlefield.battleFieldVector[vec.x][vec.y].SetIsSelectAble(true);
								}
							}
						}
						else
						{
							for (int x = 0; x < rowFieldSize; ++x)
							{
								for (int y = 0; y < columnFieldSize; ++y)
								{
									if (battlefield.battleFieldVector[x][y].GetAllyType() == BattleAllyType::EMPTY)
									{
										battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
									}
									else
									{
										battlefield.battleFieldVector[x][y].SetIsSelectAble(false);
									}
								}
							}
						}
						
						break;

					case Card_Target::CHARACTER:
						for (int x = 0; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[x][y].GetAllyType() == BattleAllyType::CHARACTER)
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
								}
								else
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(false);
								}
							}
						}
						break;

					case Card_Target::ENEMY:
						for (int x = 0; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[x][y].GetAllyType() == BattleAllyType::ENEMY)
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
								}
								else
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(false);
								}
							}
						}
						break;

					case Card_Target::ALL:
						for (int x = 0; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[x][y].GetAllyType() != BattleAllyType::EMPTY)
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
								}
								else
								{
									battlefield.battleFieldVector[x][y].SetIsSelectAble(false);
								}
							}
						}
						break;


					case Card_Target::LINE_CHARACTER:
						for (int x = 0; x < rowFieldSize / 2; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
							}
						}
						battlefield.selectMode = Battlefield::SelectMode::Line;
						break;

					case Card_Target::LINE_ENEMY:
						for (int x = rowFieldSize / 2; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
							}
						}
						battlefield.selectMode = Battlefield::SelectMode::Line;
						break;

					case Card_Target::LINE_ALL:
						for (int x = 0; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								battlefield.battleFieldVector[x][y].SetIsSelectAble(true);
							}
						}
						battlefield.selectMode = Battlefield::SelectMode::Line;
						break;
					}
					/////////// 특수 조건 체크 코드
					// 
					// ex) 체력이 절반 이하인 적을 목표로 한다던지...
					///////////////////////////////

					Vector2DInt releasedIndex = battlefield.releasedBattleObjectIndex;

					
					Vector2DInt randomCharecterPos;
					Vector2DInt randomEnemyPos;
					Vector2DInt randomAllPos;
					bool isMoreTargetAndEffectMapNeeded = false;

					switch (targetAndEffect.first.targetType)
					{
					case Card_Target::NONE:
						break;
					case Card_Target::EMPTY:
					case Card_Target::CHARACTER:
					case Card_Target::ENEMY:
						if (releasedIndex != Vector2DInt{ -1, -1 })
						{
							if (battlefield.battleFieldVector[releasedIndex.x][releasedIndex.y].GetAllyType() == targetType)
							{
								targetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][releasedIndex.y];
							}
						}
						break;

					case Card_Target::ALL:
						if (releasedIndex != Vector2DInt{ -1, -1 })
						{
							if (battlefield.battleFieldVector[releasedIndex.x][releasedIndex.y].GetAllyType() != BattleAllyType::EMPTY)
							{
								targetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][releasedIndex.y];
							}
						}
						break;


					case Card_Target::LINE_CHARACTER:
						if (releasedIndex != Vector2DInt{ -1, -1 })
						{
							if (0 <= releasedIndex.x && releasedIndex.x <= 1)
							{
								isMoreTargetAndEffectMapNeeded = false;
								for (int y = 0; y < columnFieldSize; ++y)
								{
									if (battlefield.battleFieldVector[releasedIndex.x][y].GetAllyType() == BattleAllyType::CHARACTER)
									{
										if (isMoreTargetAndEffectMapNeeded == false)
										{
											targetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][y];
											isMoreTargetAndEffectMapNeeded = true;
										}
										else
										{
											auto copiedTargetAndEffect = targetAndEffect;
											copiedTargetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][y];

											card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
										}
									}
								}
								if (isMoreTargetAndEffectMapNeeded == false)
								{
									card->ForceToDiscard();
									break;
								}
							}
						}
						break;

					case Card_Target::LINE_ENEMY:
						if (releasedIndex != Vector2DInt{ -1, -1 })
						{
							if (2 <= releasedIndex.x && releasedIndex.x <= 3)
							{
								isMoreTargetAndEffectMapNeeded = false;
								for (int y = 0; y < columnFieldSize; ++y)
								{
									if (battlefield.battleFieldVector[releasedIndex.x][y].GetAllyType() == BattleAllyType::ENEMY)
									{
										if (isMoreTargetAndEffectMapNeeded == false)
										{
											targetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][y];
											isMoreTargetAndEffectMapNeeded = true;
										}
										else
										{
											auto copiedTargetAndEffect = targetAndEffect;
											copiedTargetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][y];

											card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
										}
									}
								}
								if (isMoreTargetAndEffectMapNeeded == false)
								{
									card->ForceToDiscard();
									break;
								}
							}
						}
						break;

					case Card_Target::LINE_ALL:
						if (releasedIndex != Vector2DInt{ -1, -1 })
						{
							isMoreTargetAndEffectMapNeeded = false;
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[releasedIndex.x][y].GetAllyType() != BattleAllyType::EMPTY)
								{
									if (isMoreTargetAndEffectMapNeeded == false)
									{
										targetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][y];
										isMoreTargetAndEffectMapNeeded = true;
									}
									else
									{
										auto copiedTargetAndEffect = targetAndEffect;
										copiedTargetAndEffect.second = battlefield.battleFieldVector[releasedIndex.x][y];

										card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
									}
								}
							}
							if (isMoreTargetAndEffectMapNeeded == false)
							{
								card->ForceToDiscard();
								break;
							}
						}
						break;


					case Card_Target::ALL_CHARACTER:
						isMoreTargetAndEffectMapNeeded = false;
						for (int x = 0; x < rowFieldSize / 2; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[x][y].GetAllyType() == BattleAllyType::CHARACTER)
								{
									if (isMoreTargetAndEffectMapNeeded == false)
									{
										targetAndEffect.second = battlefield.battleFieldVector[x][y];
										isMoreTargetAndEffectMapNeeded = true;
									}
									else
									{
										auto copiedTargetAndEffect = targetAndEffect;
										copiedTargetAndEffect.second = battlefield.battleFieldVector[x][y];

										card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
									}
								}
							}
						}
						if (isMoreTargetAndEffectMapNeeded == false)
						{
							card->ForceToDiscard();
							break;
						}
						break;

					case Card_Target::ALL_ENEMY:
						isMoreTargetAndEffectMapNeeded = false;
						for (int x = rowFieldSize / 2; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[x][y].GetAllyType() == BattleAllyType::ENEMY)
								{
									if (isMoreTargetAndEffectMapNeeded == false)
									{
										targetAndEffect.second = battlefield.battleFieldVector[x][y];
										isMoreTargetAndEffectMapNeeded = true;
									}
									else
									{
										auto copiedTargetAndEffect = targetAndEffect;
										copiedTargetAndEffect.second = battlefield.battleFieldVector[x][y];

										card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
									}
								}
							}
						}
						if (isMoreTargetAndEffectMapNeeded == false)
						{
							card->ForceToDiscard();
							break;
						}
						break;

					case Card_Target::ALL_ALL:
						isMoreTargetAndEffectMapNeeded = false;
						for (int x = 0; x < rowFieldSize; ++x)
						{
							for (int y = 0; y < columnFieldSize; ++y)
							{
								if (battlefield.battleFieldVector[x][y].GetAllyType() != BattleAllyType::EMPTY)
								{
									if (isMoreTargetAndEffectMapNeeded == false)
									{
										targetAndEffect.second = battlefield.battleFieldVector[x][y];
										isMoreTargetAndEffectMapNeeded = true;
									}
									else
									{
										auto copiedTargetAndEffect = targetAndEffect;
										copiedTargetAndEffect.second = battlefield.battleFieldVector[x][y];

										card->GetTargetAndEffectMap().insert(copiedTargetAndEffect);
									}
								}
							}
						}
						if (isMoreTargetAndEffectMapNeeded == false)
						{
							card->ForceToDiscard();
							break;
						}
						break;


					case Card_Target::RANDOM_CHARACTER:
						randomCharecterPos = battlefield.GetRandomAllyTarget();
						if (randomCharecterPos == Vector2DInt{ -1, -1 })
						{
							card->ForceToDiscard();
						}
						else
						{
							targetAndEffect.second = battlefield.battleFieldVector[randomCharecterPos.x][randomCharecterPos.y];
						}
						break;

					case Card_Target::RANDOM_ENEMY:
						randomEnemyPos = battlefield.GetRandomEnemyTarget();
						if (randomEnemyPos == Vector2DInt{ -1, -1 })
						{
							card->ForceToDiscard();
						}
						else
						{
							targetAndEffect.second = battlefield.battleFieldVector[randomEnemyPos.x][randomEnemyPos.y];
						}
						break;

					case Card_Target::RANDOM_ALL:
						randomAllPos = battlefield.GetRandomTarget();
						if (randomAllPos == Vector2DInt{ -1, -1 })
						{
							card->ForceToDiscard();
						}
						else
						{
							targetAndEffect.second = battlefield.battleFieldVector[randomAllPos.x][randomAllPos.y];
						}
						break;
					}
				}
			}

			if (confirmedTargetAmount == targetAmount)
			{
				Engine::GetLogger().LogEvent("Change Card State : Targeting -> Activating. All targets are confirmed.");
				card->card_state = Card_State::ACTIVATING;
				activatingCardIndex = i;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////

		if (card->pCard_state == Card_State::TARGETING && card->card_state == Card_State::ACTIVATING)
		{
			Engine::GetSFML().PlaySound(card->GetCardID());
			if (card->effectListOnActivated.size() != 0)
			{
				for (std::unique_ptr<Effect> &e : card->effectListOnActivated)
				{
					e->SetOrigin(Vector2D{ card->geometry.cur_posX , card->geometry.cur_posY });             /////// 임시값
					e->SetDuration(0.7f);                                                                    /////// 임시값
					Engine::GetEffectSystem().effectList.emplace_back(std::move(e));
					card->effectListOnActivated.resize(card->effectListOnActivated.size() - 1);
				}
			}

			if (card->doesUsingTime == true)
			{
				// spend time
				GameClock::ChangeCurrentTime(card->current_spendingTime);
			}

			// UI나 MOVE 카드가 아니라면
			if ((std::find(card->current_subtype.begin(), card->current_subtype.end(), Card_Subtype::MOVE) == card->current_subtype.end()) && (std::find(card->current_subtype.begin(), card->current_subtype.end(), Card_Subtype::UI) == card->current_subtype.end()) && (card->doesUsingTime == true))
			{
				if (battlefield.battleFieldVector[card->GetCardCastingHeroIndex().x][card->GetCardCastingHeroIndex().y].GetCharacterData() != nullptr)
				{
					// 행동불가로 만듬
					battlefield.battleFieldVector[card->GetCardCastingHeroIndex().x][card->GetCardCastingHeroIndex().y].GetCharacterData()->UpdateActableCount(-1);
				}
			}

			for (auto& targetAndEffect : card->GetTargetAndEffectMap())
			{
				// If card caster doesn't exist
				if (battlefield.battleFieldVector[card->GetCardCastingHeroIndex().x][card->GetCardCastingHeroIndex().y].GetCharacterData() == nullptr)
				{
					if (card->current_type != Card_Type::UI) 
					{
						card->ForceToDiscard();
						break;
					}
					else
					{
						targetAndEffect.first.effect(card->GetCardCastingHeroIndex(), targetAndEffect.second, targetAndEffect.first);
					}
				}
				else
				{
					targetAndEffect.first.effect(card->GetCardCastingHeroIndex(), targetAndEffect.second, targetAndEffect.first);
				}
			}
		}
	}
	Arrange();

	hoveredCardIndex = -1;
	clickedCardIndex = -1;
	targetingCardIndex = -1;

	// Check Index of Specific Card States
	for (int i = handSize; i > 0; --i)
	{
		if (cardList[i]->card_state == Card_State::HOVERED)
		{
			if (hoveredCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are being hovered!");
			}
			hoveredCardIndex = i;
		}
		else if (cardList[i]->card_state == Card_State::CLICKED)
		{
			if (clickedCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are being clicked!");
			}
			clickedCardIndex = i;
		}
		else if (cardList[i]->card_state == Card_State::TARGETING)
		{
			if (targetingCardIndex != -1)
			{
				Engine::GetLogger().LogError("Multiple Cards are on targeting!");
			}
			targetingCardIndex = i;
		}
	}
}

void Player_Hand::Draw()
{
	doodle::push_settings();
	doodle::set_rectangle_mode(doodle::RectMode::Center);
#ifdef _DEBUG
	// 판정 Range 
	doodle::no_fill();
	doodle::set_outline_color(255, 255);
	doodle::draw_rectangle(cardActivateZone.posX + drawOffset.x, cardActivateZone.posY + drawOffset.y, cardActivateZone.width, cardActivateZone.height);
	doodle::draw_ellipse(cardActivateZone.posX + drawOffset.x, cardActivateZone.posY + drawOffset.y, 3, 3);
#endif
	doodle::pop_settings();

	int handSize = Get_real_handSize();
	for (int i = 1; i < handSize; ++i)
	{
		if (cardList[i]->card_state != Card_State::HOVERED)
		{
			cardList[i]->Draw();
		}
	}

	if (hoveredCardIndex != -1)
	{
		cardList[hoveredCardIndex]->Draw();
	}
}

Player_Hand player_hand;