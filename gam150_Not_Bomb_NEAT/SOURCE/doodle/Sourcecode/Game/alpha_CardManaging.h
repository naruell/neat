﻿#pragma once
// GAM150
// alpha_CardManaging.h
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/Background.h"
#include "CardManager.h"

class Alpha_CardManaging : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() { return "Alpha_CardManaging"; }

private:
	Background background;
};