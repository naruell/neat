﻿#pragma once
// GAM150
// final_eventRoom.h
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/Background.h"

class Final_EventRoom : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	void LoadBackground();
	void ResetTimer();

	std::string GetName() { return "Final_EventRoom"; }

private:
	Background background1;
	Background background2;

	int pEventPhase;
};