﻿// GAM150
// Ending.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Ending.h"

#include "../Engine/Engine.h"
#include "../Engine/text.h"

void Ending::Load()
{
	timer = 0;
	pTimer = 0;

	endingBox = std::make_unique<TextBox>(1, "Finally, you defeated the owner of this dungeon and conquered the dungeon.\nBut... there nothing!!\nNo gold, no treasure, even no small piece of cards!!\nYou ask your teammates, \"Did anyone already been here..?\"\n\nThe adventure has been ended.\n Press ESC to end the game.", Vector2D{ 0, 0 }, Vector2D{ static_cast<double>(Engine::GetWindow().GetSize().x), static_cast<double>(Engine::GetWindow().GetSize().y) });

	endingBox->SetTextAlignX(TextAlignX::CENTER);
	endingBox->SetTextAlignY(TextAlignY::CENTER);
	endingBox->SetTextSize(20);
	endingBox->SetTextFillColor(doodle::Color(255, 255));
}

void Ending::Update(double)
{
}

void Ending::Draw()
{
	Engine::GetWindow().Clear({ 0, 0, 0, 255 });
	endingBox->Draw();
}

void Ending::Unload()
{
}
