﻿// GAM150
// cardPlie.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "cardPile.h"

#include <algorithm>
#include <random>
#include <iostream>
#include <doodle/angle.hpp>
#include <doodle/drawing.hpp>

CardPile::CardPile(Vector2D position) : position(position)
{
	cardList = {};
}

void CardPile::Draw(double radian)
{
	if(coverImage == nullptr)
	{
		coverImage = std::make_unique<doodle::Image>("Assets/CardDesign/moon.png");
	}
	
	int size = GetSize();

	for (int i = 0; i < size; ++i)
	{
		doodle::push_settings();

		doodle::set_fill_color(180, 255);
		doodle::set_outline_width(2);
		doodle::set_outline_color(255, 255);
		doodle::apply_translate(position.x, position.y + i *  3);
		doodle::apply_rotate(radian);
		doodle::draw_rectangle(0, 0, 90, 126);
		doodle::apply_rotate(doodle::PI);
		doodle::draw_image(*coverImage, 0, 0, 90, 126);
		
		doodle::pop_settings();
	}

	if (size == 0)
	{
		doodle::push_settings();
		doodle::no_fill();
		doodle::set_outline_color(0, 255);
		doodle::apply_translate(position.x, position.y);
		doodle::apply_rotate(radian);
		doodle::pop_settings();
	}
	
	doodle::set_font_size(10);
	doodle::set_fill_color(0, 255);
	doodle::draw_text(std::to_string(size), position.x + 8, position.y + size * 3);
}

void CardPile::Shuffle()
{
	auto rd = std::random_device{};
	auto rng = std::default_random_engine{ rd() };
	std::shuffle(std::begin(cardList), std::end(cardList), rng);
}

void CardPile::Sort()
{
	std::sort(cardList.begin(), cardList.end(), [&](std::pair<int, int> elem1, std::pair<int, int> elem2)
		{
			if (elem1.first != elem2.first)
			{
				return elem1.first < elem2.first;
			}
			return elem1.second < elem2.second;
		});
}

std::vector<std::pair<int, int>> CardPile::Copy() const noexcept
{
	return cardList;
}

void CardPile::Clear()
{
	cardList.clear();
}

void CardPile::Push_back(std::pair<int, int> id)
{
	cardList.push_back(id);
}

void CardPile::Push_back(std::vector<std::pair<int, int>> idList)
{
	for (std::pair<int, int> id : idList)
	{
		Push_back(id);
	}
}

void CardPile::Log()
{
	for (std::pair<int, int> id : cardList)
	{
		std::cout << "{ " << id.first << ", " << id.second << " }\n";
	}
}

std::pair<int, int> CardPile::MoveCard(int index)
{
	std::pair<int, int> id = cardList[index];
	cardList.erase(cardList.begin() + index);
	return id;
}

int CardPile::GetSize()
{
	return static_cast<int>(cardList.size());
}

std::pair<int, int> CardPile::GetCardID(int index)
{
	return cardList[index];
}

CardPile deck(Vector2D{620, -330});
CardPile graveyard(Vector2D{-620, -330});