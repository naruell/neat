﻿#pragma once
// GAM150
// BomberGorilla.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class BomberGorilla : public Enemy
{
public:
	using Enemy::Enemy;

	~BomberGorilla() {};
	void Load() override;
private:
};