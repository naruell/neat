#pragma once
// GAM150
// Lich.h
// Team Neat
// Primary : Junhyuk Cha
// All content ? 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class Lich : public Enemy
{
public:
	using Enemy::Enemy;

	~Lich() {};
	void Load() override;
private:
};