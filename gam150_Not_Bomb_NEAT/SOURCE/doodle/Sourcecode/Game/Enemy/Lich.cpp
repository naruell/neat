﻿// GAM150
// Lich.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Lich.h"
#include "../../Environment/constants.h"
#include"../AI.h"

void Lich::Load()
{
	sprite.Load(PATH_LICH_IMAGE);
	AttackOneHeroPattern* pattern1 = new AttackOneHeroPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern1, 15);
	AttackRandomTwoTilesPattern* pattern3 = new AttackRandomTwoTilesPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern3, 5);
	Add40PercentDamagePattern* pattern5 = new Add40PercentDamagePattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern5, 10);
	HealLowestHPEnemyPattern* pattern6 = new HealLowestHPEnemyPattern(this, PatternType::HEAL);
	ai.PushBackPatterns(pattern6, 40);
}