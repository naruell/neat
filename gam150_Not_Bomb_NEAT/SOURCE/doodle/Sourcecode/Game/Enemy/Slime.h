﻿#pragma once
// GAM150
// Slime.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class Slime : public Enemy
{
public:
	using Enemy::Enemy;

	~Slime() {};
	void Load() override;
private:
};