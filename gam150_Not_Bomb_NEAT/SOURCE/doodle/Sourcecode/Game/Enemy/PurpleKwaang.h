﻿#pragma once
// GAM150
// PurpleKwaang.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class PurpleKwaang : public Enemy
{
public:
	using Enemy::Enemy;

	~PurpleKwaang() {};
	void Load() override;
private:
};