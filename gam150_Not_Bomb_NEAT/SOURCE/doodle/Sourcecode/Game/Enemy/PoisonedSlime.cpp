// GAM150
// PoisonedSlime.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content ? 2020 DigiPen (USA) Corporation, all rights reserved.

#include "PoisonedSlime.h"
#include "../../Environment/constants.h"
#include"../AI.h"

void PoisonedSlime::Load()
{
	sprite.Load(PATH_POISONEDSLIME_IMAGE);
	AttackOneHeroPattern* pattern1 = new AttackOneHeroPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern1, 10);
	AttackRandomTwoTilesPattern* pattern2 = new AttackRandomTwoTilesPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern2, 10);
	Add40PercentDamagePattern* pattern3 = new Add40PercentDamagePattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern3, 5);
}
