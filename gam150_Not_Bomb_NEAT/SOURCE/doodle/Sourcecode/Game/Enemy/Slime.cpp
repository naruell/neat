﻿// GAM150
// Slime.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Slime.h"
#include "../../Environment/constants.h"
#include "../AI.h"

void Slime::Load()
{
	sprite.Load(PATH_SLIME_IMAGE);
	AttackOneHeroPattern* pattern1 = new AttackOneHeroPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern1, 10);
	DoNothingPattern* pattern2 = new DoNothingPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern2, 0);
	AttackRandomTwoTilesPattern* pattern3 = new AttackRandomTwoTilesPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern3, 10);
	AttackRandomOneColumnPattern* pattern4 = new AttackRandomOneColumnPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern4, 0);
	Add40PercentDamagePattern* pattern5 = new Add40PercentDamagePattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern5, 0);
	HealLowestHPEnemyPattern* pattern6 = new HealLowestHPEnemyPattern(this, PatternType::HEAL);
	ai.PushBackPatterns(pattern6, 0);
	BigBombPattern* pattern7 = new BigBombPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern7, 0);
	AddApArmorPattern* pattern8 = new AddApArmorPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern8, 0);
}