﻿#pragma once
// GAM150
// PoisonedSlime.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Enemy.h"

class PoisonedSlime : public Enemy
{
public:
	using Enemy::Enemy;

	~PoisonedSlime() {};
	void Load() override;
private:
};