﻿// GAM150
// RedKwang.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "RedKwaang.h"
#include "../../Environment/constants.h"
#include"../AI.h"

void RedKwaang::Load()
{
	sprite.Load(PATH_KWAANG_IMAGE);
	AttackRandomOneColumnPattern* pattern1 = new AttackRandomOneColumnPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern1, 10);
	AddAdArmorPattern* pattern2 = new AddAdArmorPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern2, 15);
	Add40PercentDamagePattern* pattern3 = new Add40PercentDamagePattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern3, 5);
}