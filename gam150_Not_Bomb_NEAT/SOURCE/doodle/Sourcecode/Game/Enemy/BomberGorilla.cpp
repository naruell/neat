﻿// GAM150
// BomberGorilla.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "BomberGorilla.h"
#include "../../Environment/constants.h"
#include"../AI.h"

void BomberGorilla::Load()
{
	sprite.Load(PATH_BOMBERGORILLA_IMAGE);
	AttackOneHeroPattern* pattern1 = new AttackOneHeroPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern1, 20);
	AttackRandomOneColumnPattern* pattern2 = new AttackRandomOneColumnPattern(this, PatternType::ATTACK);
	ai.PushBackPatterns(pattern2, 30);
	Add40PercentDamagePattern* pattern3 = new Add40PercentDamagePattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern3, 20);
	BigBombPattern* pattern4 = new BigBombPattern(this, PatternType::PREPARATION);
	ai.PushBackPatterns(pattern4, 50);
}