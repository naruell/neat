﻿#pragma once
// GAM150
// prototype_textBox.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"
#include "../Engine/Input.h"
#include "../Engine/text.h"

class Prototype_textBox : public GameState
{
public:
	Prototype_textBox();
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() override { return "Prototype_textBox"; }
private:
	int fontIndex;

	TextBox* textBox;

	Input::InputKey nextLevelKey = Input::KeyboardButton::Right;

	struct InfoInterface
	{
		double positionX = 0;
		double positionY = 0;
		double fontSize = 20;
	};
	InfoInterface infoInterface;
};