﻿// GAM150
// Reward.cpp
// Team Neat
// Primary : Byeongjun Kim, Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Reward.h"

#include "../Engine/Engine.h"
#include "../Engine/helper.h"

#include "card.h"
#include "cardPile.h"
#include "HeroSelect.h"

void Reward::Load(int rewardTimes)
{
	const int classCardKinds{ 3 };
	const int neutralCardKinds{ 10 };

	rewardPage = 0;
	rewardLastPage = rewardTimes;
	isRewardEnd = false;
	
	std::vector<int> characterTypes{};
	for (int i = 0; i < Battlefield::BATTLEFIELDROW/2; i++) 
	{
		for (int j = 0; j < Battlefield::BATTLEFIELDCOLUMN; j++) 
		{
			if (battlefield.battleFieldVector[i][j].GetAllyType() == BattleAllyType::CHARACTER) 
			{
				characterTypes.push_back(static_cast<int>(battlefield.battleFieldVector[i][j].GetCharacterData()->GetCharacterType()));
			}
		}
	}
	int deadVectorSize{ static_cast<int>(battlefield.deadCharacterVector.size()) };
	for (int i = 0; i < deadVectorSize; i++) 
	{
		characterTypes.push_back(static_cast<int>(battlefield.deadCharacterVector[i].GetCharacterData()->GetCharacterType()));
	}

	int characterTypesCount{ static_cast<int>(characterTypes.size()) };

	// -1 represents class card (random)
	while (characterTypes.size() < HeroSelect::maxHeroPicking)
	{
		characterTypes.push_back(-1);
	}	
	// -2 represents skip card
	while (characterTypes.size() < rewardOptionsCount)
	{
		characterTypes.push_back(-2);
	}

	int characterTypesVectorSize{ static_cast<int>(characterTypes.size()) };
	for (int i = 0; i < characterTypesVectorSize * rewardTimes; i++)
	{
		if ((i / rewardOptionsCount) == 1) {
			if (characterTypes[(i % rewardOptionsCount)] == -2) {
				cardID.push_back({ static_cast<int>(CHARACTERTYPE::UI), 23 });
			}
			else {
				cardID.push_back({ static_cast<int>(CHARACTERTYPE::NEUTRAL), doodle::random(0, neutralCardKinds) });
			}
		}
		else {
			if (characterTypes[(i % rewardOptionsCount)] == -1) {
				cardID.push_back({ characterTypes[doodle::random(0, characterTypesCount)],doodle::random(0, classCardKinds) });
			}
			else if (characterTypes[(i % rewardOptionsCount)] == -2) {
				cardID.push_back({ static_cast<int>(CHARACTERTYPE::UI), 23 });
			}
			else
			{
				cardID.push_back({ characterTypes[(i % rewardOptionsCount)], doodle::random(0, classCardKinds) });
			}
		}
	}

	/*
	while (cardID.size() < static_cast<int>(rewardOptionsCount * rewardTimes))
	{
		cardID.push_back({ 0,6 });
	}
	*/

	for (int i = 0; i < rewardOptionsCount * rewardTimes; i++)
	{
		cardList.emplace_back(Vector2Card(cardID[i]));
		//cardList[i]->geometry.angle = 0; // Hovered 는 자동으로 angle 0됨 
		cardList[i]->geometry.posX = (-5 * cardList[i]->geometry.width) + ((i % rewardOptionsCount) * (10.0 / (rewardOptionsCount - 1.0)) * cardList[i]->geometry.width);
			//((i % rewardOptionsCount) - rewardOptionsCount ) * cardList[i]->geometry.width * rewardOptionsCount;
		cardList[i]->geometry.cur_posX = 0;
		cardList[i]->geometry.cur_posY = Engine::GetWindow().GetSize().y / 2.0 * i;
	}
}

void Reward::Update()
{
	if (isRewardEnd == true) { return; }

	for (int i = rewardPage * rewardOptionsCount; i < rewardPage * rewardOptionsCount + rewardOptionsCount; i++)
	{
		cardList[i]->SetExplanation();
		cardList[i]->Update();
		cardList[i]->card_state = Card_State::HOVERED;
		cardList[i]->pCard_state = Card_State::HOVERED;
	}

	if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left)) 
	{
		Vector2DInt mousePos{ Engine::GetInput().GetMousePos() };
		
		for (int i = rewardPage * rewardOptionsCount; i < rewardPage * rewardOptionsCount + rewardOptionsCount; i++)
		{
			if (isRectCollided(mousePos, { cardList[i]->geometry.cur_posX - cardList[i]->geometry.cur_width / 2
			, cardList[i]->geometry.cur_posY - cardList[i]->geometry.cur_height / 2 },
				{ cardList[i]->geometry.cur_posX + cardList[i]->geometry.cur_width / 2
			   , cardList[i]->geometry.cur_posY + cardList[i]->geometry.cur_height / 2 }))
			{
				if (cardID[i] != std::pair{ static_cast<int>(CHARACTERTYPE::UI), 23 })
				{
					deck.Push_back(cardID[i]);
				}
				rewardPage++;
				if (rewardPage >= rewardLastPage)
				{
					Unload();
				}
				break;
			}
		}
	}
}

void Reward::Draw()
{
	if (isRewardEnd == true) { return; }

	for (int i = rewardPage * rewardOptionsCount; i < rewardPage * rewardOptionsCount + rewardOptionsCount; i++)
	{
		cardList[i] ->Draw();
	}
}

void Reward::Unload()
{
	isRewardEnd = true;
	for (auto card : cardList) 
	{
		delete card;
	}
	cardList.clear();
	cardID.clear();
	if (Engine::GetSFML().GetCurrentBgm() == E_BGM::BATTLEBGM)
	{
		Engine::GetSFML().ChangeCurrentBgm(E_BGM::NONBATTLEBGM);
		Engine::GetSFML().ChangeMusicVolume(20);
		Engine::GetSFML().PlayMusic(true);
	}
}

bool Reward::IsRewardEnd()
{
	return isRewardEnd;
}

Reward reward;