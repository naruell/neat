﻿// GAM150
// Warrior.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Warrior.h"
#include "../../Environment/constants.h"

void Warrior::Load()
{
	sprite.Load(PATH_WARRIOR_IMAGE);
}
