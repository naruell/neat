﻿#pragma once
// GAM150
// Vampire.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

class Vampire : public Character
{
public:
	using Character::Character;

	~Vampire() {};
	void Load() override;
private:

};