﻿// GAM150
// Mage.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Mage.h"
#include "../../Environment/constants.h"

void Mage::Load()
{
	sprite.Load(PATH_MAGE_IMAGE);
}