﻿#pragma once
// GAM150
// Warrior.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

class Warrior : public Character
{
public:
	using Character::Character;

	~Warrior() {};
	void Load() override;
private:
	
};