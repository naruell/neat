﻿#pragma once
// GAM150
// Priest.h
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

class Priest : public Character
{
public:
	using Character::Character;

	~Priest() {};
	void Load() override;
private:

};