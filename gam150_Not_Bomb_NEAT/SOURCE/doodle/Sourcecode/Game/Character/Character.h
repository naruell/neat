﻿#pragma once
// GAM150
// Character.h
// Team Neat
// Primary : Sunghwan Cho made the interface of the class and Junhyuk Cha made the function's definition.
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>
#include <functional>

#include "../../Engine/Sprite.h"
#include "../../Engine/Geometry/Vector2D.h"
#include "../../Environment/constants.h"

enum class CHARACTERISTIC
{
	NEUTRAL,
	DAY,
	NIGHT,
	GRAY
};

class Character
{
public:

	int current_HP;
	int original_HP;
	struct CharacterOffset 
	{
		int adOffset;
		int apOffset;
		int healOffset;
		int movementOffset;
	};

	CharacterOffset currentOffset;

	int healthSectionNum = -1;
protected:
	const std::string characterName;
	const CHARACTERTYPE characterType;
	const CHARACTERISTIC characteristic;

	bool isHovered = false;
	int actableCount = 1;

	CharacterOffset originalOffset;
public:
	Character() = default;
	Character(std::string charName, int hp, int adOff, int apOff, int movementOff, int healOff, CHARACTERTYPE ctype, CHARACTERISTIC nature);
	//CHARACTER(const int cNum = 0, const CHARACTERTYPE cType = CHARACTERTYPE::NEUTRAL);
	//CHARACTER(const CHARACTER& charac);
	virtual ~Character();
	//CHARACTER& operator= (const CHARACTER& nChar);
	virtual void Load() = 0;
	void Update(OBJECTSTATE uiState);
	void DrawImage(Vector2D position);
	void DrawHP(Vector2D position);

	void SetExplanation(); // Do I need this? maybe.

	void DealtAdDamage(int value, int offset, Vector2D position);
	void DealtApDamage(int value, int offset, Vector2D position);
	void GetHealed(int value, int offset, Vector2D position);
	//virtual void OnHovered() = 0; // Do I need this? maybe.
	//virtual void OnClicked() = 0; // Do I need this? maybe.
	//virtual void AllocateOnHovered(std::function<void> newFunction) = 0;
	//virtual void AllocateOnClicked(std::function<void> newFunction) = 0;

	int GetActableCount() const noexcept;
	void SetActableCount(int setTo);
	void UpdateActableCount(int amount);

	int GetOriginalHP();
	CharacterOffset GetOriginalOffset();

	void SetOriginalHP(int hp);
	void SetOriginalAdOffset(int offset);
	void SetOriginalApOffset(int offset);
	void SetOriginalHealOffset(int offset);


	CHARACTERTYPE GetCharacterType();

	OBJECTSTATE GetCharacterUIState();
protected:
	std::function<void()> ToBeExecutedOnHovered = nullptr;
	std::function<void()> ToBeExecutedOnClicked = nullptr;
	/*virtual void HealEquation() = 0;
	virtual void DamageEquation() = 0;*/
	OBJECTSTATE characterUIState = OBJECTSTATE::DEFAULT;
	Sprite sprite;

private:
	inline static doodle::Image* healthImage;
	inline static doodle::Image* adDamageImage;
	inline static doodle::Image* apDamageImage;
	inline static doodle::Image* moveImage;
	inline static doodle::Image* healImage;
	inline static bool isIconLoaded = false;
};