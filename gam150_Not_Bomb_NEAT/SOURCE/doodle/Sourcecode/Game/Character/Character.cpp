﻿// GAM150
// AI.cpp
// Team Neat
// Primary : Sunghwan Cho made the interface of the class and Junhyuk Cha made the definition of the function.
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Character.h"

#include <doodle/doodle.hpp>

#include "../../Engine/Engine.h"

Character::Character(std::string charName, int hp, int adOff, int apOff, int movementOff, int healOff,  CHARACTERTYPE ctype, CHARACTERISTIC nature) : characterName(charName),
original_HP(hp), current_HP(hp), characterType(ctype), characteristic(nature)
{
	originalOffset.adOffset = adOff;
	originalOffset.apOffset = apOff;
	originalOffset.movementOffset = movementOff;
	originalOffset.healOffset = healOff;
	currentOffset.adOffset = adOff;
	currentOffset.apOffset = apOff;
	currentOffset.healOffset = healOff;
	currentOffset.movementOffset = movementOff;

	if (!isIconLoaded)
	{
		healthImage = new doodle::Image{ PATH_HEALTH_IMAGE };
		adDamageImage = new doodle::Image{ PATH_ADDAMAGE_IMAGE };
		apDamageImage = new doodle::Image{ PATH_APDAMAGE_IMAGE };
		moveImage = new doodle::Image{ PATH_MOVE_IMAGE };
		healImage = new doodle::Image{ PATH_HEAL_IMAGE };
		isIconLoaded = true;
	}

	int value = 0;
	while (value < original_HP)
	{
		value += 5;
		++healthSectionNum;
	}
}

Character::~Character()
{
	delete healthImage;
	delete adDamageImage;
	delete apDamageImage;
	delete moveImage;
	delete healImage;
	healthImage = nullptr;
	adDamageImage = nullptr;
	apDamageImage = nullptr;
	moveImage = nullptr;
	healImage = nullptr;
	isIconLoaded = false;
}

void Character::Update(OBJECTSTATE uiState)
{
	characterUIState = uiState;
	switch (characterUIState)
	{
	case OBJECTSTATE::PRESSED:
		break;
	case OBJECTSTATE::RELEASED:
		break;
	case OBJECTSTATE::DEFAULT:
		break;
	}
}

void Character::DrawImage(Vector2D position)
{
	doodle::push_settings();
	if (actableCount <= 0)
	{
		doodle::set_tint_color(120, 188);
	}
	sprite.Draw(TranslateMatrix{ position.x, position.y });
	doodle::pop_settings();

	if (characterUIState == OBJECTSTATE::HOVERED || characterUIState == OBJECTSTATE::PRESSED || characterUIState == OBJECTSTATE::RELEASED)
	{
		SetExplanation();
	}
}

void Character::DrawHP(Vector2D position)
{
	doodle::push_settings();
	doodle::set_rectangle_mode(doodle::RectMode::Corner);
	doodle::draw_rectangle(position.x - doodle::Width / 24, position.y + doodle::Height / 15.4, doodle::Width / 12, 15);
	doodle::set_fill_color(doodle::HexColor{ 0xff0000ff });
	if (current_HP > 0)
	{
		doodle::no_outline();
		doodle::draw_rectangle(position.x - doodle::Width / 24, position.y + doodle::Height / 15.4, static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP) * static_cast<double>(current_HP), 15);
	}
	doodle::set_outline_width(1);
	doodle::set_outline_color(doodle::HexColor{ 0x000000ff });
	double oneSectionLength = static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP);
	for (double i = 1; i <= healthSectionNum; ++i)
	{
		doodle::draw_line(position.x - doodle::Width / 24 + i * 5 * oneSectionLength, position.y + doodle::Height / 15.4, position.x - doodle::Width / 24 + i * 5 * oneSectionLength, position.y + doodle::Height / 15.4 + 15);
	}
	doodle::pop_settings();
}

void Character::SetExplanation()
{
	sprite.Draw(TranslateMatrix{ static_cast<double>(doodle::Width) / 1.09 - static_cast<double>(doodle::Width) / 2, -static_cast<double>(doodle::Height) / 3.33 + static_cast<double>(doodle::Height) / 2 });
	doodle::push_settings();

	doodle::set_rectangle_mode(doodle::RectMode::Corner);
	doodle::draw_rectangle(static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 7.5, doodle::Width / 12, 15);
	doodle::set_fill_color(doodle::HexColor{ 0xff0000ff });
	if (current_HP > 0)
	{
		doodle::no_outline();
		doodle::draw_rectangle(static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 7.5, static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP) * static_cast<double>(current_HP), 15);
	}
	doodle::set_outline_width(1);
	doodle::set_outline_color(doodle::HexColor{ 0x000000ff });
	double oneSectionLength = static_cast<double>(doodle::Width / 12) / static_cast<double>(original_HP);
	for (double i = 1; i <= healthSectionNum; ++i)
	{
		doodle::draw_line(static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24 + i * 5 * oneSectionLength, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 7.5,
			static_cast<double>(static_cast<double>(doodle::Width)) / 1.09 - static_cast<double>(doodle::Width) / 2 - doodle::Width / 24 + i * 5 * oneSectionLength, -static_cast<double>(doodle::Height) / 4.28 + static_cast<double>(doodle::Height) / 2 + static_cast<double>(doodle::Height) / 144 + 22.5);
	}
	doodle::pop_settings();
	doodle::push_settings();
	doodle::set_image_mode(doodle::RectMode::Center);
	doodle::set_font_size(15);
	doodle::draw_image(*healthImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0, static_cast<double>(doodle::Height)/36, static_cast<double>(doodle::Height)/36);
	doodle::draw_text("   " + std::to_string(current_HP) + " / " + std::to_string(original_HP), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0);
	doodle::draw_image(*adDamageImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 40.0, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(currentOffset.adOffset), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height)/2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 40.0);
	doodle::draw_image(*apDamageImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 20.0, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(currentOffset.apOffset), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 20.0);
	doodle::draw_image(*moveImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 40.0 * 3, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(currentOffset.movementOffset), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 40.0*3);
	doodle::draw_image(*healImage, static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 + static_cast<double>(doodle::Height) / 80.0 - static_cast<double>(doodle::Height) / 10.0, static_cast<double>(doodle::Height) / 36, static_cast<double>(doodle::Height) / 36);
	doodle::draw_text("   " + std::to_string(currentOffset.healOffset), static_cast<double>(doodle::Width) / 6.0 * 5.0 + static_cast<double>(doodle::Width) / 27 - static_cast<double>(doodle::Width) / 2, static_cast<double>(doodle::Height) / 2 - static_cast<double>(doodle::Height) / 5.0 * 2.0 - static_cast<double>(doodle::Height) / 10.0);
	doodle::pop_settings();
}

void Character::DealtAdDamage(int value, int offset, Vector2D position)
{
	int valuePlusOffset = value + offset;
	if (valuePlusOffset < 0)
	{
		valuePlusOffset = 0;
	}
	current_HP -= (valuePlusOffset);
	Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(valuePlusOffset), position, doodle::Color(255, 0, 0), 50, 2);
	Engine::GetLogger().LogEvent("Dealt " + std::to_string(value) + "AD damage. Current HP : " + std::to_string(current_HP));
}

void Character::DealtApDamage(int value, int offset, Vector2D position)
{
	int valuePlusOffset = value + offset;
	if (valuePlusOffset < 0)
	{
		valuePlusOffset = 0;
	}
	current_HP -= (valuePlusOffset);
	Engine::GetEffectSystem().GenerateTextEffect("-" + std::to_string(valuePlusOffset), position, doodle::Color(255, 0, 0), 50, 2);
	Engine::GetLogger().LogEvent("Dealt " + std::to_string(value) + "AP damage. Current HP : " + std::to_string(current_HP));
}

void Character::GetHealed(int value, int offset, Vector2D position)
{
	int valuePlusOffset = value + offset;
	if (valuePlusOffset < 0)
	{
		valuePlusOffset = 0;
	}
	if ((current_HP + valuePlusOffset) > original_HP)
	{
		Engine::GetEffectSystem().GenerateTextEffect("+" + std::to_string(original_HP - current_HP), position, doodle::Color(144, 238, 144), 50, 2);
		current_HP = original_HP;
	}
	else
	{
		Engine::GetEffectSystem().GenerateTextEffect("+" + std::to_string(valuePlusOffset), position, doodle::Color(144, 238, 144), 50, 2);
		current_HP += (valuePlusOffset);
	}
}

int Character::GetActableCount() const noexcept
{
	return actableCount;
}

void Character::SetActableCount(int setTo)
{
	actableCount = setTo;
}

void Character::UpdateActableCount(int amount)
{
	actableCount += amount;
}

int Character::GetOriginalHP()
{
	return original_HP;
}

Character::CharacterOffset Character::GetOriginalOffset()
{
	return originalOffset;
}

void Character::SetOriginalHP(int hp)
{
	original_HP = hp;
}

void Character::SetOriginalAdOffset(int offset)
{
	originalOffset.adOffset = offset;
}

void Character::SetOriginalApOffset(int offset)
{
	originalOffset.apOffset = offset;
}

void Character::SetOriginalHealOffset(int offset)
{
	originalOffset.healOffset = offset;
}

CHARACTERTYPE Character::GetCharacterType()
{
	return characterType;
}

OBJECTSTATE Character::GetCharacterUIState()
{
	return characterUIState;
}
