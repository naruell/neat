﻿// GAM150
// Solaire.cpp
// Team Neat
// Primary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Solaire.h"
#include "../../Environment/constants.h"

void Solaire::Load()
{
	sprite.Load(PATH_SOLAIRE_IMAGE);
}
