﻿// GAM150
// prototype_MapandRoom.cpp
// Team Neat
// Primary : Sunghwan Cho
// 5/24/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "prototype_MapandRoom.h"

#include "../Engine/Engine.h"
#include "../Environment/constants.h"

#include "EventRoom.h"
#include "GameMap.h"

void Prototype_MapandRoom::Load()
{
	if(isLoaded == false && isFirstMapLoaded == false)
	{
		Engine::GetGameMap().SetIsMapCleared(false);
		Engine::GetGameMap().Load(PATH_FIRST_MAP);
		isLoaded = true;
		isFirstMapLoaded = true;
		return;
	}
	if (isLoaded == false && isSecondMapLoaded == false)
	{
		Engine::GetGameMap().SetIsMapCleared(false);
		Engine::GetGameMap().Load(PATH_SECOND_MAP);
		isLoaded = true;
		isSecondMapLoaded = true;
	}
}

void Prototype_MapandRoom::Update(double deltaTime)
{
	if(Engine::GetGameMap().GetIndex() == 1 && Engine::GetGameMap().GetIsMapCleared() == true)
	{
		event.SetRoomType(7);
		Engine::GetGameStateManager().SetNextState(GAMESTATE::FINAL_EVENTROOM);
	}
	Engine::GetGameMap().Update(deltaTime);
}

void Prototype_MapandRoom::Draw()
{
	Engine::GetWindow().Clear({ 255, 255, 255, 255 });
	Engine::GetGameMap().Draw();
}

void Prototype_MapandRoom::Unload()
{
	if(Engine::GetGameMap().GetIsMapCleared() == true)
	{
		Engine::GetGameMap().Unload();
		isLoaded = false;
	}
}
