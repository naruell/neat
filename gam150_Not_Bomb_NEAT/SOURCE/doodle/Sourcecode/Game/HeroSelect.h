﻿#pragma once
// GAM150
// HeroSelect.h
// Team Neat
// Primary : Byoengjun Kim
// Secondary : Sunghwan Cho, Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/Engine.h"
#include "../Engine/helper.h"
#include "Character/Character.h"

class HeroSelect {
public:
	static const int windowSeparatorX{ 5 }; // 화면 몇 등분할지의 x // 상수 예정
	static const int windowSeparatorY{ 4 }; // 화면 몇 등분할지의 y // 상수 예정
	static const int rowsPicking{ 2 }; // 픽창 가로 칸수 // 상수 예정
	static const int columnsPicking{ 3 }; // 세로 칸수 // 상수 예정

	static const int maxHeroPicking{ 3 };

public:
	HeroSelect() = default;

	void Load(); // 로드에서 캐릭들 벡터에 넣어야함
	void Update();
	void Draw();
	void Unload();

	std::vector<Character*> getSelectableVector();
	std::vector<Vector2DInt> getSelectingVector();
	void AddCharacters(Character* add);

	bool isDrawingInfo = false;
	Vector2DInt indexWhereDrawingInfo{ -1,-1 };
	Vector2DInt selectedCharacterIndex = { -1, -1 };
private:
	void UpdateCardList();

	std::vector<std::unique_ptr<Card>> cardList;

	std::vector<Character*> selectable{};
	std::vector<Vector2DInt> selecting{};

	std::unique_ptr<doodle::Image> characterCardSpt;

	std::unique_ptr<doodle::Image> characterHPSpt;
	std::unique_ptr<doodle::Image> characterADSpt;
	std::unique_ptr<doodle::Image> characterAPSpt;
	std::unique_ptr<doodle::Image> characterMovementSpt;
	std::unique_ptr<doodle::Image> characterHealSpt;
};

extern HeroSelect heroSelect;