﻿// GAM150
// alpha_CardManaging.cpp
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "alpha_CardManaging.h"
#include "cardPile.h"
#include "hand.h"

void Alpha_CardManaging::Load()
{
	timer = 0;
	pTimer = 0;
	background.Add(PATH_CARDMANAGER_BACKGROUND_RAWIMAGE);
	cardManager.Load();
}

void Alpha_CardManaging::Update([[maybe_unused]]double deltaTime)
{
	pTimer = timer;
	timer += deltaTime;

	if (pTimer <= 0.2 && 0.2 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 9 });
	}
	else if (pTimer <= 0.4 && 0.4 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 20 });
	}
	else if (pTimer <= 0.6 && 0.6 < timer)
	{
		player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 21 });
	}

	cardManager.Update();
	player_hand.Update();
}

void Alpha_CardManaging::Draw()
{
	Engine::GetWindow().Clear({ 77, 77, 77, 255 });

	doodle::push_settings();
	doodle::apply_scale(Engine::GetWindow().GetSize().x / static_cast<double>(background[0].texturePtr->GetSize().x),
		Engine::GetWindow().GetSize().y / static_cast<double>(background[0].texturePtr->GetSize().y));
	background.Draw();
	doodle::pop_settings();

	cardManager.Draw();
	player_hand.Draw();
}

void Alpha_CardManaging::Unload()
{
	cardManager.Unload();
	player_hand.Clear_hand();
	graveyard.Clear();
}
