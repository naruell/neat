﻿// GAM150
// alpha_Credit.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "alpha_Credit.h"

#include <fstream>

#include "hand.h"
#include "../Engine/text.h"
#include "../Environment/constants.h"

void Alpha_Credit::Load()
{
	timer = 0;
	pTimer = 0;
	player_hand.Draw_card({ static_cast<int>(CHARACTERTYPE::UI), 4 });

	std::string creditText = "";
	
	std::ifstream inputFileStream{ PATH_CREDIT_TXT };
	if (!inputFileStream)
	{
		Engine::GetLogger().LogError("Failed to open file for reading!");
		return;
	}
	inputFileStream.exceptions(inputFileStream.exceptions() | std::ios_base::badbit);

	while (!inputFileStream.eof())
	{
		std::string tempStr;
		std::getline(inputFileStream, tempStr);
		creditText += tempStr;
		creditText += "\n";
	}
	
	creditBox = std::make_unique<TextBox>(1, creditText, Vector2D{ 0, 0 }, Vector2D{ static_cast<double>(Engine::GetWindow().GetSize().x), static_cast<double>(Engine::GetWindow().GetSize().y) });
	
	creditBox->SetTextAlignX(TextAlignX::LEFT);
	creditBox->SetTextSize(20);
}

void Alpha_Credit::Update(double)
{
	player_hand.Update();
}

void Alpha_Credit::Draw()
{
	Engine::GetWindow().Clear({ 0, 0, 0, 255 });
	Engine::GetEffectSystem().Run();
	creditBox->Draw();
	
	player_hand.Draw();
}

void Alpha_Credit::Unload()
{
	player_hand.Clear_hand();
}
