﻿// GAM150
// battlefield.cpp
// Team Neat
// Primary : Byeongjun Kim, Junhyuk Cha
// Secondary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "battlefield.h"

#include <doodle/environment.hpp>
#include <doodle/input.hpp>

#include "../Engine/SFML.h"
#include "GameClock.h"
#include "hand.h"

bool operator==(const Vector2DInt& Index1, const Vector2DInt& Index2)
{
	if (Index1.x == Index2.x && Index1.y == Index2.y)
	{
		return true;
	}
	return false;
};

void Battlefield::Load()
{
	reward.Load(2);
	Engine::GetSFML().ChangeCurrentBgm(E_BGM::BATTLEBGM);
	Engine::GetSFML().ChangeMusicVolume(7);
	Engine::GetSFML().PlayMusic(true);

	for (auto hero : GetAllyBattleField())
	{
		if (hero.GetCharacterData() != nullptr)
		{
			if (hero.GetCharacterData()->current_HP <= 0)
			{
				hero.GetCharacterData()->current_HP = hero.GetCharacterData()->GetOriginalHP();
				battlefield.deadCharacterVector.push_back(hero);
				BattleObject empty(Vector2DInt{ -1, -1 });
				battlefield.GetObject(hero.GetPosition()).ChangeObject(empty);
			}
		}
	}
}

void Battlefield::Update(double pTimer, double timer)
{
	UpdateCurrentMouseLocation();

	if (moveUpKey.IsKeyReleased())
	{
		if (selection.y > 0) { selection.y--; }
	}
	if (moveDownKey.IsKeyReleased())
	{
		if (selection.y < Battlefield::BATTLEFIELDCOLUMN - 1) { selection.y++; }
	}
	if (moveLeftKey.IsKeyReleased())
	{
		if (selection.x > 0) { selection.x--; }
	}
	if (moveRightKey.IsKeyReleased())
	{
		if (selection.x < Battlefield::BATTLEFIELDROW - 1) { selection.x++; }
	}
	if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left) && player_hand.DoesAnyCardClicked() == false)
	{
		double width_divide6 = doodle::Width / 6.0;
		double height_divide5 = doodle::Height / 5.0;
		int ClickedPosX{ doodle::get_mouse_x() + Engine::GetWindow().GetSize().x / 2 };
		int ClickedPosY{ Engine::GetWindow().GetSize().y - (doodle::get_mouse_y() + Engine::GetWindow().GetSize().y / 2) };
		if (ClickedPosX >= width_divide6 * 1 && ClickedPosX <= width_divide6 * 5 &&
			ClickedPosY >= height_divide5 * 1 && ClickedPosY <= height_divide5 * 4 && player_hand.DoesAnyCardClicked() == false && player_hand.DoesAnyCardTargeting() == false)
		{
			pressedPos = { static_cast<int>(ClickedPosX / width_divide6) - 1 ,
				static_cast<int>(ClickedPosY / height_divide5) - 1 };
		}
		mousePressedIndex = currentMouseIndex;
	}
	if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left))
	{
		double width_divide6 = doodle::Width / 6.0;
		double height_divide5 = doodle::Height / 5.0;
		int ClickedPosX{ doodle::get_mouse_x() + Engine::GetWindow().GetSize().x / 2 };
		int ClickedPosY{ Engine::GetWindow().GetSize().y - (doodle::get_mouse_y() + Engine::GetWindow().GetSize().y / 2) };
		if ((ClickedPosX >= width_divide6 * 1 && ClickedPosX <= width_divide6 * 5 &&
			ClickedPosY >= height_divide5 * 1 && ClickedPosY <= height_divide5 * 4) &&
			(pressedPos.x == static_cast<int>(ClickedPosX / width_divide6) - 1) &&
			(pressedPos.y == static_cast<int>(ClickedPosY / height_divide5) - 1))
		{
			selection = { static_cast<int>(ClickedPosX / width_divide6) - 1 ,
				static_cast<int>(ClickedPosY / height_divide5) - 1 };
		}
		pressedPos = { -1,-1 };
	}

	int rowFieldSize{ static_cast<int>(battleFieldVector.size()) };
	int columnFieldSize{ static_cast<int>(battleFieldVector[0].size()) };

	//set releasedBattleObjectIndex to can't selected location
	releasedBattleObjectIndex = { -1, -1 };

	// Check the mouse UI state and send it in character & enemy update function by parameter.
	for (int i = 0; i < rowFieldSize; i++)
	{

		for (int j = 0; j < columnFieldSize; j++)
		{
			//Check UISTATE::HOVERED
			if (battleFieldVector[i][j].GetPosition() == currentMouseIndex && player_hand.DoesAnyCardHovered() == false && player_hand.DoesAnyCardClicked() == false)
			{
				if (battleFieldVector[i][j].GetCharacterData() != nullptr)
				{
					//Check UISTATE::PRESSED
					if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
					{
						battleFieldVector[i][j].GetCharacterData()->Update(OBJECTSTATE::PRESSED);
					}
					//Check UISTATE::RELEASED
					else if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left) && mousePressedIndex == currentMouseIndex)
					{
						battleFieldVector[i][j].GetCharacterData()->Update(OBJECTSTATE::RELEASED);
						releasedBattleObjectIndex = battleFieldVector[i][j].GetPosition();
					}
					//UISTATE::HOVERED
					else
					{
						battleFieldVector[i][j].GetCharacterData()->Update(OBJECTSTATE::HOVERED);
						hoveredBattleObjectIndex = battleFieldVector[i][j].GetPosition();
					}
				}
				else if (battleFieldVector[i][j].GetEnemyData() != nullptr)
				{
					//Check UISTATE::PRESSED
					if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
					{
						battleFieldVector[i][j].GetEnemyData()->Update(OBJECTSTATE::PRESSED);
					}
					//Check UISTATE::RELEASED
					else if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left) && mousePressedIndex == currentMouseIndex)
					{
						battleFieldVector[i][j].GetEnemyData()->Update(OBJECTSTATE::RELEASED);
						releasedBattleObjectIndex = battleFieldVector[i][j].GetPosition();
					}
					//UISTATE::HOVERED
					else
					{
						battleFieldVector[i][j].GetEnemyData()->Update(OBJECTSTATE::HOVERED);
						hoveredBattleObjectIndex = battleFieldVector[i][j].GetPosition();
					}
				}
				else
				{
					if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left) && mousePressedIndex == currentMouseIndex &&
						battleFieldVector[i][j].GetIsSelectAble() == true)
					{
						releasedBattleObjectIndex = battleFieldVector[i][j].GetPosition();
					}
				}
			}
			else
			{
				if (battleFieldVector[i][j].GetCharacterData() != nullptr)
				{
					battleFieldVector[i][j].GetCharacterData()->Update(OBJECTSTATE::DEFAULT);
				}
				else if (battleFieldVector[i][j].GetEnemyData() != nullptr)
				{
					battleFieldVector[i][j].GetEnemyData()->Update(OBJECTSTATE::DEFAULT);
				}
			}
		}
	}

	if (isHeroTurn)
	{
		CheckIfMouseIsClickedTurnButton();
	}

	if (isHeroTurn && isEnemySelectTarget == false)
	{
		for (auto& enemy : battlefield.GetEnemyBattleField())
		{
			if (enemy.GetEnemyData() != nullptr)
			{
				enemy.GetEnemyData()->GetAI().SelectNextPattern();
			}
		}
		isEnemySelectTarget = true;
	}

	if (isHeroTurn && isLimitedTimeUpdated == false)
	{
		GameClock::CalculateNextLimitedTime();
		isLimitedTimeUpdated = true;
	}

	if (isHeroTurn && isPlayerDrawCard == false)
	{
		if (isFirstTurn)
		{
			if (pTimer <= 0.8 && 0.8 < timer)
			{

				player_hand.Draw_from_deck(3-static_cast<int>(deadCharacterVector.size()));
				isPlayerDrawCard = true;
				isFirstTurn = false;
			}
		}
		else
		{

			player_hand.Draw_from_deck(3 - static_cast<int>(deadCharacterVector.size()));
			isPlayerDrawCard = true;
		}
	}

	if (isHeroTurn && isHeroGetActPower == false)
	{
		for (auto character : GetAllyBattleField())
		{
			if (character.GetCharacterData() != nullptr)
			{
				if (character.GetCharacterData()->GetActableCount() < 1)
				{
					character.GetCharacterData()->UpdateActableCount(1);
				}
				else
				{
					character.GetCharacterData()->SetActableCount(1);
				}
			}

		}
		isHeroGetActPower = true;
	}

	//enemy attact
	if (isHeroTurn == false)
	{
		for (auto& enemy : GetEnemyBattleField())
		{
			if (enemy.GetEnemyData() != nullptr)
			{
				if (enemy.GetEnemyData()->GetAI().GetNextPattern() != nullptr)
				{
					enemy.GetEnemyData()->GetAI().GetNextPattern()->Execute();
				}
			}
		}
		isHeroTurn = true;
		isEnemySelectTarget = false;
		isLimitedTimeUpdated = false;
		isHeroGetActPower = false;
		isPlayerDrawCard = false;
	}

	for (int i = 0; i < rowFieldSize; i++)
	{
		for (int j = 0; j < columnFieldSize; j++)
		{
			battleFieldVector[i][j].DeleteObject();
		}
	}

	//겜끝났는지 (임시)
	int count = 0;
	for (auto enemies : GetEnemyBattleField())
	{
		if (enemies.GetAllyType() == BattleAllyType::ENEMY)
		{
			++count;
		}
	}
	if (count == 0)
	{
		isGameEnded = true;
		for (auto hero : GetAllyBattleField())
		{
			if (hero.GetCharacterData() != nullptr)
			{
				hero.GetHealed(hero.GetCharacterData()->GetOriginalHP() / 5, 0);
			}
		}
	}
}

void Battlefield::Draw()
{
	{
		using namespace doodle;
		push_settings();
		set_outline_color(HexColor(0xFFFFFF80));

		double width_divide6 = Width / 6.0;
		double height_divide5 = Height / 5.0;
		for (int i = 1; i <= 4; i++)
		{
			draw_line(width_divide6 - Engine::GetWindow().GetSize().x / 2, height_divide5 * i - Engine::GetWindow().GetSize().y / 2
				, width_divide6 * 5 - Engine::GetWindow().GetSize().x / 2, height_divide5 * i - Engine::GetWindow().GetSize().y / 2);
		}
		for (int i = 1; i <= 5; i++)
		{
			draw_line(width_divide6 * i - Engine::GetWindow().GetSize().x / 2, height_divide5 - Engine::GetWindow().GetSize().y / 2
				, width_divide6 * i - Engine::GetWindow().GetSize().x / 2, height_divide5 * 4 - Engine::GetWindow().GetSize().y / 2);
		}

		if (isHeroTurn)
		{
			DrawEnemyPatternRange();
		}
		DrawSelection(selection);
		int rowFieldSize{ static_cast<int>(battleFieldVector.size()) };
		int columnFieldSize{ static_cast<int>(battleFieldVector[0].size()) };
		for (int i = 0; i < rowFieldSize; i++)
		{
			for (int j = 0; j < columnFieldSize; j++)
			{
				battleFieldVector[i][j].Draw();
			}
		}

		pop_settings();
	}
	DrawNextTurnButton();
}

void Battlefield::UpdateCurrentMouseLocation()
{
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;
	int ClickedPosX{ doodle::get_mouse_x() + Engine::GetWindow().GetSize().x / 2 };
	int ClickedPosY{ Engine::GetWindow().GetSize().y - (doodle::get_mouse_y() + Engine::GetWindow().GetSize().y / 2) };
	if (ClickedPosX >= width_divide6 * 1 && ClickedPosX <= width_divide6 * 5 &&
		ClickedPosY >= height_divide5 * 1 && ClickedPosY <= height_divide5 * 4)
	{
		currentMouseIndex = { static_cast<int>(ClickedPosX / width_divide6) - 1, static_cast<int>(ClickedPosY / height_divide5) - 1 };
	}
	else
	{
		currentMouseIndex = { -3, -3 };
	}
}

void Battlefield::DrawEnemyPatternRange()
{
	for (auto& enemy : GetEnemyBattleField())
	{
		if (enemy.GetEnemyData() != nullptr)
		{
			if (enemy.GetEnemyData()->GetAI().GetNextPattern() != nullptr)
			{
				switch (enemy.GetEnemyData()->GetAI().GetNextPattern()->patternType)
				{
				case PatternType::ATTACK:
					for (auto attackRanges : enemy.GetEnemyData()->GetAI().GetNextPattern()->attackRange)
					{
						doodle::push_settings();
						doodle::no_outline();
						if (enemy.GetEnemyData()->GetEnemyUIState() == OBJECTSTATE::HOVERED)
						{
							doodle::set_fill_color(254, 107 * abs(cos(Engine::GetElapsedTime() / 4650)), 115 * abs(cos(Engine::GetElapsedTime() / 4650)), 256);
						}
						else
						{
							doodle::set_fill_color(doodle::HexColor(0xfe6b736e));
						}
						double width_divide6 = doodle::Width / 6.0;
						double height_divide5 = doodle::Height / 5.0;

						double posX{ width_divide6 * (1.0 + attackRanges.x) };
						double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + attackRanges.y)) };
						doodle::set_rectangle_mode(doodle::RectMode::Corner);

						doodle::draw_rectangle(posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2, width_divide6, height_divide5);
						doodle::pop_settings();
					}
					break;
				case PatternType::HEAL:
					for (auto healRanges : enemy.GetEnemyData()->GetAI().GetNextPattern()->healRange)
					{
						doodle::push_settings();
						doodle::no_outline();
						if (enemy.GetEnemyData()->GetEnemyUIState() == OBJECTSTATE::HOVERED)
						{
							doodle::set_fill_color(107 * abs(cos(Engine::GetElapsedTime() / 4650)), 254, 115 * abs(cos(Engine::GetElapsedTime() / 4650)), 256);
						}
						else
						{
							doodle::set_fill_color(doodle::HexColor(0x6bfe7360));
						}
						double width_divide6 = doodle::Width / 6.0;
						double height_divide5 = doodle::Height / 5.0;

						double posX{ width_divide6 * (1.0 + healRanges.first.x) };
						double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + healRanges.first.y)) };
						doodle::set_rectangle_mode(doodle::RectMode::Corner);

						doodle::draw_rectangle(posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2, width_divide6, height_divide5);
						doodle::pop_settings();
					}
					break;
				case PatternType::PREPARATION:
					for (auto preparationRange : enemy.GetEnemyData()->GetAI().GetNextPattern()->preparationRange)
					{
						doodle::push_settings();
						doodle::no_outline();
						if (enemy.GetEnemyData()->GetEnemyUIState() == OBJECTSTATE::HOVERED)
						{
							doodle::set_fill_color(254, 107 * abs(cos(Engine::GetElapsedTime() / 4650)), 254, 256);
						}
						else
						{
							doodle::set_fill_color(doodle::HexColor(0xfe6bfe6e));
						}
						double width_divide6 = doodle::Width / 6.0;
						double height_divide5 = doodle::Height / 5.0;

						double posX{ width_divide6 * (1.0 + preparationRange.x) };
						double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + preparationRange.y)) };
						doodle::set_rectangle_mode(doodle::RectMode::Corner);

						doodle::draw_rectangle(posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2, width_divide6, height_divide5);
						doodle::pop_settings();
					}
					break;
				}
				
			}
		}
	}
}

void Battlefield::initializeBattleFieldVector()
{
	for (int i = 0; i < BATTLEFIELDROW; i++)
	{
		std::vector<BattleObject> threeEmptyThings{};
		for (int j = 0; j < BATTLEFIELDCOLUMN; j++)
		{
			threeEmptyThings.push_back(BattleObject(Vector2DInt{ i,j }));
		}
		battleFieldVector.push_back(threeEmptyThings);
	}
}

void Battlefield::SwapObject(Vector2DInt Pos1, Vector2DInt Pos2)
{
	auto Temp = GetObjectData(Pos1);
	GetObject(Pos1).ChangeObject(GetObject(Pos2));
	GetObject(Pos2).ChangeObject(Temp);
}

Battlefield::BattleObject& Battlefield::GetObject(Vector2DInt Pos)
{
	return battleFieldVector[Pos.x][Pos.y];	// 이거 -1 해서 받아야하나?
}

Battlefield::BattleObject Battlefield::GetObjectData(Vector2DInt Pos)
{
	return battleFieldVector[Pos.x][Pos.y];
}

std::vector<Battlefield::BattleObject> Battlefield::GetEnemyBattleField()
{
	std::vector<BattleObject> EnemyField = {};
	for (int i = BATTLEFIELDROW / 2; i < BATTLEFIELDROW; i++)
	{
		for (int j = 0; j < BATTLEFIELDCOLUMN; j++)
		{
			EnemyField.push_back(battleFieldVector[i][j]);
		}
	}
	return EnemyField;
}

std::vector<Battlefield::BattleObject> Battlefield::GetAllyBattleField()
{
	std::vector<BattleObject> AllyField = {};
	for (int i = 0; i < BATTLEFIELDROW / 2; i++)
	{
		for (int j = 0; j < BATTLEFIELDCOLUMN; j++)
		{
			AllyField.push_back(battleFieldVector[i][j]);
		}
	}
	return AllyField;
}

Vector2DInt Battlefield::GetRandomPosNoexcept()
{
	Vector2DInt Pos{ static_cast<int>(doodle::random() * BATTLEFIELDROW), static_cast<int>(doodle::random() * BATTLEFIELDCOLUMN) };
	return Pos;
}

Vector2DInt Battlefield::GetRandomEnemyPosNoexcept()
{
	Vector2DInt Pos{ static_cast<int>(doodle::random() * (BATTLEFIELDROW / 2) + 2), static_cast<int>(doodle::random() * BATTLEFIELDCOLUMN) };
	return Pos;
}

Vector2DInt Battlefield::GetRandomAllyPosNoexcept()
{
	Vector2DInt Pos{ static_cast<int>(doodle::random() * (BATTLEFIELDROW / 2)),  static_cast<int>(doodle::random() * BATTLEFIELDCOLUMN) };
	return Pos;
}

Vector2DInt Battlefield::GetRandomTarget()
{
	std::vector<Battlefield::BattleObject> ableField = {};
	for (int i = 0; i < Battlefield::BATTLEFIELDROW; i++)
	{
		for (int j = 0; j < Battlefield::BATTLEFIELDCOLUMN; j++)
		{
			if (battleFieldVector[i][j].GetAllyType() != BattleAllyType::EMPTY)
			{
				ableField.push_back(battleFieldVector[i][j]);
			}
		}
	}
	if (ableField.size() == 0) { return { -1, -1 }; }
	return ableField[static_cast<int>(doodle::random() * static_cast<int>(ableField.size()))].GetPosition();
}

Vector2DInt Battlefield::GetRandomAllyTarget()
{
	std::vector<BattleObject> AllyBattleField{ GetAllyBattleField() };
	int FieldSize{ static_cast<int>(AllyBattleField.size()) };
	std::vector<BattleObject> ableField = {};
	for (int i = 0; i < FieldSize; i++)
	{
		if (AllyBattleField[i].GetAllyType() != BattleAllyType::EMPTY)
		{
			ableField.push_back(AllyBattleField[i]);
		}
	}
	if (ableField.size() == 0) { return { -1, -1 }; }
	return ableField[static_cast<int>(doodle::random() * static_cast<int>(ableField.size()))].GetPosition();
}

Vector2DInt Battlefield::GetRandomEnemyTarget()
{
	std::vector<BattleObject> EnemyBattleField{ GetEnemyBattleField() };
	int FieldSize{ static_cast<int>(EnemyBattleField.size()) };
	std::vector<BattleObject> ableField = {};
	for (int i = 0; i < FieldSize; i++)
	{
		if (EnemyBattleField[i].GetAllyType() != BattleAllyType::EMPTY)
		{
			ableField.push_back(EnemyBattleField[i]);
		}
	}
	if (ableField.size() == 0) { return { -1, -1 }; }
	return ableField[static_cast<int>(doodle::random() * static_cast<int>(ableField.size()))].GetPosition();
}

Vector2DInt Battlefield::GetRandomEmptyTarget()
{
	int rowSize = static_cast<int>(battleFieldVector.size());
	int column = static_cast<int>(battleFieldVector[0].size());
	std::vector<BattleObject> ableField = {};
	for (int x = 0; x < rowSize; ++x)
	{
		for (int y = 0; y < column; ++y)
		{
			if (battleFieldVector[x][y].GetAllyType() == BattleAllyType::EMPTY)
			{
				ableField.push_back(battleFieldVector[x][y]);
			}
		}
	}
	if (ableField.size() == 0) { return { -1, -1 }; }
	return ableField[static_cast<int>(doodle::random() * static_cast<int>(ableField.size()))].GetPosition();

}

Vector2DInt Battlefield::getSelection()
{
	return selection;
}

void Battlefield::DrawSelection(Vector2DInt index)
{
	if (battleFieldVector[index.x][index.y].GetCharacterData() != nullptr && battleFieldVector[index.x][index.y].GetCharacterData()->GetActableCount() <= 0)
	{
		return;
	}

	doodle::push_settings();
	doodle::set_outline_color(doodle::HexColor(0x00FFFF00));
	doodle::set_fill_color(doodle::HexColor(0x00FF0099));

	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (1.0 + index.x) };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + index.y)) };

	doodle::draw_ellipse(posX - Engine::GetWindow().GetSize().x / 2 + width_divide6 / 2, posY - Engine::GetWindow().GetSize().y / 2 + height_divide5 / 4, width_divide6 / 2, height_divide5 / 4);
	doodle::pop_settings();
}

void Battlefield::DrawNextTurnButton()
{
	doodle::push_settings();
	if (isHeroTurn)
	{
		doodle::set_fill_color(doodle::HexColor{ 0xbfff00ff });
		doodle::set_outline_color(doodle::HexColor{ 0xbfff00ff });
	}
	else
	{
		doodle::set_fill_color(doodle::HexColor{ 0xa0a0a0ff });
		doodle::set_outline_color(doodle::HexColor{ 0xa0a0a0ff });
	}
	doodle::set_rectangle_mode(doodle::RectMode::Center);
	doodle::draw_rectangle(static_cast<double>(Engine::GetWindow().GetSize().x) / 6.0 * 2.5, -static_cast<double>(Engine::GetWindow().GetSize().y) / 20.0,
		static_cast<double>(Engine::GetWindow().GetSize().x) / 12.0, static_cast<double>(Engine::GetWindow().GetSize().y) / 40.0 * 3.0);
	doodle::set_fill_color(doodle::HexColor{ 0x000000ff });
	doodle::set_outline_color(doodle::HexColor{ 0x000000ff });
	doodle::set_font_size(16);
	doodle::draw_text("END TURN", static_cast<double>(Engine::GetWindow().GetSize().x) / 6.4 * 2.5, -static_cast<double>(Engine::GetWindow().GetSize().y) / 16.0);

	doodle::pop_settings();
}

void Battlefield::CheckIfMouseIsClickedTurnButton()
{
	double left_x_pos = static_cast<double>(Engine::GetWindow().GetSize().x) / 6.0 * 2.5 - static_cast<double>(Engine::GetWindow().GetSize().x) / 24.0;
	double right_x_pos = static_cast<double>(Engine::GetWindow().GetSize().x) / 6.0 * 2.5 + static_cast<double>(Engine::GetWindow().GetSize().x) / 24.0;
	double top_y_pos = -static_cast<double>(Engine::GetWindow().GetSize().y) / 20.0 + static_cast<double>(Engine::GetWindow().GetSize().y) / 80.0 * 3.0;
	double bottom_y_pos = -static_cast<double>(Engine::GetWindow().GetSize().y) / 20.0 - static_cast<double>(Engine::GetWindow().GetSize().y) / 80.0 * 3.0;
	if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left) && doodle::get_mouse_x() > left_x_pos && doodle::get_mouse_x() < right_x_pos && doodle::get_mouse_y() > bottom_y_pos && doodle::get_mouse_y() < top_y_pos)
	{
		isTurnButtonPressed = true;
	}
	else if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
	{
		isTurnButtonPressed = false;
	}

	if (isTurnButtonPressed && Engine::GetInput().IsMouseReleased(Input::MouseButton::Left) && doodle::get_mouse_x() > left_x_pos && doodle::get_mouse_x() < right_x_pos && doodle::get_mouse_y() > bottom_y_pos && doodle::get_mouse_y() < top_y_pos)
	{
		isHeroTurn = false;
	}
}

bool Battlefield::GetIsPlayerTurn()
{
	return isHeroTurn;
}

BattleAllyType Battlefield::BattleObject::GetAllyType() const
{
	return type;
}

void Battlefield::BattleObject::SetAllyType(BattleAllyType allyType)
{
	type = allyType;
}

Character* Battlefield::BattleObject::GetCharacterData()
{
	return characterdata;
}

Enemy* Battlefield::BattleObject::GetEnemyData()
{
	return enemydata;
}

Vector2DInt Battlefield::BattleObject::GetPosition()
{
	return index;
}

void Battlefield::BattleObject::SetPosition(Vector2DInt nextIndex)
{
	index = nextIndex;
}

void Battlefield::BattleObject::ChangeObject(BattleObject newObject)
{
	type = newObject.GetAllyType();
	characterdata = nullptr;
	enemydata = nullptr;
	if (newObject.GetAllyType() == BattleAllyType::CHARACTER) { characterdata = newObject.GetCharacterData(); }
	else if (newObject.GetAllyType() == BattleAllyType::ENEMY) { enemydata = newObject.GetEnemyData(); }
	else (Engine::GetLogger().LogError("Error : Not valid type!"));
}

void Battlefield::BattleObject::Update()
{
}

void Battlefield::BattleObject::DrawField()
{
	doodle::push_settings();
	doodle::set_outline_color(doodle::HexColor(0x00FFFFFF));
	if (isSelectAble == true)
	{
		if (index == battlefield.currentMouseIndex)
		{
			doodle::set_fill_color(150, 150 * abs(cos(Engine::GetElapsedTime() / 4650)), 0);
		}
		else if (battlefield.selectMode == SelectMode::Line &&
			index.x == battlefield.currentMouseIndex.x)
		{
			doodle::set_fill_color(150, 150 * abs(cos(Engine::GetElapsedTime() / 4650)), 0);

		}
		else
		{
			doodle::set_fill_color(150, 150, 0);
		}
	}
	else
	{
		doodle::no_fill();
	}
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (1.0 + static_cast<double>(index.x)) };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + index.y)) };

	doodle::set_rectangle_mode(doodle::RectMode::Corner);

	if (type == BattleAllyType::CHARACTER || type == BattleAllyType::ENEMY || isSelectAble == true)
	{
		doodle::draw_rectangle(posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2, width_divide6, height_divide5);
	}
	doodle::pop_settings();

	doodle::push_settings();
	doodle::no_fill();
	doodle::set_outline_color(doodle::HexColor{ 0x66ff00ff });
	doodle::set_rectangle_mode(doodle::RectMode::Corner);
	posX = width_divide6 * (1.0 + static_cast<double>(battlefield.currentMouseIndex.x));
	posY = Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + battlefield.currentMouseIndex.y));

	doodle::draw_rectangle(posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2, width_divide6, height_divide5);
	doodle::pop_settings();
}

void Battlefield::BattleObject::DrawCharacter()
{
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (2.0 + static_cast<double>(index.x)) - width_divide6 / 2 };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (1.0 + static_cast<double>(index.y))) - height_divide5 / 2 };

	if (type == BattleAllyType::CHARACTER)
	{
		characterdata->DrawImage({ posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2 });
		characterdata->DrawHP({ posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2 });
	}
	else if (type == BattleAllyType::ENEMY)
	{
		enemydata->Draw({ posX - Engine::GetWindow().GetSize().x / 2, posY - Engine::GetWindow().GetSize().y / 2 });
	}
}

void Battlefield::BattleObject::Draw()
{
	DrawField();
	DrawCharacter();
}

void Battlefield::BattleObject::DeleteObject()
{
	if (characterdata != nullptr)
	{
		if (characterdata->current_HP <= 0)
		{
			this->characterdata->current_HP = this->characterdata->GetOriginalHP();
			this->characterdata->SetActableCount(1);
			battlefield.deadCharacterVector.push_back(*this);
			BattleObject empty(Vector2DInt{ -1, -1 });
			battlefield.GetObject(this->index).ChangeObject(empty);
		}
	}
	else if (enemydata != nullptr)
	{
		if (enemydata->current_HP <= 0)
		{
			Engine::GetSFML().PlaySound(enemydata->GetEnemyType());
			this->enemydata->current_damage = this->enemydata->GetOriginalDamage();
			this->enemydata->current_HP = this->enemydata->GetOriginalHP();
			this->enemydata->current_adArmor = this->enemydata->GetOriginalAdArmor();
			this->enemydata->current_apArmor = this->enemydata->GetOriginalApArmor();
			battlefield.deadEnemyVector.push_back(*this);
			BattleObject empty(Vector2DInt{ -1, -1 });
			battlefield.GetObject(this->index).ChangeObject(empty);
		}
	}
}

void Battlefield::BattleObject::SetIsSelectAble(bool selectAble)
{
	isSelectAble = selectAble;
}

bool Battlefield::BattleObject::GetIsSelectAble()
{
	return isSelectAble;
}

void Battlefield::BattleObject::DealtAdDamage(int value, int offset)
{
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (1.0 + index.x) - Engine::GetWindow().GetSize().x / 2 + width_divide6 / 2 };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + index.y)) - Engine::GetWindow().GetSize().y / 2 + height_divide5 / 2 };
	if (characterdata != nullptr)
	{
		characterdata->DealtAdDamage(value, offset, { posX, posY });
	}
	else if (enemydata != nullptr)
	{
		enemydata->DealtAdDamage(value, offset, { posX, posY });
	}
}

void Battlefield::BattleObject::DealtApDamage(int value, int offset)
{
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (1.0 + index.x) - Engine::GetWindow().GetSize().x / 2 + width_divide6 / 2 };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + index.y)) - Engine::GetWindow().GetSize().y / 2 + height_divide5 / 2 };
	if (characterdata != nullptr)
	{
		characterdata->DealtApDamage(value, offset, { posX, posY });
	}
	else if (enemydata != nullptr)
	{
		enemydata->DealtApDamage(value, offset, { posX, posY });
	}
}

void Battlefield::BattleObject::DealtTrueDamage(int value, int offset)
{
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (1.0 + index.x) - Engine::GetWindow().GetSize().x / 2 + width_divide6 / 2 };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + index.y)) - Engine::GetWindow().GetSize().y / 2 + height_divide5 / 2 };
	if (enemydata != nullptr)
	{
		enemydata->DealtTrueDamage(value, offset, { posX, posY });
	}
}

void Battlefield::BattleObject::GetHealed(int value, int offset)
{
	double width_divide6 = doodle::Width / 6.0;
	double height_divide5 = doodle::Height / 5.0;

	double posX{ width_divide6 * (1.0 + index.x) - Engine::GetWindow().GetSize().x / 2 + width_divide6 / 2 };
	double posY{ Engine::GetWindow().GetSize().y - (height_divide5 * (2.0 + index.y)) - Engine::GetWindow().GetSize().y / 2 + height_divide5 / 2 };

	if (characterdata != nullptr)
	{
		characterdata->GetHealed(value, offset, { posX, posY });
	}
	else if (enemydata != nullptr)
	{
		enemydata->GetHealed(value, offset, { posX, posY });
	}
}

Battlefield battlefield;