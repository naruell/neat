﻿#pragma once
// GAM150
// Splash.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/GameState.h"

class Splash : public GameState 
{
public:
	Splash();

	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() override { return "Splash"; }

public:
	const double SplashDuration = 7;
};