﻿#pragma once

// GAM150
// card.h
// Team Neat
// Primary : Duhwan Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>
#include <functional>
#include <map>

#include "../Engine/text.h"
#include "../Engine/Particle/effectHeader.h"
#include "../Engine/Geometry/Vector2D.h"
#include "../Environment/constants.h"
#include "battlefield.h"

struct Card_Geometry
{
	double cur_textSize{ 10.0 };
	double cur_posX{ 400.0 }, cur_posY{ 0.0 }, cur_width{ 90.0 }, cur_height{ 126.0 }, cur_angle{ 2.14 };
	double outline_width{ 7.2 };

	const double textSize{ 13.0 };
	double posX{ 400.0 }, posY{ 0.0 }, width{ 90.0 }, height{ 126.0 }, angle{ 2.14 };

	const double hoveredTextScale{ 2.1 };
	const double hoveredXoffset{ 0.0 }, hoveredYoffset{ 100.0 }, hoveredAngle{ 0.0 };

	double draw_checkPoint_posX{ 0.0 }, draw_checkPoint_posY{ -300.0 };
	double discard_checkPoint_posY{ 0.0 };

	const double targeting_posX{ 300 }, targeting_posY{ 200 };

	const double disappearPosX{ -100000000 }, disappearPosY{ -100000000 }; /////// 이거 고쳐야되려나
};

enum class Card_Type
{
	NEUTRAL,
	RELATED,
	UI
};

enum class Card_Subtype
{
	PHYSICAL,
	MAGICAL,
	MOVE,
	HEAL,
	LUCK,
	TIMEFLOW,
	TIMEADD,
	DRAW,
	ELSE,
	UI
};

enum class Card_State
{
	NORMAL,
	DRAWING,
	DISCARDING,
	HOVERED,
	CLICKED,
	TARGETING,
	ACTIVATING,
	ACTIVATED
};

enum class Card_Effect_Progress
{
	SOLVED,
	ACTIVATING,
	ERR
};

enum class Card_Target
{
	NONE,
	EMPTY,
	CHARACTER,
	ENEMY,
	ALL,

	LINE_CHARACTER,
	LINE_ENEMY,
	LINE_ALL,

	ALL_CHARACTER,
	ALL_ENEMY,
	ALL_ALL,

	RANDOM_CHARACTER,
	RANDOM_ENEMY,
	RANDOM_ALL
};

struct Card_Effect
{
	Card_Effect(std::string explanation, Card_Target targetType, BattleAllyType targetAllyType, std::function<void(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)> effect, std::vector<int> effectValue, std::vector<Vector2DInt> moveableIndex = {})
		: explanation{ explanation }, targetType{ targetType }, targetAllyType{ targetAllyType }, effect{ effect }, effectValue{ effectValue }, moveableIndex{ moveableIndex } {};
	std::string explanation;
	Card_Target targetType;
	BattleAllyType targetAllyType;
	std::function<void(Vector2DInt, Battlefield::BattleObject, const Card_Effect&)> effect;

	std::vector<Vector2DInt> moveableIndex;
	bool operator<(const Card_Effect& ce) const { return (explanation < ce.explanation); }

	std::vector<int> effectValue;
};

class Card
{
public:
	Card_Geometry geometry;
	Vector2D drawOffset;

	std::unique_ptr<TextBox> nameTextBox;
	std::unique_ptr<TextBox> explanationTextBox;
	std::unique_ptr<TextBox> spendingTimeBox;

	std::string current_name;
	Card_Type current_type;
	std::vector<Card_Subtype> current_subtype;
	TimeOfDay current_notBomb;
	int current_spendingTime;

	bool doesUsingTime = true;

	Card_State pCard_state = Card_State::DRAWING;
	Card_State card_state = Card_State::DRAWING;

	std::vector<std::unique_ptr<Effect>> effectListOnActivated;

protected:
	const std::pair<int, int> cardID;
	const std::string original_name;
	const Card_Type original_type;
	const std::vector<Card_Subtype> original_subtype;
	const TimeOfDay original_notBomb;
	const int original_spendingTime;

	std::multimap < Card_Effect, Battlefield::BattleObject > targetAndEffectMap;
	Vector2DInt cardCastingHeroIndex;

	int cardLevel = 0;

	double drawAnimationDuration = 0.0;
	double discardAnimationDuration = 0.0;

public:
	Card();
	Card(std::pair<int, int> Vector2DInt, std::string name, Card_Type type, std::vector<Card_Subtype> subtype, TimeOfDay notBomb, int spendingTime, std::vector<Card_Effect>);

	void Draw();
	void Update();

	void OnHovered();
	void OnClicked();
	void OnUsed();

	void AllocateOnHovered(std::function<void()> newFunction);
	void AllocateOnClicked(std::function<void()> newFunction);

	void ForceToDiscard();

	std::multimap< Card_Effect, Battlefield::BattleObject >& GetTargetAndEffectMap();

	void SetOffset(Vector2D offset);
	const Vector2D GetOffset();

	void SetCardCastingHeroIndex(Vector2DInt index);
	Vector2DInt GetCardCastingHeroIndex() const;

	void SetExplanation(Battlefield::BattleObject* casterPtr = nullptr);
	void SetDefaultExplanation() const noexcept;

	std::pair<int, int> GetCardID() const noexcept;

	void Reset();

private:
	std::function<void()> ToBeExecutedOnHovered = nullptr;
	std::function<void()> ToBeExecutedOnClicked = nullptr;

	std::unique_ptr<doodle::Image> dividerSpt = nullptr;
	std::unique_ptr<doodle::Image> nameSpt = nullptr;

	std::unique_ptr<doodle::Image> notSpt = nullptr;
	std::unique_ptr<doodle::Image> bombSpt = nullptr;
	std::unique_ptr<doodle::Image> notBombSpt = nullptr;
	std::unique_ptr<doodle::Image> UISpt = nullptr;

	std::unique_ptr<doodle::Image> APiconSpt = nullptr;
	std::unique_ptr<doodle::Image> ADiconSpt = nullptr;
	std::unique_ptr<doodle::Image> MoveiconSpt = nullptr;
	std::unique_ptr<doodle::Image> HealiconSpt = nullptr;
	std::unique_ptr<doodle::Image> TimeAddiconSpt = nullptr;
	std::unique_ptr<doodle::Image> TimeFlowiconSpt = nullptr;
	std::unique_ptr<doodle::Image> DrawiconSpt = nullptr;
};