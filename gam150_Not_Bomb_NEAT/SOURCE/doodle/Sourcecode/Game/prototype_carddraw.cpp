﻿// GAM150
// prototype_carddraw.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "prototype_carddraw.h"

#include <doodle/environment.hpp>

#include "../Engine/Engine.h"
#include "hand.h"

void Prototype_carddraw::Load()
{

}

void Prototype_carddraw::Update(double)
{
	if (removeCardKey.IsKeyReleased())
	{
		player_hand.Remove_card(-1);
	}
	else if (clearHandKey.IsKeyReleased())
	{
		player_hand.Clear_hand();
	}
	else if (nextLevelKey.IsKeyReleased())
	{
		Engine::GetGameStateManager().SetNextState(GAMESTATE::ALPHA_HEROSELECT);
	}
}

void Prototype_carddraw::Draw()
{
	Engine::GetWindow().Clear({ 0, 0, 0, 255 });

	using namespace doodle;
	// x, y축
	set_outline_color(HexColor(0xFFFFFFFF));
	draw_line(-Width * 1.0, 0.0, Width * 1.0, 0.0);
	draw_line(0.0, -Height * 1.0, 0.0, Height * 1.0);

	// 가짜 덱 /////////////////////////////////////////////////////////
	int handSize = player_hand.Get_handSize();
	int deckSize = 15 - handSize;
	for (int i = 0; i < deckSize; ++i)
	{
		push_settings();
		// card geometry 기본값에 맞게 설정
		set_fill_color(HexColor{ 0xFF0000FF });
		apply_translate(400, i * 3);
		apply_rotate(2.14f);
		draw_rectangle(0, 0, 50, 80);
		pop_settings();
	}
	set_font_size(10);
	draw_text(std::to_string(deckSize), 400, deckSize * 3);
	/////////////////////////////////////////////////////////////////////

	player_hand.Update();
	player_hand.Draw();
}

void Prototype_carddraw::Unload()
{

}
