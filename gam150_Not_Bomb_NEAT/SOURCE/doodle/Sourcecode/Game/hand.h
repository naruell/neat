﻿#pragma once
// GAM150
// hand.h
// Team Neat
// Primary : Duhwan Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>

#include "card.h"

struct Zone
{
	double posX, posY, width, height;
};

class Player_Hand
{
public:
	Player_Hand();

	// void Run();
	void Update();
	void Draw();

	void Draw_card(std::pair<int, int> id, int amount = 1, int index = 0);
	void Draw_from_deck(int amount);
	void Remove_card(int index);
	void Clear_hand();

	int Get_handSize();
	int Get_real_handSize();

	bool DoesAnyCardHovered() const noexcept;
	bool DoesAnyCardClicked() const noexcept;
	bool DoesAnyCardTargeting() const noexcept;

	void SetLastCardCasterIndex(Vector2DInt indexTo);
	Vector2DInt GetLastCardCasterIndex();

	void MoveCardActivateZone(Vector2D offset);

	std::unique_ptr<Card>& GetCard(int index);
	const int GetLastCardIndex() const noexcept;

	void Set_current_manxHnadSize(int sizeTo);
	void Unload();

	int current_maxHandSize{ 8 };
private:
	void Arrange();

	const int original_maxHandSize{ 8 };
	std::vector<std::unique_ptr<Card>> cardList;
	
	Vector2DInt lastCardCasterIndex;
	int hoveredCardIndex{ -1 };
	int clickedCardIndex{ -1 };
	int targetingCardIndex{ -1 };
	int activatingCardIndex{ -1 };

	Zone cardActivateZone{ 0.0f , 0.0f, 750.0f, 300.0f };
	Vector2D drawOffset;
};

extern Player_Hand player_hand;