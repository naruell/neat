﻿#pragma once
// GAM150
// alpha_HeroSelect.h
// Team Neat
// Primary : Byeongjun Kim
// Secondary : Sunghwan Cho
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../Engine/Background.h"
#include "../Engine/GameState.h"
#include "battlefield.h"
#include "Character/Warrior.h"
#include "Character/Priest.h"
#include "Character/Mage.h"
#include "Character/HighNoon.h"
#include "Character/Solaire.h"
#include "Character/Vampire.h"


class Alpha_HeroSelect : public GameState
{
public:
	void Load() override;
	void Update(double deltaTime) override;
	void Draw() override;
	void Unload() override;

	std::string GetName() { return "Alpha_HeroSelect"; }

private:
	Background background;

	Warrior w = { "warrior", 20, 2, -2, 0, -2,CHARACTERTYPE::WARRIOR, CHARACTERISTIC::NEUTRAL };
	Warrior* w_p = &w;

	Priest p = { "priest", 19, -2, 1, 0, 2,CHARACTERTYPE::PRIEST, CHARACTERISTIC::NEUTRAL };
	Priest* p_p = &p;

	Mage m = { "mage", 17, -2, 2, 0, 1, CHARACTERTYPE::MAGE, CHARACTERISTIC::NEUTRAL };
	Mage* m_p = &m;

	HighNoon h = { "highnoon", 19, 2, -2, 0, -1, CHARACTERTYPE::HIGHNOON, CHARACTERISTIC::NEUTRAL };
	HighNoon* h_p = &h;

	Solaire s = { "solaire", 17, 1, 1, 0, 0, CHARACTERTYPE::SOLAIRE, CHARACTERISTIC::NEUTRAL };
	Solaire* s_p = &s;

	Vampire v = { "vampire", 17, 0, 2, 0, -2, CHARACTERTYPE::VAMPIRE, CHARACTERISTIC::NEUTRAL };
	Vampire* v_p = &v;
};