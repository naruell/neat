﻿// GAM150
// Window.cpp
// Team Neat
// Primary : Duhwan Kim, Kevin Wright
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Window.h"

#include <doodle/window.hpp>        // doodle window stuff
#include <doodle/drawing.hpp>	// doodle clear background

#include "Engine.h"			// Engine.GetWindow()

void Window::Init(std::string windowName) 
{
	doodle::create_window(windowName);
	doodle::set_rectangle_mode(doodle::RectMode::Center);
	doodle::set_image_mode(doodle::RectMode::Center);

	doodle::toggle_full_screen();
}

void Window::Resize(int newWidth, int newHeight) { screenSize.x = newWidth; screenSize.y = newHeight; }

Vector2DInt Window::GetSize() { return screenSize; }

void Window::Clear(doodle::Color color) { doodle::clear_background(doodle::Color{ color }); }

void Window::Update() { doodle::update_window(); }

void on_window_resized(int new_width, int new_height) { Engine::GetWindow().Resize(new_width, new_height); }