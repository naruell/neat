﻿// GAM150
// textManager.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "textManager.h"

#include <fstream>

#include "Engine.h"
#include "../Environment/constants.h"

void TextManager::Init()
{
	Engine::GetLogger().LogEvent("Load Text Manager");
	for (std::string fontPath : FONT_PATHS)
	{
		loadFont(fontPath);
	}
}

const FontInfo& TextManager::getFontInfo(int fontID) const
{
	return fontInfoList.find(fontID)->second;
}

const std::map<int, CharacterInfo>& TextManager::getCharacterInfo(int fontID) const
{
	return charInfoList.find(fontID)->second;
}

const double TextManager::CalculateStringWidth(int fontID, const std::string& text, double doodleTextSize) const noexcept
{
	if (text.size() == 0) { return 0; }

	const auto& fontInfo = fontInfoList.find(fontID)->second;
	const auto& charInfo = charInfoList.find(fontID)->second;
	const double scaledTextSize = doodleTextSize / static_cast<double>(fontInfo.size);

	const auto& firstChar = charInfo.find(text[0])->second;
	if (text.size() == 1)
	{
		if (text == " ") {return static_cast<double>(firstChar.xOffset + firstChar.xAdvance - fontInfo.padding[1]);}
		return (static_cast<double>(firstChar.xOffset) + static_cast<double>(firstChar.width)) * scaledTextSize;
	}

	double width = static_cast<double>(firstChar.xOffset + firstChar.xAdvance - fontInfo.padding[1]);
	
	int textSize = static_cast<int>(text.size());
	for (int index = 1; index < textSize - 1; ++index)
	{
		const auto& curCharInfo = charInfo.find(text[index])->second;
		width += static_cast<double>(curCharInfo.xAdvance) - static_cast<double>(fontInfo.padding[1]);
	}

	width += charInfo.find(text[text.size() - 1])->second.width;
	
	return width * scaledTextSize;
}

const double TextManager::CalculateStringWidth(TextBox& textBox, int lineIndex) const noexcept
{
	return CalculateStringWidth(textBox.GetFontInfo().id, textBox.GetText(lineIndex), textBox.GetTextInfo().doodleTextSize);
}

const double TextManager::CalculateStringWidth(TextBox& textBox, std::string& text) const noexcept
{
	return CalculateStringWidth(textBox.GetFontInfo().id, text, textBox.GetTextInfo().doodleTextSize);
}

enum class ReadMode
{
	KEY,
	VALUE
};

void TextManager::loadFont(std::string fontPath)
{
	Engine::GetLogger().LogEvent("Loading font from " + fontPath);
	const int fontID = doodle::create_distance_field_bitmap_font(fontPath);
	if (fontID < 0)
	{
		Engine::GetLogger().LogError("Failed to load font from " + fontPath + ".");
		return;
	}

	std::ifstream inputFileStream{ fontPath };
	if (!inputFileStream) 
	{
		Engine::GetLogger().LogError("Failed to open file for reading!");
		return;
	}
	inputFileStream.exceptions(inputFileStream.exceptions() | std::ios_base::badbit);
	
	FontInfo fontInfo;
	std::map<int, CharacterInfo> charactorInfo;

	std::map<std::string, std::vector<std::map<std::string, std::vector<std::string>>>> fileData;
	// ::map< category  , std::vector<std::map<    key    ,    value   <           >>>>

	Engine::GetLogger().LogEvent("Reading font information from " + fontPath);

	std::string pCategory;
	while (!inputFileStream.eof())
	{
		std::string category;
		inputFileStream >> category;
		if (category == pCategory)
		{
			fileData[category].resize(fileData[category].size() + 1);
		}
		else if (fileData[category].size() == 0)
		{
			fileData[category].resize(1);
		}

		std::string tempLine;
		std::getline(inputFileStream, tempLine);

		std::string keyStr;
		std::vector<std::string> valueStr = { "" };
		int valueIndex = 0;

		ReadMode readMode = ReadMode::KEY;
		bool isReadingString = false;

		int textIndex = 0;
		for (int i : tempLine)
		{
			if (readMode == ReadMode::KEY && i == ' ') { ++textIndex; continue; }

			switch (readMode)
			{
			case ReadMode::KEY:
				if (i != '=') { keyStr += static_cast<char>(i); }
				else { readMode = ReadMode::VALUE; }
				break;

			case ReadMode::VALUE:
				if (i == '\"' || (i == ' ' && isReadingString == false))
				{
					if (i == '\"') { isReadingString = !isReadingString; }
					if (isReadingString == false)
					{
						// Reading key & value string complete
						fileData[category][fileData[category].size() - 1].insert(std::make_pair(keyStr, valueStr));

						keyStr = "";
						valueStr = { "" };
						valueIndex = 0;
						readMode = ReadMode::KEY;
					}
				}
				else if (i == ',')
				{
					valueStr.push_back("");
					++valueIndex;
				}
				else
				{
					valueStr[valueIndex] += static_cast<char>(i);
				}
				break;
			}
			++textIndex;
			if (textIndex == tempLine.size() && (keyStr != "" && valueStr.size() != 0))
			{
				fileData[category][fileData[category].size() - 1].insert(std::make_pair(keyStr, valueStr));

				keyStr = "";
				valueStr = { "" };
				valueIndex = 0;
				readMode = ReadMode::KEY;
			}
		}
		pCategory = category;
	}

	fontInfo.id         = fontID;
	fontInfo.name       = fileData["info"][0]["face"][0];
	fontInfo.size       = std::stoi(fileData["info"][0]["size"][0]);
	fontInfo.padding[0] = std::stoi(fileData["info"][0]["padding"][0]);
	fontInfo.padding[1] = std::stoi(fileData["info"][0]["padding"][1]);
	fontInfo.padding[2] = std::stoi(fileData["info"][0]["padding"][2]);
	fontInfo.padding[3] = std::stoi(fileData["info"][0]["padding"][3]);
	fontInfo.height     = std::stoi(fileData["common"][0]["lineHeight"][0]);

	for (auto charFileData : fileData["char"])
	{
		CharacterInfo singleCharInfo;
		singleCharInfo.id       = std::stoi(charFileData["id"][0]);
		singleCharInfo.width    = std::stoi(charFileData["width"][0]);
		singleCharInfo.xOffset  = std::stoi(charFileData["xoffset"][0]);
		singleCharInfo.xAdvance = std::stoi(charFileData["xadvance"][0]);
		charactorInfo.insert(std::make_pair(singleCharInfo.id, singleCharInfo));
	}
	
	fontInfoList.insert(std::make_pair(fontID, fontInfo));
	charInfoList.insert(std::make_pair(fontID, charactorInfo));
	Engine::GetLogger().LogEvent("Successfully loaded font \"" + fontInfo.name + ", ID : " + std::to_string(fontID) + "\"");
}