﻿// GAM150
// TextureManager.cpp
// Team Neat
// Primary : Byeongjun Kim, Kevin Wright
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "TextureManager.h"
#include "Engine.h"
#include "Texture.h"

Texture* TextureManager::Load(const std::string& filePath)
{
	if (pathToTexture.find(filePath) == pathToTexture.end()) 
	{
		pathToTexture[filePath] = new Texture(filePath);
	}
	return pathToTexture[filePath];
}

void TextureManager::Unload()
{
	Engine::GetLogger().LogEvent("Clear Textures");
	for (std::pair<std::string, Texture*> texturePtr : pathToTexture) 
	{
		delete texturePtr.second;
	}
	pathToTexture.clear();
}