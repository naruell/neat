﻿#pragma once
// GAM150
// text.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <map>
#include <doodle/color.hpp>
#include <doodle/drawing.hpp>

#include "Geometry/Vector2D.h"

enum class TextAlignX
{
	LEFT,
	RIGHT,
	CENTER
};

enum class TextAlignY
{
	TOP,
	CENTER
};

enum class AutoNextLine
{
	BY_WORD,
	BY_CHARACTER
};

struct TextInfo
{
	double doodleTextSize;
	TextAlignX alignX;
	TextAlignY alignY;
	doodle::Color fillColor;
	doodle::Color outlineColor;
};

struct FontInfo
{
	int id;
	std::string name;
	int size;
	int padding[4]; // Up, Right, Down, Left
	int height;
};

struct CharacterInfo
{
	int id;
	int width;
	int xOffset;
	int xAdvance;
};

struct TextBox
{
public:
	TextBox(int fontID, std::string text, Vector2D pos, Vector2D size);
	TextBox(int fontID, std::string text, Vector2D pos, Vector2D size, TextInfo textInfo, AutoNextLine autoNextLine);
	// AutoNextLine 에 따라 텍스트 분할하기

	void AutoWrap();
	void Draw() noexcept;

	void SetText(std::string textTo);
	// 텍스트 분할 다시하기
	const std::string& GetText() const noexcept;
	const std::string& GetText(int lineIndex) const noexcept;
	// 인덱스 체크!!

	const Vector2D& GetSize() const noexcept;
	void SetSize(Vector2D sizeTo);

	const Vector2D& GetPosition() const noexcept;
	void setPosition(Vector2D positionTo);

	const FontInfo& GetFontInfo() const noexcept;
	const std::map<int, CharacterInfo>& GetCharacterInfo() const noexcept;
	const TextInfo& GetTextInfo() const noexcept;
	void SetTextInfo(TextInfo textInfoTo);

	void SetTextSize(double textSizeTo); // Special cases to change textInfo directly.
	void SetTextAlignX(TextAlignX alignTo);
	void SetTextAlignY(TextAlignY alignTo);
	void SetTextFillColor(doodle::Color colorTo);

	const AutoNextLine& GetAutoNextLine() const noexcept;
	void SetAutoNextLine(AutoNextLine autoNextLineTo);

private:

	std::string fullText;
	std::vector<std::string> dividedText;
	std::vector<double> dividedTextWidth;

	Vector2D position;
	Vector2D size;

	TextInfo textInfo;
	AutoNextLine autoNextLine;

	const FontInfo& fontInfo;
	const std::map<int, CharacterInfo>& characterInfo;

	bool shouldBeAutoWrappped = true;
};