﻿// GAM150
// Logger.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Logger.h"

#include <iostream>    // cout
#include <Windows.h>

enum TextColor : int
{
	BLACK, BLUE, GREEN, AQUA, RED, PURPLE,
	YELLOW, WHITE, GRAY, LIGHTBLUE, LIGHTGREEN,
	LIGHTAQUA, LIGHTRED, LIGHTPURPLE, LIGHTYELLOW,
	BRIGHTWHITE
};

Logger::Logger(Severity severity, bool useConsole) : outStream("Trace.log"), startTime(std::chrono::system_clock::now()) 
{
	minLevel = severity;
	if (useConsole == true) { outStream.set_rdbuf(std::cout.rdbuf()); }
}

void Logger::Log(Logger::Severity severity, std::string message) 
{
	if (severity >= minLevel) 
	{
		outStream.precision(4);
		outStream << '[' << std::fixed << GetSecondsSinceStart() << "]\t";

		switch (severity) 
		{
		case Severity::Verbose:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), TextColor::GRAY);
			outStream << "Verb \t";
			break;
		case Severity::Debug:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), TextColor::LIGHTYELLOW);
			outStream << "Debug\t";
			break;
		case Severity::Error:
			SetConsoleTextAttribute( GetStdHandle(STD_OUTPUT_HANDLE), TextColor::LIGHTRED);
			outStream << "Error\t";
			break;
		case Severity::Event:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), TextColor::LIGHTGREEN);
			outStream << "Event\t";
			break;
		}
		if (severity != Severity::Error)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), TextColor::WHITE);
		}
		outStream << message;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), TextColor::WHITE);

#ifdef _DEBUG
		outStream << std::endl;
#else
		outStream << '\n';
#endif
	}
}