﻿#pragma once
// GAM150
// Window.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>
#include <doodle/color.hpp>

#include "Geometry/Vector2D.h"

class Window 
{
public:
	void Init(std::string windowName);
	void Resize(int newWidth, int newHeight);
	Vector2DInt GetSize();
	void Clear(doodle::Color color);
	void Update();
private:
	Vector2DInt screenSize;
};