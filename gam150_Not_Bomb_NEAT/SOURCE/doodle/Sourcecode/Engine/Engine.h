﻿#pragma once
// GAM150
// Engine.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <chrono>
#include <random>

#include "GameStateManager.h"
#include "Input.h"
#include "Window.h"
#include "Logger.h"
#include "Particle/ParticleSystem.h"
#include "textManager.h"
#include "TextureManager.h"
#include "SFML.h"
#include "../Game/GameMap.h"


class Engine 
{
public:
	static Engine& Instance() { static Engine instance; return instance; }
	static Logger& GetLogger() { return Instance().logger; }
	static Input& GetInput() { return Instance().input; }
	static Window& GetWindow() { return Instance().window; }
	static GameStateManager& GetGameStateManager() { return Instance().gameStateManager; }
	static EffectSystem& GetEffectSystem() { return Instance().effectSystem; }
	static TextManager& GetTextManager() { return Instance().textManager; }
	static TextureManager& GetTextureManager() { return Instance().textureManager; }
	static SFML& GetSFML() { return Instance().sfml; }
	static GameMap& GetGameMap() { return Instance().gameMap; }

	void Init(std::string windowName);
	void Shutdown();
	void Update();
	bool HasGameEnded();

	static double GetElapsedTime() { return Instance().elapsedTime; }
	static double GetDeltaTime() { return Instance().deltaTime; }

	void ShowCollision(bool newShowCollisionValue) { showCollision = newShowCollisionValue; }
	bool ShowCollision() { return showCollision; }

	std::mt19937 gen;
private:
	Engine();
	~Engine();

	Logger logger;
	GameStateManager gameStateManager;
	Input input;
	Window window;
	EffectSystem effectSystem;
	TextManager textManager;
	TextureManager textureManager;
	SFML sfml;
	GameMap gameMap;

	static constexpr double TARGET_FPS = 30.0;
	std::chrono::system_clock::time_point lastTick;
	double deltaTime;
	double elapsedTime = 0;

	int frameCount = 0;
	double sumFPS = 0;
	std::chrono::system_clock::time_point FPS_Logging_Tick;
	static constexpr double FPS_Logging_Term = 5.0;

	bool showCollision; // boolean for whether showing collision box or not
};