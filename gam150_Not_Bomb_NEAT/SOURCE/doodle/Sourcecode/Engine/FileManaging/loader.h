﻿#pragma once
// GAM150
// loader.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "../../Json/json/json.h"

using namespace Json;

void EncryptingData(std::string filePath);

void DecryptingData(std::string filePath);

void LoadGameData(std::string filePath);