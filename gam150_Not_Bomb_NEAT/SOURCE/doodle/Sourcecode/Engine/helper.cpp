﻿// GAM150
// Helper.cpp
// Team Neat
// Primary : Duhwan Kim, Byeongjun Kim
// Secondary : Junhyuk Cha
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Helper.h"

#include <iostream>
#include <unordered_map>

#include "Engine.h"
#include "Texture.h"
#include "TextureManager.h"
#include "../Game/cardEffectFunction.h"
#include "../Environment/constants.h"

double Map(double value, double low1, double high1, double low2, double high2)
{
	return (low2 + (value - low1) * (high2 - low2) / (high1 - low1));
}

CustomButton* MakeButton(Vector2DInt StartPos, Vector2D ButtonSize, std::function<void()> WhatItWillDo = []() { Engine::GetLogger().LogError("Empty Button!"); })
{
	return new CustomButton(StartPos, ButtonSize, WhatItWillDo);
}

bool isRectCollided(Vector2D beChecked, Vector2D bottemLeft, Vector2D topRight)
{
	return (bottemLeft.x <= beChecked.x && beChecked.x <= topRight.x &&
		bottemLeft.y <= beChecked.y && beChecked.y <= topRight.y);
}
std::string Spt2Png(std::string spriteInfoFile)
{
	if (spriteInfoFile.substr(spriteInfoFile.find_last_of('.')) != ".spt")
	{
		throw std::runtime_error("Bad Filetype.  " + spriteInfoFile + " not a sprite info file (.spt)");
	}
	std::ifstream inFile(spriteInfoFile);

	if (inFile.is_open() == false)
	{
		throw std::runtime_error("Failed to load " + spriteInfoFile);
	}

	std::string text;
	inFile >> text;
	return text;
}

Card* Vector2Card([[maybe_unused]] std::pair<int, int> id)
{
	switch (id.first)
	{
	case static_cast<int>(CHARACTERTYPE::UI):
		switch (id.second)
		{
		case -1:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), -1 }, "Warrior", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroPicking::UI_CardEffect_Warrior });
			break;
		case -2:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), -2 }, "Mage", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroPicking::UI_CardEffect_Mage });
			break;
		case -3:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), -3 }, "Priest", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{HeroPicking::UI_CardEffect_Priest});
			break;
		case -4:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), -4 }, "Vampire", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroPicking::UI_CardEffect_Vampire });
			break;
		case -6:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), -6 }, "Solaire", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroPicking::UI_CardEffect_Solaire });
			break;
		case -7:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), -7 }, "Highnoon", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroPicking::UI_CardEffect_Highnoon });
			break;

		case 0:
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 1 }, "New Game", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_NewGame });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 2 }, "Credit", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_Credit });
			break;

		case 3:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 3 }, "Quit", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_Quit });
			break;

		case 4:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 4 }, "Back", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_Title });
			break;

		case 5:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 5 }, "Yeah!!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_CreateTestEffect });
			break;

		case 6:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 6 }, "Confirm!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_ConfirmHeroSelect });
			break;

		case 7:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 7 }, "Battle!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_ConfirmRoster });
			break;

		case 8:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 8 }, "Deck Managing!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_DeckManaging });
			break;

		case 9:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 9 }, "Confirm!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_ConfirmDeckManaging });
			break;
		case 10:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 10 }, "Need some help?", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_DrawCardRemoveExplanation });
			break;
		case 11:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 11 }, "Need some help?", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_DrawCardHeroInformationExplanation });
			break;
		case 12:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 12 }, "   Management is finished!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_CardManagingFinish });
			break;
		case 13:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 13 }, "Accept my destiny", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_AgreeEvent });
			break;
		case 14:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 14 }, "Refuse my destiny", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_DisagreeEvent });
			break;

		case 16:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 16 }, "AD", Card_Type::UI, { Card_Subtype::UI, Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroOffset::UI_CardEffect_AD });
			break;
		case 17:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 17 }, "AP", Card_Type::UI, { Card_Subtype::UI, Card_Subtype::MAGICAL }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroOffset::UI_CardEffect_AP });
			break;
		case 18:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 18 }, "Heal", Card_Type::UI, { Card_Subtype::UI, Card_Subtype::HEAL }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroOffset::UI_CardEffect_Heal });
			break;
		case 19:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 19 }, "Movement", Card_Type::UI, { Card_Subtype::UI, Card_Subtype::MOVE }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ HeroOffset::UI_CardEffect_Movement });
			break;
		case 20:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 20 }, "Page Up!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ CardManaging::UI_CardEffect_PageUp });
			break;
		case 21:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 21 }, "Page Down!", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ CardManaging::UI_CardEffect_PageDown  });
			break;
		case 22:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 22 }, "Tutorial", Card_Type::UI, { Card_Subtype::UI, Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_Tutorial });
			break;
		case 23:
			return new Card({ static_cast<int>(CHARACTERTYPE::UI), 23 }, "Skip!", Card_Type::UI, { Card_Subtype::UI, Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_SkipTutorial });
			break;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::NEUTRAL): // Neutral Card
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 0 }, "Smash!", Card_Type::NEUTRAL, { Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Neutral_CardEffect_Smash });
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 1 }, "Fireball", Card_Type::NEUTRAL, { Card_Subtype::MAGICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Neutral_CardEffect_Fireball });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 2 }, "Slash!", Card_Type::NEUTRAL, { Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Neutral_CardEffect_Slash });
			break;

		case 3:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 3 }, "Firestrike!", Card_Type::NEUTRAL, { Card_Subtype::MAGICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Neutral_CardEffect_Flamestrike });
			break;

		case 4:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 4 }, "Up and Down!", Card_Type::NEUTRAL, { Card_Subtype::MOVE }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Neutral_CardEffect_UpandDown });
			break;

		case 5:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 5 }, "Left and Right!", Card_Type::NEUTRAL, { Card_Subtype::MOVE }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Neutral_CardEffect_LeftandRight });
			break;

		case 6:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 6 }, "Cooperation", Card_Type::NEUTRAL, { Card_Subtype::DRAW }, TimeOfDay::NIGHT, 1, std::vector<Card_Effect>{ Neutral_CardEffect_Cooperation });
			break;

		case 7:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 7 }, "Time Managing", Card_Type::NEUTRAL, { Card_Subtype::TIMEADD, Card_Subtype::DRAW }, TimeOfDay::NIGHT, 0, std::vector<Card_Effect>{ Netural_CardEffect_TimeManaging });
			break;

		case 8:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 8 }, "First Aid", Card_Type::NEUTRAL, { Card_Subtype::HEAL }, TimeOfDay::DAY, 1, std::vector<Card_Effect>{ Netural_CardEffect_FirstAid });
			break;

		case 9:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 9 }, "Sun Spike", Card_Type::NEUTRAL, { Card_Subtype::ELSE }, TimeOfDay::DAY, 2, std::vector<Card_Effect>{ Netural_CardEffect_SunSpike });
			break;

		case 10:
			return new Card({ static_cast<int>(CHARACTERTYPE::NEUTRAL), 10 }, "Like a King!", Card_Type::NEUTRAL, { Card_Subtype::MOVE }, TimeOfDay::NORMAL, 2, std::vector<Card_Effect>{ Neutral_CardEffect_LikeaKing });
			break;;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::WARRIOR):
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::WARRIOR), 0 }, "Shield Slam", Card_Type::RELATED, { Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 2, std::vector<Card_Effect>{ Warrior_CardEffect_ShieldSlam });
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::WARRIOR), 1 }, "Execute", Card_Type::RELATED, { Card_Subtype::PHYSICAL, Card_Subtype::DRAW }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Warrior_CardEffect_Execute });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::WARRIOR), 2 }, "Whirlwind", Card_Type::RELATED, { Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Warrior_CardEffect_WhirlWind });
			break;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::MAGE):
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::MAGE), 0 }, "Blink", Card_Type::RELATED, { Card_Subtype::MOVE }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Mage_CardEffect_Blink });
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::MAGE), 1 }, "Arcane Intellect", Card_Type::RELATED, { Card_Subtype::MAGICAL, Card_Subtype::DRAW }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Mage_CardEffect_ArcaneIntellect });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::MAGE), 2 }, "Shock!", Card_Type::RELATED, { Card_Subtype::MAGICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Mage_CardEffect_Shock });
			break;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::PRIEST):
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::PRIEST), 0 }, "Healing Hands", Card_Type::RELATED, { Card_Subtype::HEAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{Priest_CardEffect_HealingHands});
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::PRIEST), 1 }, "Healing Rain", Card_Type::RELATED, { Card_Subtype::HEAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{Preist_CardEffect_HealingRain, Preist_CardEffect_HealingRain, Preist_CardEffect_HealingRain});
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::PRIEST), 2 }, "Circle of Healing", Card_Type::RELATED, { Card_Subtype::HEAL }, TimeOfDay::NORMAL, 2, std::vector<Card_Effect>{Priest_CardEffect_CircleofHealing});
			break;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::VAMPIRE):
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::VAMPIRE), 0 }, "To the Moon", Card_Type::RELATED, { Card_Subtype::TIMEFLOW, Card_Subtype::DRAW }, TimeOfDay::NIGHT, 0, std::vector<Card_Effect>{ Vampire_CardEffect_TotheMoon });
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::VAMPIRE), 1 }, "Blood Reckoning", Card_Type::RELATED, { Card_Subtype::MAGICAL, Card_Subtype::HEAL }, TimeOfDay::NIGHT, 2, std::vector<Card_Effect>{ Vampire_CardEffect_BloodReckoning });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::VAMPIRE), 2 }, "Chalice of Blood", Card_Type::RELATED, { Card_Subtype::ELSE }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ Vampire_CardEffect_ChaliceofBlood });
			break;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::SOLAIRE):
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::SOLAIRE), 0 }, "Solar Blaze", Card_Type::RELATED, { Card_Subtype::PHYSICAL }, TimeOfDay::DAY, 1, std::vector<Card_Effect>{ Solaire_CardEffect_SolarBlaze });
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::SOLAIRE), 1 }, "Hour of Glory", Card_Type::RELATED, { Card_Subtype::TIMEFLOW }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ Solaire_CardEffect_HourofGlory });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::SOLAIRE), 2 }, "Sweltering Sun", Card_Type::RELATED, { Card_Subtype::PHYSICAL }, TimeOfDay::DAY, 2, std::vector<Card_Effect>{ Solaire_CardEffect_SwelteringSun });
			break;
		}
		break;

	case static_cast<int>(CHARACTERTYPE::HIGHNOON):
		switch (id.second)
		{
		case 0:
			return new Card({ static_cast<int>(CHARACTERTYPE::HIGHNOON), 0 }, "Scatter Shot", Card_Type::RELATED, { Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 2, std::vector<Card_Effect>{HighNoon_CardEffect_IndiscriminateShot, HighNoon_CardEffect_IndiscriminateShot, HighNoon_CardEffect_IndiscriminateShot });
			break;

		case 1:
			return new Card({ static_cast<int>(CHARACTERTYPE::HIGHNOON), 1 }, "Fast Reload", Card_Type::RELATED, { Card_Subtype::PHYSICAL, Card_Subtype::DRAW }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ HighNoon_CardEffect_FastReload });
			break;

		case 2:
			return new Card({ static_cast<int>(CHARACTERTYPE::HIGHNOON), 2 }, "Bull's Eye", Card_Type::RELATED, { Card_Subtype::PHYSICAL }, TimeOfDay::NORMAL, 1, std::vector<Card_Effect>{ HighNoon_CardEffect_BullsEye });
			break;
		}
		break;
	}
	std::cout << ("You tried to draw {" + std::to_string(id.first) + ", " + std::to_string(id.second) + "}!\n");
	Engine::GetLogger().LogError("You tried to draw {" + std::to_string(id.first) + ", " + std::to_string(id.second) + "}!");
	return new Card({ 0, 3 }, "Quit", Card_Type::UI, { Card_Subtype::UI }, TimeOfDay::NORMAL, 0, std::vector<Card_Effect>{ UI_CardEffect_Quit });
}

std::string Int2CharacterTypeString(int num)
{
	switch (num)
	{
	case 0:
		return "NEUTRAL";
		break;
	case 1:
		return "WARRIOR";
		break;
	case 2:
		return "MAGE";
		break;
	case 3:
		return "PRIEST";
		break;
	case 4:
		return "VAMPIRE";
		break;
	case 5:

		break;
	case 6:
		return "SOLAIRE";
		break;
	case 7:
		return "HIGHNOON";
		break;
	}
	return "ERROR";
}

std::string Int2CardTypeString(int num)
{
	switch (num)
	{
	case 0:
		return "PHYSICAL";
		break;
	case 1:
		return "MAGICAL";
		break;
	case 2:
		return "MOVE";
		break;
	case 3:
		return "HEAL";
		break;
	case 4:
		return "LUCK";
		break;
	case 5:
		return "TIMEFLOW";
		break;
	case 6:
		return "TIMEADD";
		break;
	case 7:
		return "DRAW";
		break;
	case 8:
		return "ELSE";
		break;
	case 9:
		return "UI";
		break;
	}
	return "ERROR";
}


CustomButton::CustomButton(Vector2DInt buttonPos, Vector2DInt buttonSize, std::function<void()> whatItWillDo) : position(buttonPos), size(buttonSize), func(whatItWillDo) {}

CustomButton::~CustomButton()
{
	position = { 0,0 };
	size = { 0,0 };
}

void CustomButton::Load(std::string filepath)
{
	TexturePtr = Engine::GetTextureManager().Load(filepath);
}

void CustomButton::Update()
{
	if (Engine::GetInput().IsMousePressed(Input::MouseButton::Left))
	{
		if (Engine::GetInput().GetMousePos().x >= position.x && Engine::GetInput().GetMousePos().x <= position.x + size.x &&
			Engine::GetInput().GetMousePos().y >= position.y && Engine::GetInput().GetMousePos().y <= position.y + size.y)
		{
			pressing = true;
		}
	}
	if (pressing)
	{
		if (Engine::GetInput().IsMouseReleased(Input::MouseButton::Left))
		{
			func();
			pressing = false;
			// func = nullptr; // 나중에 넣어야할 듯?
		}
	}
}

void CustomButton::Draw(TransformMatrix transformMatrix)
{
	if (TexturePtr == nullptr)
	{
		doodle::push_settings();
		doodle::set_rectangle_mode(doodle::RectMode::Corner);
		doodle::set_outline_color(doodle::Color::Color(0xFF0000FF));
		doodle::draw_rectangle(position.x, position.y, size.x, size.y);
		doodle::pop_settings();
	}
	//if (TexturePtr != nullptr) 
	else
	{
		TexturePtr->Draw(transformMatrix * TranslateMatrix{ static_cast<double>(position.x + TexturePtr->GetSize().x / 2),
			static_cast<double>(position.y + TexturePtr->GetSize().y / 2) }
		, { 0,0 }, { TexturePtr->GetSize().x,TexturePtr->GetSize().y });
	}
}

Texture* CustomButton::getTexturePtr()
{
	return TexturePtr;
}

doodle::Color GetHeroColor(CHARACTERTYPE type)
{
	switch (type)
	{
	case CHARACTERTYPE::UI:
		return doodle::HexColor(0x38FFC0FF);
		break;

	case CHARACTERTYPE::NEUTRAL:
		return doodle::HexColor(0x7F6B5DFF);
		break;

	case CHARACTERTYPE::WARRIOR:
		return doodle::HexColor(0xFF0000FF);
		break;

	case CHARACTERTYPE::MAGE:
		return doodle::HexColor(0xFF00FFFF);
		break;

	case CHARACTERTYPE::PRIEST:
		return doodle::HexColor(0xFFFFFFFF);
		break;

	case CHARACTERTYPE::VAMPIRE:
		return doodle::HexColor(0x800000FF);
		break;

	case CHARACTERTYPE::SOLAIRE:
		return doodle::HexColor(0xFFFF00FF);
		break;

	case CHARACTERTYPE::HIGHNOON:
		return doodle::HexColor(0xFF7700FF);
		break;
	}
	return doodle::HexColor(0x7F6B5DFF);;
}

std::map<std::pair<int, int>, std::string> cardIDToSound
{
	{{0, 0}, "Assets/Sound/hammer.wav"}, {{0, 1}, "Assets/Sound/fire.wav"}, {{0, 2}, "Assets/Sound/sword.wav"}, {{0, 3}, "Assets/Sound/fire.wav"}, {{0, 4}, "Assets/Sound/running.wav"}, {{0, 5}, "Assets/Sound/running.wav"}, {{0, 6}, "Assets/Sound/reinforce.wav"}, {{0, 7}, "Assets/Sound/clock.wav"}, {{0, 8}, "Assets/Sound/heal.wav"}, {{0, 9}, "Assets/Sound/fire.wav"}, {{0, 10}, "Assets/Sound/running.wav"},
	{{1, 0}, "Assets/Sound/hammer.wav"}, {{1, 1}, "Assets/Sound/sword.wav"}, {{1, 2}, "Assets/Sound/sword.wav"},
	{{2, 2}, "Assets/Sound/fire.wav"},
	{{3, 0}, "Assets/Sound/heal.wav"}, {{3, 1}, "Assets/Sound/heal.wav"}, {{3, 2}, "Assets/Sound/heal.wav"},
	{{4, 0}, "Assets/Sound/clock.wav"}, {{4, 1}, "Assets/Sound/fire.wav"}, {{4, 2}, "Assets/Sound/heal.wav"},
	{{6, 0}, "Assets/Sound/sword.wav"}, {{6, 1}, "Assets/Sound/clock.wav"}, {{6, 2}, "Assets/Sound/hammer.wav"},
	{{7, 0}, "Assets/Sound/gun.wav"}, {{7, 2}, "Assets/Sound/gun.wav"},

};

std::unordered_map<ENEMYTYPE, std::string> enemyTypeToSound
{
	{ENEMYTYPE::SLIME, "Assets/Sound/slime_dead.wav" }
};