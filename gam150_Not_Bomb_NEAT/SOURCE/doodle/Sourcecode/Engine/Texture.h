﻿#pragma once
// GAM150
// texture.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <doodle/image.hpp>	// doodle::image

#include "Geometry/Vector2D.h"	//Vector2D, Vector2DInt
#include "TransformMatrix.h"

typedef unsigned Color;

class Texture {
	friend class TextureManager;
public:
	Texture();
	Texture(const std::filesystem::path& filePath);
	void Load(const std::filesystem::path& filePath);
	void Draw(TransformMatrix displayMatrix);
	void Draw(TransformMatrix displayMatrix, Vector2DInt texelPos, Vector2DInt frameSize);
	Vector2DInt GetSize();
	Color GetPixel(Vector2DInt pos);
private:
	doodle::Image image;
	Vector2DInt size;
};