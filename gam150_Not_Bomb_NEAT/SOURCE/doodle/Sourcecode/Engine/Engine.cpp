﻿// GAM150
// Engine.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Engine.h"

Engine::Engine() : gameStateManager(),
#ifdef _DEBUG
logger(Logger::Severity::Debug, true)
#else 
logger(Logger::Severity::Event, false)
#endif
{}

Engine::~Engine() {}

void Engine::Init(std::string windowName)
{
	GetLogger().LogEvent("Engine Init");
	lastTick = std::chrono::system_clock::now();
	FPS_Logging_Tick = std::chrono::system_clock::now();
	GetWindow().Init(windowName);
	GetTextManager().Init();

	gen.seed(static_cast<unsigned int>(time(0)));

	//LoadGameData(PATH_SAVEFILE);
}

void Engine::Shutdown()
{
	GetLogger().LogEvent("Engine Shutdown");
}

void Engine::Update()
{
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	deltaTime = std::chrono::duration<double>(now - lastTick).count();
	elapsedTime += deltaTime;

	if (std::chrono::duration<double>(now - FPS_Logging_Tick).count() >= FPS_Logging_Term)
	{
		FPS_Logging_Tick = now;
		GetLogger().LogEvent("FPS : " + std::to_string(sumFPS / frameCount));
		sumFPS = 0.0;
		frameCount = 0;
	}

	if (deltaTime >= 1 / Engine::TARGET_FPS)
	{
		GetLogger().LogVerbose("Engine Update");
		lastTick = now;
		GetGameStateManager().Update(deltaTime);
		GetInput().Update();
		GetWindow().Update();

		frameCount++;
		sumFPS += 1 / deltaTime;
	}
}

bool Engine::HasGameEnded()
{
	return GetGameStateManager().HasGameEnded();
}