﻿#pragma once
// GAM150
// TextureManager.h
// Team Neat
// Primary : Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>
#include <map>

class Texture;

class TextureManager {
public:
	Texture* Load(const std::string& filePath);
	void Unload();

private:
	std::map<std::string, Texture*> pathToTexture;
};
