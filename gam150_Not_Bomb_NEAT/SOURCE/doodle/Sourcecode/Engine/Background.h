﻿#pragma once
// GAM150
// Background.h
// Team Neat
// Primary : Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>
#include "../Engine/Geometry/Vector2D.h"
#include "../Engine/Engine.h"

class Texture;

class Background
{
private:
	struct ParallaxInfo
	{
		Texture* texturePtr;
		int level;
	};
	std::vector<ParallaxInfo> backgrounds;
public:
	Background();
	void Add(const std::string& texturePath, int level =1);
	void Unload();
	void Draw(Vector2D camera = { 0,0 });

	ParallaxInfo operator[](int i);
};
