﻿#pragma once
// GAM150
// Sprite.h
// Team Neat
// Primary : Duhwan Kim, Kevin Wright
// Secondary : Byeongjun Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <string>			//std::string

#include "Animation.h"  //Animation
#include "Geometry/Vector2D.h"  //Vector2D, Vector2DInt
#include "Texture.h"			// Texture

class GameObject;

class Sprite {
public:
	Sprite();
	~Sprite();
	void Load(std::string texturePath);
	void Update(double dt);
	void Draw(TransformMatrix displayMatrix);
	void PlayAnimation(int anim);
	bool IsAnimationDone();
	void SetHotSpot(Vector2DInt position, int index = 0); // Change the hotSpot what already made
	void AddHotSpot(Vector2DInt position);
	Vector2DInt GetHotSpot(int index);
	Vector2DInt GetFrameSize() const;
	Vector2DInt GetFrameTexel(int frameNum) const;
private:
	Texture texture;
	int currAnim = 0;
	Vector2DInt frameSize;
	std::vector<Vector2DInt> frameTexel;
	std::vector<Animation*> animations;
	std::vector<Vector2DInt> hotspots;
};

