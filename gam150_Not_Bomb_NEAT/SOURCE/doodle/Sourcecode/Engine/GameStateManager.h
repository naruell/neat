﻿#pragma once
// GAM150
// GameStateManager.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <vector>

#include "GameState.h"

class GameStateManager 
{
public:
	GameStateManager();

	void AddGameState(GameState& gameState);
	void Update(double deltaTime);
	void SetNextState(GAMESTATE initState);
	void Shutdown();
	void ReloadState();
	bool HasGameEnded() { return state == State::EXIT; }

private:
	enum class State 
	{
		START,
		LOAD,
		RUNNING,
		UNLOAD,
		SHUTDOWN,
		EXIT,
	};

	std::vector<GameState*> gameStates;
	State state;
	GameState* currGameState;
	GameState* nextGameState;
};