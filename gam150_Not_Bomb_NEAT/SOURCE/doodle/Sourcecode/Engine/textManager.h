﻿#pragma once
// GAM150
// textManager.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <map>

#include "text.h"

class TextManager
{
public:
	void Init();

	const FontInfo& getFontInfo(int fontID) const;
	const std::map<int, CharacterInfo>& getCharacterInfo(int fontID) const;

	const double CalculateStringWidth(int fontID, const std::string& text, double doodleTextSize) const noexcept;
	const double CalculateStringWidth(TextBox& textBox, int lineIndex) const noexcept;
	const double CalculateStringWidth(TextBox& textBox, std::string& text) const noexcept;
private:

	void loadFont(std::string fontPath);

	std::map<int, FontInfo> fontInfoList;
	std::map<int, std::map<int, CharacterInfo>> charInfoList;
};