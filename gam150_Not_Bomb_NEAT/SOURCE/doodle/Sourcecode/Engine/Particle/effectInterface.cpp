﻿// GAM150
// effectInterface.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "effectInterface.h"

#include <doodle/drawing.hpp>
#include <doodle/environment.hpp>
#include <doodle/input.hpp>

#include "../Engine.h"

void InfoInterface::Run()
{
	Update();
	Draw();
}

void InfoInterface::Setup()
{
	positionX = static_cast<float>(-doodle::Width / 2);
	positionY = static_cast<float>(doodle::Height / 2);

	effectAmount = static_cast<int>(Engine::GetEffectSystem().effectList.size());
	for (std::unique_ptr<Effect> &e : Engine::GetEffectSystem().effectList)
	{
		particleAmount += e->GetParticlesSize();
	}

	isSetup = true;
}

void InfoInterface::Update()
{
	if (!isSetup) { Setup(); }

	positionX = static_cast<float>(-doodle::Width / 2);
	positionY = static_cast<float>(doodle::Height / 2) - fontSize * 2;

	effectAmount = static_cast<int>(Engine::GetEffectSystem().effectList.size());
	particleAmount = 0;
	for (std::unique_ptr<Effect>& e : Engine::GetEffectSystem().effectList)
	{
		particleAmount += e->GetParticlesSize();
	}
	
	int mouseX = doodle::get_mouse_x();
	int mouseY = doodle::get_mouse_y();
	origin = Vector2D{ static_cast<double>(mouseX), static_cast<double>(mouseY) };

	for (std::unique_ptr<Effect>& e : effectsList)
	{
		e->SetOrigin(origin);
	}
}

void InfoInterface::Draw()
{
	doodle::push_settings();
	doodle::set_fill_color(doodle::Color{255, 255, 255, 255});
	doodle::set_font_size(fontSize);
	doodle::draw_text("Current Effect Count : " + std::to_string(effectAmount), positionX, positionY);
	doodle::draw_text("Current Particle Count : " + std::to_string(particleAmount), positionX, positionY - fontSize);
	doodle::pop_settings();
}

void InfoInterface::GenerateEffect()
{
	// effectSystem.Effects.push_back(effectsList[effectIndex]);
}


InfoInterface::InfoInterface()
{
	effectsList.emplace_back(new Click1Effect{ Vector2D{ 0, 0 } });
	//effectsList.push_back(new image1Effect{ Vec2{ 0, 0 }, IMAGEPATH });
	effectsList.emplace_back(new TestEffect{ Vector2D{ 0, 0 } });
}

InfoInterface::~InfoInterface()
{

}

InfoInterface infoInterface;