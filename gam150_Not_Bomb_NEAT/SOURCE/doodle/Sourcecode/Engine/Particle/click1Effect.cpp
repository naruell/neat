﻿// GAM150
// click1Effect.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "click1Effect.h"

#include <doodle/input.hpp>
#include <doodle/random.hpp>
#include <doodle/angle.hpp>

#include "../helper.h"

void Click1Circle::MemberSetup()
{
	fillColor = doodle::Color{ 0, 0, 0, 0 };
	outlineColor = doodle::Color{ 13, 130, 180, 255 };
	outlineWidth = 5;
	size_w = 0;
}

void Click1Circle::MemberUpdate()
{
	double delta = -showedTime * 4;
	if (delta <= -2) { delta = -2 ; }
	size_w += (3 + delta) * 120 * (showedTime - pShowedTime);
	outlineWidth -= 0.1 * 120 * (showedTime - pShowedTime);

	double alpha = 255 - showedTime * 200;
	if (alpha <= 0) { alpha = 0; }
	outlineColor.alpha =  alpha;

	double green = 255 - showedTime * 300;
	if (green <= 0) { green = 0; }
	outlineColor.green = green;

	double blue = showedTime * 500;
	if (blue >= 255) { blue = 255; }
	outlineColor.blue = blue;

	if (outlineWidth <= 0) 
	{
		outlineWidth = 0;
		endCondition = true;
	}
}

void Click1Circle::MemberDraw()
{
	doodle::set_fill_color(fillColor);
	doodle::set_outline_color(outlineColor);
	if (outlineWidth != 0) { doodle::set_outline_width(outlineWidth); }
	else { doodle::no_outline(); }
	doodle::draw_ellipse(position.x, position.y, size_w, size_w);
}

void Click1Line::MemberSetup()
{
	outPos.x = position.x + length * cos(angle);
	outPos.y = position.y + length * sin(angle);

	fillColor = doodle::Color{ 13, 255, 255, 155 };
	outlineColor = doodle::Color{ 0, 255, 111, 100 };
}

void Click1Line::MemberUpdate()
{
	length -= length / 15 * 120 * (showedTime - pShowedTime);

	outPos.x = position.x + length * cos(angle);
	outPos.y = position.y + length * sin(angle);

	if (length <= 20) { endCondition = true; }
}

void Click1Line::MemberDraw()
{
	doodle::set_fill_color(fillColor);

	doodle::set_outline_width(2);
	doodle::set_outline_color(outlineColor);
	doodle::draw_line(position.x, position.y, outPos.x, outPos.y);

	doodle::set_outline_width(1);
	doodle::set_outline_color(fillColor);
	doodle::draw_line(position.x, position.y, outPos.x, outPos.y);\
}

void Click1Twinkle::MemberSetup()
{
	fillColor = doodle::Color{ 255, 255, 255, 255 };
	outlineColor = doodle::Color{ 255, 255, 255, 150 };
	size_w = 10;
	size_h = 13;

	duration = 0;
}

void Click1Twinkle::MemberUpdate()
{
	size_w = Map(showedTime, 0, 0.3, 10, 0);
	size_h = Map(showedTime, 0, 0.3, 15, 0);
}

void Click1Twinkle::MemberDraw()
{
	doodle::set_fill_color(fillColor);
	doodle::set_outline_color(outlineColor);
	double x = position.x;
	double y = position.y;
	double w = size_w;
	double h = size_h;

	for (int h_sign = -1; h_sign <= 1; h_sign += 2)
	{
		for (int w_sign = -1; w_sign <= 1; w_sign += 2)
		{
			doodle::draw_quad(x, y, x, y + h_sign * h, x + w_sign * w / 4, y + h_sign * h / 4, x + w_sign * w, y);
		}
	}
}

void Click1Effect::GenerateParticle()
{
	double mouseX = static_cast<double>(doodle::get_mouse_x());
	double mouseY = static_cast<double>(doodle::get_mouse_y());

	particleList.emplace_back(new Click1Circle(Vector2D{ mouseX, mouseY }));
	for (int i = 0; i <= 15; ++i)
	{
		double angle = doodle::TWO_PI / 15.0 * static_cast<double>(i) + static_cast<double>(doodle::random(-10, 10));
		particleList.emplace_back(new Click1Line(Vector2D{ mouseX, mouseY }, angle));
	}

	for (int i = 0; i <= 7; ++i)
	{
		double angle = doodle::TWO_PI / 10.0 * static_cast<double>(i) + static_cast<double>(doodle::random(-10, 10));
		double distance = static_cast<double>(doodle::random(5, 20));
		particleList.emplace_back(new Click1Twinkle(Vector2D{ mouseX + distance * cos(angle), mouseY + distance * sin(angle) }));
	}
}

void Click1Effect::GenerateTwinkle(Vector2D pos)
{
	particleList.emplace_back(new Click1Twinkle(pos));
}

void Click1Effect::MemberSetup()
{

}

void Click1Effect::MemberRun()
{
	if (pShowedTime <= 0.1 && showedTime >= 0.1)
	{

		for (int i = 0; i <= 6; ++i)
		{
			double angle = doodle::TWO_PI / 10 * i + static_cast<double>(doodle::random(-10, 10));
			double distance = static_cast<double>(doodle::random(10, 30));
			particleList.emplace_back(new Click1Twinkle(Vector2D{ origin.x + distance * cos(angle), origin.y + distance * sin(angle) }));
		}
	}
	
	if (pShowedTime <= 0.2 && showedTime >= 0.2)
	{

		for (int i = 0; i <= 9; ++i)
		{
			double angle = doodle::TWO_PI / 10 * i + static_cast<double>(doodle::random(-10, 10));
			double distance = static_cast<double>(doodle::random(20, 35));
			particleList.emplace_back(new Click1Twinkle(Vector2D{ origin.x + distance * cos(angle), origin.y + distance * sin(angle) }));
		}
	}

	if (pShowedTime <= 1 && showedTime >= 1)
	{
		endCondition = true;
	}

	for (int i = static_cast<int>(particleList.size()) - 1; i >= 0; --i)
	{
		particleList[i]->Run();

		if (particleList[i]->isEnded())
		{
			particleList.erase(particleList.begin() + i);
		}
	}
}

Click1Effect::~Click1Effect()
{
}