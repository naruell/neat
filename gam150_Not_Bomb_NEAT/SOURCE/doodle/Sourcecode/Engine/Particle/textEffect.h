﻿#pragma once
// GAM150
// textEffect.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "ParticleSystem.h"

struct TextParticle : Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;

	TextParticle(std::string text, Vector2D pos, doodle::Color textColor, double textSize, double dur) : Particle("TextParticle", pos, Vector2D{ 0, 0 }, Vector2D{ 0, 0 }, dur), text(text), textColor(textColor), textSize(textSize) {};
	std::string text;
	doodle::Color textColor;
	double textSize;
};

class TextEffect : public Effect
{
public:
	void GenerateParticle() override;
	void MemberSetup() override;
	void MemberRun() override;

	TextEffect(std::string text, Vector2D origin, doodle::Color textColor, double textSize, double dur) : Effect(origin, dur), text(text), textColor(textColor), textSize(textSize) {};
	~TextEffect() override;

private:
	std::string text;
	doodle::Color textColor;
	double textSize;
};