﻿// GAM150
// testEffect.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "testEffect.h"

#include <doodle/angle.hpp>
#include <doodle/environment.hpp>

#include "../helper.h"

void TestParticle_circle::MemberSetup()
{
	fillColor = doodle::Color{ 255, 0, 0, 255 };
	outlineColor = doodle::Color{ 0, 0, 0, 255 };
	size_w = 12;
	size_h = 12;
}

void TestParticle_circle::MemberUpdate()
{
	velocity += acceleration * 120 * (showedTime - pShowedTime);
	position += velocity * 120 * (showedTime - pShowedTime);

	if (255 - showedTime * 190 <= 0) { endCondition = true; }
}

void TestParticle_circle::MemberDraw()
{
	double alpha = 255 - (showedTime * 190);
	if (alpha <= 0) { alpha = 0; }

	outlineColor.alpha = alpha;
	fillColor.alpha = alpha;
	doodle::set_outline_color(outlineColor);
	doodle::set_fill_color(fillColor);

	doodle::draw_ellipse(position.x, position.y, size_w, size_h);
}

void TestParticle_rect::MemberSetup()
{
	fillColor = doodle::Color{ 0, 0, 255, 255 };
	outlineColor = doodle::Color{ 0, 0, 0, 255 };
	size_w = 12;
	size_h = 12;
}

void TestParticle_rect::MemberUpdate()
{
	velocity += acceleration * 120 * (showedTime - pShowedTime);
	position += velocity * 120 * (showedTime - pShowedTime);

	if (255 - showedTime * 190 <= 0) { endCondition = true; }
}

void TestParticle_rect::MemberDraw()
{
	doodle::set_rectangle_mode(doodle::RectMode::Center);

	double alpha = 255 - (showedTime * 190);
	if (alpha <= 0) { alpha = 0; }
	outlineColor.alpha = alpha;
	fillColor.alpha = alpha;

	doodle::set_outline_color(outlineColor);
	doodle::set_fill_color(fillColor);

	doodle::apply_translate(position.x, position.y);
	double theta = Map(position.x, 0, static_cast<double>(doodle::Width), 0, doodle::TWO_PI * 2);
	doodle::apply_rotate(theta);
	doodle::draw_rectangle(0, 0, size_w, size_h);
}

void TestEffect::GenerateParticle()
{
	int particleSeed = doodle::random(0, 2);
	switch (particleSeed)
	{
	case 0:
		particleList.emplace_back(new TestParticle_circle(origin));
		break;

	case 1:
		particleList.emplace_back(new TestParticle_rect(origin));
		break;
	}
}

void TestEffect::MemberSetup()
{

}

void TestEffect::MemberRun()
{
	for (int i = static_cast<int>(particleList.size()) - 1; i >= 0; --i)
	{
		particleList[i]->Run();

		if (particleList[i]->isEnded())
		{
			particleList.erase(particleList.begin() + i);
		}
	}

	GenerateParticle();
}

TestEffect::~TestEffect()
{
}