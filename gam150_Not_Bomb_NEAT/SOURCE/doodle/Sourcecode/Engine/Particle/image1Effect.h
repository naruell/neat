﻿#pragma once
// GAM150
// Image1Effect.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "ParticleSystem.h"

struct PixelParticle : public Particle
{
	void MemberSetup() override;
	void MemberUpdate() override;
	void MemberDraw() override;

	const int originPixelSize{ 2 };
	int pixelSize{ 2 };	// 1 for original

	int particleIndex{ -1 };
	double noiseSeedX{ 0 };
	double noiseSeedY{ 0 };

	PixelParticle() : Particle("nullPixel", Vector2D{0, 0}, Vector2D{ 0, 0 }, Vector2D{ 0, 0 }) {};
	PixelParticle(int particleIndex, Vector2D pos, const int originPixelSize) : particleIndex{ particleIndex }, originPixelSize{ originPixelSize }, Particle("pixelParticle", pos, Vector2D{ 0, 0 }, Vector2D{ 0, 0 }) {};
};

class Image1Effect : public Effect
{
public:
	void GenerateParticle() override;
	void MemberSetup() override;
	void MemberRun() override;

	Vector2DInt GetSize();

	Image1Effect(Vector2D origin, const std::string& imagePath) : imagePath{ imagePath }, Effect(origin) {};
	Image1Effect(Vector2D origin, const std::string& imagePath, int pixelSize) : pixelSize{ pixelSize }, imagePath{ imagePath }, Effect(origin) {};
	~Image1Effect() override;

private:
	std::string imagePath;

	const int pixelSize{ 2 };

	int imageWidth{ 0 };
	int imageHeight{ 0 };

	double effectrangeX{ -1 };
	double effectrangeY{ -1 };

	Vector2DInt imageSize;
};