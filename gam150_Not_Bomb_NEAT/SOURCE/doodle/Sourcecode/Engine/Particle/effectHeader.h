﻿#pragma once
// GAM150
// effectHeader.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

// Particle system - Particle, Effect, System
#include "ParticleSystem.h"

// Effects run in particle/effect system
#include "image1Effect.h"
#include "testEffect.h"
#include "click1Effect.h"
#include "textEffect.h"