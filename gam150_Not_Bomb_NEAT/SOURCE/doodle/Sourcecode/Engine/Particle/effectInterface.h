﻿#pragma once
// GAM150
// effectInterface.h
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "effectHeader.h"

class InfoInterface
{
public:
	void Setup();
	void Run();
	void Update();
	void Draw();

	void GenerateEffect(); // 포인터 복사 문제로 보류

	InfoInterface();
	~InfoInterface();

private:
	bool isSetup{ false };

	Vector2D origin{ 0, 0 };
	double positionX{ 0 };
	double positionY{ 0 };
	double fontSize{ 20 };

	std::vector<std::unique_ptr<Effect>> effectsList;
	int effectIndex{ 0 };

	int effectAmount{ 0 };
	int particleAmount{ 0 };
};

extern InfoInterface infoInterface;

/* 
 * InfoInterface 의 기능
 * 현재 파티클 종류, 실행되고 있는(alive한) 이펙트 갯수, 실행되고 있는(alive한) 파티클 갯수 표시
 * 이펙트 변경(특정 키를 눌러서), 초기화, 최근 이펙트 삭제(되돌리기)
 * 배경 색상 또는 이미지 변경
 */