﻿#pragma once
//GAM150
//Graph.h
//Team Neat
//Primary : Sunghwan Cho
//5/23/2020
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include <set>

namespace CDATASTRUCTURE
{
	template<typename Type>
	class CTECGraph
	{
	public:
		CTECGraph() = default;
		~CTECGraph() = default;
		
	private:
		static constexpr int MAXIMUM = 31;
		bool adjacencyMatrix[MAXIMUM][MAXIMUM];
		Type lables[MAXIMUM];
		int manyVertices;
		
	public:
		void AddVertex(const Type& value);
		void AddEdge(int source, int target);
		void RemoveEdge(int source, int target);
		Type& operator[] (int vertex);
		Type operator[] (int vertex) const;
		int Size() const;
		bool IsEdge(int source, int target) const;
		std::set<int> neighbors(int vertex) const;
	};
	
}

#include "Graph.inl"
