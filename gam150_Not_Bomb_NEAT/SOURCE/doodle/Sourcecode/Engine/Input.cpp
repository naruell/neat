﻿// GAM150
// Input.cpp
// Team Neat
// Primary : Duhwan Kim
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Input.h"

#include <doodle/input.hpp>

#include "Engine.h"

Input::Input() 
{
	keyDown.resize(static_cast<int>(KeyboardButton::Count));
	wasKeyDown.resize(static_cast<int>(KeyboardButton::Count));

	mousePressed.resize(static_cast<int>(MouseButton::Count));
	wasMousePressed.resize(static_cast<int>(MouseButton::Count));
}

bool Input::IsKeyDown(KeyboardButton key) const 
{
	return keyDown[static_cast<int>(key)];
}

bool Input::IsKeyUp(KeyboardButton key) const 
{
	return keyDown[static_cast<int>(key)] == false;
}

bool Input::IsKeyPressed(KeyboardButton key) const
{
	return keyDown[static_cast<int>(key)] == true && wasKeyDown[static_cast<int>(key)] == false;
}

bool Input::IsKeyReleased(KeyboardButton key) const 
{
	return keyDown[static_cast<int>(key)] == false && wasKeyDown[static_cast<int>(key)] == true;
}

bool Input::IsMouseDown(MouseButton mouse) const 
{
	return mousePressed[static_cast<int>(mouse)];
}

bool Input::IsMouseUp(MouseButton mouse) const 
{
	return mousePressed[static_cast<int>(mouse)] == false;
}

bool Input::IsMousePressed(MouseButton mouse) const
{
	return mousePressed[static_cast<int>(mouse)] == true && wasMousePressed[static_cast<int>(mouse)] == false;
}

bool Input::IsMouseReleased(MouseButton mouse) const 
{
	return mousePressed[static_cast<int>(mouse)] == false && wasMousePressed[static_cast<int>(mouse)] == true;
}

void Input::SetKeyDown(KeyboardButton key, bool value) 
{
	keyDown[static_cast<int>(key)] = value;
}

void Input::SetMouseDown(MouseButton mouse, bool value) 
{
	mousePressed[static_cast<int>(mouse)] = value;
}

Input::KeyboardButton Input::GetPressedKey() const noexcept
{
	int index = 0;
	for (bool b : keyDown)
	{
		if (b == false && wasKeyDown[index] == true)
		{
			return static_cast<Input::KeyboardButton>(index);
		}
		++index;
	}

	return Input::KeyboardButton::None;
}

Vector2DInt Input::GetMousePos() const noexcept
{
	return mousePos;
}

Vector2DInt Input::GetPMousePos() const noexcept
{
	return pMousePos;
}

Vector2DInt Input::GetMouseDelta() const noexcept
{
	return mousePos - pMousePos;
}

void Input::Update() 
{
	wasKeyDown = keyDown;
	wasMousePressed = mousePressed;

	pMouseX = mouseX;
	pMouseY = mouseY;

	mouseX = doodle::get_mouse_x();
	mouseY = doodle::get_mouse_y();

	pMousePos = { pMouseX, pMouseY };
	mousePos = { mouseX, mouseY };
}

Input::InputKey::InputKey(KeyboardButton button) : button(button) {}

bool Input::InputKey::IsKeyDown() const
{
	return Engine::GetInput().IsKeyDown(button);
}

bool Input::InputKey::IsKeyUp() const
{
	return Engine::GetInput().IsKeyUp(button);
}

bool Input::InputKey::IsKeyPressed() const
{
	return Engine::GetInput().IsKeyPressed(button);
}

bool Input::InputKey::IsKeyReleased() const
{
	return Engine::GetInput().IsKeyReleased(button);
}

void Input::InputKey::Reassign(KeyboardButton buttonTo)
{
	button = buttonTo;
}