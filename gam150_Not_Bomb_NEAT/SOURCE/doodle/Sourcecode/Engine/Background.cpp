﻿// GAM150
// Background.cpp
// Team Neat
// Primary : 
// All content © 2020 DigiPen (USA) Corporation, all rights reserved.

#include "Background.h"
#include "TransformMatrix.h"
#include "Texture.h"

// Set Background's camera's rect size
Background::Background() {}

// Add another backgrounds
void Background::Add(const std::string& texturePath, int level)
{
	backgrounds.push_back({ Engine::GetTextureManager().Load(texturePath), level });
}

// Background unload
void Background::Unload()
{
	backgrounds.clear();
}

// Background Draw
void Background::Draw(Vector2D camera)
{
	for (ParallaxInfo& levelInfo : backgrounds) 
	{
		levelInfo.texturePtr->Draw(TranslateMatrix(Vector2D{ -camera.x / levelInfo.level, -camera.y }), { 0,0 },
			{ levelInfo.texturePtr->GetSize().x,levelInfo.texturePtr->GetSize().y });
	}
}

Background::ParallaxInfo Background::operator[](int i)
{
	return backgrounds[i];
}
