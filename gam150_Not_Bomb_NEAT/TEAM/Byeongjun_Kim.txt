* Byeongjun Kim
* Test Manager
* Main contributions : 
	Engine stuff(Animation, Background, TextureManager, TransformMatrix)
	Helper
	alpha_CardManaging
	alpha_HeroSelect
	battlefield
	CardManager
	EventRoom
	final_EventRoom
	HeroSelect
	prototype_battlefield
	Reward
	roster
	RosterManager
	Texture Manager
	Unused Code :
		Assets\Temp&Trash&Unused\temp.cpp
				              temp.h
	assets :
		Assets\CardDesign\CardBase_Bomb.png
				CardBase_Normal.png
				CardBase_Not.png
				CardDraw.png
				Else.png
				heal.png
				Rod.png
				Shoes.png
				Sword.png
				TimeAdd.png
				TimeFlow.png

		Assets\Characters\CharacterTypes\ (All things in here)

		Assets\Characters\CharacterStats\ADarmor.png
					     AParmor.png
					     CardDraw.png
					     Else.png
					     heal.png
					     Heart.png
					     Rod.png
					     Heart.png
					     Rod.png
					     Shoes.png
					     Sword.png
					     TimeAdd.png
					     TimeFlow.png

		Assets\Graphics\ (All things excludes in \CardManager, \System)

		Assets\GraveStone\GraveStone.png

		Assets\Mouse\Roaster\ (All things... folder name is error :) )

		Assets\Temp&Trash&Unused\Barrior.png
					Time.png

* Secondary contribution :
	Engine stuff(Sprite)
	cardEffectFunction.h

* I learned the base is most important. Because of base, I changed my code many times.
I enjoyed my project because this experience was fun and valuable. Also, thanks for my teammates because of good cooperation.
