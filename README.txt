Summary
	Game name : Not Bomb
	Team name : Neat
	Team members : Sunghwan Cho(sunghwan.cho), Duhwan Kim(duhwan.kim), Byeongjun Kim(byeongjun.kim), Junhyuk Cha(junhyuk.cha)
	GAM150S20KR
	High concept : This is mainly a CCG game and has some RPG features. The player explores through the dungeon to get heroes(player-controllable characters) and hero (skill) cards to make decks out of them and fight with enemies.
	Copyright notice : As a condition of your accessing this area, you agree to be bound by the following terms and conditions: 
	The games software was created by students of DigiPen Institute of Technology (DigiPen), and all copyright and other rights in such is owned by DigiPen. While DigiPen allows you to access, download and use the software for non-commercial, home use you hereby expressly agree that you will not otherwise copy, distribute, modify, or (to the extent not otherwise permitted by law) decompile, disassemble or reverse engineer the games software. 
	THE GAMES SOFTWARE IS MADE AVAILABLE BY DIGIPEN AS-IS AND WITHOUT WARRANTY OF ANY KIND BY DIGIPEN. DIGIPEN HEREBY EXPRESSLY DISCLAIMS ANY SUCH WARRANTY, INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. 
	WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, DIGIPEN SHALL NOT BE LIABLE IN DAMAGES OR OTHERWISE FOR ANY DAMAGE OR INJURY FROM DOWNLOADING, DECOMPRESSING, RUNNING OR ATTEMPTING TO RUN, USING OR OTHERWISE DEALING WITH, IN ANY WAY, THE GAMES SOFTWARE CONTAINED IN THIS AREA, NOR SHALL DIGIPEN BE LIABLE FOR ANY INCIDENTAL, CONSEQUENTIAL, EXEMPLARY OR OTHER TYPES OF DAMAGES ARISING FROM ACCESS TO OR USE OF THE GAMES SOFTWARE. 
	YOU HEREBY AGREE TO INDEMNIFY, DEFEND AND HOLD HARMLESS DIGIPEN AND ITS DIRECTORS, OFFICERS, EMPLOYEES, AGENTS, CONSULTANTS AND CONTRACTORS AGAINST ALL LIABILITY OF ANY KIND ARISING OUT OF YOUR DOWNLOADING, DECOMPRESSING, RUNNING OR ATTEMPTING TO RUN, USING OR OTHERWISE DEALING WITH, IN ANY WAY, THE GAMES SOFTWARE. 
	DIGIPEN MAKES NO WARRANTIES OR REPRESENTATIONS THAT THE GAMES SOFTWARE IS FREE OF MALICIOUS PROGRAMMING, INCLUDING, WITHOUT LIMITATION, VIRUSES, TROJAN HORSE PROGRAMS, WORMS, MACROS AND THE LIKE. AS THE PARTY ACCESSING THE GAMES SOFTWARE IT IS YOUR RESPONSIBILITY TO GUARD AGAINST AND DEAL WITH THE EFFECTS OF ANY SUCH MALICIOUS PROGRAMMING. 

Installation instructions
	Go to INSTALLATION folder and execute Not_Bomb_Setup.exe file.

“How To Play”
	After download it, execute the game, then you can see the title screen. Drag the tutorial card out, read it and play the game!

Controls
	Using left mouse button to using cards, clicking battle tiles, maps. And using right mouse button to see the heroes' stats in hero select screen or delete cards in certain situation.

Cheat codes
	When playing battle, press '_1' key to stop the battle and go to console and type numbers to get card to hand.
	ex1) 0 1
	-> 0 means Neutral card and 1 means the second card of the Neutral. So I get "Fireball" card.
	ex2) 3 0
	-> 3 means Priest card and 0 means the first card of the Priest. So I get "Healing Hands" card.
	YOU CAN SEE THE INDEX OF THE CARD IN THE helper.cpp

Credits
	President: Claude Comair
	Instructor: David Ly
	Teaching Assistant: Minui Lee

	<<Team Neat>>
	Producer : Sunghwan Cho
	Lead Programmer : Duhwnan Kim
	Lead Designer : Junhyuk Cha
	Test Manager : Byeongjun Kim

	<<Copyrights>>
	sword.wav : https://freesound.org/people/qubodup/sounds/184422/
		license - https://creativecommons.org/licenses/by/3.0/
	kick.wav : https://freesound.org/people/Smullen93/sounds/340354/
		license - https://creativecommons.org/licenses/by/3.0/
	hammer.wav : https://freesound.org/people/SilverIllusionist/sounds/513826/
		license - https://creativecommons.org/licenses/by/3.0/
	gun.wav : https://freesound.org/people/TheNikonProductions/sounds/337697/
		license - https://creativecommons.org/licenses/by/3.0/
	heal.wav : https://freesound.org/people/jtn191/sounds/514271/
		license - https://creativecommons.org/licenses/by/3.0/
	reinforce.wav : https://freesound.org/people/MATRIXXX_/sounds/523753/
		license - https://creativecommons.org/licenses/by/3.0/
	running.wav : https://freesound.org/people/stintx/sounds/107624/
	battle_bgm.wav : Music: < epic > from Bensound.com
	non_battle_bgm.wav : Music: < the_duel > from Bensound.com

	Open source libraray for parsing json format file : https://github.com/open-source-parsers/jsoncpp

	<<Special Thanks>>
	Sehoon Kim for made a nice assets for our game!